var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Chi cục An toàn vệ sinh thực phẩm             ",
   "address": "59 Nguyễn Thị Minh Khai,Phường Bến Thành,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.772546,
   "Latitude": 106.691655
 },
 {
   "STT": 2,
   "Name": "Chi cục Dân số - Kế hoạch hóa gia đình",
   "address": "250 Pastuer,Phường 8,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.786289,
   "Latitude": 106.689615
 },
 {
   "STT": 3,
   "Name": "Trung tâm Bảo vệ Sức khoẻ lao động và Môi trường",
   "address": "49 Bis Điện Biên Phủ,Phường Đa Kao,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.790483,
   "Latitude": 106.697942
 },
 {
   "STT": 4,
   "Name": "Trung tâm Chăm sóc sức khỏe sinh sản",
   "address": "957 Đường 3 tháng 2,Phường 7,Quận 11,Hồ Chí Minh",
   "Longtitude": 10.762429,
   "Latitude": 106.657827
 },
 {
   "STT": 5,
   "Name": "Trung tâm Dinh Dưỡng",
   "address": "178 Lê Văn Sỹ,Phường 10,Quận Phú Nhuận,Hồ Chí Minh",
   "Longtitude": 10.792839,
   "Latitude": 106.670673
 },
 {
   "STT": 6,
   "Name": "Trung tâm Giám định Y khoa",
   "address": "105 Bùi Hữu Nghĩa,Phường 5,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.753195,
   "Latitude": 106.674791
 },
 {
   "STT": 7,
   "Name": "Trung tâm Kiểm chuẩn Xét nghiệm",
   "address": "75A Cao Thắng,Phường 3,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.771941,
   "Latitude": 106.679691
 },
 {
   "STT": 8,
   "Name": "Trung tâm Kiểm nghiệm Thuốc, Mỹ phẩm, Thực phẩm",
   "address": "53-55 Lê Thị Riêng,Phường Bến Thành,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.771216,
   "Latitude": 106.691486
 },
 {
   "STT": 9,
   "Name": "Trung tâm Pháp y ",
   "address": "336 Trần Phú,Phường 7,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.753998,
   "Latitude": 106.670486
 },
 {
   "STT": 10,
   "Name": "Trung tâm Truyền thông Giáo dục sức khỏe",
   "address": "59B Nguyễn Thị Minh Khai,Phường Bến Thành,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.773842,
   "Latitude": 106.690006
 },
 {
   "STT": 11,
   "Name": "Phòng Y tế Quận 1 ",
   "address": "47 Lê Duẩn,Phường Bến Nghé,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.780591,
   "Latitude": 106.699282
 },
 {
   "STT": 12,
   "Name": "Phòng Y tế Quận 2",
   "address": "168 Trương Văn Bang,Phường Thạnh Mỹ Lợi,Quận 2,Hồ Chí Minh",
   "Longtitude": 10.775679,
   "Latitude": 106.755825
 },
 {
   "STT": 13,
   "Name": "Phòng Y tế Quận 3",
   "address": "154 Trần Quốc Thảo,Phường 7,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.782067,
   "Latitude": 106.682289
 },
 {
   "STT": 14,
   "Name": "Phòng Y tế Quận 4",
   "address": "22-24 Nguyễn Tất Thành,Phường 12,Quận 4,Hồ Chí Minh",
   "Longtitude": 10.765389,
   "Latitude": 106.702652
 },
 {
   "STT": 15,
   "Name": "Phòng Y tế Quận 5",
   "address": "203 An Dương Vương,Phường 8,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.759475,
   "Latitude": 106.66905
 },
 {
   "STT": 16,
   "Name": "Phòng Y tế Quận 6",
   "address": "111 Mai Xuân Thưởng,Phường 4,Quận 6,Hồ Chí Minh",
   "Longtitude": 10.746126,
   "Latitude": 106.648568
 },
 {
   "STT": 17,
   "Name": "Phòng Y tế Quận 7",
   "address": "07 đường Tân Phú,Phường Tân Phú,Quận 7,Hồ Chí Minh",
   "Longtitude": 10.732094,
   "Latitude": 106.726341
 },
 {
   "STT": 18,
   "Name": "Phòng Y tế Quận 8",
   "address": "số 4 đường 1011 Phạm Thế Hiển,Phường 5,Quận 8,Hồ Chí Minh",
   "Longtitude": 10.740462,
   "Latitude": 106.666032
 },
 {
   "STT": 19,
   "Name": "Phòng Y tế Quận 9",
   "address": "2/304 Xa Lộ Hà Nội,Phường Hiệp Phú,Quận 9,Hồ Chí Minh",
   "Longtitude": 10.844685,
   "Latitude": 106.781764
 },
 {
   "STT": 20,
   "Name": "Phòng Y tế Quận 10",
   "address": "271-273 Vĩnh Viễn ,Phường 5,Quận 10,Hồ Chí Minh",
   "Longtitude": 10.76367,
   "Latitude": 106.667792
 },
 {
   "STT": 21,
   "Name": "Phòng Y tế Quận 11",
   "address": "270 Bình Thới,Phường 10,Quận 11,Hồ Chí Minh",
   "Longtitude": 10.760772,
   "Latitude": 106.647891
 },
 {
   "STT": 22,
   "Name": "Phòng Y tế Quận 12",
   "address": "1 Đường Lê Thị Riêng,Phường Thới An,Quận 12,Hồ Chí Minh",
   "Longtitude": 10.873533,
   "Latitude": 106.655153
 },
 {
   "STT": 23,
   "Name": "Phòng Y tế huyện Bình Chánh ",
   "address": "E8/9A Nguyễn Hữu Trí,Thị trấn Tân Túc,Huyện Bình Chánh,Hồ Chí Minh",
   "Longtitude": 10.691338,
   "Latitude": 106.572913
 },
 {
   "STT": 24,
   "Name": "Phòng Y tế Quận Bình Tân ",
   "address": "521 Kinh Dương Vương,Phường An Lạc,Quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.737156,
   "Latitude": 106.614909
 },
 {
   "STT": 25,
   "Name": "Phòng Y tế Quận Bình Thạnh ",
   "address": "6 Phan Đăng Lưu,Phường 14,Quận Bình Thạnh,Hồ Chí Minh",
   "Longtitude": 10.810229,
   "Latitude": 106.695028
 },
 {
   "STT": 26,
   "Name": "Phòng Y tế huyện Cần Giờ ",
   "address": "Khu hành chính UBND KP Giồng Aođường Lương Văn Nho,Thị trấn Cần Thạnh,Huyện Cần Giờ,Hồ Chí Minh",
   "Longtitude": 10.508327,
   "Latitude": 106.8635
 },
 {
   "STT": 27,
   "Name": "Phòng Y tế huyện Củ Chi",
   "address": "KP7 (nằm trong UBND,Thị trấn Củ Chi,Huyện Củ Chi,Hồ Chí Minh",
   "Longtitude": 10.976478,
   "Latitude": 106.502304
 },
 {
   "STT": 28,
   "Name": "Phòng Y tế Quận Gò Vấp",
   "address": "19 Quang Trung,Phường 10,Quận Gò Vấp,Hồ Chí Minh",
   "Longtitude": 10.832455,
   "Latitude": 106.666286
 },
 {
   "STT": 29,
   "Name": "Phòng Y tế huyện Hóc Môn",
   "address": "65/2B Bà Triệu,Thị trấn Hóc Môn,Huyện Hóc Môn,Hồ Chí Minh",
   "Longtitude": 10.881564,
   "Latitude": 106.593111
 },
 {
   "STT": 30,
   "Name": "Phòng Y tế huyện Nhà Bè ",
   "address": "330 Nguyễn BìnhẤp 1,Xã Phú Xuân,Huyện Nhà Bè,Hồ Chí Minh",
   "Longtitude": 10.67674,
   "Latitude": 106.735479
 },
 {
   "STT": 31,
   "Name": "Phòng Y tế Quận Phú Nhuận",
   "address": "103/18 Trần Huy Liệu,Phường 12,Quận Phú Nhuận,Hồ Chí Minh",
   "Longtitude": 10.805221,
   "Latitude": 106.686853
 },
 {
   "STT": 32,
   "Name": "Phòng Y tế Quận Tân Bình ",
   "address": "387A Trường Chính,Phường 14,Quận Tân Bình,Hồ Chí Minh",
   "Longtitude": 10.800491,
   "Latitude": 106.640109
 },
 {
   "STT": 33,
   "Name": "Phòng Y tế Quận Tân Phú ",
   "address": "70A Thoại Ngọc Hầu,Phường Hòa Thạnh,Quận Tân Phú,Hồ Chí Minh",
   "Longtitude": 10.781713,
   "Latitude": 106.636122
 },
 {
   "STT": 34,
   "Name": "Phòng Y tế Quận Thủ Đức ",
   "address": "43 Nguyễn Văn Bá,Phường Bình Thọ,Quận Thủ Đức,Hồ Chí Minh",
   "Longtitude": 10.848253,
   "Latitude": 106.772084
 },
 {
   "STT": 35,
   "Name": "Trung tâm Phòng chống HIV-AIDS",
   "address": "121 Lý Chính Thắng,Phường 7,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.787095,
   "Latitude": 106.685133
 },
 {
   "STT": 36,
   "Name": "Trung tâm Pháp y tâm thần khu vực thành phố Hồ Chỉ Minh",
   "address": "152-154, 400/12 Hồng Bàng, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7549813,
   "Latitude": 106.6575154
 }
];