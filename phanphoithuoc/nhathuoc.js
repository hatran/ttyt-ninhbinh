var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Nhà thuốc Đào Tiến 3",
   "address": "224-226-240 Cách Mạng Tháng 8, Phường  10, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.779968,
   "Latitude": 106.67793
 },
 {
   "STT": 2,
   "Name": "Nhà thuốc Đào Tiến 4",
   "address": "61 Trương Định, Phường  6, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7769017,
   "Latitude": 106.6904103
 },
 {
   "STT": 3,
   "Name": "Nhà thuốc Đức Đạt",
   "address": "49 Nguyễn Phúc Nguyên, Phường  10, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7788676,
   "Latitude": 106.6808976
 },
 {
   "STT": 4,
   "Name": "Nhà thuốc Hoàng Hân",
   "address": "92 Vườn Chuối, Phường  4, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7736461,
   "Latitude": 106.6830662
 },
 {
   "STT": 5,
   "Name": "Nhà thuốc Hồng Ân 1",
   "address": "453/77B6 Lê Văn Sỹ, Phường  12, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.789123,
   "Latitude": 106.67303
 },
 {
   "STT": 6,
   "Name": "Nhà thuốc Hồng Ngọc 1",
   "address": "87 Trần Quang Diệu, Phường  14, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.78853,
   "Latitude": 106.677874
 },
 {
   "STT": 7,
   "Name": "Nhà thuốc Song Thư",
   "address": "02 Phạm Đình Toái, Phường  6, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776511,
   "Latitude": 106.686308
 },
 {
   "STT": 8,
   "Name": "Nhà thuốc Thiên An",
   "address": "306 Lê Văn Sỹ, Phường  14, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.787205,
   "Latitude": 106.679388
 },
 {
   "STT": 9,
   "Name": "Nhà thuốc Thiên Phước",
   "address": "269 Điện Biên Phủ, Phường  7, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7805658,
   "Latitude": 106.6878301
 },
 {
   "STT": 10,
   "Name": "Nhà thuốc Xuân Nam",
   "address": "44 Vườn Chuối, Phường  4, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7729369,
   "Latitude": 106.6834228
 },
 {
   "STT": 11,
   "Name": "Nhà thuốc Yến Châu",
   "address": "175/69 Nguyễn Thiện Thuật, Phường  1, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7700153,
   "Latitude": 106.6795832
 },
 {
   "STT": 12,
   "Name": "Nhà thuốc 360",
   "address": "484 (25A) Tây Hòa, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8257841,
   "Latitude": 106.7660349
 },
 {
   "STT": 13,
   "Name": "Nhà thuốc Ái Nhi",
   "address": "101 Đường 61, KP3, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.828464,
   "Latitude": 106.772422
 },
 {
   "STT": 14,
   "Name": "Nhà thuốc An",
   "address": "3/2A Lò Lu, Phường  Trường Thạnh, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.824676,
   "Latitude": 106.808714
 },
 {
   "STT": 15,
   "Name": "Nhà thuốc An Tâm",
   "address": "5 Quang Trung, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8480722,
   "Latitude": 106.7748092
 },
 {
   "STT": 16,
   "Name": "Nhà thuốc An Thanh",
   "address": "265 Lê Văn Việt, KP4, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8442857,
   "Latitude": 106.7847618
 },
 {
   "STT": 17,
   "Name": "Nhà thuốc Anh Thư",
   "address": "420 Lê Văn Việt, Phường  Tăng Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8456612,
   "Latitude": 106.7954892
 },
 {
   "STT": 18,
   "Name": "Nhà thuốc Bảo Trâm",
   "address": "5 Đường 7, KP3, Phường  Phước Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.816694,
   "Latitude": 106.7740029
 },
 {
   "STT": 19,
   "Name": "Nhà thuốc Bình An",
   "address": "A1/7, KP2, Lê Văn Việt, Phường  Tăng Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8455926,
   "Latitude": 106.7935789
 },
 {
   "STT": 20,
   "Name": "Nhà thuốc Công Hiển",
   "address": "259 Đỗ Xuân Hợp, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.822096,
   "Latitude": 106.7715302
 },
 {
   "STT": 21,
   "Name": "Nhà thuốc Cúc Phương",
   "address": "435C Hoàng Hữu Nam, Phường  Long Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8749109,
   "Latitude": 106.8151492
 },
 {
   "STT": 22,
   "Name": "Nhà thuốc Đăng Anh",
   "address": "169A Lò Lu, Phường  Trường Thạnh, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8251836,
   "Latitude": 106.8168374
 },
 {
   "STT": 23,
   "Name": "Nhà thuốc Đông Phương",
   "address": "479A Hoàng Hữu Nam, Ấp Giãn Dân, Phường  Long Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8763095,
   "Latitude": 106.8153646
 },
 {
   "STT": 24,
   "Name": "Nhà thuốc Đức Hạnh",
   "address": "472A1 Lê Văn Việt, Phường  Tăng Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8468178,
   "Latitude": 106.8001059
 },
 {
   "STT": 25,
   "Name": "Nhà thuốc Đức Hiền",
   "address": "69 Man Thiện, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8531297,
   "Latitude": 106.7912236
 },
 {
   "STT": 26,
   "Name": "Nhà thuốc Đức Trọng",
   "address": "4 Lê Văn Việt, Phường Tăng Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.84417,
   "Latitude": 106.789094
 },
 {
   "STT": 27,
   "Name": "Nhà thuốc Gia Lương",
   "address": "32A Tây Hòa, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.826204,
   "Latitude": 106.763126
 },
 {
   "STT": 28,
   "Name": "Nhà thuốc Gia Lương 8",
   "address": "01 Dương Đình Hội, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8169109,
   "Latitude": 106.7747176
 },
 {
   "STT": 29,
   "Name": "Nhà thuốc Hải Anh",
   "address": "1483 Nguyễn Duy Trinh, Phường  Trường Thạnh, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8062599,
   "Latitude": 106.8220639
 },
 {
   "STT": 30,
   "Name": "Nhà thuốc Hiệp Phú",
   "address": "28 Lê Văn Việt, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8475331,
   "Latitude": 106.7761057
 },
 {
   "STT": 31,
   "Name": "Nhà thuốc Hoàng Lê",
   "address": "590 Đỗ Xuân Hợp, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8113597,
   "Latitude": 106.7760085
 },
 {
   "STT": 32,
   "Name": "Nhà thuốc Hoàng Nguyễn",
   "address": "362A, KP4, Đỗ Xuân Hợp, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8261232,
   "Latitude": 106.7690887
 },
 {
   "STT": 33,
   "Name": "Nhà thuốc Hoàng Thông",
   "address": "Số 1 Đường 5, Phường  Phước Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.818288,
   "Latitude": 106.773085
 },
 {
   "STT": 34,
   "Name": "Nhà thuốc Hoàng Trinh",
   "address": "106/1A Đường 109, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8207564,
   "Latitude": 106.7786867
 },
 {
   "STT": 35,
   "Name": "Nhà thuốc Hồng Trang",
   "address": "109 Ngô Quyền, KP2, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8438775,
   "Latitude": 106.7771218
 },
 {
   "STT": 36,
   "Name": "Nhà thuốc Hùng Hạnh",
   "address": "C2/2 Lê Văn Việt, KP2, Phường  Tăng Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.846019,
   "Latitude": 106.795721
 },
 {
   "STT": 37,
   "Name": "Nhà thuốc Hưng Thịnh I",
   "address": "7 Hoàng Hữu Nam, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8503278,
   "Latitude": 106.8136432
 },
 {
   "STT": 38,
   "Name": "Nhà thuốc Kim Hồng",
   "address": "104 Nguyễn Văn Tăng, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.84434,
   "Latitude": 106.8159529
 },
 {
   "STT": 39,
   "Name": "Nhà thuốc Lan Anh",
   "address": "36 Đường 154, KP3, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8676021,
   "Latitude": 106.805991
 },
 {
   "STT": 40,
   "Name": "Nhà thuốc Long Trường",
   "address": "1318 Nguyễn Duy Trinh, Phường  Long Trường, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8049374,
   "Latitude": 106.816484
 },
 {
   "STT": 41,
   "Name": "Nhà thuốc Minh Châu",
   "address": "90 Trần Hưng Đạo, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8478923,
   "Latitude": 106.7750966
 },
 {
   "STT": 42,
   "Name": "Nhà thuốc Minh Minh",
   "address": "3I Đường 882, Phường  Phú Hữu, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7897824,
   "Latitude": 106.7975914
 },
 {
   "STT": 43,
   "Name": "Nhà thuốc Mỹ Anh",
   "address": "Số 1 Đường 154, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8678621,
   "Latitude": 106.8048441
 },
 {
   "STT": 44,
   "Name": "Nhà thuốc Ngọc Anh",
   "address": "224 Đình Phong Phú, KP3, Phường  Tăng Nhơn Phú B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8311939,
   "Latitude": 106.7835975
 },
 {
   "STT": 45,
   "Name": "Nhà thuốc Ngọc Minh",
   "address": "57 (26) KP2 Tây Hòa, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8259919,
   "Latitude": 106.7652549
 },
 {
   "STT": 46,
   "Name": "Nhà thuốc Nguyễn Hà",
   "address": "89A Đường 154, KP3, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8674683,
   "Latitude": 106.8076958
 },
 {
   "STT": 47,
   "Name": "Nhà thuốc Nguyên Vũ II",
   "address": "131 Tăng Nhơn Phú, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8306881,
   "Latitude": 106.7746434
 },
 {
   "STT": 48,
   "Name": "Nhà thuốc Như Ý",
   "address": "209 Lê Lợi, KP2, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8462908,
   "Latitude": 106.7750553
 },
 {
   "STT": 49,
   "Name": "Nhà thuốc Như Ý",
   "address": "769 Nguyễn Xiển, Ấp Long Hòa, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8524911,
   "Latitude": 106.8333518
 },
 {
   "STT": 50,
   "Name": "Nhà thuốc Phúc Đan",
   "address": "32 Trần Hưng Đạo, KP2, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8460568,
   "Latitude": 106.7744118
 },
 {
   "STT": 51,
   "Name": "Nhà thuốc Phúc Phượng",
   "address": "121 Tây Hòa, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.825175,
   "Latitude": 106.762665
 },
 {
   "STT": 52,
   "Name": "Nhà thuốc Phúc Thịnh",
   "address": "458 Nguyễn Văn Tăng, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8424393,
   "Latitude": 106.8282282
 },
 {
   "STT": 53,
   "Name": "Nhà thuốc Phước An",
   "address": "B15, KP1, Lã Xuân Oai, Phường  Tăng Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8398715,
   "Latitude": 106.7953202
 },
 {
   "STT": 54,
   "Name": "Nhà thuốc Phước Hải",
   "address": "677 Lê Văn Việt, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8499181,
   "Latitude": 106.8124777
 },
 {
   "STT": 55,
   "Name": "Nhà thuốc Phước Khang",
   "address": "59 Đình Phong Phú, KP1, Phường  Tăng Nhơn Phú B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8354968,
   "Latitude": 106.7828576
 },
 {
   "STT": 56,
   "Name": "Nhà thuốc Phước Long",
   "address": "305 Đỗ Xuân Hợp, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.820009,
   "Latitude": 106.7747459
 },
 {
   "STT": 57,
   "Name": "Nhà thuốc Phương Thảo",
   "address": "330 (B164) Lê Văn Việt, Phường  Tăng Nhơn Phú B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.844237,
   "Latitude": 106.785445
 },
 {
   "STT": 58,
   "Name": "Nhà thuốc Quỳnh Anh",
   "address": "102 Nguyễn Văn Tăng, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.847547,
   "Latitude": 106.814154
 },
 // {
 //   "STT": 59,
 //   "Name": "Nhà thuốc Quỳnh Chi",
 //   "address": "85/2B Nguyễn Văn Tăng, Ấp Gò Công, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
 //   "Longtitude": 10.3645799,
 //   "Latitude": 106.6781219
 // },
 {
   "STT": 60,
   "Name": "Nhà thuốc SGP Minh Phú",
   "address": "1329 Nguyễn Duy Trinh, Phường  Long Trường, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.805048,
   "Latitude": 106.8164152
 },
 {
   "STT": 61,
   "Name": "Nhà thuốc Số 16",
   "address": "Kios 3, Chợ Tăng Nhơn Phú, Lê Văn Việt, Phường  Tăng Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.844439,
   "Latitude": 106.788939
 },
 {
   "STT": 62,
   "Name": "Nhà thuốc Số 23",
   "address": "44A Nguyễn Văn Tăng, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8465482,
   "Latitude": 106.8164482
 },
 {
   "STT": 63,
   "Name": "Nhà thuốc Số 77 ",
   "address": "91 Đường 18, Phường  Phước Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8147961,
   "Latitude": 106.7712092
 },
 {
   "STT": 64,
   "Name": "Nhà thuốc SPG Hồng Gấm",
   "address": "530 Lã Xuân Oai, Phường  Long Trường, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8178698,
   "Latitude": 106.8093178
 },
 {
   "STT": 65,
   "Name": "Nhà thuốc SPG Minh Thư",
   "address": "1209 Nguyễn Xiển, Phường  Long Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8634309,
   "Latitude": 106.837621
 },
 {
   "STT": 66,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "21B Làng Tăng Phú, Phường  Tăng Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8445301,
   "Latitude": 106.7872545
 },
 {
   "STT": 67,
   "Name": "Nhà thuốc Thanh Bình",
   "address": "243 Nguyễn Văn Tăng, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8426498,
   "Latitude": 106.8285107
 },
 {
   "STT": 68,
   "Name": "Nhà thuốc Thanh Tuyền",
   "address": "35 Man Thiện, KP5, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8457763,
   "Latitude": 106.7865099
 },
 {
   "STT": 69,
   "Name": "Nhà thuốc Thanh Tuyền II",
   "address": "53 Nguyễn Văn Tăng, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8474471,
   "Latitude": 106.8158412
 },
 {
   "STT": 70,
   "Name": "Nhà thuốc Thiên Ân",
   "address": "103 Lê Văn Việt, KP3, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.846966,
   "Latitude": 106.777553
 },
 {
   "STT": 71,
   "Name": "Nhà thuốc Thuận Thiên",
   "address": "84A, KP2, Đỗ Xuân Hợp, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8288298,
   "Latitude": 106.7685528
 },
 {
   "STT": 72,
   "Name": "Nhà thuốc Thùy Trang",
   "address": "595A Lê Văn Việt, Phường  Tăng Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8467845,
   "Latitude": 106.7982779
 },
 {
   "STT": 73,
   "Name": "Nhà thuốc Trâm Anh",
   "address": "31 Đường 339, KP4, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8230422,
   "Latitude": 106.7725396
 },
 {
   "STT": 74,
   "Name": "Nhà thuốc Trí Tâm",
   "address": "14 Đại Lộ II, Phường  Phước Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8177313,
   "Latitude": 106.773007
 },
 {
   "STT": 75,
   "Name": "Nhà thuốc Tuấn Minh",
   "address": "21 Đường 154, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8678315,
   "Latitude": 106.8054063
 },
 {
   "STT": 76,
   "Name": "Nhà thuốc Vân Anh",
   "address": "84 Đại Lộ II, Phường  Phước Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8177313,
   "Latitude": 106.773007
 },
 {
   "STT": 77,
   "Name": "Nhà thuốc Vạn Hạnh",
   "address": "20 Nam Cao, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.851331,
   "Latitude": 106.807226
 },
 {
   "STT": 78,
   "Name": "Nhà thuốc 126",
   "address": "126 Lạc Long Quân, Phường  3, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7588677,
   "Latitude": 106.6390256
 },
 {
   "STT": 79,
   "Name": "Nhà thuốc Á Châu 3",
   "address": "530 Nguyễn Chí Thanh, Phường  7, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7585112,
   "Latitude": 106.6613837
 },
 {
   "STT": 80,
   "Name": "Nhà thuốc An Châu",
   "address": "108 Lạc Long Quân, Phường  3, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7588428,
   "Latitude": 106.6394274
 },
 {
   "STT": 81,
   "Name": "Nhà thuốc Diễm Ngọc",
   "address": "09 Đường số 5 Cư xá Bình Thới, Phường  8, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7628298,
   "Latitude": 106.6488233
 },
 {
   "STT": 82,
   "Name": "Nhà thuốc Hồng Điệp",
   "address": "29 Dương Đình Nghệ, Phường  8, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.760016,
   "Latitude": 106.649682
 },
 {
   "STT": 83,
   "Name": "Nhà thuốc Hồng Phúc",
   "address": "23 Lý Thường Kiệt, Phường  7, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7569436,
   "Latitude": 106.6618222
 },
 {
   "STT": 84,
   "Name": "Nhà thuốc Mai Khương",
   "address": "236 Lạc Long Quân, Phường  10, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762648,
   "Latitude": 106.642335
 },
 {
   "STT": 85,
   "Name": "Nhà thuốc Minh Phước",
   "address": "144 Đội Cung, Phường  9, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.764464,
   "Latitude": 106.64698
 },
 {
   "STT": 86,
   "Name": "Nhà thuốc Mỹ Linh",
   "address": "618 Nguyễn Chí Thanh, Phường  4, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758153,
   "Latitude": 106.659055
 },
 {
   "STT": 87,
   "Name": "Nhà thuốc Ngọc Ánh",
   "address": "47 Dương Đình Nghệ, Phường  8, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759706,
   "Latitude": 106.649617
 },
 {
   "STT": 88,
   "Name": "Nhà thuốc Phương Thành",
   "address": "51 Đường số 2, Cư xá Bình Thới, Phường  8, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7631989,
   "Latitude": 106.6486295
 },
 {
   "STT": 89,
   "Name": "Nhà thuốc Thanh Thủy",
   "address": "179 Lạc Long Quân, Phường  3, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762881,
   "Latitude": 106.642064
 },
 {
   "STT": 90,
   "Name": "Nhà thuốc Thủy Trúc",
   "address": "630 Nguyễn Chí Thanh, Phường  4, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758123,
   "Latitude": 106.65871
 },
 {
   "STT": 91,
   "Name": "Nhà thuốc Á Châu 3",
   "address": "44 Bis Lê Văn Linh, Phường  12, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7637555,
   "Latitude": 106.7067672
 },
 {
   "STT": 92,
   "Name": "Nhà thuốc Bảo Ngọc II",
   "address": "36 Xóm Chiếu, Phường  13, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7594999,
   "Latitude": 106.7114454
 },
 {
   "STT": 93,
   "Name": "Nhà thuốc Đức Minh",
   "address": "19 Vĩnh Hội, Phường  4, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.756198,
   "Latitude": 106.702844
 },
 {
   "STT": 94,
   "Name": "Nhà thuốc Minh Đức 5",
   "address": "333 Tôn Đản, Phường  15, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.75439,
   "Latitude": 106.705673
 },
 {
   "STT": 95,
   "Name": "Nhà thuốc Ngọc Tuyến",
   "address": "909 Tân Vĩnh, Phường  4, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7578369,
   "Latitude": 106.701282
 },
 {
   "STT": 96,
   "Name": "Nhà thuốc 302",
   "address": "302 Trần Hưng Đạo, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7523027,
   "Latitude": 106.6626996
 },
 {
   "STT": 97,
   "Name": "Nhà thuốc An Đông",
   "address": "553 An Dương Vương, Phường  8, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7574041,
   "Latitude": 106.6743262
 },
 {
   "STT": 98,
   "Name": "Nhà thuốc Anh Toàn",
   "address": "004 lô D, Chung cư Hùng Vương, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7548258,
   "Latitude": 106.6644546
 },
 {
   "STT": 99,
   "Name": "Nhà thuốc Bảo Châu",
   "address": "208 Trần Bình Trọng, Phường  4, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7606877,
   "Latitude": 106.67988
 },
 {
   "STT": 100,
   "Name": "Nhà thuốc Cẩm Hà 3",
   "address": "016 lô C, Chung cư Mạc Thiên Tích, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7554376,
   "Latitude": 106.650589
 },
 {
   "STT": 101,
   "Name": "Nhà thuốc Công Vinh",
   "address": "5/7  Huỳnh Mẫn Đạt, Phường  5, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7528956,
   "Latitude": 106.6770665
 },
 {
   "STT": 102,
   "Name": "Nhà thuốc Cúc Phương",
   "address": "18B Trần Bình Trọng, Phường  1  , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7548169,
   "Latitude": 106.6815994
 },
 {
   "STT": 103,
   "Name": "Nhà thuốc Duy Kiên",
   "address": "699 Trần Hưng Đạo, Phường  1  , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7557353,
   "Latitude": 106.6835238
 },
 {
   "STT": 104,
   "Name": "Nhà thuốc Hòa Bình ",
   "address": "31 Bùi Hữu Nghĩa, Phường  5, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7516882,
   "Latitude": 106.6750884
 },
 {
   "STT": 105,
   "Name": "Nhà thuốc Hồng Phúc",
   "address": "153 A Hải Thượng Lãn Ông, Phường  13, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7504535,
   "Latitude": 106.6587176
 },
 {
   "STT": 106,
   "Name": "Nhà thuốc Hùng Dũng",
   "address": "819 Nguyễn Trãi, Phường  14, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7527479,
   "Latitude": 106.6579502
 },
 {
   "STT": 107,
   "Name": "Nhà thuốc Huỳnh Khôi 1",
   "address": "94/1A  94/1A Hùng Vương, Phường  9, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7576083,
   "Latitude": 106.6697195
 },
 {
   "STT": 108,
   "Name": "Nhà thuốc Khoa Khang",
   "address": "33 Bùi Hữu Nghĩa, Phường  5, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7518824,
   "Latitude": 106.6750421
 },
 {
   "STT": 109,
   "Name": "Nhà thuốc Kiều Tạ",
   "address": "2D Phạm Hữu Trí, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7559159,
   "Latitude": 106.6585978
 },
 {
   "STT": 110,
   "Name": "Nhà thuốc Kim Phụng",
   "address": "59 Nguyễn Văn Cừ, Phường  1  , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7555672,
   "Latitude": 106.6854798
 },
 {
   "STT": 111,
   "Name": "Nhà thuốc Kim Sa",
   "address": "322/5 An Dương Vương, Phường  4, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7601029,
   "Latitude": 106.679431
 },
 {
   "STT": 112,
   "Name": "Nhà thuốc Lê Khang 2",
   "address": "22 Đặng Thái Thân, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7551744,
   "Latitude": 106.6653626
 },
 {
   "STT": 113,
   "Name": "Nhà thuốc Long Diên",
   "address": "206 Cao Đạt, Phường  1  , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7542784,
   "Latitude": 106.6826813
 },
 {
   "STT": 114,
   "Name": "Nhà thuốc Long Hải",
   "address": "19 Phú Hữu, Phường  14, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7521983,
   "Latitude": 106.6531852
 },
 {
   "STT": 115,
   "Name": "Nhà thuốc Lý Dung",
   "address": "136 Huỳnh Mẫn Đạt, Phường  3, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7570229,
   "Latitude": 106.6761981
 },
 {
   "STT": 116,
   "Name": "Nhà thuốc Minh Khang",
   "address": "57A Trần Bình Trọng, Phường  1  , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7538116,
   "Latitude": 106.6820188
 },
 {
   "STT": 117,
   "Name": "Nhà thuốc Minh Liên",
   "address": "586 Nguyễn Trãi, Phường  8, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7544092,
   "Latitude": 106.6680404
 },
 {
   "STT": 118,
   "Name": "Nhà thuốc Minh Loan",
   "address": "003 Phan Văn Trị, Phường  2, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7578541,
   "Latitude": 106.6840535
 },
 {
   "STT": 119,
   "Name": "Nhà thuốc Mỹ Tùng",
   "address": "142 Lê Hồng Phong, Phường  3, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7579677,
   "Latitude": 106.6782074
 },
 {
   "STT": 120,
   "Name": "Nhà thuốc Nancy",
   "address": "615 Trần Hưng Đạo, Phường  1  , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7560925,
   "Latitude": 106.6851464
 },
 {
   "STT": 121,
   "Name": "Nhà thuốc Ngọc Loan ",
   "address": "251 B Trần Phú, Phường  9, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7580677,
   "Latitude": 106.6749994
 },
 {
   "STT": 122,
   "Name": "Nhà thuốc Ngân Hà",
   "address": "014F Đặng Thái Thân, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7547435,
   "Latitude": 106.6658394
 },
 {
   "STT": 123,
   "Name": "Nhà thuốc Ngân Hà 6",
   "address": "12 Đặng Thái Thân, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7546485,
   "Latitude": 106.6655529
 },
 {
   "STT": 124,
   "Name": "Nhà thuốc Ngân Hà 9",
   "address": "024  Lô E Chung cư Hùng Vương, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7542552,
   "Latitude": 106.664777
 },
 {
   "STT": 125,
   "Name": "Nhà thuốc Ngọc An",
   "address": "006 C/c 21-41 Tản Đà, Phường  10, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7512528,
   "Latitude": 106.6644546
 },
 {
   "STT": 126,
   "Name": "Nhà thuốc Ngọc Hạnh",
   "address": "34B Nguyễn Duy Dương, Phường  8, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7562334,
   "Latitude": 106.6720319
 },
 {
   "STT": 127,
   "Name": "Nhà thuốc Ngọc Tuyết",
   "address": "33 Thuận Kiều, Phường  12           , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7562976,
   "Latitude": 106.658186
 },
 {
   "STT": 128,
   "Name": "Nhà thuốc Ngọc Yến",
   "address": "84 Ký Hòa, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7544105,
   "Latitude": 106.6604793
 },
 {
   "STT": 129,
   "Name": "Nhà thuốc Nguyễn Biểu ",
   "address": "59 Nguyễn Biểu, Phường  1  , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7540864,
   "Latitude": 106.6841645
 },
 {
   "STT": 130,
   "Name": "Nhà thuốc Nguyễn Hoàng",
   "address": "230 Trần  Phú Phường  9, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7580156,
   "Latitude": 106.674554
 },
 {
   "STT": 131,
   "Name": "Nhà thuốc Nhà Thuốc 2",
   "address": "40 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7557109,
   "Latitude": 106.6585829
 },
 {
   "STT": 132,
   "Name": "Nhà thuốc Nhân Khánh",
   "address": "1163 Trần Hưng Đạo, Phường  5, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7528263,
   "Latitude": 106.6723618
 },
 {
   "STT": 133,
   "Name": "Nhà thuốc Phan Chương",
   "address": "191 An Bình, Phường  7, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7539015,
   "Latitude": 106.671428
 },
 {
   "STT": 134,
   "Name": "Nhà thuốc Phú Cường",
   "address": "267A Nguyễn Chí Thanh, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7572778,
   "Latitude": 106.6566566
 },
 {
   "STT": 135,
   "Name": "Nhà thuốc Phuc Thịnh",
   "address": "2G Lý Thường Kiệt Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7564221,
   "Latitude": 106.6627029
 },
 {
   "STT": 136,
   "Name": "Nhà thuốc Phương Hiền",
   "address": "118 An Binh ,Phường  5, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7525618,
   "Latitude": 106.6726139
 },
 {
   "STT": 137,
   "Name": "Nhà thuốc Quang Thắng",
   "address": "002 lô B cc Ngô Quyền, Phường  9, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7590579,
   "Latitude": 106.6658625
 },
 {
   "STT": 138,
   "Name": "Nhà thuốc Quỳnh Trâm",
   "address": "520 Nguyễn Tri Phương, Phường  8, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7675587,
   "Latitude": 106.667294
 },
 {
   "STT": 139,
   "Name": "Nhà thuốc Số 36",
   "address": "937 Trần Hưng Đạo, Phường  2 , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7543467,
   "Latitude": 106.6776435
 },
 {
   "STT": 140,
   "Name": "Nhà thuốc Tâm Việt",
   "address": "172 Nguyễn Biểu, Phường  2, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7577704,
   "Latitude": 106.6831723
 },
 {
   "STT": 141,
   "Name": "Nhà thuốc Thái Hà",
   "address": "139 Bùi Hữu Nghĩa, Phường  7, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7543537,
   "Latitude": 106.6745538
 },
 {
   "STT": 142,
   "Name": "Nhà thuốc Thái Hòa",
   "address": "62 Tản Đà, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7524655,
   "Latitude": 106.6645477
 },
 {
   "STT": 143,
   "Name": "Nhà thuốc Thân Dân",
   "address": "724 Trần Hưng Đạo, Phường  2, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7545601,
   "Latitude": 106.6775816
 },
 {
   "STT": 144,
   "Name": "Nhà thuốc Thanh Tâm",
   "address": "91 Trần Phú, Phường  4, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7610247,
   "Latitude": 106.6777873
 },
 {
   "STT": 145,
   "Name": "Nhà thuốc Thành Thái",
   "address": "441 An Dương Vương, Phường  3, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7583249,
   "Latitude": 106.6765733
 },
 {
   "STT": 146,
   "Name": "Nhà thuốc Thảo Loan",
   "address": "112 Trần Tuấn Khải, Phường  5, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7519871,
   "Latitude": 106.6742915
 },
 {
   "STT": 147,
   "Name": "Nhà thuốc Thiên Lộc",
   "address": "418/124 Trần Phú, Phường  7, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7532667,
   "Latitude": 106.6679123
 },
 {
   "STT": 148,
   "Name": "Nhà thuốc Thiên Ngọc",
   "address": "179 Nguyễn Văn Cừ, Phường  2 , Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758687,
   "Latitude": 106.6840931
 },
 {
   "STT": 149,
   "Name": "Nhà thuốc Thùy Dương ",
   "address": "73 Phạm Hữu Chí, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7556871,
   "Latitude": 106.6587867
 },
 {
   "STT": 150,
   "Name": "Nhà thuốc Trần Thái",
   "address": "124 Ngô Quyền Phường  9, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7619115,
   "Latitude": 106.6651012
 },
 {
   "STT": 151,
   "Name": "Nhà thuốc Tri Thức",
   "address": "38 Phạm Đôn, Phường  10, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7517226,
   "Latitude": 106.6634294
 },
 {
   "STT": 152,
   "Name": "Nhà thuốc Triều Vy 5",
   "address": "177 Bùi Hữu Nghĩa, Phường  7, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7551189,
   "Latitude": 106.6742705
 },
 {
   "STT": 153,
   "Name": "Nhà thuốc Trọng Nghĩa",
   "address": "2F Lý Thường Kiệt,  Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.757014,
   "Latitude": 106.6621747
 },
 {
   "STT": 154,
   "Name": "Nhà thuốc Trường Thịnh",
   "address": "1047  Nguyễn Trãi, Phường  14, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.752952,
   "Latitude": 106.6513801
 },
 {
   "STT": 155,
   "Name": "Nhà thuốc Tuấn",
   "address": "31 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7562666,
   "Latitude": 106.6581893
 },
 {
   "STT": 156,
   "Name": "Nhà thuốc Tuyết Hùng",
   "address": "020 Lô E C/c Nguyễn Trãi, Phường  8, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7544417,
   "Latitude": 106.6688775
 },
 {
   "STT": 157,
   "Name": "Nhà thuốc Vì Dân 2",
   "address": "210 A Trần Bình Trọng, Phường  4, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.760714,
   "Latitude": 106.679821
 },
 {
   "STT": 158,
   "Name": "Nhà thuốc 79",
   "address": "21/4 Lâm Văn Bền, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.738529,
   "Latitude": 106.716054
 },
 {
   "STT": 159,
   "Name": "Nhà thuốc Bảo Duy",
   "address": "649 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7387844,
   "Latitude": 106.7302251
 },
 {
   "STT": 160,
   "Name": "Nhà thuốc Bảo Duy 2",
   "address": "745 Huỳnh Tấn Phát, Phường  Phú thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7200089,
   "Latitude": 106.737788
 },
 {
   "STT": 161,
   "Name": "Nhà thuốc Bảo Tín",
   "address": "44 Tân Mỹ,  Phường  Tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7368817,
   "Latitude": 106.7182034
 },
 {
   "STT": 162,
   "Name": "Nhà thuốc Bích Ngọc 2",
   "address": "408 Lê Văn Lương, Phường  Tân Hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7370906,
   "Latitude": 106.7030716
 },
 {
   "STT": 163,
   "Name": "Nhà thuốc Bích Ngọc 3",
   "address": "7/7 Lê Văn Lương, Phường  Tân Phong, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7307919,
   "Latitude": 106.70068
 },
 {
   "STT": 164,
   "Name": "Nhà thuốc Bình Minh",
   "address": "383 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7448975,
   "Latitude": 106.729244
 },
 {
   "STT": 165,
   "Name": "Nhà thuốc Công Thiện",
   "address": "1228 Huỳnh Tấn Phát, Phường  Tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7202429,
   "Latitude": 106.7366764
 },
 {
   "STT": 166,
   "Name": "Nhà thuốc Cường Thịnh",
   "address": "76/9 Lâm Văn Bền, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7474519,
   "Latitude": 106.715991
 },
 {
   "STT": 167,
   "Name": "Nhà thuốc Đan Huy",
   "address": "502 Huỳnh Tấn Phát, Phường  Tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7353564,
   "Latitude": 106.7307046
 },
 {
   "STT": 168,
   "Name": "Nhà thuốc Diễm Thuận",
   "address": "30/93 Lâm Văn Bền, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7447031,
   "Latitude": 106.7158162
 },
 {
   "STT": 169,
   "Name": "Nhà thuốc Đức Việt",
   "address": "1579A Huỳnh Tấn Phát, Phường  Phú Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7125946,
   "Latitude": 106.737092
 },
 {
   "STT": 170,
   "Name": "Nhà thuốc Duy Khang",
   "address": "529A Lê Văn Lương, Phường  Tân Phong, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7350371,
   "Latitude": 106.7024642
 },
 {
   "STT": 171,
   "Name": "Nhà thuốc Gia My",
   "address": "858 Huỳnh Tấn Phát, Phường  Tân phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.729761,
   "Latitude": 106.7323686
 },
 {
   "STT": 172,
   "Name": "Nhà thuốc Hải Ngọc",
   "address": "289 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7513855,
   "Latitude": 106.7285541
 },
 {
   "STT": 173,
   "Name": "Nhà thuốc Hậu Nghĩa",
   "address": "671 Trần Xuân Soạn, Phường  Tân Hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.751152,
   "Latitude": 106.703304
 },
 {
   "STT": 174,
   "Name": "Nhà thuốc Hoàng Dũng",
   "address": "46 Bùi Văn Ba, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7504954,
   "Latitude": 106.736635
 },
 {
   "STT": 175,
   "Name": "Nhà thuốc Hoàng Gia",
   "address": "81 Mai Văn Vĩnh, Phường  Tân Quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7388758,
   "Latitude": 106.7137001
 },
 {
   "STT": 176,
   "Name": "Nhà thuốc Hoàng Gia",
   "address": "101 Phạm Hữu Lầu, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.704475,
   "Latitude": 106.7317468
 },
 {
   "STT": 177,
   "Name": "Nhà thuốc Hoàng Hà",
   "address": "1631 Huỳnh Tấn Phát, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7018404,
   "Latitude": 106.7381763
 },
 {
   "STT": 178,
   "Name": "Nhà thuốc Hoàng Nguyên",
   "address": "294 Huỳnh Tấn Phát, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7513772,
   "Latitude": 106.7285546
 },
 {
   "STT": 179,
   "Name": "Nhà thuốc Hổng Hạnh",
   "address": "28/17B Lý Phục Man, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7433979,
   "Latitude": 106.72597
 },
 {
   "STT": 180,
   "Name": "Nhà thuốc Hồng Nữ",
   "address": "641 Trần Xuân Soạn, Phường  Tân Hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.751179,
   "Latitude": 106.703551
 },
 {
   "STT": 181,
   "Name": "Nhà thuốc Hưng Thịnh",
   "address": "1131 Huỳnh Tấn Phát, Phường  Phú Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7217481,
   "Latitude": 106.7361653
 },
 {
   "STT": 182,
   "Name": "Nhà thuốc Hữu Châu",
   "address": "781 Huỳnh Tấn Phát, Phường  Phú Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.717893,
   "Latitude": 106.738229
 },
 {
   "STT": 183,
   "Name": "Nhà thuốc Hữu Nghị",
   "address": "21/6 Lâm Văn Bền, Phường  Tân Quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.73871,
   "Latitude": 106.715771
 },
 {
   "STT": 184,
   "Name": "Nhà thuốc Hữu Tâm",
   "address": "178 Nguyễn Thị Thập, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7382919,
   "Latitude": 106.7163803
 },
 {
   "STT": 185,
   "Name": "Nhà thuốc Huỳnh Lê",
   "address": "36/6 Phạm Hữu Lầu, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7051464,
   "Latitude": 106.7370808
 },
 {
   "STT": 186,
   "Name": "Nhà thuốc Huỳnh Trí",
   "address": "675 Trần Xuân Soạn, Phường  Tân Hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.751061,
   "Latitude": 106.70345
 },
 {
   "STT": 187,
   "Name": "Nhà thuốc Khai Tâm",
   "address": "22A Phạm Hữu Lầu, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7051328,
   "Latitude": 106.7371012
 },
 {
   "STT": 188,
   "Name": "Nhà thuốc Khánh Phương",
   "address": "166 Lâm Văn Bền, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.742588,
   "Latitude": 106.7158725
 },
 {
   "STT": 189,
   "Name": "Nhà thuốc Kim Ngân",
   "address": "487/27 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7558021,
   "Latitude": 106.7220282
 },
 {
   "STT": 190,
   "Name": "Nhà thuốc Liên Phương",
   "address": "48 Huỳnh Tấn Phát, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7555809,
   "Latitude": 106.7224949
 },
 {
   "STT": 191,
   "Name": "Nhà thuốc Minh Châu",
   "address": "58 Đường số 17, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7449573,
   "Latitude": 106.7129429
 },
 {
   "STT": 192,
   "Name": "Nhà thuốc Minh Phúc",
   "address": "1009 Trần Xuân Soạn, Phường  Tân Hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749728,
   "Latitude": 106.701791
 },
 {
   "STT": 193,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "173 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747572,
   "Latitude": 106.729576
 },
 {
   "STT": 194,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "88/27 Nguyễn Văn Quỳ, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.740893,
   "Latitude": 106.731776
 },
 {
   "STT": 195,
   "Name": "Nhà thuốc Minh Thi",
   "address": "125 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7397546,
   "Latitude": 106.7301337
 },
 {
   "STT": 196,
   "Name": "Nhà thuốc Mỹ An 2",
   "address": "62 Lý Phục Man, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.741737,
   "Latitude": 106.725142
 },
 {
   "STT": 197,
   "Name": "Nhà thuốc Mỹ Hiền",
   "address": "949 Trần Xuân Soạn, Phường  Tân Hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.750586,
   "Latitude": 106.6994069
 },
 {
   "STT": 198,
   "Name": "Nhà thuốc Nam Long",
   "address": "20/3B Gò Ô Môi, Phường  Phú Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.729016,
   "Latitude": 106.734169
 },
 {
   "STT": 199,
   "Name": "Nhà thuốc Ngân Châu",
   "address": "135  Đường số 17, Phường  Tân Quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.744736,
   "Latitude": 106.710249
 },
 {
   "STT": 200,
   "Name": "Nhà thuốc Ngọc Minh",
   "address": "41/9B Huỳnh Tấn Phát, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.753431,
   "Latitude": 106.726514
 },
 {
   "STT": 201,
   "Name": "Nhà thuốc Ngọc Tâm",
   "address": "12/5 Bùi Văn Ba, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.750042,
   "Latitude": 106.731806
 },
 {
   "STT": 202,
   "Name": "Nhà thuốc Nguyên Khang",
   "address": "489/15 Trần Xuân Soạn, Phường  Tân Hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748959,
   "Latitude": 106.693332
 },
 {
   "STT": 203,
   "Name": "Nhà thuốc Nguyễn Lữ",
   "address": "5/8 Đường số 9, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.746706,
   "Latitude": 106.70862
 },
 {
   "STT": 204,
   "Name": "Nhà thuốc Nguyễn Thanh",
   "address": "156 Lê Văn Lương, Phường  Tân Hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7479335,
   "Latitude": 106.7046536
 },
 {
   "STT": 205,
   "Name": "Nhà thuốc Nhà thuốc Thảo",
   "address": "46/1 Lâm Văn Bền, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.751006,
   "Latitude": 106.715627
 },
 {
   "STT": 206,
   "Name": "Nhà thuốc Nhân Hòa",
   "address": "169 Lên Văn Lương, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748039,
   "Latitude": 106.704749
 },
 {
   "STT": 207,
   "Name": "Nhà thuốc Nhân Tâm",
   "address": "487/5 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7536185,
   "Latitude": 106.7283857
 },
 {
   "STT": 208,
   "Name": "Nhà thuốc Nhật Phương",
   "address": "8 Huỳnh Tấn Phát, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7544869,
   "Latitude": 106.724168
 },
 {
   "STT": 209,
   "Name": "Nhà thuốc Nhật Thủy",
   "address": "75 Lê Văn Lương, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749787,
   "Latitude": 106.7050243
 },
 {
   "STT": 210,
   "Name": "Nhà thuốc Như Xuân",
   "address": "16/5 Bùi Văn Ba, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749712,
   "Latitude": 106.730388
 },
 {
   "STT": 211,
   "Name": "Nhà thuốc Phong Châu",
   "address": "51/54 Mai Văn Vĩnh, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.744475,
   "Latitude": 106.7138049
 },
 {
   "STT": 212,
   "Name": "Nhà thuốc Phú Châu",
   "address": "30/28 Lâm Văn Bền, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7482943,
   "Latitude": 106.7158661
 },
 {
   "STT": 213,
   "Name": "Nhà thuốc Phú Thịnh",
   "address": "93 Trần Xuân Soạn, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7542768,
   "Latitude": 106.7204236
 },
 {
   "STT": 214,
   "Name": "Nhà thuốc Phú Thịnh 2",
   "address": "150 Nguyễn Thị Thập, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7382918,
   "Latitude": 106.716382
 },
 {
   "STT": 215,
   "Name": "Nhà thuốc Phúc Lâm",
   "address": "22 Nguyễn Thị Thập, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.738936,
   "Latitude": 106.718688
 },
 {
   "STT": 216,
   "Name": "Nhà thuốc Phước Lộc",
   "address": "5 Đường số 8, Phường  Tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7361175,
   "Latitude": 106.719051
 },
 {
   "STT": 217,
   "Name": "Nhà thuốc Phương Hồng",
   "address": "334 Lê Văn Lương, Phường  Tân Hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7256713,
   "Latitude": 106.698238
 },
 {
   "STT": 218,
   "Name": "Nhà thuốc Quốc Thanh",
   "address": "599/15 Tần Xuấn Soạn, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7518217,
   "Latitude": 106.7114548
 },
 {
   "STT": 219,
   "Name": "Nhà thuốc Quỳnh Anh",
   "address": "S41-1 Bùi Bằng Đoàn, Phường  Tân Phong, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.728986,
   "Latitude": 106.7086013
 },
 {
   "STT": 220,
   "Name": "Nhà thuốc Quỳnh Anh 3",
   "address": "45-47 Hưng Phước 3, Phường  Tân Phong, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7304868,
   "Latitude": 106.7075856
 },
 {
   "STT": 221,
   "Name": "Nhà thuốc Quỳnh Giao",
   "address": "153 Phạm Hữu Lầu, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7058063,
   "Latitude": 106.7441997
 },
 {
   "STT": 222,
   "Name": "Nhà thuốc Tâm Khoa",
   "address": "14 Tân Mỹ, Phường  Tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7375478,
   "Latitude": 106.7181827
 },
 {
   "STT": 223,
   "Name": "Nhà thuốc Tân Thuận",
   "address": "40 Huỳnh Tấn Phát, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7556127,
   "Latitude": 106.7223133
 },
 {
   "STT": 224,
   "Name": "Nhà thuốc Thanh Quang",
   "address": "1051 Huỳnh Tấn Phát, Phường  Phú Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6739492,
   "Latitude": 106.7667121
 },
 {
   "STT": 225,
   "Name": "Nhà thuốc Thế Danh",
   "address": "80A Nguyễn Thị Thập, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7378779,
   "Latitude": 106.7239272
 },
 {
   "STT": 226,
   "Name": "Nhà thuốc Thiên Tâm",
   "address": "1729 Huỳnh Tấn Phát, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7018404,
   "Latitude": 106.7381763
 },
 {
   "STT": 227,
   "Name": "Nhà thuốc Tiến Thịnh",
   "address": "1000 Huỳnh Tấn Phát, Phường  Tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7246328,
   "Latitude": 106.7346132
 },
 {
   "STT": 228,
   "Name": "Nhà thuốc Trọng Nhân",
   "address": "167 Phạm Hữu Lầu, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.704476,
   "Latitude": 106.7325977
 },
 {
   "STT": 229,
   "Name": "Nhà thuốc Trọng Phú",
   "address": "365 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.750597,
   "Latitude": 106.727821
 },
 {
   "STT": 230,
   "Name": "Nhà thuốc Trúc Duyên",
   "address": "95 Gò Ô Môi, Phường  Phú Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7297309,
   "Latitude": 106.7352199
 },
 {
   "STT": 231,
   "Name": "Nhà thuốc Từ Phương",
   "address": "15 Phú Thuận, Phường  Phú Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.731335,
   "Latitude": 106.7332598
 },
 {
   "STT": 232,
   "Name": "Nhà thuốc Từ Phương 2",
   "address": "14C Tân Mỹ, Phường  Tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7375478,
   "Latitude": 106.7181827
 },
 {
   "STT": 233,
   "Name": "Nhà thuốc Vân Anh",
   "address": "818 Huỳnh Tấn Phát, Phường  Tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.729598,
   "Latitude": 106.7322559
 },
 {
   "STT": 234,
   "Name": "Nhà thuốc Xuân Mai",
   "address": "55 Bùi Văn Ba, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749931,
   "Latitude": 106.732491
 },
 {
   "STT": 235,
   "Name": "Nhà thuốc Y Đức",
   "address": "64 Lâm Văn Bền, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7473145,
   "Latitude": 106.7158028
 },
 {
   "STT": 236,
   "Name": "Nhà thuốc 65",
   "address": "92 Dương Bá Trạc, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748721,
   "Latitude": 106.688303
 },
 {
   "STT": 237,
   "Name": "Nhà thuốc Công Thành",
   "address": "205 Dương Bá Trạc, Phường  1, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.742317,
   "Latitude": 106.689334
 },
 {
   "STT": 238,
   "Name": "Nhà thuốc Hồng Loan",
   "address": "217 Âu Dương Lân, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.744449,
   "Latitude": 106.683965
 },
 {
   "STT": 239,
   "Name": "Nhà thuốc Ngọc Thu",
   "address": "283 Âu Dương Lân, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.742764,
   "Latitude": 106.684929
 },
 {
   "STT": 240,
   "Name": "Nhà thuốc Ngọc Vinh",
   "address": "295 Dương Bá Trạc, Phường  1, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.744704,
   "Latitude": 106.689259
 },
 {
   "STT": 241,
   "Name": "Nhà thuốc Quốc Vương",
   "address": "263 Âu Dương Lân, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7433061,
   "Latitude": 106.6845353
 },
 {
   "STT": 242,
   "Name": "Nhà thuốc Sinh Đôi 3",
   "address": "51 Dương Bá Trạc, Phường  1, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749353,
   "Latitude": 106.688451
 },
 {
   "STT": 243,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "75 Nguyễn Thị Tần, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747536,
   "Latitude": 106.685639
 },
 {
   "STT": 244,
   "Name": "Nhà thuốc Thúy Liễu",
   "address": "283 Âu Dương Lân, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.742764,
   "Latitude": 106.684929
 },
 {
   "STT": 245,
   "Name": "Nhà thuốc Vân Anh 3",
   "address": "450C/4 Dương Bá Trạc, Phường  1, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.740776,
   "Latitude": 106.687675
 },
 {
   "STT": 246,
   "Name": "Nhà thuốc Anh Khôi",
   "address": "D8/2/1A Ấp 2, Xã Bình Lợi, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7961716,
   "Latitude": 106.5793333
 },
 {
   "STT": 247,
   "Name": "Nhà thuốc Cẩm Tú",
   "address": "E8/1A Ấp 5, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7781128,
   "Latitude": 106.5750148
 },
 {
   "STT": 248,
   "Name": "Nhà thuốc Dũ Quyền 2",
   "address": "E1/7 Ấp 5, Xã Vĩnh Lộc B , Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7982704,
   "Latitude": 106.5561927
 },
 {
   "STT": 249,
   "Name": "Nhà thuốc Dược Linh",
   "address": "A5/4X Ấp 1, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776099,
   "Latitude": 106.5780894
 },
 {
   "STT": 250,
   "Name": "Nhà thuốc Hoàng Tuấn",
   "address": "B6/27 Đường liên Ấp 2-6, Ấp 2, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8181617,
   "Latitude": 106.5705809
 },
 {
   "STT": 251,
   "Name": "Nhà thuốc Hồng Ân",
   "address": "D6/11 Ấp 4, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.854765,
   "Latitude": 106.567099
 },
 {
   "STT": 252,
   "Name": "Nhà thuốc Hồng Hạnh",
   "address": "B12D/11U Ấp 2, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7941362,
   "Latitude": 106.578819
 },
 {
   "STT": 253,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "D7/221 Ấp 4, Quốc lộ 50, Xã Đa Phước, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6917747,
   "Latitude": 106.6548981
 },
 {
   "STT": 254,
   "Name": "Nhà thuốc Lộc Ngân",
   "address": "F2/3A Ấp 6, Quách Điêu, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8148204,
   "Latitude": 106.5773201
 },
 {
   "STT": 255,
   "Name": "Nhà thuốc Minh Dung",
   "address": "D9/37 D Ấp 4, Xã Hưng Long, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.657965,
   "Latitude": 106.691021
 },
 {
   "STT": 256,
   "Name": "Nhà thuốc Minh Quân",
   "address": "A3/3 Ấp 2, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.658964,
   "Latitude": 106.6562402
 },
 {
   "STT": 257,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "C9/4 Ấp 4 Xã, Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.65925,
   "Latitude": 106.689784
 },
 {
   "STT": 258,
   "Name": "Nhà thuốc Ngân Hà 8",
   "address": "Số 7, Lô 1, Đường số 4, Khu dân cư Tân Tạo, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7257848,
   "Latitude": 106.5851735
 },
 {
   "STT": 259,
   "Name": "Nhà thuốc Ngọc Điệp",
   "address": "E2/1A1 Ấp 5, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.86353,
   "Latitude": 106.569774
 },
 {
   "STT": 260,
   "Name": "Nhà thuốc Ngọc Ngân 397",
   "address": "B6/195 Ấp 2, Xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7306442,
   "Latitude": 106.6509137
 },
 {
   "STT": 261,
   "Name": "Nhà thuốc Ngọc Phú",
   "address": "C10/3 Ấp 4A, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7291388,
   "Latitude": 106.6769167
 },
 {
   "STT": 262,
   "Name": "Nhà thuốc Phát Lộc ",
   "address": "F9/14A Hương lộ 80, Ấp 6, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8113963,
   "Latitude": 106.5757703
 },
 {
   "STT": 263,
   "Name": "Nhà thuốc Phong Thư",
   "address": "F2/2 Ấp 6, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8181488,
   "Latitude": 106.5708708
 },
 {
   "STT": 264,
   "Name": "Nhà thuốc Phương Loan",
   "address": "F3/1X2 Ấp 6, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8181488,
   "Latitude": 106.5708708
 },
 {
   "STT": 265,
   "Name": "Nhà thuốc Quang Minh",
   "address": "F1/63 Quách Điêu, Ấp 6, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8152165,
   "Latitude": 106.5766015
 },
 {
   "STT": 266,
   "Name": "Nhà thuốc Tâm An",
   "address": "33 Đường số 20, Khu dân cư Him Lam 6A, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7307508,
   "Latitude": 106.6869255
 },
 {
   "STT": 267,
   "Name": "Nhà thuốc Thanh Hằng 2",
   "address": "190B/7 Ấp 2, Xã An Phú Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.68182,
   "Latitude": 106.6101
 },
 {
   "STT": 268,
   "Name": "Nhà thuốc Thanh Nhàn",
   "address": "1/26 Ấp 4, Xã Tân Quý Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6591187,
   "Latitude": 106.6051463
 },
 {
   "STT": 269,
   "Name": "Nhà thuốc Thanh Phóng",
   "address": "C3/3A Nữ Dân Công, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8320926,
   "Latitude": 106.5634782
 },
 {
   "STT": 270,
   "Name": "Nhà thuốc Thảo Trân",
   "address": "A1/3 Ấp 1, Nguyễn Cửu Phú, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7120352,
   "Latitude": 106.579822
 },
 {
   "STT": 271,
   "Name": "Nhà thuốc Trâm Anh",
   "address": "A5/14V Ấp 1, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7563625,
   "Latitude": 106.5882543
 },
 {
   "STT": 272,
   "Name": "Nhà thuốc Trần Đỗ",
   "address": "F1/18F Ấp 6, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.818282,
   "Latitude": 106.5681569
 },
 {
   "STT": 273,
   "Name": "Nhà thuốc Trần Đỗ 2",
   "address": "F1/6 Ấp 6, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.818282,
   "Latitude": 106.5681569
 },
 {
   "STT": 274,
   "Name": "Nhà thuốc Trường Sơn",
   "address": "A3/37B Ấp 1, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776099,
   "Latitude": 106.5780894
 },
 {
   "STT": 275,
   "Name": "Nhà thuốc Tuấn Tài",
   "address": "C7/06 Ấp 4A, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6591958,
   "Latitude": 106.6879783
 },
 {
   "STT": 276,
   "Name": "Nhà thuốc Tuyết Nhung",
   "address": "B8A/12A Ấp 2, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7941362,
   "Latitude": 106.578819
 },
 {
   "STT": 277,
   "Name": "Nhà thuốc Việt Hà",
   "address": "A1/5 Ấp 1, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7127569,
   "Latitude": 106.5985183
 },
 {
   "STT": 278,
   "Name": "Nhà thuốc Việt My 2",
   "address": "B7/181/1A Ấp 2, Xã Đa Phước, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.658964,
   "Latitude": 106.6562402
 },
 {
   "STT": 279,
   "Name": "Nhà thuốc Ý Mi 3",
   "address": "A12/14 Ấp 2, Quốc lộ 50, Xã Bình Hung, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7405816,
   "Latitude": 106.6562229
 },
 {
   "STT": 280,
   "Name": "Nhà thuốc Đông Nam",
   "address": "19 Lê Quang Định, Phường  14, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8043147,
   "Latitude": 106.6980985
 },
 {
   "STT": 281,
   "Name": "Nhà thuốc Hà Anh",
   "address": "186-188 Xô Viết Nghệ Tĩnh, Phường  21, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.797848,
   "Latitude": 106.71104
 },
 {
   "STT": 282,
   "Name": "Nhà thuốc Ngọc Hân",
   "address": "344 Xô Viết Nghệ Tĩnh, Phường  25, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.803578,
   "Latitude": 106.711561
 },
 {
   "STT": 283,
   "Name": "Nhà thuốc Nhân Ái",
   "address": "265 Bùi Đình Túy, Phường  24, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.806238,
   "Latitude": 106.704967
 },
 {
   "STT": 284,
   "Name": "Nhà thuốc An Hảo",
   "address": "29 Đường số 28, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8351757,
   "Latitude": 106.6387868
 },
 {
   "STT": 285,
   "Name": "Nhà thuốc Anh Quốc",
   "address": "85/845 Nguyễn Văn Nghi, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8263719,
   "Latitude": 106.681036
 },
 {
   "STT": 286,
   "Name": "Nhà thuốc Gia Anh",
   "address": "16 Lê Văn Thọ, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8371524,
   "Latitude": 106.6582983
 },
 {
   "STT": 287,
   "Name": "Nhà thuốc Gia Bình",
   "address": "152 Nguyễn Oanh, Phường  17, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.84958,
   "Latitude": 106.677605
 },
 {
   "STT": 288,
   "Name": "Nhà thuốc Gia Phong",
   "address": "60 Phan Văn Trị, Phường  10, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.829588,
   "Latitude": 106.681477
 },
 {
   "STT": 289,
   "Name": "Nhà thuốc Khải Hoàn 1",
   "address": "23/6B Thống Nhất, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.833357,
   "Latitude": 106.663847
 },
 {
   "STT": 290,
   "Name": "Nhà thuốc Khánh Tâm",
   "address": "818/56B Nguyễn Kiệm, Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8128831,
   "Latitude": 106.678665
 },
 {
   "STT": 291,
   "Name": "Nhà thuốc Kiều Hoa",
   "address": "117 Đường số 3, Phường  9, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8479017,
   "Latitude": 106.6516028
 },
 {
   "STT": 292,
   "Name": "Nhà thuốc Kim Loan",
   "address": "148 Đường số 59, Phường  14, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8550704,
   "Latitude": 106.6522233
 },
 {
   "STT": 293,
   "Name": "Nhà thuốc Lê Dung",
   "address": "107 Lê Văn Thọ, Phường  9, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8448761,
   "Latitude": 106.6570115
 },
 {
   "STT": 294,
   "Name": "Nhà thuốc Minh Thư",
   "address": "10/3 Phạm văn Chiêu, Phường  14, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.851508,
   "Latitude": 106.655305
 },
 {
   "STT": 295,
   "Name": "Nhà thuốc Minh Trang",
   "address": "36 Huỳnh Khương An, Phường  5, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.823972,
   "Latitude": 106.688472
 },
 {
   "STT": 296,
   "Name": "Nhà thuốc Phúc Lộc",
   "address": "198 Nguyễn Văn nghi, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8263675,
   "Latitude": 106.6822861
 },
 {
   "STT": 297,
   "Name": "Nhà thuốc Phương Nam",
   "address": "980 Quang Trung, Phường  8, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8370981,
   "Latitude": 106.6560086
 },
 {
   "STT": 298,
   "Name": "Nhà thuốc Phương Trang",
   "address": "13A Đường số 11, Phường  17, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.84015,
   "Latitude": 106.665797
 },
 {
   "STT": 299,
   "Name": "Nhà thuốc Thanh Thảo",
   "address": "13/87C Phan Huy Ích, Phường  14, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.839998,
   "Latitude": 106.637401
 },
 {
   "STT": 300,
   "Name": "Nhà thuốc Thanh Thoại",
   "address": "5/27 Trương Đăng Quế, Phường  1, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8129381,
   "Latitude": 106.6835211
 },
 {
   "STT": 301,
   "Name": "Nhà thuốc Thanh Thùy",
   "address": "103 Nguyễn Thái Sơn, Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8195032,
   "Latitude": 106.6828841
 },
 {
   "STT": 302,
   "Name": "Nhà thuốc Thiên Phú",
   "address": "226/25 Nguyễn Văn Lượng, Phường  17, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8384138,
   "Latitude": 106.6794448
 },
 {
   "STT": 303,
   "Name": "Nhà thuốc Thống Nhất",
   "address": "2/5 Thống Nhất, Phường  10, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8308665,
   "Latitude": 106.6636258
 },
 {
   "STT": 304,
   "Name": "Nhà thuốc Tuyết Nhi",
   "address": "107/4Bis Thống Nhất, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8394135,
   "Latitude": 106.6653068
 },
 {
   "STT": 305,
   "Name": "Nhà thuốc Việt",
   "address": "223 Nguyễn Thái Sơn, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.825006,
   "Latitude": 106.687259
 },
 {
   "STT": 306,
   "Name": "Nhà thuốc Vy Lan 8",
   "address": "671 Quang Trung, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.837513,
   "Latitude": 106.658555
 },
 {
   "STT": 307,
   "Name": "Nhà thuốc Hải Bình",
   "address": "018A Hồ Văn Huê, Phường  9, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.802186,
   "Latitude": 106.675886
 },
 {
   "STT": 308,
   "Name": "Nhà thuốc Hồng Phúc",
   "address": "90 Trần Hữu Trang, Phường  10, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792959,
   "Latitude": 106.671944
 },
 {
   "STT": 309,
   "Name": "Nhà thuốc Mai Lan",
   "address": "7 Nguyễn Trọng Tuyển, Phường  15, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7970729,
   "Latitude": 106.680808
 },
 {
   "STT": 310,
   "Name": "Nhà thuốc Minh An",
   "address": "C4 Hoa Cúc, Phường  7, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7986798,
   "Latitude": 106.6888661
 },
 {
   "STT": 311,
   "Name": "Nhà thuốc Ngọc Quỳnh",
   "address": "89 Trần Khắc Chân, Phường  9, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8001505,
   "Latitude": 106.6776007
 },
 {
   "STT": 312,
   "Name": "Nhà thuốc PK Thanh Bình",
   "address": "629 Nguyễn Kiệm, Phường  9, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.807242,
   "Latitude": 106.678316
 },
 {
   "STT": 313,
   "Name": "Nhà thuốc Thái Châu",
   "address": "491/43C Huỳnh Văn Bánh, Phường  13, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790689,
   "Latitude": 106.6692039
 },
 {
   "STT": 314,
   "Name": "Nhà thuốc Thanh Tâm",
   "address": "188 Huỳnh Văn Bánh, Phường  12, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792102,
   "Latitude": 106.677337
 },
 {
   "STT": 315,
   "Name": "Nhà thuốc Trang Phượng",
   "address": "50 Nhiêu Tứ, Phường  7, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8001791,
   "Latitude": 106.6890317
 },
 {
   "STT": 316,
   "Name": "Nhà thuốc Trường Thọ",
   "address": "74/1 Phan Tây Hồ, Phường  7, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.80125,
   "Latitude": 106.686554
 },
 {
   "STT": 317,
   "Name": "Nhà thuốc An Thái",
   "address": "30 Hoàng Hoa Thám, Phường  12, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.797855,
   "Latitude": 106.647284
 },
 {
   "STT": 318,
   "Name": "Nhà thuốc Anh Khương",
   "address": "416 Phạm Văn Hai, Phường  5, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.789661,
   "Latitude": 106.66074
 },
 {
   "STT": 319,
   "Name": "Nhà thuốc Bảo Liên",
   "address": "766/92 CMT8, Phường  5, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7883321,
   "Latitude": 106.6622783
 },
 {
   "STT": 320,
   "Name": "Nhà thuốc Cúc",
   "address": "104 Bạch Đằng, Phường  1, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.815424,
   "Latitude": 106.674624
 },
 {
   "STT": 321,
   "Name": "Nhà thuốc Đống Đa",
   "address": "9 Phú Hòa, Phường  8, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785777,
   "Latitude": 106.6550682
 },
 {
   "STT": 322,
   "Name": "Nhà thuốc Đức Nguyên",
   "address": "301 Lê Văn Sỹ, Phường  1, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.794808,
   "Latitude": 106.667201
 },
 {
   "STT": 323,
   "Name": "Nhà thuốc Đức Tài",
   "address": "106 Bình Giã, Phường  13, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8015651,
   "Latitude": 106.6450787
 },
 {
   "STT": 324,
   "Name": "Nhà thuốc Duy Lý",
   "address": "67A Phạm Văn Hai, Phường  3, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.795026,
   "Latitude": 106.663747
 },
 {
   "STT": 325,
   "Name": "Nhà thuốc Hoa Mai",
   "address": "79 Thành Mỹ, Phường  8, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7817211,
   "Latitude": 106.6533548
 },
 {
   "STT": 326,
   "Name": "Nhà thuốc Hồng Hà 2",
   "address": "373/89 Lý Thường Kiệt, Phường  9, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776549,
   "Latitude": 106.653282
 },
 {
   "STT": 327,
   "Name": "Nhà thuốc Hồng Lam",
   "address": "23 Nguyễn Sỹ Sách, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8190762,
   "Latitude": 106.6341655
 },
 {
   "STT": 328,
   "Name": "Nhà thuốc Hồng Nhật",
   "address": "155 Bạch Đằng, Phường  2, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.815668,
   "Latitude": 106.6745849
 },
 {
   "STT": 329,
   "Name": "Nhà thuốc Khánh Hoàng",
   "address": "27 Tân tiến, Phường  8, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7844843,
   "Latitude": 106.6522278
 },
 {
   "STT": 330,
   "Name": "Nhà thuốc Khôi Minh",
   "address": "50A Phan Huy Ích, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8266745,
   "Latitude": 106.6313136
 },
 {
   "STT": 331,
   "Name": "Nhà thuốc Kim Dung",
   "address": "62 Nhất Chi Mai, Phường  13, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8050139,
   "Latitude": 106.6396409
 },
 {
   "STT": 332,
   "Name": "Nhà thuốc Linh Chi",
   "address": "18 Hồng Lạc, Phường  11, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.787015,
   "Latitude": 106.648875
 },
 {
   "STT": 333,
   "Name": "Nhà thuốc Minh Chi",
   "address": "373/220 Lý Thường Kiệt, Phường  8, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7851514,
   "Latitude": 106.6539499
 },
 {
   "STT": 334,
   "Name": "Nhà thuốc Minh Đức",
   "address": "140 Ni Sư Huỳnh Liên, Phường  10, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7839001,
   "Latitude": 106.6466983
 },
 {
   "STT": 335,
   "Name": "Nhà thuốc Minh Huy",
   "address": "136 Phạm Văn Bạch, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8216252,
   "Latitude": 106.638913
 },
 {
   "STT": 336,
   "Name": "Nhà thuốc Minh Phúc",
   "address": "157 Trần Văn Quang, Phường  10, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.780555,
   "Latitude": 106.647504
 },
 {
   "STT": 337,
   "Name": "Nhà thuốc Minh Tuấn",
   "address": "64B Nhất Chi Mai, Phường  13, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8053139,
   "Latitude": 106.639689
 },
 {
   "STT": 338,
   "Name": "Nhà thuốc Minh Yến",
   "address": "309 Nguyễn Thái Bình, Phường  12, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7965465,
   "Latitude": 106.6534845
 },
 {
   "STT": 339,
   "Name": "Nhà thuốc Mỹ Dung",
   "address": "03 Ngô Thị Thu Minh, Phường  2, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7952118,
   "Latitude": 106.6633564
 },
 {
   "STT": 340,
   "Name": "Nhà thuốc Nam Hải",
   "address": "21 Bình Giã, Phường  13, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.799972,
   "Latitude": 106.644271
 },
 {
   "STT": 341,
   "Name": "Nhà thuốc Ngô Gia",
   "address": "129 Nguyễn Thái Bình, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.799019,
   "Latitude": 106.6560609
 },
 {
   "STT": 342,
   "Name": "Nhà thuốc Ngọc Phú",
   "address": "89 Phạm Văn Hai, Phường  3, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.794459,
   "Latitude": 106.663394
 },
 {
   "STT": 343,
   "Name": "Nhà thuốc Nhà thuốc số 1",
   "address": "4/1 Hoàng Việt, Phường  4, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.796931,
   "Latitude": 106.659004
 },
 {
   "STT": 344,
   "Name": "Nhà thuốc Nhật Tân",
   "address": "226 Nghĩa Phát, Phường  7, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7847394,
   "Latitude": 106.656931
 },
 {
   "STT": 345,
   "Name": "Nhà thuốc Phúc An Khang 2",
   "address": "515 Lạc Long Quân, Phường  10, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.775147,
   "Latitude": 106.6480629
 },
 {
   "STT": 346,
   "Name": "Nhà thuốc Phước Thành",
   "address": "176 Nghĩa Phát, Phường  7, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785624,
   "Latitude": 106.657695
 },
 {
   "STT": 347,
   "Name": "Nhà thuốc Phương Yến",
   "address": "157 Nguyễn Thái Bình, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.798736,
   "Latitude": 106.6555829
 },
 {
   "STT": 348,
   "Name": "Nhà thuốc Quang Anh",
   "address": "016-017 CC 2 Bàu Cát, Đồng Đen, Phường  14, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7935294,
   "Latitude": 106.6403628
 },
 {
   "STT": 349,
   "Name": "Nhà thuốc Quang Minh 5",
   "address": "68 Nguyễn Sỹ Sách, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8199036,
   "Latitude": 106.6359249
 },
 {
   "STT": 350,
   "Name": "Nhà thuốc Số 41",
   "address": "1 Ni Sư Huỳnh Liên, Phường  10, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7851522,
   "Latitude": 106.6456697
 },
 {
   "STT": 351,
   "Name": "Nhà thuốc Số 42",
   "address": "355 Hoàng Văn Thụ, Phường  2, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.799993,
   "Latitude": 106.661126
 },
 {
   "STT": 352,
   "Name": "Nhà thuốc Tâm An",
   "address": "892 Lạc Long Quân, Phường  8, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.783308,
   "Latitude": 106.6506733
 },
 {
   "STT": 353,
   "Name": "Nhà thuốc Tân Bình 6",
   "address": "492 Lý Thường Kiệt, Phường  7, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.786965,
   "Latitude": 106.653758
 },
 {
   "STT": 354,
   "Name": "Nhà thuốc Thanh Tâm",
   "address": "355C Nguyễn Trọng Tuyển, Phường  1, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.798358,
   "Latitude": 106.666386
 },
 {
   "STT": 355,
   "Name": "Nhà thuốc Thanh Tùng",
   "address": "118/125/2K Phan Huy Ích, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.824391,
   "Latitude": 106.631257
 },
 {
   "STT": 356,
   "Name": "Nhà thuốc Thùy Trâm",
   "address": "81 Hoàng Hoa Thám, Phường  13, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8016232,
   "Latitude": 106.6474451
 },
 {
   "STT": 357,
   "Name": "Nhà thuốc Thùy Trâm 1",
   "address": "102 Phạm Văn Hai, Phường  2, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.796707,
   "Latitude": 106.662943
 },
 {
   "STT": 358,
   "Name": "Nhà thuốc Toàn Tâm",
   "address": "19/5 Nguyễn Phúc Chu, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.821457,
   "Latitude": 106.63114
 },
 {
   "STT": 359,
   "Name": "Nhà thuốc Trúc Mai",
   "address": "233 Phạm Văn Hai, Phường  5, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790085,
   "Latitude": 106.661433
 },
 {
   "STT": 360,
   "Name": "Nhà thuốc Trường Huy",
   "address": "49 Nguyễn Chánh Sắt, Phường  13, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8057923,
   "Latitude": 106.6453469
 },
 {
   "STT": 361,
   "Name": "Nhà thuốc Tuyết Minh",
   "address": "54 Đông Hồ, Phường  8, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7835929,
   "Latitude": 106.6529443
 },
 {
   "STT": 362,
   "Name": "Nhà thuốc Vũ",
   "address": "15 CC Bàu Cát 1, Phường  14, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7934392,
   "Latitude": 106.6432611
 },
 {
   "STT": 363,
   "Name": "Nhà thuốc Vui khỏe",
   "address": "173 Phan Huy Ích, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8319782,
   "Latitude": 106.633408
 },
 {
   "STT": 364,
   "Name": "Nhà thuốc CTTNHH Nam Trân Hân",
   "address": "3A35 ấp 2, xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939388,
   "Latitude": 106.5787705
 },
 {
   "STT": 365,
   "Name": "Nhà thuốc Hiệu thuốc Tâm Phúc số 11 - Công ty CPDP Phong Phú",
   "address": "E1/6B Quốc lộ 50, ấp 5, xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7405078,
   "Latitude": 106.6562283
 },
 {
   "STT": 366,
   "Name": "Nhà thuốc Mỹ Châu 5",
   "address": "122-124 Trường Chinh, Phường  12, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.794522,
   "Latitude": 106.650848
 },
 {
   "STT": 367,
   "Name": "Nhà thuốc HT Tâm Phúc 5",
   "address": "373/87 Lý Thường Kiệt, Phường  9, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.778794,
   "Latitude": 106.655454
 },
 {
   "STT": 368,
   "Name": "Nhà thuốc Tâm Phúc 4",
   "address": "80/9B Phan Huy Ích, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.837785,
   "Latitude": 106.636635
 },
 {
   "STT": 369,
   "Name": "Nhà thuốc Hiệu thuốc 89",
   "address": "69 Nguyền Biểu, P1  , Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7542406,
   "Latitude": 106.6841475
 },
 {
   "STT": 370,
   "Name": "Nhà thuốc Phano 20",
   "address": "142 Trần Quốc Thảo, Phường  7, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7851557,
   "Latitude": 106.6829146
 },
 {
   "STT": 371,
   "Name": "Nhà thuốc Hiệu Thuốc Số 6",
   "address": "65 Thuận Kiều P12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.757286,
   "Latitude": 106.6580693
 },
 {
   "STT": 372,
   "Name": "Nhà thuốc HT Số 1",
   "address": "44 Nguyễn Trãi, P2, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758444,
   "Latitude": 106.6808457
 },
 {
   "STT": 373,
   "Name": "Nhà thuốc HT Số 15",
   "address": "157B Nguyễn Trãi, P2, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7572412,
   "Latitude": 106.6786853
 },
 {
   "STT": 374,
   "Name": "Nhà thuốc HT Số 19",
   "address": "373 An Dương Vương, P3, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758985,
   "Latitude": 106.6783421
 },
 {
   "STT": 375,
   "Name": "Nhà thuốc HT Số 39",
   "address": "170 Le Hồng Phong, P3, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7586438,
   "Latitude": 106.6779188
 },
 {
   "STT": 376,
   "Name": "Nhà thuốc HT Số 35",
   "address": "215 Le Hồng Phong, P4, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7594483,
   "Latitude": 106.6772753
 },
 {
   "STT": 377,
   "Name": "Nhà thuốc HT Số 40",
   "address": "314 An Dương Vương, P4, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7599921,
   "Latitude": 106.6797509
 },
 {
   "STT": 378,
   "Name": "Nhà thuốc HT Số 4",
   "address": "55 Bui Hữu nghĩa, P5, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7550508,
   "Latitude": 106.6733602
 },
 {
   "STT": 379,
   "Name": "Nhà thuốc HT Số 26",
   "address": "220 Bến Hàm Tử, P5, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7503768,
   "Latitude": 106.6759122
 },
 {
   "STT": 380,
   "Name": "Nhà thuốc HT Số 11",
   "address": "179 An Bình, P7, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7536061,
   "Latitude": 106.6716202
 },
 {
   "STT": 381,
   "Name": "Nhà thuốc HT Số 38",
   "address": "168 Nguyễn Tri Phương, P9, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7595412,
   "Latitude": 106.6690215
 },
 {
   "STT": 382,
   "Name": "Nhà thuốc HT Số 34",
   "address": "243 Trần Hưng Đạo, 10, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7519984,
   "Latitude": 106.6636501
 },
 {
   "STT": 383,
   "Name": "Nhà thuốc HT Số 29",
   "address": "22A Phan Văn Khỏe, P13, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7490517,
   "Latitude": 106.6550966
 },
 {
   "STT": 384,
   "Name": "Nhà thuốc HT Số 49",
   "address": "5A Phan Văn Khỏe, P13, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749088,
   "Latitude": 106.6555395
 },
 {
   "STT": 385,
   "Name": "Nhà thuốc HT Số 7",
   "address": "69 Trạng Tử, P14, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7517876,
   "Latitude": 106.6529262
 },
 {
   "STT": 386,
   "Name": "Nhà thuốc HT Số 24",
   "address": "893 Nguyễn Trãi, P14, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.75273,
   "Latitude": 106.6560176
 },
 {
   "STT": 387,
   "Name": "Nhà thuốc HT Số 33",
   "address": "316 Hồng Bàng, P15, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7536212,
   "Latitude": 106.6519582
 },
 {
   "STT": 388,
   "Name": "Nhà thuốc HT Số 47",
   "address": "262 Hung Vương, P15, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7623168,
   "Latitude": 106.6766796
 },
 {
   "STT": 389,
   "Name": "Nhà thuốc HT 11",
   "address": "142A Phạm Hữu Lầu, Phường   Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7057053,
   "Latitude": 106.7438843
 },
 {
   "STT": 390,
   "Name": "Nhà thuốc Số 1 ",
   "address": "1 Lê Thạch, P12, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.764271,
   "Latitude": 106.705685
 },
 {
   "STT": 391,
   "Name": "Nhà thuốc Số 2 ",
   "address": "698 Đoàn Văn Bơ, P16, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566941,
   "Latitude": 106.7148871
 },
 {
   "STT": 392,
   "Name": "Nhà thuốc Số 6 ",
   "address": "189 Đoàn Văn Bơ, p13, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7625375,
   "Latitude": 106.7051303
 },
 {
   "STT": 393,
   "Name": "Nhà thuốc Số 7 ",
   "address": "249 Đoàn Văn Bơ, P12, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.761937,
   "Latitude": 106.706163
 },
 {
   "STT": 394,
   "Name": "Nhà thuốc Số 8 ",
   "address": "34 lô K CXVH, p6, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7613787,
   "Latitude": 106.7001302
 },
 {
   "STT": 395,
   "Name": "Nhà thuốc Số 11 ",
   "address": "183E6 Tôn Thất Thuyết. P4, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.753369,
   "Latitude": 106.705496
 },
 {
   "STT": 396,
   "Name": "Nhà thuốc Số 14 ",
   "address": "52 Lô N cư xá Vĩnh Hội,p6, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7613787,
   "Latitude": 106.7001302
 },
 {
   "STT": 397,
   "Name": "Nhà thuốc Số 18 ",
   "address": "104 Hoàng Diệu, p12, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.763988,
   "Latitude": 106.7043012
 },
 {
   "STT": 398,
   "Name": "Nhà thuốc Số 20 ",
   "address": "290 Đoàn Văn Bơ, P10, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.761702,
   "Latitude": 106.70629
 },
 {
   "STT": 399,
   "Name": "Nhà thuốc Số 25 ",
   "address": "B163C Xóm Chiếu, p16, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.756141,
   "Latitude": 106.709682
 },
 {
   "STT": 400,
   "Name": "Nhà thuốc Số 28 ",
   "address": "198-200 Nguyễn Tất Thành, p13, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7624288,
   "Latitude": 106.708345
 },
 {
   "STT": 401,
   "Name": "Nhà thuốc Số 32 ",
   "address": "451 Đoàn Văn Bơ, p13, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7601315,
   "Latitude": 106.709226
 },
 {
   "STT": 402,
   "Name": "Nhà thuốc Số 33 ",
   "address": "58 Tôn Đản, p10, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7610119,
   "Latitude": 106.7073939
 },
 {
   "STT": 403,
   "Name": "Nhà thuốc Số 34 ",
   "address": "C126 Xóm Chiếu, p14, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.757883,
   "Latitude": 106.710465
 },
 {
   "STT": 404,
   "Name": "Nhà thuốc Số 36 ",
   "address": "B83 Xóm Chiếu, p16, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758713,
   "Latitude": 106.71119
 },
 {
   "STT": 405,
   "Name": "Nhà thuốc Số 39 ",
   "address": "110 Đoàn Văn Bơ, P9, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.760094,
   "Latitude": 106.708982
 },
 {
   "STT": 406,
   "Name": "Nhà thuốc Số 47 ",
   "address": "A105 Nguyễn Thần Hiến, p18, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7538504,
   "Latitude": 106.7159708
 },
 {
   "STT": 407,
   "Name": "Nhà thuốc Số 48",
   "address": "B367 Đoàn Văn Bơ, p18, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.756894,
   "Latitude": 106.717374
 },
 {
   "STT": 408,
   "Name": "Nhà thuốc Số 51 ",
   "address": "56 Lô O cư xá Vĩnh Hội, p9, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7616455,
   "Latitude": 106.7004408
 },
 {
   "STT": 409,
   "Name": "Nhà thuốc số 57 ",
   "address": "259Bis Tôn Thất Thuyết, p3, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7530278,
   "Latitude": 106.6983066
 },
 {
   "STT": 410,
   "Name": "Nhà thuốc Số 59",
   "address": "173 Xóm Chiếu, P16, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7558039,
   "Latitude": 106.7095279
 },
 {
   "STT": 411,
   "Name": "Nhà thuốc Số 63 ",
   "address": "311 Lô L khu tái thiết HD, p8, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7913296,
   "Latitude": 106.6515306
 },
 {
   "STT": 412,
   "Name": "Nhà thuốc PHA NO 16",
   "address": "313 Hoàng Diệu, p6, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.761882,
   "Latitude": 106.703493
 },
 {
   "STT": 413,
   "Name": "Nhà thuốc Số 67 ",
   "address": "47-49A Nguyễn Thần Hiến, P18, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7559565,
   "Latitude": 106.7173023
 },
 {
   "STT": 414,
   "Name": "Hiệu thuốc 20",
   "address": "129 Âu Cơ, Phường  14, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771507,
   "Latitude": 106.650076
 },
 {
   "STT": 415,
   "Name": "CTTNHH BV CTCH Sài Gòn",
   "address": "232 Lê Văn Sỹ, Phường  1, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7949141,
   "Latitude": 106.6672791
 },
 {
   "STT": 416,
   "Name": "Nhà thuốc 152",
   "address": "152-154 Hồng Bàng, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7548411,
   "Latitude": 106.6595793
 },
 {
   "STT": 417,
   "Name": "Nhà thuốc Bạch Liên",
   "address": "39 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566995,
   "Latitude": 106.6581306
 },
 {
   "STT": 418,
   "Name": "Nhà thuốc Bảo Khang",
   "address": "010 Lô F Đặng Thái Thân,  Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7552555,
   "Latitude": 106.6653659
 },
 {
   "STT": 419,
   "Name": "Nhà thuốc Bảo Vy",
   "address": "90/34 Lê Hồng Phong, Phường  2, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7619119,
   "Latitude": 106.6763338
 },
 {
   "STT": 420,
   "Name": "Nhà thuốc Hạnh",
   "address": "41 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.756733,
   "Latitude": 106.6581596
 },
 {
   "STT": 421,
   "Name": "Nhà thuốc Hoàng Thông",
   "address": "181 Trần Tuấn Khải, Phường  5, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7527862,
   "Latitude": 106.6734697
 },
 {
   "STT": 422,
   "Name": "Nhà thuốc Hồng Lạc",
   "address": "699 Trần Hưng Đạo, Phường  1, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7557353,
   "Latitude": 106.6835238
 },
 {
   "STT": 423,
   "Name": "Nhà thuốc Huy Giang",
   "address": "014 Lô D Mạc Thiên Tích, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7540578,
   "Latitude": 106.6648965
 },
 {
   "STT": 424,
   "Name": "Nhà thuốc Kim Tiền Thảo",
   "address": "06 Huỳnh Mẫn Đạt, Phường  1, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7528279,
   "Latitude": 106.6768382
 },
 {
   "STT": 425,
   "Name": "Nhà thuốc Minh  Huyền",
   "address": "27 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7562063,
   "Latitude": 106.6581989
 },
 {
   "STT": 426,
   "Name": "Nhà thuốc Minh  Tâm",
   "address": "37D Thuận kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566629,
   "Latitude": 106.6581058
 },
 {
   "STT": 427,
   "Name": "Nhà thuốc Nam Sang ",
   "address": "018 Lô E, C/c Hùng Vương, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7606918,
   "Latitude": 106.6741153
 },
 {
   "STT": 428,
   "Name": "Nhà thuốc Ngân Hà 9",
   "address": "024 Lô C Mạc Thiên Tích, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7549748,
   "Latitude": 106.6669894
 },
 {
   "STT": 429,
   "Name": "Nhà thuốc Ngọc Diệp",
   "address": "37C Thuận kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566261,
   "Latitude": 106.6581085
 },
 {
   "STT": 430,
   "Name": "Nhà thuốc Ngọc Mai",
   "address": "931-935 Trần Hưng Đạo, Phường  1, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7540757,
   "Latitude": 106.677949
 },
 {
   "STT": 431,
   "Name": "Nhà thuốc Nguyễn Chí Thanh",
   "address": "203 Nguyễn Chí Thanh, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.757603,
   "Latitude": 106.6579953
 },
 {
   "STT": 432,
   "Name": "Nhà thuốc Như Yến",
   "address": "002 Lô D Mạc Thiên Tích, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7548258,
   "Latitude": 106.6644546
 },
 {
   "STT": 433,
   "Name": "Nhà thuốc Phúc An Khang",
   "address": "904-906 Trần Hưng Đạo, Phường  7, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7539112,
   "Latitude": 106.6738337
 },
 {
   "STT": 434,
   "Name": "Nhà thuốc Quang Minh",
   "address": "032 Lô C Mạc Thiên Tích, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7547759,
   "Latitude": 106.6639568
 },
 {
   "STT": 435,
   "Name": "Nhà thuốc Số 10",
   "address": "943 Trần Hưng Đạo, Phường  1, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7542986,
   "Latitude": 106.6774516
 },
 {
   "STT": 436,
   "Name": "Nhà thuốc Tấn Thuyên",
   "address": "21 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7561126,
   "Latitude": 106.6581873
 },
 {
   "STT": 437,
   "Name": "Nhà thuốc Tân Việt",
   "address": "23 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7561421,
   "Latitude": 106.6581818
 },
 {
   "STT": 438,
   "Name": "Nhà thuốc Thái Bình",
   "address": "37 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7564856,
   "Latitude": 106.6581188
 },
 {
   "STT": 439,
   "Name": "Nhà thuốc Thanh Mai",
   "address": "006D C/c Hùng Vương, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7532842,
   "Latitude": 106.6603784
 },
 {
   "STT": 440,
   "Name": "Nhà thuốc Thu Hà",
   "address": "020 Lô C, C/c Hùng vương, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7537961,
   "Latitude": 106.6622899
 },
 {
   "STT": 441,
   "Name": "Nhà thuốc Tiến Hải",
   "address": "203B Nguyễn Chí Thanh, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.757603,
   "Latitude": 106.6579953
 },
 {
   "STT": 442,
   "Name": "Nhà thuốc Túy Phương",
   "address": "08 Lô C Mạc Thiên Tích, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7553005,
   "Latitude": 106.6680107
 },
 {
   "STT": 443,
   "Name": "Nhà thuốc Vũ Tùng",
   "address": "196 Lê Hồng Phong, Phường  4, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7594185,
   "Latitude": 106.6776875
 },
 {
   "STT": 444,
   "Name": "Nhà thuốc Xuân Chi ",
   "address": "010 Lô D Mạc Thiên Tích, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.76535,
   "Latitude": 106.6711
 },
 {
   "STT": 445,
   "Name": "Nhà thuốc Diệu Thanh",
   "address": "390B Phan Văn Khỏe, Phường  5, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747099,
   "Latitude": 106.642507
 },
 {
   "STT": 446,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "139B Bình Tiên, Phường  8, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.742976,
   "Latitude": 106.643127
 },
 {
   "STT": 447,
   "Name": "Nhà thuốc Khánh Sơn",
   "address": "687 Phạm Văn Chí, Phường  7, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7381168,
   "Latitude": 106.6347216
 },
 {
   "STT": 448,
   "Name": "Nhà thuốc Kim Ngọc",
   "address": "44 Hậu Giang, Phường  2, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749784,
   "Latitude": 106.647728
 },
 {
   "STT": 449,
   "Name": "Nhà thuốc Kim Oanh",
   "address": "306 Bãi Sậy, Phường  5, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.746381,
   "Latitude": 106.640454
 },
 {
   "STT": 450,
   "Name": "Nhà thuốc Kim Phương",
   "address": "9 Mai Xuân Thưởng, Phường  3, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.743795,
   "Latitude": 106.6490543
 },
 {
   "STT": 451,
   "Name": "Nhà thuốc Liên Hương",
   "address": "201 Hậu Giang, Phường  5, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749417,
   "Latitude": 106.644817
 },
 {
   "STT": 452,
   "Name": "Nhà thuốc Minh Dung ",
   "address": "191 Văn Thân, Phường  8, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.740496,
   "Latitude": 106.636313
 },
 {
   "STT": 453,
   "Name": "Nhà thuốc Minh Huệ",
   "address": "8G Cư xá Phú Lâm D, Phường  10, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.738574,
   "Latitude": 106.631575
 },
 {
   "STT": 454,
   "Name": "Nhà thuốc Phú Tâm Hai",
   "address": "276A Bà Hom, Phường  13, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.756084,
   "Latitude": 106.624834
 },
 {
   "STT": 455,
   "Name": "Nhà thuốc Phúc Lộc",
   "address": "143 Lý Chiêu Hoàng, Phường  10, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7360972,
   "Latitude": 106.6334805
 },
 {
   "STT": 456,
   "Name": "Nhà thuốc Phúc Thiện",
   "address": "36 Gia Phú, Phường  3, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747837,
   "Latitude": 106.6549415
 },
 {
   "STT": 457,
   "Name": "Nhà thuốc Số 2",
   "address": "108 An Dương Vương, Phường  11, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.743162,
   "Latitude": 106.623837
 },
 {
   "STT": 458,
   "Name": "Nhà thuốc Tấn Huy",
   "address": "236 Minh Phụng, Phường  6, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.75333,
   "Latitude": 106.643164
 },
 {
   "STT": 459,
   "Name": "Nhà thuốc Thiên Hưng",
   "address": "629 Hậu Giang, Phường  11, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747603,
   "Latitude": 106.633912
 },
 {
   "STT": 460,
   "Name": "Nhà thuốc Thiện Mỹ",
   "address": "245A Nguyễn Văn Luông, Phường  12, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7507904,
   "Latitude": 106.6352944
 },
 {
   "STT": 461,
   "Name": "Nhà thuốc Thiện Thư",
   "address": "Số 4 Lô D Phạm Phú Thứ, Phường  4, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7450101,
   "Latitude": 106.643531
 },
 {
   "STT": 462,
   "Name": "Nhà thuốc Thu Thủy",
   "address": "68G Cư xá Phú Lâm D, Phường  10, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7399173,
   "Latitude": 106.6321354
 },
 {
   "STT": 463,
   "Name": "Nhà thuốc 25 (Giang)",
   "address": "A1/18 Hương lộ 80 Ấp 1, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8113963,
   "Latitude": 106.5757703
 },
 {
   "STT": 464,
   "Name": "Nhà thuốc Ái Như",
   "address": "D7/23 Trịnh Như Khuê, Ấp 4, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6649546,
   "Latitude": 106.5713498
 },
 {
   "STT": 465,
   "Name": "Nhà thuốc An Bình ",
   "address": "F4/12 Ấp 6, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.688829,
   "Latitude": 106.6240138
 },
 {
   "STT": 466,
   "Name": "Nhà thuốc Bạch Tuyết",
   "address": "3A76/1 Tỉnh Lộ 10 Ấp 3, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7761435,
   "Latitude": 106.542813
 },
 {
   "STT": 467,
   "Name": "Nhà thuốc Bảo Ngọc",
   "address": "D7/24 Ấp 4, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6630417,
   "Latitude": 106.5672462
 },
 {
   "STT": 468,
   "Name": "Nhà thuốc Bình Hưng",
   "address": "A30/02 Ấp 1, Quốc lộ 50, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6917747,
   "Latitude": 106.6548981
 },
 {
   "STT": 469,
   "Name": "Nhà thuốc Cây Cám",
   "address": "B14B/5 Ấp 2, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939585,
   "Latitude": 106.5786322
 },
 {
   "STT": 470,
   "Name": "Nhà thuốc Châu Sương",
   "address": "D11/3 Ấp 4 Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8521,
   "Latitude": 106.57357
 },
 {
   "STT": 471,
   "Name": "Nhà thuốc Chợ Đệm",
   "address": "A8/8 Nguyễn Hữu Trí, KPhường  1, TT Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6963374,
   "Latitude": 106.5933314
 },
 {
   "STT": 472,
   "Name": "Nhà thuốc Đồng Cát",
   "address": "C5/10 Hưng Nhơn, Xã Tân Kiên , Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7144676,
   "Latitude": 106.5904645
 },
 {
   "STT": 473,
   "Name": "Nhà thuốc Đức Phúc",
   "address": "1A93 Ấp 1, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7732635,
   "Latitude": 106.5519705
 },
 {
   "STT": 474,
   "Name": "Nhà thuốc Gia Hân",
   "address": "A13/1 Nguyễn Hữu Trí, KPhường  1, TT Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.692399,
   "Latitude": 106.57819
 },
 {
   "STT": 475,
   "Name": "Nhà thuốc Hoàn Hảo",
   "address": "G2/29 Ấp 7, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.847681,
   "Latitude": 106.5865704
 },
 {
   "STT": 476,
   "Name": "Nhà thuốc Hoàng Anh",
   "address": "B5/6C Ấp 2 Trần Đại Nghĩa, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7457818,
   "Latitude": 106.5410982
 },
 {
   "STT": 477,
   "Name": "Nhà thuốc Hoàng Phương",
   "address": "F7/21 Ấp 6 ,Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8176868,
   "Latitude": 106.5562322
 },
 {
   "STT": 478,
   "Name": "Nhà thuốc Hồng Luyến",
   "address": "C1/33, Ấp 3, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939733,
   "Latitude": 106.5792914
 },
 {
   "STT": 479,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "D7/221 Quốc lộ 50 Ấp 4, Xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7361399,
   "Latitude": 106.6562158
 },
 {
   "STT": 480,
   "Name": "Nhà thuốc Hưng Thịnh",
   "address": "A3/7 Ấp 1, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7322911,
   "Latitude": 106.5813356
 },
 {
   "STT": 481,
   "Name": "Nhà thuốc Hưng Thịnh",
   "address": "D16/38, Ấp 4, Xã Tân Quý Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.666887,
   "Latitude": 106.5967126
 },
 {
   "STT": 482,
   "Name": "Nhà thuốc Hương Trầm",
   "address": "D7/9 Ấp 4, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8124813,
   "Latitude": 106.5806832
 },
 {
   "STT": 483,
   "Name": "Nhà thuốc Hữu Đức",
   "address": "B5/146 Ấp 2, Xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7306442,
   "Latitude": 106.6509137
 },
 {
   "STT": 484,
   "Name": "Nhà thuốc Khánh Ngọc",
   "address": "C2/20R1 Ấp 4, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.664104,
   "Latitude": 106.694259
 },
 {
   "STT": 485,
   "Name": "Nhà thuốc Kim Ngân",
   "address": "A26/5 Quốc lộ 50, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6917747,
   "Latitude": 106.6548981
 },
 {
   "STT": 486,
   "Name": "Nhà thuốc Minh Đức",
   "address": "A13/7B KP1, Thị trấn Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6844901,
   "Latitude": 106.5731392
 },
 {
   "STT": 487,
   "Name": "Nhà thuốc Minh Đức",
   "address": "D7/63 Trịnh Như Khuê ấp 4, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6663753,
   "Latitude": 106.5730101
 },
 {
   "STT": 488,
   "Name": "Nhà thuốc Minh Kha",
   "address": "E9/6A, KPhường  5, Thị trấn Tân Túc, , Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6844901,
   "Latitude": 106.5731392
 },
 {
   "STT": 489,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "A3/2A Nguyễn Cửu Phú, Ấp 1, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7165741,
   "Latitude": 106.5813709
 },
 {
   "STT": 490,
   "Name": "Nhà thuốc Ngọc Anh",
   "address": "C3/32 Ấp 4, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.664215,
   "Latitude": 106.694298
 },
 {
   "STT": 491,
   "Name": "Nhà thuốc Ngọc Long",
   "address": "D17/40B ấp 4, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.658623,
   "Latitude": 106.691402
 },
 {
   "STT": 492,
   "Name": "Nhà thuốc Ngọc Mỹ 2",
   "address": "A6/10 Quốc lộ 50, Ấp 1, Xã Bình Hung, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7298,
   "Latitude": 106.6562
 },
 {
   "STT": 493,
   "Name": "Nhà thuốc Nhật Anh",
   "address": "B1/17 Ấp 2A, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7971688,
   "Latitude": 106.6417812
 },
 {
   "STT": 494,
   "Name": "Nhà thuốc Nhị Loan",
   "address": "C5/31 Ấp 3, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939733,
   "Latitude": 106.5792914
 },
 {
   "STT": 495,
   "Name": "Nhà thuốc Phát Lộc",
   "address": "C1/12 Ấp 3, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939733,
   "Latitude": 106.5792914
 },
 {
   "STT": 496,
   "Name": "Nhà thuốc Phong Phú",
   "address": "B5/122A Quốc lộ 50, Xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7047314,
   "Latitude": 106.6548773
 },
 {
   "STT": 497,
   "Name": "Nhà thuốc Phúc Lộc Thịnh",
   "address": "D6/8 Quốc lộ 1A, Ấp 4 Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6670322,
   "Latitude": 106.5725534
 },
 {
   "STT": 498,
   "Name": "Nhà thuốc Phương Trinh",
   "address": "A8/22 Ấp 1, Xã Quy Đức, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6476525,
   "Latitude": 106.6545975
 },
 {
   "STT": 499,
   "Name": "Nhà thuốc Song Phúc",
   "address": "2C Quốc lộ 1, Ấp 4, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.664292,
   "Latitude": 106.569372
 },
 {
   "STT": 500,
   "Name": "Nhà thuốc Thái Hòa",
   "address": "A3/4 Ấp 1, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7322911,
   "Latitude": 106.5813356
 },
 {
   "STT": 501,
   "Name": "Nhà thuốc Thanh Bình",
   "address": "A8/22A Ấp 1, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7562009,
   "Latitude": 106.5881675
 },
 {
   "STT": 502,
   "Name": "Nhà thuốc Thanh Ngọc",
   "address": "G16/35 Ấp 7, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7418662,
   "Latitude": 106.542358
 },
 {
   "STT": 503,
   "Name": "Nhà thuốc Thiên Long 5",
   "address": "G/15/30 Ấp 7, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7661714,
   "Latitude": 106.5230542
 },
 {
   "STT": 504,
   "Name": "Nhà thuốc Thu Thúy",
   "address": "C4/3 Ấp 3, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939733,
   "Latitude": 106.5792914
 },
 {
   "STT": 505,
   "Name": "Nhà thuốc Thu Vân",
   "address": "A9/25 KPhường  1, Thị trấn Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6844901,
   "Latitude": 106.5731392
 },
 {
   "STT": 506,
   "Name": "Nhà thuốc Tùng Lộc",
   "address": "G2/6B Ấp 7, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8479835,
   "Latitude": 106.5866452
 },
 {
   "STT": 507,
   "Name": "Nhà thuốc Anh Đào",
   "address": "421/5A Phan Xích Long, Phường  2, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.797393,
   "Latitude": 106.685518
 },
 {
   "STT": 508,
   "Name": "Nhà thuốc Bích Thủy",
   "address": "123 Đặng Văn Ngữ, Phường  14, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792157,
   "Latitude": 106.668001
 },
 {
   "STT": 509,
   "Name": "Nhà thuốc Đức Tâm",
   "address": "137 Hồ Văn Huê, Phường  9, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.805227,
   "Latitude": 106.67788
 },
 {
   "STT": 510,
   "Name": "Nhà thuốc Gia Khánh",
   "address": "24 Nguyễn Văn Đậu, Phường  4, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.804973,
   "Latitude": 106.687197
 },
 {
   "STT": 511,
   "Name": "Nhà thuốc Hoàng Diễm",
   "address": "32 Đặng Văn Ngữ, Phường  10, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.795416,
   "Latitude": 106.669675
 },
 {
   "STT": 512,
   "Name": "Nhà thuốc Hồng Ân",
   "address": "110A Đặng Văn Ngữ, Phường  14, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.793664,
   "Latitude": 106.668893
 },
 {
   "STT": 513,
   "Name": "Nhà thuốc Hưng",
   "address": "143 Lê Văn Sỹ, Phường  13, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.791704,
   "Latitude": 106.671419
 },
 {
   "STT": 514,
   "Name": "Nhà thuốc Khánh Châu",
   "address": "142 Cô Giang, Phường  2, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7968495,
   "Latitude": 106.6841295
 },
 {
   "STT": 515,
   "Name": "Nhà thuốc Kim Tố",
   "address": "62B Phan Xích Long, Phường  3, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.79981,
   "Latitude": 106.684474
 },
 {
   "STT": 516,
   "Name": "Nhà thuốc Lê Khanh",
   "address": "296 Phan Đình Phùng, Phường  1, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.79766,
   "Latitude": 106.68113
 },
 {
   "STT": 517,
   "Name": "Nhà thuốc Minh Phú",
   "address": "6 Nguyễn Đình Chiểu, Phường  3, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8033249,
   "Latitude": 106.679209
 },
 {
   "STT": 518,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "125A Trần Hữu Trang,  Phường  10., Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792913,
   "Latitude": 106.672237
 },
 {
   "STT": 519,
   "Name": "Nhà thuốc Mỹ Lan",
   "address": " 213 Thích Quảng Đức, Phường  4, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.807165,
   "Latitude": 106.67976
 },
 {
   "STT": 520,
   "Name": "Nhà thuốc Mỹ Thành",
   "address": "100D Thích Quảng Đức, Phường  5, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8062935,
   "Latitude": 106.68114
 },
 {
   "STT": 521,
   "Name": "Nhà thuốc Ngọc Dung",
   "address": "2 Lê Tự Tài, Phường  4, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8047232,
   "Latitude": 106.6790229
 },
 {
   "STT": 522,
   "Name": "Nhà thuốc Ngọc Nhung",
   "address": "63 Phan Xích Long, Phường  3, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.803735,
   "Latitude": 106.681094
 },
 {
   "STT": 523,
   "Name": "Nhà thuốc Ngọc Thiện",
   "address": "111 Cô Giang, Phường  1, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7970277,
   "Latitude": 106.6840291
 },
 {
   "STT": 524,
   "Name": "Nhà thuốc Phan Đình Phùng",
   "address": "64 Phan Đình Phùng, Phường  2, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.794115,
   "Latitude": 106.6843559
 },
 {
   "STT": 525,
   "Name": "Nhà thuốc Phú Quý",
   "address": "84/2 Trần Hữu Trang, Phường  10, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.79332,
   "Latitude": 106.672147
 },
 {
   "STT": 526,
   "Name": "Nhà thuốc Phú Thành",
   "address": "466 Huỳng Văn Bánh, Phường  14, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792254,
   "Latitude": 106.668779
 },
 {
   "STT": 527,
   "Name": "Nhà thuốc Quang Minh",
   "address": "111 Trần Hữu Trang,  Phường  10, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.793419,
   "Latitude": 106.67244
 },
 {
   "STT": 528,
   "Name": "Nhà thuốc Số 18",
   "address": "96A Hồ Văn Huê, Phường  9, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.801869,
   "Latitude": 106.6763239
 },
 {
   "STT": 529,
   "Name": "Nhà thuốc Số 4",
   "address": "262 Nguyễn Trọng Tuyển, Phường  8, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.797964,
   "Latitude": 106.672911
 },
 {
   "STT": 530,
   "Name": "Nhà thuốc Thắng Lợi",
   "address": "A2 004 CC Phan Xích Long, Phường  7, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7994135,
   "Latitude": 106.6866129
 },
 {
   "STT": 531,
   "Name": "Nhà thuốc Thanh Huyền",
   "address": "126B Trần Kế Xương, Phường  7, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.798906,
   "Latitude": 106.690323
 },
 {
   "STT": 532,
   "Name": "Nhà thuốc Thanh Trà",
   "address": "104 Cao Thắng, Phường  17, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792406,
   "Latitude": 106.681608
 },
 {
   "STT": 533,
   "Name": "Nhà thuốc Thanh Xuân",
   "address": "7 Trương Quốc Dung, Phường  8, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.798751,
   "Latitude": 106.672959
 },
 {
   "STT": 534,
   "Name": "Nhà thuốc Thu Hồng",
   "address": "554 Nguyễn Kiệm, Phường  4, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8045316,
   "Latitude": 106.6788071
 },
 {
   "STT": 535,
   "Name": "Nhà thuốc Trần Quang",
   "address": "31 Đỗ Tấn Phong, Phường  9, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.800978,
   "Latitude": 106.677858
 },
 {
   "STT": 536,
   "Name": "Nhà thuốc Trung Nguyên",
   "address": "93B Phan Xích Long, Phường  2, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7971813,
   "Latitude": 106.6906867
 },
 {
   "STT": 537,
   "Name": "Nhà thuốc Anh Đào",
   "address": "235 Lũy Bán Bích, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7701049,
   "Latitude": 106.6316
 },
 {
   "STT": 538,
   "Name": "Nhà thuốc Chung Sơn",
   "address": "281 Lũy Bán Bích, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771506,
   "Latitude": 106.631787
 },
 {
   "STT": 539,
   "Name": "Nhà thuốc Cửu Long",
   "address": "177C Lũy Bán Bích, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.768459,
   "Latitude": 106.631613
 },
 {
   "STT": 540,
   "Name": "Nhà thuốc Diệu Anh",
   "address": "75A Thạch Lam, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7767663,
   "Latitude": 106.6309672
 },
 {
   "STT": 541,
   "Name": "Nhà thuốc Hải Châu",
   "address": "323 Tân Kỳ Tân Quý, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8013911,
   "Latitude": 106.6245889
 },
 {
   "STT": 542,
   "Name": "Nhà thuốc Hoàn Hảo",
   "address": "116 Gò Dầu, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7959705,
   "Latitude": 106.6248552
 },
 {
   "STT": 543,
   "Name": "Nhà thuốc Hoàng Hà",
   "address": "98 Cầu Xéo, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.796697,
   "Latitude": 106.623658
 },
 {
   "STT": 544,
   "Name": "Nhà thuốc Hồng Phúc",
   "address": "208 Nguyễn Sơn, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.782746,
   "Latitude": 106.627348
 },
 {
   "STT": 545,
   "Name": "Nhà thuốc Hữu Thuận",
   "address": "237 Tân Sơn Nhì, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7965315,
   "Latitude": 106.6304631
 },
 {
   "STT": 546,
   "Name": "Nhà thuốc Khoa Nguyên",
   "address": "132 Gò Dầu, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7960122,
   "Latitude": 106.6240305
 },
 {
   "STT": 547,
   "Name": "Nhà thuốc Kim Duyên",
   "address": "72 Tân Sơn Nhì, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.799799,
   "Latitude": 106.632976
 },
 {
   "STT": 548,
   "Name": "Nhà thuốc Kim Tân",
   "address": "131 Độc Lập, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7923396,
   "Latitude": 106.6311912
 },
 {
   "STT": 549,
   "Name": "Nhà thuốc Lê Khanh",
   "address": "268 Độc Lập, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7930378,
   "Latitude": 106.6288111
 },
 {
   "STT": 550,
   "Name": "Nhà thuốc Mai Anh",
   "address": "110 Thống Nhất, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7913202,
   "Latitude": 106.6313726
 },
 {
   "STT": 551,
   "Name": "Nhà thuốc Mai Linh",
   "address": "213 Độc Lập, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792981,
   "Latitude": 106.6279202
 },
 {
   "STT": 552,
   "Name": "Nhà thuốc Minh Châu",
   "address": "525 Tân Kỳ Tân Quý, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7983201,
   "Latitude": 106.6165017
 },
 {
   "STT": 553,
   "Name": "Nhà thuốc Minh Quân",
   "address": "108 Vườn Lài, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.788214,
   "Latitude": 106.631113
 },
 {
   "STT": 554,
   "Name": "Nhà thuốc Minh Trang",
   "address": "43 Độc Lập, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7920076,
   "Latitude": 106.6337454
 },
 {
   "STT": 555,
   "Name": "Nhà thuốc Minh Tuấn",
   "address": "219 Tân Sơn Nhì, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7963203,
   "Latitude": 106.6303217
 },
 {
   "STT": 556,
   "Name": "Nhà thuốc Ngọc A Châu",
   "address": "211 Độc Lập, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7929608,
   "Latitude": 106.6279619
 },
 {
   "STT": 557,
   "Name": "Nhà thuốc Phúc Nguyên",
   "address": "1R Văn Cao, Phường  Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.782142,
   "Latitude": 106.620599
 },
 {
   "STT": 558,
   "Name": "Nhà thuốc Phương Uyên",
   "address": "468 Lũy Bán Bích, Phường  Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.779847,
   "Latitude": 106.635889
 },
 {
   "STT": 559,
   "Name": "Nhà thuốc Quốc Thịnh",
   "address": "242A Gò Dầu, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7959144,
   "Latitude": 106.6186047
 },
 {
   "STT": 560,
   "Name": "Nhà thuốc Quỳnh Mai",
   "address": "116 Gò Dầu, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7959705,
   "Latitude": 106.6248552
 },
 {
   "STT": 561,
   "Name": "Nhà thuốc số 43",
   "address": "05 Độc Lập, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7916617,
   "Latitude": 106.6369117
 },
 {
   "STT": 562,
   "Name": "Nhà thuốc Song Ân",
   "address": "190 Hòa Bình, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771043,
   "Latitude": 106.6301411
 },
 {
   "STT": 563,
   "Name": "Nhà thuốc Tâm Hiền",
   "address": "155C Lũy Bán Bích, Phường  Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7671936,
   "Latitude": 106.6316711
 },
 {
   "STT": 564,
   "Name": "Nhà thuốc Tấn Minh",
   "address": "153 Nguyễn Sơn, Phường  Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.782376,
   "Latitude": 106.62742
 },
 {
   "STT": 565,
   "Name": "Nhà thuốc Tân Phú",
   "address": "259 Nguyễn Sơn, Phường  Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.78316,
   "Latitude": 106.624972
 },
 {
   "STT": 566,
   "Name": "Nhà thuốc Tân Quý A",
   "address": "293B,C Tân Kỳ Tân Quý, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8021867,
   "Latitude": 106.6265684
 },
 {
   "STT": 567,
   "Name": "Nhà thuốc Thành Duyên",
   "address": "248 Hòa Bình, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7717067,
   "Latitude": 106.6280394
 },
 {
   "STT": 568,
   "Name": "Nhà thuốc Thanh Phước",
   "address": "129A Gò Dầu, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7957955,
   "Latitude": 106.6227819
 },
 {
   "STT": 569,
   "Name": "Nhà thuốc Thành Trí",
   "address": "344 Trịnh Đình Trọng, Phường  Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776267,
   "Latitude": 106.63608
 },
 {
   "STT": 570,
   "Name": "Nhà thuốc Thanh Trúc",
   "address": "175 Tân Kỳ Tân Quý, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.801258,
   "Latitude": 106.628499
 },
 {
   "STT": 571,
   "Name": "Nhà thuốc Thảo Phương",
   "address": "177 Gò Dầu, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7960329,
   "Latitude": 106.6166923
 },
 {
   "STT": 572,
   "Name": "Nhà thuốc Thiên Phượng",
   "address": "40 C/c Gò Dầu, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7959152,
   "Latitude": 106.6273818
 },
 {
   "STT": 573,
   "Name": "Nhà thuốc Thiện Tâm",
   "address": "162 Độc Lập, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792782,
   "Latitude": 106.630404
 },
 {
   "STT": 574,
   "Name": "Nhà thuốc Thúy Hằng",
   "address": "365B Nguyễn Sơn, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7846326,
   "Latitude": 106.6204963
 },
 {
   "STT": 575,
   "Name": "Nhà thuốc Trân Châu",
   "address": "246 Tân Kỳ Tân Quý, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8007716,
   "Latitude": 106.6227089
 },
 {
   "STT": 576,
   "Name": "Nhà thuốc Tuấn Thông",
   "address": "278 Hòa Bình, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7731083,
   "Latitude": 106.6265976
 },
 {
   "STT": 577,
   "Name": "Nhà thuốc Tuyết Nhung",
   "address": "321D Thoại Ngọc Hầu, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776689,
   "Latitude": 106.627559
 },
 {
   "STT": 578,
   "Name": "Nhà thuốc Việt Châu",
   "address": "525 Lũy Bán Bích, Phường  Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777023,
   "Latitude": 106.634089
 },
 {
   "STT": 579,
   "Name": "Nhà thuốc Vinh Hải",
   "address": "179 Nguyễn Sơn, Phường  Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.782576,
   "Latitude": 106.626818
 },
 {
   "STT": 580,
   "Name": "Nhà thuốc Vy Lan 2",
   "address": "42 Độc Lập, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7919632,
   "Latitude": 106.6357601
 },
 {
   "STT": 581,
   "Name": "Nhà thuốc Mỹ Sơn",
   "address": "25/10, KP 22, Đường Mã Lò, Phường  Bình Hưng Hòa A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8198128,
   "Latitude": 106.6087089
 },
 {
   "STT": 582,
   "Name": "Nhà thuốc Minh Khôi",
   "address": "502 Kinh Dương Vương, Phường  An Lạc, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7380044,
   "Latitude": 106.6147452
 },
 {
   "STT": 583,
   "Name": "Nhà thuốc Xuân Trường",
   "address": "646 Hồ Học Lãm, Phường  Bình Trị Đông B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7340907,
   "Latitude": 106.6035737
 },
 {
   "STT": 584,
   "Name": "Nhà thuốc Bạch Yến",
   "address": "1318 Tỉnh Lộ 10, Phường  Tân Tạo , Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7563031,
   "Latitude": 106.5924087
 },
 {
   "STT": 585,
   "Name": "Nhà thuốc 80",
   "address": "116 Lê Đình Cẩn, Phường  Tân Tạo, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7591192,
   "Latitude": 106.6025564
 },
 {
   "STT": 586,
   "Name": "Nhà thuốc An Tâm",
   "address": "968 Đường Tân Kỳ Tân Quý, Phường  Bình Hưng Hòa, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7925535,
   "Latitude": 106.6068019
 },
 {
   "STT": 587,
   "Name": "Nhà thuốc Thu Hằng",
   "address": "246 Đất Mới, Phường  Bình Trị Đông, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7700369,
   "Latitude": 106.609504
 },
 {
   "STT": 588,
   "Name": "Nhà thuốc Thiên Long 3",
   "address": "75 Bùi Dương Lịch, Phường  Bình Hưng Hòa B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8201208,
   "Latitude": 106.5985107
 },
 {
   "STT": 589,
   "Name": "Nhà thuốc Sơn Kỳ 2",
   "address": "46 Tây Lân, Bình Trị Đông A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7712502,
   "Latitude": 106.5876551
 },
 {
   "STT": 590,
   "Name": "Nhà thuốc Phúc Thảo",
   "address": "213/6 Lê Đình Cẩn, Phường  Tân Tạo, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7597308,
   "Latitude": 106.5959342
 },
 {
   "STT": 591,
   "Name": "Nhà thuốc Ngọc Mai",
   "address": "334 Tỉnh lộ 10, Phường  Bình Trị Đông, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7564353,
   "Latitude": 106.6235562
 },
 {
   "STT": 592,
   "Name": "Nhà thuốc Minh Thu",
   "address": "340 Tỉnh Lộ 10, Phường  Bình Trị Đông, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7565077,
   "Latitude": 106.6233429
 },
 {
   "STT": 593,
   "Name": "Nhà thuốc Duy Linh",
   "address": "1 Đường Chiến Lược, Phường  Bình Trị Đông, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.787144,
   "Latitude": 106.59993
 },
 {
   "STT": 594,
   "Name": "Nhà thuốc Kiều Oanh",
   "address": "109 Đường Tây Lân, Phường  Bình Trị ĐôngA, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7717759,
   "Latitude": 106.58611
 },
 {
   "STT": 595,
   "Name": "Nhà thuốc Thanh Mai",
   "address": "159 LK 4-5, Phường  Bình Hưng Hòa B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7994939,
   "Latitude": 106.5867855
 },
 {
   "STT": 596,
   "Name": "Nhà thuốc Hưng Phước",
   "address": "247 Nguyễn Thiện Thuật, Phường  1, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7692037,
   "Latitude": 106.6782665
 },
 {
   "STT": 597,
   "Name": "Nhà thuốc Đức Mạnh",
   "address": "003 Lô J Chung cư Nguyễn Thiện Thuật, Phường  1, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.754319,
   "Latitude": 106.6917304
 },
 {
   "STT": 598,
   "Name": "Nhà thuốc Hồng Nghi",
   "address": "006 Lô G Chung cư Nguyễn Thiện Thuật, Phường  1, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7684307,
   "Latitude": 106.6765914
 },
 {
   "STT": 599,
   "Name": "Nhà thuốc Bình Minh",
   "address": "29 Lô B Chung cư Nguyễn Thiện Thuật, Phường  1, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.768881,
   "Latitude": 106.6778323
 },
 {
   "STT": 600,
   "Name": "Nhà thuốc Hoàng Phúc",
   "address": "13 Cao Thắng, Phường  2, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7688364,
   "Latitude": 106.6833324
 },
 {
   "STT": 601,
   "Name": "Nhà thuốc Minh Chương",
   "address": "467 Điện Biên Phủ, Phường  3, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7719455,
   "Latitude": 106.678336
 },
 {
   "STT": 602,
   "Name": "Nhà thuốc Gia Phương",
   "address": "186 Nguyễn Thiện Thuật, Phường  3, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7687342,
   "Latitude": 106.6792047
 },
 {
   "STT": 603,
   "Name": "Nhà thuốc Ngọc Loan",
   "address": "509 Điện Biên Phủ, Phường  3, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7712383,
   "Latitude": 106.6775269
 },
 {
   "STT": 604,
   "Name": "Nhà thuốc Hồng Mai",
   "address": "242/74 Nguyễn Thiện Thuật, Phường  3, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7705288,
   "Latitude": 106.6793669
 },
 {
   "STT": 605,
   "Name": "Nhà thuốc Hữu Đức",
   "address": "439 Điện Biên Phủ, Phường  3, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7722741,
   "Latitude": 106.6787636
 },
 {
   "STT": 606,
   "Name": "Nhà thuốc Long Châu 3",
   "address": "379 Hai Bà Trưng, Phường  8, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7900971,
   "Latitude": 106.688752
 },
 {
   "STT": 607,
   "Name": "Nhà thuốc Phương Minh",
   "address": "1/30 Trần Văn Đang, Phường  9, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.783842,
   "Latitude": 106.679506
 },
 {
   "STT": 608,
   "Name": "Nhà thuốc Diễm Hằng",
   "address": "453/92 Lê Văn Sỹ, Phường  12, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.789285,
   "Latitude": 106.673049
 },
 {
   "STT": 609,
   "Name": "Nhà thuốc Tâm Nghĩa",
   "address": "155 Phó Cơ Điều, Phường  6, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7605489,
   "Latitude": 106.656059
 },
 {
   "STT": 610,
   "Name": "Nhà thuốc Việt Đức",
   "address": "92 Nguyễn Thị Nhỏ, Phường  15, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.77247,
   "Latitude": 106.653059
 },
 {
   "STT": 611,
   "Name": "Nhà thuốc Triều An",
   "address": "119-121 Lạc Long Quân, Phường  1, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758482,
   "Latitude": 106.6383479
 },
 {
   "STT": 612,
   "Name": "Nhà thuốc Hải Châu",
   "address": "16 Lô C2, chung cư Lý Thường Kiệt, Phường  7, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7554376,
   "Latitude": 106.650589
 },
 {
   "STT": 613,
   "Name": "Nhà thuốc Phú Cường",
   "address": "423/28 Lạc Long Quân, Phường  5, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.773045,
   "Latitude": 106.6449389
 },
 {
   "STT": 614,
   "Name": "Nhà thuốc Âu Dược",
   "address": "77-79 Thuận Kiều, Phường  4, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.757958,
   "Latitude": 106.658213
 },
 {
   "STT": 615,
   "Name": "Nhà thuốc Yến Khanh 1",
   "address": "22 Ông Ích Khiêm, Phường  14, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.765726,
   "Latitude": 106.646176
 },
 {
   "STT": 616,
   "Name": "Nhà thuốc Yến Khanh 2",
   "address": "532 Nguyễn Chí Thanh, Phường  7, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7585525,
   "Latitude": 106.661359
 },
 {
   "STT": 617,
   "Name": "Nhà thuốc Hồng Hạnh",
   "address": "1B Đường số 2, cư xá Lữ Gia, Phường  15, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7722553,
   "Latitude": 106.6572743
 },
 {
   "STT": 618,
   "Name": "Nhà thuốc Minh Trung",
   "address": "8/67 Tân Hóa, Phường  1, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7569009,
   "Latitude": 106.6384379
 },
 {
   "STT": 619,
   "Name": "Nhà thuốc Số 11",
   "address": "205 Đội Cung, Phường  9, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.761455,
   "Latitude": 106.645555
 },
 {
   "STT": 620,
   "Name": "Nhà thuốc Thái Sơn",
   "address": "29 Nguyễn Kiệm, Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.81057,
   "Latitude": 106.680923
 },
 {
   "STT": 621,
   "Name": "Nhà thuốc Thùy Trang",
   "address": "147 Nguyễn Kiệm, Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8131116,
   "Latitude": 106.6786775
 },
 {
   "STT": 622,
   "Name": "Nhà thuốc Đại Đường",
   "address": "790/16 Nguyễn Kiệm, Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8242433,
   "Latitude": 106.6789919
 },
 {
   "STT": 623,
   "Name": "Nhà thuốc Kim Hoan",
   "address": "34A Nguyễn Thái Sơn, Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.820185,
   "Latitude": 106.685214
 },
 {
   "STT": 624,
   "Name": "Nhà thuốc Ngọc Phúc",
   "address": "33/8A Nguyễn Thái Sơn Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8147809,
   "Latitude": 106.6780592
 },
 {
   "STT": 625,
   "Name": "Nhà thuốc Gia An",
   "address": "105 Huỳnh Khương An, Phường  5, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.822108,
   "Latitude": 106.688969
 },
 {
   "STT": 626,
   "Name": "Nhà thuốc Kiều Nga",
   "address": "206 Lê Đức Thọ, Phường  6, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8411295,
   "Latitude": 106.6779539
 },
 {
   "STT": 627,
   "Name": "Nhà thuốc Bảo Duy",
   "address": "25/1 Đường số 27, Phường  6, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.841748,
   "Latitude": 106.67834
 },
 {
   "STT": 628,
   "Name": "Nhà thuốc Nhật Thanh",
   "address": "72 Lê Đức Thọ, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8331908,
   "Latitude": 106.6823543
 },
 {
   "STT": 629,
   "Name": "Nhà thuốc Anh Duy",
   "address": "75 Đường số10, Phường  8, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8439154,
   "Latitude": 106.6497311
 },
 {
   "STT": 630,
   "Name": "Nhà thuốc Thái Bình",
   "address": "35 Đường số 21, Phường  8, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8409455,
   "Latitude": 106.6506665
 },
 {
   "STT": 631,
   "Name": "Nhà thuốc Vạn Hạnh",
   "address": "111 Quang Trung, Phường  10, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8277473,
   "Latitude": 106.6757348
 },
 {
   "STT": 632,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "119 Quang Trung, Phường  10, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8347714,
   "Latitude": 106.6641228
 },
 {
   "STT": 633,
   "Name": "Nhà thuốc ABC",
   "address": "11/25 Nguyễn Oanh, Phường  10, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8316192,
   "Latitude": 106.6770196
 },
 {
   "STT": 634,
   "Name": "Nhà thuốc Bảo Vy",
   "address": "60/8 Lê Văn Thọ, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.840445,
   "Latitude": 106.657531
 },
 {
   "STT": 635,
   "Name": "Nhà thuốc Sông Hương",
   "address": "278 Lê Văn Thọ, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.846494,
   "Latitude": 106.657238
 },
 {
   "STT": 636,
   "Name": "Nhà thuốc Minh Huy",
   "address": "130/6B Lê văn Thọ, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.845986,
   "Latitude": 106.658004
 },
 {
   "STT": 637,
   "Name": "Nhà thuốc Ngọc Thư",
   "address": "28, Đường số 11, Phuờng 11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.842471,
   "Latitude": 106.6605359
 },
 {
   "STT": 638,
   "Name": "Nhà thuốc Lê Thám",
   "address": "48/338B Quang Trung, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.840786,
   "Latitude": 106.643119
 },
 {
   "STT": 639,
   "Name": "Nhà thuốc Ngọc Hà",
   "address": "44/304A Quang Trung, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.839152,
   "Latitude": 106.642401
 },
 {
   "STT": 640,
   "Name": "Nhà thuốc Phương Anh",
   "address": "37/257B Phan Huy Ích, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.836205,
   "Latitude": 106.635945
 },
 {
   "STT": 641,
   "Name": "Nhà thuốc Phúc Linh",
   "address": "70/476 Huỳnh Văn Nghệ, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.829923,
   "Latitude": 106.638587
 },
 {
   "STT": 642,
   "Name": "Nhà thuốc Ngọc Sơn",
   "address": "23/9 Lê Đức Thọ, Phường  16, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.850315,
   "Latitude": 106.661319
 },
 {
   "STT": 643,
   "Name": "Nhà thuốc Dược Khoa",
   "address": "1/2 Lê Đức Thọ, Phường  16, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.850254,
   "Latitude": 106.663834
 },
 {
   "STT": 644,
   "Name": "Nhà thuốc Phước Bảo",
   "address": "304/9C Đường 26/3, Phường  16, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7997175,
   "Latitude": 106.6062831
 },
 {
   "STT": 645,
   "Name": "Nhà thuốc Đức Hương",
   "address": "34/22  Lê Văn Thọ, Phường  16, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.848547,
   "Latitude": 106.657061
 },
 {
   "STT": 646,
   "Name": "Nhà thuốc Khánh Quỳnh",
   "address": "48 Đường số 10, Phường  16, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8448381,
   "Latitude": 106.6463873
 },
 {
   "STT": 647,
   "Name": "Nhà thuốc Nhân Ái 1",
   "address": "1174/102A Lê Đức Thọ, Phường  17, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.839594,
   "Latitude": 106.679138
 },
 {
   "STT": 648,
   "Name": "Nhà thuốc Phương Quỳnh",
   "address": "15E Nguyên Hồng, Phường  1, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8190936,
   "Latitude": 106.6905958
 },
 {
   "STT": 649,
   "Name": "Nhà thuốc Minh Trang",
   "address": "33/28 Nguyễn Thái Sơn, Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.820971,
   "Latitude": 106.6851609
 },
 {
   "STT": 650,
   "Name": "Nhà thuốc Vinh Châu",
   "address": "312 Dương Quảng Hàm, Phường  5, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.831484,
   "Latitude": 106.6872645
 },
 {
   "STT": 651,
   "Name": "Nhà thuốc Bến Cát",
   "address": "173/6 Dương Quảng Hàm, Phường  5, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.828549,
   "Latitude": 106.690425
 },
 {
   "STT": 652,
   "Name": "Nhà thuốc Huỳnh Minh",
   "address": "48/11 Phạm Văn Chiêu, Phường  9, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.846485,
   "Latitude": 106.6450289
 },
 {
   "STT": 653,
   "Name": "Nhà thuốc Mỵ Nương",
   "address": "146 Đường Số 10, Phường  9, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8406874,
   "Latitude": 106.6559604
 },
 {
   "STT": 654,
   "Name": "Nhà thuốc Minh Thảo 2",
   "address": "180 Nguyễn Văn Lượng, Phường  17, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.838779,
   "Latitude": 106.673612
 },
 {
   "STT": 655,
   "Name": "Nhà thuốc Khánh Vân",
   "address": "53 Lý Thường Kiệt, Phường  4, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.823617,
   "Latitude": 106.683213
 },
 {
   "STT": 656,
   "Name": "Nhà thuốc Nhị Minh",
   "address": "278 Dương Quảng Hàm, Phường  5, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.835135,
   "Latitude": 106.6859829
 },
 {
   "STT": 657,
   "Name": "Nhà thuốc Ngã Năm",
   "address": "107 Nguyễn Văn Nghi, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.826166,
   "Latitude": 106.6804499
 },
 {
   "STT": 658,
   "Name": "Nhà thuốc Hồng Đức",
   "address": "125 Nguyễn Du, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.826196,
   "Latitude": 106.685242
 },
 {
   "STT": 659,
   "Name": "Nhà thuốc Đỗ Xuân",
   "address": "02 Đường số 2, Phường  10, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.832812,
   "Latitude": 106.6829939
 },
 {
   "STT": 660,
   "Name": "Nhà thuốc Hữu Vinh",
   "address": "72/487B Phan Huy Ích, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.832827,
   "Latitude": 106.63802
 },
 {
   "STT": 661,
   "Name": "Nhà thuốc Khánh Nam",
   "address": "22/10 Phan Huy Ích, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.843679,
   "Latitude": 106.639872
 },
 {
   "STT": 662,
   "Name": "Nhà thuốc Trường Thịnh",
   "address": "442 Lê Đức Thọ, Phường  17, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8422038,
   "Latitude": 106.6763489
 },
 {
   "STT": 663,
   "Name": "Nhà thuốc Minh Hương",
   "address": "54/18 Đường số 11, Phường  5, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.827416,
   "Latitude": 106.697307
 },
 {
   "STT": 664,
   "Name": "Nhà thuốc An Nhơn",
   "address": "64/715B Nguyễn Oanh, Phường  6, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.842891,
   "Latitude": 106.677154
 },
 {
   "STT": 665,
   "Name": "Nhà thuốc Quang Lộc",
   "address": "17/7Z Phạm Văn Chiêu, Phường  9, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.850858,
   "Latitude": 106.654928
 },
 {
   "STT": 666,
   "Name": "Nhà thuốc Minh Khiêm",
   "address": "10 Đường số 5, Phường  9, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.843342,
   "Latitude": 106.65325
 },
 {
   "STT": 667,
   "Name": "Nhà thuốc Quang Hợp",
   "address": "288 Đường số 10, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8437,
   "Latitude": 106.6503479
 },
 {
   "STT": 668,
   "Name": "Nhà thuốc Việt Tiến",
   "address": "69 Đường số 51, Phường  14, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.847679,
   "Latitude": 106.6412455
 },
 {
   "STT": 669,
   "Name": "Nhà thuốc Hồ Sỹ",
   "address": "17/2 Phan Huy Ích, Phường  14, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.839109,
   "Latitude": 106.635103
 },
 {
   "STT": 670,
   "Name": "Nhà thuốc Tú Anh",
   "address": "18/6 Lê Đức Thọ, Phường  16, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.852234,
   "Latitude": 106.660712
 },
 {
   "STT": 671,
   "Name": "Nhà thuốc 183",
   "address": "183/5A Bùi Viện, Phường  Phạm Ngũ Lão, Quận , Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.766024,
   "Latitude": 106.691264
 },
 {
   "STT": 672,
   "Name": "Nhà thuốc Bích Hà",
   "address": "137 Trần Đình Xu, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762735,
   "Latitude": 106.687724
 },
 {
   "STT": 673,
   "Name": "Nhà thuốc Đại Cát",
   "address": "27 Thạch Thị Thanh, Phường  Tân Định, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790194,
   "Latitude": 106.692628
 },
 {
   "STT": 674,
   "Name": "Nhà thuốc Đan Linh",
   "address": "33 Hồ Hảo Hớn, Phường  Cô Giang, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.760767,
   "Latitude": 106.692719
 },
 {
   "STT": 675,
   "Name": "Nhà thuốc Đi Cờ Li Nít",
   "address": "225 Lê Thánh Tôn, Phường  Bến Nghé, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7722656,
   "Latitude": 106.6966047
 },
 {
   "STT": 676,
   "Name": "Nhà thuốc Đông Phương",
   "address": "16 Trần Đình Xu, Phường  Cô Giang, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759268,
   "Latitude": 106.6923489
 },
 {
   "STT": 677,
   "Name": "Nhà thuốc Đức Huy",
   "address": "202 Nguyễn Cư Trinh, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7631858,
   "Latitude": 106.6878506
 },
 {
   "STT": 678,
   "Name": "Nhà thuốc Hải",
   "address": "185D Cống Quỳnh, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.766503,
   "Latitude": 106.688422
 },
 {
   "STT": 679,
   "Name": "Nhà thuốc Hoa Sen",
   "address": "Phòng 301, số 22- 22Bis Lê Thánh Tôn, Phường  Bến Nghé, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7762926,
   "Latitude": 106.7009576
 },
 {
   "STT": 680,
   "Name": "Nhà thuốc Hoàng Vũ",
   "address": "301F Trần Hưng Đạo, Phường  Cô Giang, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7625269,
   "Latitude": 106.690582
 },
 {
   "STT": 681,
   "Name": "Nhà thuốc Hồng Ân",
   "address": "15/1 Nguyễn Cảnh Chân, Phường  Cầu Kho, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7606036,
   "Latitude": 106.6870414
 },
 {
   "STT": 682,
   "Name": "Nhà thuốc Hồng Đài",
   "address": "20 Bùi Viện, Phường  Phạm Ngũ Lão, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.767966,
   "Latitude": 106.69479
 },
 {
   "STT": 683,
   "Name": "Nhà thuốc Hồng Đức",
   "address": "348 Bến Chương Dương, Phường  Cầu Kho, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566419,
   "Latitude": 106.689306
 },
 {
   "STT": 684,
   "Name": "Nhà thuốc Hồng Hân",
   "address": "1B Tôn Thất Tùng, Phường  Phạm Ngũ Lão, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.76953,
   "Latitude": 106.68876
 },
 {
   "STT": 685,
   "Name": "Nhà thuốc Hồng Hoa",
   "address": "457/1 Trần Hưng Đạo, Phường  Cầu Kho, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7584447,
   "Latitude": 106.6881776
 },
 {
   "STT": 686,
   "Name": "Nhà thuốc Huệ Tịnh",
   "address": "125 Trần Đình Xu, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762599,
   "Latitude": 106.687925
 },
 {
   "STT": 687,
   "Name": "Nhà thuốc Hương Trang",
   "address": "52 Trịnh Văn Cấn, Phường  Cầu Ông Lãnh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.767257,
   "Latitude": 106.696122
 },
 {
   "STT": 688,
   "Name": "Nhà thuốc Khanh Linh",
   "address": "64-68 Hai Bà Trưng, Phường  Bến Nghé, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777369,
   "Latitude": 106.7042737
 },
 {
   "STT": 689,
   "Name": "Nhà thuốc Khương Duy",
   "address": "406 Hai Bà Trưng, Phường  Tân Định, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.791075,
   "Latitude": 106.6881499
 },
 {
   "STT": 690,
   "Name": "Nhà thuốc Linh Chi",
   "address": "122B Trần Đình Xu, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762403,
   "Latitude": 106.6886125
 },
 {
   "STT": 691,
   "Name": "Nhà thuốc Minh Châu",
   "address": "65 Calmette, Phường  Nguyễn Thái Bình, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7673495,
   "Latitude": 106.6995094
 },
 {
   "STT": 692,
   "Name": "Nhà thuốc Minh Trí",
   "address": "C6/9 Nguyễn Trãi, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7591861,
   "Latitude": 106.6833073
 },
 {
   "STT": 693,
   "Name": "Nhà thuốc Nam Thành",
   "address": "118 Lý Tự Trọng, Phường  Bến Nghé, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7739239,
   "Latitude": 106.6968638
 },
 {
   "STT": 694,
   "Name": "Nhà thuốc Ngọc Hiếu",
   "address": "112 Cống Quỳnh, Phường  Phạm Ngũ Lão, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.765195,
   "Latitude": 106.690447
 },
 {
   "STT": 695,
   "Name": "Nhà thuốc Ngoc Lan",
   "address": "3 Nguyễn Thị Minh Khai, Phường  Phạm Ngũ Lão, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.770702,
   "Latitude": 106.687045
 },
 {
   "STT": 696,
   "Name": "Nhà thuốc Nguyên Châu",
   "address": "160 Trần Quang Khải, Phường  Tân Định, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.791828,
   "Latitude": 106.690237
 },
 {
   "STT": 697,
   "Name": "Nhà thuốc Nguyễn Kim Hồng",
   "address": "11D Nguyễn Thị Minh Khai, Phường  Bến Nghé, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.786126,
   "Latitude": 106.701402
 },
 {
   "STT": 698,
   "Name": "Nhà thuốc Nhân Nghĩa",
   "address": "55/27 Lê Thị Hồng Gấm, Phường  Nguyễn Thái Bình, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.768444,
   "Latitude": 106.69822
 },
 {
   "STT": 699,
   "Name": "Nhà thuốc Như Ý",
   "address": "282/5A Cống Quỳnh, Phường  Phạm Ngũ Lão, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.768228,
   "Latitude": 106.68583
 },
 {
   "STT": 700,
   "Name": "Nhà thuốc Phạm Thị Hoàng Phượng",
   "address": "601 Trần Hưng Đạo, Phường  Cầu Kho, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7565085,
   "Latitude": 106.6854644
 },
 {
   "STT": 701,
   "Name": "Nhà thuốc Phú Lợi",
   "address": "112 Cô Bắc, Phường  Cô Giang, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.764704,
   "Latitude": 106.694386
 },
 {
   "STT": 702,
   "Name": "Nhà thuốc Phúc Nguyên",
   "address": "172 Lê Thánh Tôn, Phường  Bến Nghé, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7731546,
   "Latitude": 106.697335
 },
 {
   "STT": 703,
   "Name": "Nhà thuốc Phuong An",
   "address": "21 Đỗ Quang Đẫu, Phường  Phạm Ngũ Lão, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.766622,
   "Latitude": 106.6910089
 },
 {
   "STT": 704,
   "Name": "Nhà thuốc Quang Ngọc",
   "address": "28 Nguyễn Đình Chiểu, Phường  Đa Kao, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7903934,
   "Latitude": 106.701233
 },
 {
   "STT": 705,
   "Name": "Nhà thuốc Số Bốn",
   "address": "105 Điện Biên Phủ, Phường  Đa Kao, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.789178,
   "Latitude": 106.696556
 },
 {
   "STT": 706,
   "Name": "Nhà thuốc Sơn Hà",
   "address": "164A Cô Giang, Phường  Cô Giang, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762031,
   "Latitude": 106.693436
 },
 {
   "STT": 707,
   "Name": "Nhà thuốc Song Binh",
   "address": "68/282A Trần Quang Khải, Phường  Tân Định, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.793481,
   "Latitude": 106.693617
 },
 {
   "STT": 708,
   "Name": "Nhà thuốc Thiên Hương 2",
   "address": "57 Cô Giang, Phường  Cầu Ông Lãnh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.763993,
   "Latitude": 106.69613
 },
 {
   "STT": 709,
   "Name": "Nhà thuốc Thiên Thanh",
   "address": "345A Nguyễn Trãi, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.761203,
   "Latitude": 106.686074
 },
 {
   "STT": 710,
   "Name": "Nhà thuốc Tín Nghĩa",
   "address": "003 Lô A1 Chung cư 1A-1B, Nguyễn Đình Chiểu, Phường  Đa Kao, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7916794,
   "Latitude": 106.7037648
 },
 {
   "STT": 711,
   "Name": "Nhà thuốc Vạn Lợi",
   "address": "380B Hai Bà Trưng, Phường  Tân Định, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7907429,
   "Latitude": 106.6886138
 },
 {
   "STT": 712,
   "Name": "Nhà thuốc Van Phuc Hung",
   "address": "205A Cống Quỳnh, Phường  Phạm Ngũ Lão, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.766738,
   "Latitude": 106.6876594
 },
 {
   "STT": 713,
   "Name": "Nhà thuốc Việt Duy",
   "address": "160B Nguyễn Cư Trinh, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.763492,
   "Latitude": 106.688604
 },
 {
   "STT": 714,
   "Name": "Nhà thuốc Việt Duy Hai",
   "address": "87 Nguyễn Cư Trinh, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.763632,
   "Latitude": 106.689921
 },
 {
   "STT": 715,
   "Name": "Nhà thuốc Việt Phát",
   "address": "45 Võ Thị Sáu, Phường  Đa Kao, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7902596,
   "Latitude": 106.6947919
 },
 {
   "STT": 716,
   "Name": "Đại lý số 23 - Công ty Sapharco",
   "address": "Kiosque số 2, Ấp 1, Chợ Đệm, TT Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6945844,
   "Latitude": 106.5799363
 },
 {
   "STT": 717,
   "Name": "Đại lý số 22 - Công ty Sapharco",
   "address": "E9/18 KP5, TT Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6979001,
   "Latitude": 106.5944539
 },
 {
   "STT": 718,
   "Name": "Đại lý số 134 - Công ty Sapharco",
   "address": "D16/10B Ấp 4, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7322911,
   "Latitude": 106.5813356
 },
 {
   "STT": 719,
   "Name": "Đại lý số 105 - Công ty Sapharco",
   "address": "D5/133 Quốc Lộ 50 Ấp 4, Xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7378841,
   "Latitude": 106.656341
 },
 {
   "STT": 720,
   "Name": "Đại lý số 60 - Công ty Sapharco",
   "address": "E18/12 Hương Lộ 80, Ấp 5 Xã Vĩnh Lộc B , Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8113963,
   "Latitude": 106.5757703
 },
 {
   "STT": 721,
   "Name": "Hiệu thuốc số 42 - Công ty CPDP Phong Phú",
   "address": "A1/9 Quốc lộ 50, Ấp 1,  Xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7318573,
   "Latitude": 106.6559323
 },
 {
   "STT": 722,
   "Name": "Hiệu thuốc số 24 - Chi nhánh Công ty CP DP Bến Thành",
   "address": "107 Điện Biên Phủ, Phường  Đa Kao, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.789151,
   "Latitude": 106.696538
 },
 {
   "STT": 723,
   "Name": "Nhà thuốc Pha No 9 - Công ty CP Dược phẩm Pha No",
   "address": "79 Điện Biên Phủ, Phường  Đa Kao, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7895506,
   "Latitude": 106.6974753
 },
 {
   "STT": 724,
   "Name": "Hiệu thuốc số 7 - Công ty CP XNK Y tế TP HCM Yteco",
   "address": "90 Hàm Nghi, Phường  Bến Nghé, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771195,
   "Latitude": 106.702983
 },
 {
   "STT": 725,
   "Name": "Nhà thuốc - Công ty TNHH Một thành viên Phòng khám đa khoa Hoàng Huỳnh Long",
   "address": "6-8 Trịnh Văn Cấn, Phường  Cầu Ông Lãnh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7677123,
   "Latitude": 106.6968018
 },
 {
   "STT": 726,
   "Name": "Nhà thuốc - Công ty CP Giáo dục & Đào tạo Hòa Thuận Phát - HTP Pharmacy",
   "address": "191 Trần Hưng Đạo, Phường  Cô Giang, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.765798,
   "Latitude": 106.6941609
 },
 {
   "STT": 727,
   "Name": "Nhà thuốc - Chi nhánh Công ty CP BV Phụ sản - Nhi Quốc tế Hạnh Phúc (Tỉnh Bình Dương)",
   "address": "37 Tôn Đức Thắng, Phường  Phạm Ngũ Lão, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.783662,
   "Latitude": 106.7031886
 },
 {
   "STT": 728,
   "Name": "Mỹ Châu 6 - Công ty cổ phần dược Minh Phúc",
   "address": "167 Nguyễn Kiệm, Phường  3, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8131071,
   "Latitude": 106.6786773
 },
 {
   "STT": 729,
   "Name": "Hiệu thuốc số 1 - Công ty CP DP Dược liệu Pharmedic",
   "address": "367 Nguyễn Trãi, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.760671,
   "Latitude": 106.685798
 },
 {
   "STT": 730,
   "Name": "Nhà thuốc - Công ty TNHH Việt An Organic",
   "address": "201 Nguyễn Thị Minh Khai, Phường  Nguyễn Cư Trinh, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7673606,
   "Latitude": 106.683799
 },
 {
   "STT": 731,
   "Name": "Hiệu thuốc Á Châu - Công ty CPDP XNK Chợ Lớn",
   "address": "450 Nguyễn Trãi, Phường  8, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7551761,
   "Latitude": 106.6716311
 },
 {
   "STT": 732,
   "Name": "Bệnh viện Mắt Cao Thắng",
   "address": "135B Trần Bình Trọng, Phường  2, Quận 1, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7572417,
   "Latitude": 106.6807838
 },
 {
   "STT": 733,
   "Name": "Nhà thuốc Ngọc Điệp",
   "address": "B14, CXPLB, P13,  Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.754644,
   "Latitude": 106.629555
 },
 {
   "STT": 734,
   "Name": "Nhà thuốc Ngọc Thanh",
   "address": "204 Bà Hom, P13,  Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.755546,
   "Latitude": 106.627543
 },
 {
   "STT": 735,
   "Name": "Nhà thuốc Vi Khang",
   "address": "148 Tân Hòa Đông, P14,  Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759723,
   "Latitude": 106.628762
 },
 {
   "STT": 736,
   "Name": "Nhà thuốc Nhân Tâm",
   "address": "222B Bà Hom, P13,  Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.755685,
   "Latitude": 106.626949
 },
 {
   "STT": 737,
   "Name": "Nhà thuốc Phong Lan",
   "address": "347 Lũy Bán Bích, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.773042,
   "Latitude": 106.632038
 },
 {
   "STT": 738,
   "Name": "Nhà thuốc NT 99",
   "address": "99 Cây Keo, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771686,
   "Latitude": 106.628724
 },
 {
   "STT": 739,
   "Name": "Nhà thuốc Minh Khôi",
   "address": "17 Hoàng Thiều Hoa, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.77297,
   "Latitude": 106.628618
 },
 {
   "STT": 740,
   "Name": "Nhà thuốc Trường Khang",
   "address": "101 Thạch Lam, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776871,
   "Latitude": 106.630394
 },
 {
   "STT": 741,
   "Name": "Nhà thuốc Kiều Anh",
   "address": "311A Thoại Ngọc Hầu, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776941,
   "Latitude": 106.627844
 },
 {
   "STT": 742,
   "Name": "Nhà thuốc Trọng Hiếu",
   "address": "35 Hoàng Xuân Hoành, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7688369,
   "Latitude": 106.6302061
 },
 {
   "STT": 743,
   "Name": "Nhà thuốc Mai Phương",
   "address": "35 Cây Keo, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771175,
   "Latitude": 106.630305
 },
 {
   "STT": 744,
   "Name": "Nhà thuốc Ngọc Châu",
   "address": "110/72A Tô Hiệu, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.772047,
   "Latitude": 106.629119
 },
 {
   "STT": 745,
   "Name": "Nhà thuốc Lăng Vân",
   "address": "41 Thạch Lam, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.77636,
   "Latitude": 106.631956
 },
 {
   "STT": 746,
   "Name": "Nhà thuốc Kim Ngọc I",
   "address": "151 Thạch Lam, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777345,
   "Latitude": 106.628814
 },
 {
   "STT": 747,
   "Name": "Nhà thuốc Huỳnh Liên",
   "address": "4 Nguyễn Mỹ Ca, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771509,
   "Latitude": 106.630589
 },
 {
   "STT": 748,
   "Name": "Nhà thuốc Tâm Phúc Đức",
   "address": "276 Hòa Bình, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7719799,
   "Latitude": 106.627244
 },
 {
   "STT": 749,
   "Name": "Nhà thuốc Bảo Châu",
   "address": "327 Thoại Ngọc Hầu, Phường   Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7766358,
   "Latitude": 106.6274276
 },
 {
   "STT": 750,
   "Name": "Nhà thuốc Quỳnh Anh",
   "address": "33 Tân Thành, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.786961,
   "Latitude": 106.639421
 },
 {
   "STT": 751,
   "Name": "Nhà thuốc Minh Trang",
   "address": "142 Huỳnh Thiện Lộc, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.779095,
   "Latitude": 106.639764
 },
 {
   "STT": 752,
   "Name": "Nhà thuốc Bảo Linh",
   "address": "42A Huỳnh Thiện Lộc, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.780034,
   "Latitude": 106.636959
 },
 {
   "STT": 753,
   "Name": "Nhà thuốc Phúc Khang",
   "address": "370 Lũy Bán Bích, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.775839,
   "Latitude": 106.63393
 },
 {
   "STT": 754,
   "Name": "Nhà thuốc Bảo Vinh",
   "address": "235A Âu Cơ, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7785809,
   "Latitude": 106.6452751
 },
 {
   "STT": 755,
   "Name": "Nhà thuốc Vạn Hạnh",
   "address": "530 Lũy Bán Bích, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7816374,
   "Latitude": 106.6361504
 },
 {
   "STT": 756,
   "Name": "Nhà thuốc Tuấn Khanh",
   "address": "319 Kênh Tân Hóa, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777497,
   "Latitude": 106.639064
 },
 {
   "STT": 757,
   "Name": "Nhà thuốc Nguyên Lộc",
   "address": "321 Kênh Tân Hóa, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777483,
   "Latitude": 106.6392123
 },
 {
   "STT": 758,
   "Name": "Nhà thuốc Kim Ngân",
   "address": "44-46 Thoại Ngọc Hầu, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.78462,
   "Latitude": 106.639167
 },
 {
   "STT": 759,
   "Name": "Nhà thuốc Như Ngọc",
   "address": "269D Trịnh Đình Trọng, Phường   Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776487,
   "Latitude": 106.6368627
 },
 {
   "STT": 760,
   "Name": "Nhà thuốc An Phúc",
   "address": "35 Nguyễn Lý, Phường   Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777747,
   "Latitude": 106.627498
 },
 {
   "STT": 761,
   "Name": "Nhà thuốc Trường Sơn",
   "address": "378E Thoại Ngọc Hầu, Phường   Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.774164,
   "Latitude": 106.621929
 },
 {
   "STT": 762,
   "Name": "Nhà thuốc Thanh Tâm",
   "address": "25B Nguyễn Sơn, Phường   Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.780851,
   "Latitude": 106.632683
 },
 {
   "STT": 763,
   "Name": "Nhà thuốc Kim Hải",
   "address": "242 Thoại Ngọc Hầu, Phường   Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7782142,
   "Latitude": 106.6293836
 },
 {
   "STT": 764,
   "Name": "Nhà thuốc Gia Linh",
   "address": "297 Thạch Lam, Phường   Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.779415,
   "Latitude": 106.622685
 },
 {
   "STT": 765,
   "Name": "Nhà thuốc Thạch Lam",
   "address": "93 Thạch Lam, Phường   Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7769303,
   "Latitude": 106.6305747
 },
 {
   "STT": 766,
   "Name": "Nhà thuốc Bách Tường",
   "address": "193 Nguyễn Sơn, Phường   Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.782686,
   "Latitude": 106.626452
 },
 {
   "STT": 767,
   "Name": "Nhà thuốc Thu Hiền",
   "address": "306 Thạch Lam, Phường   Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.779275,
   "Latitude": 106.6240739
 },
 {
   "STT": 768,
   "Name": "Nhà thuốc Anh Duy",
   "address": "55 Phú Thọ Hòa, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7862729,
   "Latitude": 106.6230102
 },
 {
   "STT": 769,
   "Name": "Nhà thuốc Phương Châu",
   "address": "2/4/45 Lê Thúc Hoạch, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.786098,
   "Latitude": 106.628291
 },
 {
   "STT": 770,
   "Name": "Nhà thuốc Minh Phúc",
   "address": "226 Phú Thọ Hòa, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7839387,
   "Latitude": 106.6309803
 },
 {
   "STT": 771,
   "Name": "Nhà thuốc Thuận Thành",
   "address": "Lê Thúc Hoạch, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7875324,
   "Latitude": 106.622411
 },
 {
   "STT": 772,
   "Name": "Nhà thuốc Bảo Phúc",
   "address": "65 Hoàng Ngọc Phách, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.78532,
   "Latitude": 106.6253629
 },
 {
   "STT": 773,
   "Name": "Nhà thuốc Thành Nhân",
   "address": "737 Lũy Bán Bích, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.783273,
   "Latitude": 106.636036
 },
 {
   "STT": 774,
   "Name": "Nhà thuốc Ái Liên",
   "address": "442 Nguyễn Sơn, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785,
   "Latitude": 106.619921
 },
 {
   "STT": 775,
   "Name": "Nhà thuốc Thanh Thảo",
   "address": "270 Vườn Lài, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.788012,
   "Latitude": 106.62591
 },
 {
   "STT": 776,
   "Name": "Nhà thuốc Thiện Phúc",
   "address": "135 Lê Thúc Hoạch, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.788136,
   "Latitude": 106.620333
 },
 {
   "STT": 777,
   "Name": "Nhà thuốc Hoàng Kim",
   "address": "785 Lũy Bán Bích, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.784502,
   "Latitude": 106.636323
 },
 {
   "STT": 778,
   "Name": "Nhà thuốc Mỹ Dung",
   "address": "402 Bình Long, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.787809,
   "Latitude": 106.617698
 },
 {
   "STT": 779,
   "Name": "Nhà thuốc Thanh Hiền",
   "address": "76/72 Lê Văn Phan, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.787005,
   "Latitude": 106.63046
 },
 {
   "STT": 780,
   "Name": "Nhà thuốc Minh An",
   "address": "76/51-53 Lê Văn Phan, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785942,
   "Latitude": 106.632167
 },
 {
   "STT": 781,
   "Name": "Nhà thuốc Thanh Bảo",
   "address": "34B Văn Cao, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.784743,
   "Latitude": 106.6216029
 },
 {
   "STT": 782,
   "Name": "Nhà thuốc Bảo Tâm",
   "address": "348 Nguyễn Sơn, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.784242,
   "Latitude": 106.622717
 },
 {
   "STT": 783,
   "Name": "Nhà thuốc Thảo Anh",
   "address": "79/10 Phú Thọ Hòa, Phường   Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7863307,
   "Latitude": 106.6276567
 },
 {
   "STT": 784,
   "Name": "Nhà thuốc Minh Đức",
   "address": "56 Trịnh Đình Thảo, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.774231,
   "Latitude": 106.636732
 },
 {
   "STT": 785,
   "Name": "Nhà thuốc Phước Khang",
   "address": "133 Khuông Việt, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7754133,
   "Latitude": 106.6394663
 },
 {
   "STT": 786,
   "Name": "Nhà thuốc Tuyết Minh",
   "address": "202 Khuông Việt, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.77525,
   "Latitude": 106.6387
 },
 {
   "STT": 787,
   "Name": "Nhà thuốc Kim Phú",
   "address": "10 Kênh Tân Hóa, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.770581,
   "Latitude": 106.635889
 },
 {
   "STT": 788,
   "Name": "Nhà thuốc Gia Nguyễn",
   "address": "213/79/20 Khuông Việt, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.781175,
   "Latitude": 106.642705
 },
 {
   "STT": 789,
   "Name": "Nhà thuốc An Đông",
   "address": "176 Kênh Tân Hóa, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7795417,
   "Latitude": 106.640621
 },
 {
   "STT": 790,
   "Name": "Nhà thuốc Việt My",
   "address": "61 Trịnh Đình Trọng, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.774944,
   "Latitude": 106.643208
 },
 {
   "STT": 791,
   "Name": "Nhà thuốc Số 32",
   "address": "411 Âu Cơ, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777568,
   "Latitude": 106.645775
 },
 {
   "STT": 792,
   "Name": "Nhà thuốc Thiên Ân",
   "address": "83 Đường số 27, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8056819,
   "Latitude": 106.6291542
 },
 {
   "STT": 793,
   "Name": "Nhà thuốc Lộc Ngân",
   "address": "236 Tân Kỳ Tân Quý, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8007698,
   "Latitude": 106.6227043
 },
 {
   "STT": 794,
   "Name": "Nhà thuốc Minh Trang",
   "address": "30/77D Đỗ Nhuận, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8034353,
   "Latitude": 106.6267108
 },
 {
   "STT": 795,
   "Name": "Nhà thuốc Ân Đức",
   "address": "81 Lê Trọng Tấn, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8074886,
   "Latitude": 106.6216433
 },
 {
   "STT": 796,
   "Name": "Nhà thuốc Thành Tín 2",
   "address": "6 Đường CN1, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8161628,
   "Latitude": 106.6111704
 },
 {
   "STT": 797,
   "Name": "Nhà thuốc Phương Mai",
   "address": "9A Đỗ Nhuận, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.804907,
   "Latitude": 106.626544
 },
 {
   "STT": 798,
   "Name": "Nhà thuốc Sơn Kỳ",
   "address": "48 Đường CN1, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8155477,
   "Latitude": 106.6105211
 },
 {
   "STT": 799,
   "Name": "Nhà thuốc Minh Anh",
   "address": "18/50 Đỗ Nhuận, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8032299,
   "Latitude": 106.624866
 },
 {
   "STT": 800,
   "Name": "Nhà thuốc Nguyễn Hoàng",
   "address": "270 Tân Kỳ Tân Quý, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8026955,
   "Latitude": 106.6273885
 },
 {
   "STT": 801,
   "Name": "Nhà thuốc Ngọc Dung",
   "address": "35 Lê Trọng Tấn, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8068329,
   "Latitude": 106.629403
 },
 {
   "STT": 802,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "443 Lê Trọng Tấn, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8089253,
   "Latitude": 106.615762
 },
 {
   "STT": 803,
   "Name": "Nhà thuốc Ngọc Phúc",
   "address": "99 Lê Trọng Tấn, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8075114,
   "Latitude": 106.6215479
 },
 {
   "STT": 804,
   "Name": "Nhà thuốc Phượng An",
   "address": "015F C/c KCN Tân Bình, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8084792,
   "Latitude": 106.6077886
 },
 {
   "STT": 805,
   "Name": "Nhà thuốc Gia An",
   "address": "27D Sơn Kỳ, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8028912,
   "Latitude": 106.6277412
 },
 {
   "STT": 806,
   "Name": "Nhà thuốc Nguyên Thảo",
   "address": "001 Lô B C/c Sơn Kỳ, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 53.7266683,
   "Latitude": -127.6476205
 },
 {
   "STT": 807,
   "Name": "Nhà thuốc Vy Anh",
   "address": "66 Nguyễn Súy, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7917172,
   "Latitude": 106.6224502
 },
 {
   "STT": 808,
   "Name": "Nhà thuốc Minh Phúc",
   "address": "225A Gò Dầu, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7960346,
   "Latitude": 106.616697
 },
 {
   "STT": 809,
   "Name": "Nhà thuốc Hòa Bình",
   "address": "219 Độc lập, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.793023,
   "Latitude": 106.6276947
 },
 {
   "STT": 810,
   "Name": "Nhà thuốc Công Hiệu II",
   "address": "184 Lê Thúc Hoạch, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7882942,
   "Latitude": 106.6190831
 },
 {
   "STT": 811,
   "Name": "Nhà thuốc Mai vân",
   "address": "350 Tân Hương, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7896398,
   "Latitude": 106.6171961
 },
 {
   "STT": 812,
   "Name": "Nhà thuốc Phương Trúc",
   "address": "100 Nguyễn Suý, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792377,
   "Latitude": 106.6225681
 },
 {
   "STT": 813,
   "Name": "Nhà thuốc Trà My",
   "address": "88 Nguyễn Suý, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7922582,
   "Latitude": 106.6224521
 },
 {
   "STT": 814,
   "Name": "Nhà thuốc Phương Mai",
   "address": "55 Tây Sơn, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.791257,
   "Latitude": 106.624283
 },
 {
   "STT": 815,
   "Name": "Nhà thuốc Anh Khoa",
   "address": "80 Tân Hương, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7898848,
   "Latitude": 106.6278111
 },
 {
   "STT": 816,
   "Name": "Nhà thuốc Hải Đăng",
   "address": "151A Tân Quý, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7946951,
   "Latitude": 106.6204393
 },
 {
   "STT": 817,
   "Name": "Nhà thuốc Việt Thành",
   "address": "371 Tân Kỳ Tân Quý, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.800433,
   "Latitude": 106.6218259
 },
 {
   "STT": 818,
   "Name": "Nhà thuốc Hồng Thắm",
   "address": "27 Tân Hương, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7911285,
   "Latitude": 106.6285247
 },
 {
   "STT": 819,
   "Name": "Nhà thuốc Ngọc Hạnh",
   "address": "509 Tân Kỳ Tân Quý, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.797455,
   "Latitude": 106.6148397
 },
 {
   "STT": 820,
   "Name": "Nhà thuốc Trang Minh 6",
   "address": "245 Tân Hương, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7896808,
   "Latitude": 106.6180906
 },
 {
   "STT": 821,
   "Name": "Nhà thuốc Tân Hương",
   "address": "80 Tân Hương, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7898848,
   "Latitude": 106.6278111
 },
 {
   "STT": 822,
   "Name": "Nhà thuốc Phước Thanh 3",
   "address": "310 Gò Dầu, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.795654,
   "Latitude": 106.615435
 },
 {
   "STT": 823,
   "Name": "Nhà thuốc Nguyệt Phương",
   "address": "27A Gò Dầu, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.794904,
   "Latitude": 106.627905
 },
 {
   "STT": 824,
   "Name": "Nhà thuốc Hương Thu",
   "address": "11A Gò Dầu, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7950098,
   "Latitude": 106.6290059
 },
 {
   "STT": 825,
   "Name": "Nhà thuốc Yến Châu 4",
   "address": "103-105 Trần Văn Ơn, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7995747,
   "Latitude": 106.6301339
 },
 {
   "STT": 826,
   "Name": "Nhà thuốc An Phước 2",
   "address": "96 Trần Văn Ơn, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7996331,
   "Latitude": 106.6298908
 },
 {
   "STT": 827,
   "Name": "Nhà thuốc Hoàng Trang",
   "address": "179 Tân Sơn Nhì, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7995844,
   "Latitude": 106.6324105
 },
 {
   "STT": 828,
   "Name": "Nhà thuốc Hoàng Phương",
   "address": "65 Tân Kỳ Tân Quý, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8030273,
   "Latitude": 106.6346381
 },
 {
   "STT": 829,
   "Name": "Nhà thuốc Xuân Mỹ",
   "address": "26/27 Tân Sơn Nhì, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.796388,
   "Latitude": 106.630744
 },
 {
   "STT": 830,
   "Name": "Nhà thuốc Phi Yến",
   "address": "271 Tân Sơn Nhì, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7973581,
   "Latitude": 106.6311293
 },
 {
   "STT": 831,
   "Name": "Nhà thuốc Số 35",
   "address": "875 Âu Cơ, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7986687,
   "Latitude": 106.6374729
 },
 {
   "STT": 832,
   "Name": "Nhà thuốc Anh Thư",
   "address": "152 Nguyễn Cửu Đàm, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8002124,
   "Latitude": 106.6283092
 },
 {
   "STT": 833,
   "Name": "Nhà thuốc Bảo Ngọc",
   "address": "49 Nguyễn Quý Anh, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8011575,
   "Latitude": 106.6278345
 },
 {
   "STT": 834,
   "Name": "Nhà thuốc Bích Vân",
   "address": "151 Tân Kỳ Tân Quý, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8026751,
   "Latitude": 106.6282191
 },
 {
   "STT": 835,
   "Name": "Nhà thuốc Bảo Thy",
   "address": "45 Trần Văn Ơn, Phường   Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8014977,
   "Latitude": 106.6309169
 },
 {
   "STT": 836,
   "Name": "Nhà thuốc Khang Châu",
   "address": "141 Thống Nhất, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7911151,
   "Latitude": 106.6319523
 },
 {
   "STT": 837,
   "Name": "Nhà thuốc Khang Phát",
   "address": "131 Độc Lập, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7923396,
   "Latitude": 106.6311912
 },
 {
   "STT": 838,
   "Name": "Nhà thuốc Thiện Minh",
   "address": "104 Trương Vĩnh Ký, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7942656,
   "Latitude": 106.6318004
 },
 {
   "STT": 839,
   "Name": "Nhà thuốc Phúc An Dược",
   "address": "42 Nguyễn Bà Tòng, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.791632,
   "Latitude": 106.6362941
 },
 {
   "STT": 840,
   "Name": "Nhà thuốc Hạnh Nguyên",
   "address": "6 Thống Nhất, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790527,
   "Latitude": 106.635015
 },
 {
   "STT": 841,
   "Name": "Nhà thuốc Hòa Bình",
   "address": "219 Độc lập, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.793023,
   "Latitude": 106.6276947
 },
 {
   "STT": 842,
   "Name": "Nhà thuốc Thanh Hiếu",
   "address": "75 Thống Nhất, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790554,
   "Latitude": 106.634796
 },
 {
   "STT": 843,
   "Name": "Nhà thuốc Thế Huy",
   "address": "80 Vườn Lài, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.787974,
   "Latitude": 106.632932
 },
 {
   "STT": 844,
   "Name": "Nhà thuốc Xuân Mai",
   "address": "122 Vườn Lài, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.786926,
   "Latitude": 106.63569
 },
 {
   "STT": 845,
   "Name": "Nhà thuốc Đức Hiếu",
   "address": "48 Độc Lập, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7919889,
   "Latitude": 106.63565
 },
 {
   "STT": 846,
   "Name": "Nhà thuốc Vườn Lài",
   "address": "787 Lũy Bán Bích, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7863107,
   "Latitude": 106.6365447
 },
 {
   "STT": 847,
   "Name": "Nhà thuốc Quan Tâm",
   "address": "94 Độc Lập, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7920782,
   "Latitude": 106.6348618
 },
 {
   "STT": 848,
   "Name": "Nhà thuốc Nhã Uyên",
   "address": "74 Cách Mạng, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7895605,
   "Latitude": 106.6337746
 },
 {
   "STT": 849,
   "Name": "Nhà thuốc Minh Trí",
   "address": "2E Vườn Lài, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.786763,
   "Latitude": 106.6360009
 },
 {
   "STT": 850,
   "Name": "Nhà thuốc Như Lan",
   "address": "56 Độc Lập, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7919968,
   "Latitude": 106.6355203
 },
 {
   "STT": 851,
   "Name": "Nhà thuốc Mai Linh",
   "address": "213 Độc Lập, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792981,
   "Latitude": 106.6279202
 },
 {
   "STT": 852,
   "Name": "Nhà thuốc Minh Hồng",
   "address": "94 Trương Vĩnh Ký, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7938498,
   "Latitude": 106.6338804
 },
 {
   "STT": 853,
   "Name": "Nhà thuốc Quốc Dũng",
   "address": "881 Lũy Bán Bích, Phường   Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7889184,
   "Latitude": 106.6370862
 },
 {
   "STT": 854,
   "Name": "Nhà thuốc Nguyên Khánh",
   "address": "159 Lý Thánh Tông, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7670039,
   "Latitude": 106.6254044
 },
 {
   "STT": 855,
   "Name": "Nhà thuốc Minh Anh",
   "address": "137 Lương Thế Vinh, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762193,
   "Latitude": 106.628971
 },
 {
   "STT": 856,
   "Name": "Nhà thuốc An Bình",
   "address": "151/141A Lũy Bán Bích, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.766655,
   "Latitude": 106.630465
 },
 {
   "STT": 857,
   "Name": "Nhà thuốc Thành Kiệt",
   "address": "40 Nguyễn Văn Yến, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7648352,
   "Latitude": 106.6248518
 },
 {
   "STT": 858,
   "Name": "Nhà thuốc Thanh Phương",
   "address": "47 Lương Thế Vinh, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.763888,
   "Latitude": 106.630567
 },
 {
   "STT": 859,
   "Name": "Nhà thuốc Minh Khoa",
   "address": "71 Lũy Bán Bích, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7616099,
   "Latitude": 106.6323459
 },
 {
   "STT": 860,
   "Name": "Nhà thuốc Trọng Vân",
   "address": "40 Lương Minh Nguyệt, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.761257,
   "Latitude": 106.630568
 },
 {
   "STT": 861,
   "Name": "Nhà thuốc Lộc An",
   "address": "140A Lũy Bán Bích, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.769363,
   "Latitude": 106.631936
 },
 {
   "STT": 862,
   "Name": "Nhà thuốc Yên Đan",
   "address": "32 Lũy Bán Bích, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.761514,
   "Latitude": 106.632599
 },
 {
   "STT": 863,
   "Name": "Nhà thuốc Tân Phú",
   "address": "153E Lũy Bán Bích, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7669219,
   "Latitude": 106.631728
 },
 {
   "STT": 864,
   "Name": "Nhà thuốc Mai Thảo",
   "address": "143 Lũy Bán Bích, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.765549,
   "Latitude": 106.63155
 },
 {
   "STT": 865,
   "Name": "Nhà thuốc Thúy Hiền",
   "address": "42D Lũy Bán Bích, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7631409,
   "Latitude": 106.632604
 },
 {
   "STT": 866,
   "Name": "Nhà thuốc Tuyết Nhung",
   "address": "200 Phan Anh, Phường   Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.765573,
   "Latitude": 106.6234302
 },
 {
   "STT": 867,
   "Name": "Nhà thuốc Thanh An",
   "address": "57 S11, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8138819,
   "Latitude": 106.6206939
 },
 {
   "STT": 868,
   "Name": "Nhà thuốc Kim Ngân",
   "address": "24A Lê Trọng Tấn, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8047838,
   "Latitude": 106.6319491
 },
 {
   "STT": 869,
   "Name": "Nhà thuốc Thiên Phú",
   "address": "3 C2, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8053496,
   "Latitude": 106.6473228
 },
 {
   "STT": 870,
   "Name": "Nhà thuốc Đức Duy",
   "address": "410B Lê Trọng Tấn, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8119303,
   "Latitude": 106.6096876
 },
 {
   "STT": 871,
   "Name": "Nhà thuốc KCN Tân Bình",
   "address": "Lô II-6, cụm 2 Lê Trọng Tấn, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8105596,
   "Latitude": 106.6126059
 },
 {
   "STT": 872,
   "Name": "Nhà thuốc Mỹ Trang",
   "address": "169 Tây Thạnh, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8103256,
   "Latitude": 106.6201046
 },
 {
   "STT": 873,
   "Name": "Nhà thuốc Khánh Quỳnh",
   "address": "152 Tây Thạnh, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8103187,
   "Latitude": 106.6200979
 },
 {
   "STT": 874,
   "Name": "Nhà thuốc Kim Châu",
   "address": "78 D11, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8122907,
   "Latitude": 106.6261831
 },
 {
   "STT": 875,
   "Name": "Nhà thuốc Tây Hồ",
   "address": "116B Tây Thạnh, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8135154,
   "Latitude": 106.6227488
 },
 {
   "STT": 876,
   "Name": "Nhà thuốc Hồng Phước",
   "address": "74 Lê Trọng Tấn, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.806699,
   "Latitude": 106.6256846
 },
 {
   "STT": 877,
   "Name": "Nhà thuốc Thiên Ân",
   "address": "420 Lê Trọng Tấn, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8087388,
   "Latitude": 106.6175259
 },
 {
   "STT": 878,
   "Name": "Nhà thuốc Thiên",
   "address": "71 D9, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8130374,
   "Latitude": 106.6261905
 },
 {
   "STT": 879,
   "Name": "Nhà thuốc Thiên Phúc",
   "address": "280D Lê Trọng Tấn, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8073823,
   "Latitude": 106.6226792
 },
 {
   "STT": 880,
   "Name": "Nhà thuốc Hoàng Anh",
   "address": "44 Dương Đức Hiền, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.808304,
   "Latitude": 106.627322
 },
 {
   "STT": 881,
   "Name": "Nhà thuốc Trâm Anh",
   "address": "202C Nguyễn Hữu Tiến, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8128397,
   "Latitude": 106.6231159
 },
 {
   "STT": 882,
   "Name": "Nhà thuốc Mỹ Hạnh",
   "address": "50 D9, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8133072,
   "Latitude": 106.6260076
 },
 {
   "STT": 883,
   "Name": "Nhà thuốc Việt An",
   "address": "007F C/c KCN Tân Bình, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.80528,
   "Latitude": 106.609316
 },
 {
   "STT": 884,
   "Name": "Nhà thuốc Hoàng Nguyên",
   "address": "80-82 D11, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8122907,
   "Latitude": 106.6261831
 },
 {
   "STT": 885,
   "Name": "Nhà thuốc Minh Vân",
   "address": "104 Tân Kỳ Tân Quý, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8036086,
   "Latitude": 106.6329286
 },
 {
   "STT": 886,
   "Name": "Nhà thuốc Băng Tâm",
   "address": "673 Trường Chinh, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8129114,
   "Latitude": 106.6327978
 },
 {
   "STT": 887,
   "Name": "Nhà thuốc Lâm Phương",
   "address": "121 Tây Thạnh, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8103062,
   "Latitude": 106.6200857
 },
 {
   "STT": 888,
   "Name": "Nhà thuốc Nhân Hòa",
   "address": "7 Hồ Đắc Di, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8058024,
   "Latitude": 106.6342925
 },
 {
   "STT": 889,
   "Name": "Nhà thuốc Nhân Đức",
   "address": "42 Nguyễn Hữu Tiến, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8095155,
   "Latitude": 106.6258927
 },
 {
   "STT": 890,
   "Name": "Nhà thuốc Thái Hà",
   "address": "C020 C/c lô C KCN Tân Bình, Phường   Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.80528,
   "Latitude": 106.609316
 },
 {
   "STT": 891,
   "Name": "Nhà thuốc Minh Tiên",
   "address": "209/96 Tôn Thất Thuyết, P3,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.755226,
   "Latitude": 106.698498
 },
 {
   "STT": 892,
   "Name": "Nhà thuốc Mỹ An",
   "address": "115 Khánh Hội, P3,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.755022,
   "Latitude": 106.703051
 },
 {
   "STT": 893,
   "Name": "Nhà thuốc Mỹ Châu",
   "address": "316 Tôn Đản, P4,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7557639,
   "Latitude": 106.70575
 },
 {
   "STT": 894,
   "Name": "Nhà thuốc Đào Tiến 5",
   "address": "256-260 Khánh Hội, P6,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7599765,
   "Latitude": 106.6982491
 },
 {
   "STT": 895,
   "Name": "Nhà thuốc Thảo Châu",
   "address": "15 Đường số 12A, P6,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759834,
   "Latitude": 106.699418
 },
 {
   "STT": 896,
   "Name": "Nhà thuốc Thảo Ly",
   "address": "122/27/118 Tôn Đản, P8,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758437,
   "Latitude": 106.7038929
 },
 {
   "STT": 897,
   "Name": "Nhà thuốc Anh Thy 2",
   "address": "005 Chung cư H1 Hoàng Diệu, P9,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762063,
   "Latitude": 106.701489
 },
 {
   "STT": 898,
   "Name": "Nhà thuốc Thiện Minh",
   "address": "006 Chung cư H1 Hoàng Diệu, P9,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762063,
   "Latitude": 106.701489
 },
 {
   "STT": 899,
   "Name": "Nhà thuốc Đại Phát",
   "address": "122 Tôn Đản, P10,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7596987,
   "Latitude": 106.7064484
 },
 {
   "STT": 900,
   "Name": "Nhà thuốc Gia Thành",
   "address": "164 Lê Quốc Hưng, P12,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.76335,
   "Latitude": 106.705554
 },
 {
   "STT": 901,
   "Name": "Nhà thuốc 129",
   "address": "473 Đoàn Văn Bơ, P13,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566947,
   "Latitude": 106.7148854
 },
 {
   "STT": 902,
   "Name": "Nhà thuốc Số 189",
   "address": "189 Đoàn Văn Bơ, P13,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7625375,
   "Latitude": 106.7051303
 },
 {
   "STT": 903,
   "Name": "Nhà thuốc Thảo Nhung",
   "address": "300A Nguyễn Tất Thành, P13,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7609859,
   "Latitude": 106.7102319
 },
 {
   "STT": 904,
   "Name": "Nhà thuốc Số 7",
   "address": "249 Đoàn Văn Bơ, P13,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7619261,
   "Latitude": 106.7061442
 },
 {
   "STT": 905,
   "Name": "Nhà thuốc Hồng Nhung",
   "address": "145 Tôn Đản, P14,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758491,
   "Latitude": 106.706819
 },
 {
   "STT": 906,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "212 Tôn Đản, P14,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.757805,
   "Latitude": 106.706378
 },
 {
   "STT": 907,
   "Name": "Nhà thuốc Thanh Dương",
   "address": "97 Xóm Chiếu, P16,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7584373,
   "Latitude": 106.710953
 },
 {
   "STT": 908,
   "Name": "Nhà thuốc Nguyệt",
   "address": "83 Xóm Chiếu, P16,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7585141,
   "Latitude": 106.7114697
 },
 {
   "STT": 909,
   "Name": "Nhà thuốc Duy",
   "address": "B163A Xóm Chiếu, P16,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7565374,
   "Latitude": 106.70979
 },
 {
   "STT": 910,
   "Name": "Nhà thuốc Tài Lộc",
   "address": "698 Đoàn Văn Bơ, P16,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566941,
   "Latitude": 106.7148871
 },
 {
   "STT": 911,
   "Name": "Nhà thuốc Mai Trâm",
   "address": "817 Đoàn Văn Bơ, P18,  Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566934,
   "Latitude": 106.7148887
 },
 {
   "STT": 912,
   "Name": "Nhà thuốc Mai Khôi II",
   "address": "C10/32 Đinh Đức Thiện ấp 3, xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6579083,
   "Latitude": 106.5815012
 },
 {
   "STT": 913,
   "Name": "Nhà thuốc Thái Ngân",
   "address": "C1/19 Phạm Hùng, ấp 4, xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7474738,
   "Latitude": 106.6690655
 },
 {
   "STT": 914,
   "Name": "Nhà thuốc Tuyết Minh",
   "address": "B13/29 Quốc lộ 50, ấp 3A, xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7401411,
   "Latitude": 106.6565339
 },
 {
   "STT": 915,
   "Name": "Nhà thuốc Mai Châu",
   "address": "C5/14 Phạm Hùng, ấp 4A,  xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7305656,
   "Latitude": 106.6755587
 },
 {
   "STT": 916,
   "Name": "Nhà thuốc Minh Hiếu",
   "address": "C4/1 Phạm Hùng, ấp 4, xã Bình hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7322833,
   "Latitude": 106.6747605
 },
 {
   "STT": 917,
   "Name": "Nhà thuốc Thanh Hải",
   "address": "A22/24 Quốc lộ 50 ấp 1A,  xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.725251,
   "Latitude": 106.6559737
 },
 {
   "STT": 918,
   "Name": "Nhà thuốc Tâm Châu",
   "address": "C2/12A Phạm Hùng, ấp 4,  xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7299546,
   "Latitude": 106.67703
 },
 {
   "STT": 919,
   "Name": "Nhà thuốc Nhân Ái",
   "address": "D10/40 Đoàn Nguyễn Tuấn ấp 4, xã Hưng Long, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6579417,
   "Latitude": 106.6081706
 },
 {
   "STT": 920,
   "Name": "Nhà thuốc An Vinh",
   "address": "E11/56 ấp 5 Đường An Phú Tây - Hưng Long, xã Hưng Long, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6689434,
   "Latitude": 106.6130884
 },
 {
   "STT": 921,
   "Name": "Nhà thuốc An Thái",
   "address": "3A92 Thanh Niên ấp 3,  xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924119,
   "Latitude": 106.5151173
 },
 {
   "STT": 922,
   "Name": "Nhà thuốc Phú Lạc",
   "address": "B5/126 Quốc lộ 50, ấp 2, xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7382505,
   "Latitude": 106.6562118
 },
 {
   "STT": 923,
   "Name": "Nhà thuốc Sinh Đôi II",
   "address": "D1/8-D1/8A Quốc lộ 50, ấp 4, xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.740445,
   "Latitude": 106.6563363
 },
 {
   "STT": 924,
   "Name": "Nhà thuốc Nguyên Khang",
   "address": "E3/80 Quốc lộ 50, ấp 5, xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.739056,
   "Latitude": 106.6561094
 },
 {
   "STT": 925,
   "Name": "Nhà thuốc Nhân Đức",
   "address": "A1/13A Nguyễn Cửu Phú, ấp 1, xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7165741,
   "Latitude": 106.5813709
 },
 {
   "STT": 926,
   "Name": "Nhà thuốc Hồng Ân",
   "address": "D16/10B Nguyễn Cửu Phú, ấp 4,  xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6970437,
   "Latitude": 106.579826
 },
 {
   "STT": 927,
   "Name": "Nhà thuốc Minh Dung 1",
   "address": "B6/5A Đường Trần Đại Nghĩa, ấp 2,  xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6992717,
   "Latitude": 106.5897629
 },
 {
   "STT": 928,
   "Name": "Nhà thuốc Thanh Lam",
   "address": "B22/463K ấp 2, xã Tân Nhựt, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7376457,
   "Latitude": 106.5479036
 },
 {
   "STT": 929,
   "Name": "Nhà thuốc Châu Ân",
   "address": "A5B/159B Trần Đại Nghĩa, ấp 1, xã Tân Nhựt, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7270599,
   "Latitude": 106.5899202
 },
 {
   "STT": 930,
   "Name": "Nhà thuốc Minh Phương",
   "address": "B23/474H ấp 2 Trần Đại Nghĩa, xã Tân Nhựt, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7383279,
   "Latitude": 106.5510049
 },
 {
   "STT": 931,
   "Name": "Nhà thuốc Thiện Trí",
   "address": "13/6A ấp 4, xã Tân Quý Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.666887,
   "Latitude": 106.5967126
 },
 {
   "STT": 932,
   "Name": "Nhà thuốc Vy Anh 2",
   "address": "15/3B Đoàn Nguyễn Tuân, ấp 2, xã Tân Quý Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6624836,
   "Latitude": 106.5993364
 },
 {
   "STT": 933,
   "Name": "Nhà thuốc Tiên Nga",
   "address": "A9/11 Hương lộ 11 ấp 1,  xã Tân Quý Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6579193,
   "Latitude": 106.5901333
 },
 {
   "STT": 934,
   "Name": "Nhà thuốc Minh Đức",
   "address": "A13/7B Nguyễn Hữu Trí, khu phố 1, Thị trấn Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6936183,
   "Latitude": 106.5825051
 },
 {
   "STT": 935,
   "Name": "Nhà thuốc Thanh Loan",
   "address": "C8/7C Bùi Thanh Khiết, khu phố 3, Thị trấn Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6897113,
   "Latitude": 106.574692
 },
 {
   "STT": 936,
   "Name": "Nhà thuốc Hải Phong",
   "address": "E10/23 Đường Thới Hòa, ấp 5,  xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8313006,
   "Latitude": 106.5713571
 },
 {
   "STT": 937,
   "Name": "Nhà thuốc Phương Như",
   "address": "F9/9 Ấp 6, xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.818282,
   "Latitude": 106.5681569
 },
 {
   "STT": 938,
   "Name": "Nhà thuốc Hoàng Huy",
   "address": "B7/6 Đường Liên ấp 6-2 ấp 2A,  xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939573,
   "Latitude": 106.5787749
 },
 {
   "STT": 939,
   "Name": "Nhà thuốc Hoàng Hà",
   "address": "A5/3K Ấp 1, xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.80388,
   "Latitude": 106.5531564
 },
 {
   "STT": 940,
   "Name": "Nhà thuốc Quang Học",
   "address": "C5/1T Đường Nữ Dân Công, ấp 3,  xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8320926,
   "Latitude": 106.5634782
 },
 {
   "STT": 941,
   "Name": "Nhà thuốc Ngọc Mai",
   "address": "E12/18A1 Đường Thới Hòa, ấp 5, xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8314163,
   "Latitude": 106.5723406
 },
 {
   "STT": 942,
   "Name": "Nhà thuốc Ngọc Lan",
   "address": "B1/8B Ấp 2, xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939573,
   "Latitude": 106.5787749
 },
 {
   "STT": 943,
   "Name": "Nhà thuốc Phước Lộc",
   "address": "F1/60T Quách Điêu, xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.832747,
   "Latitude": 106.570025
 },
 {
   "STT": 944,
   "Name": "Nhà thuốc Quang Huy 6",
   "address": "F2/39A Đường Liên ấp 2-6 ấp 6, xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8181617,
   "Latitude": 106.5705809
 },
 {
   "STT": 945,
   "Name": "Nhà thuốc Hồng Sáng",
   "address": "D20/4D Đường Võ Văn Vân, ấp 4, xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8108766,
   "Latitude": 106.578687
 },
 {
   "STT": 946,
   "Name": "Nhà thuốc Nhật Quỳnh 2",
   "address": "A5/43Q Đường 1A ấp 1A, xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7781128,
   "Latitude": 106.5750148
 },
 {
   "STT": 947,
   "Name": "Nhà thuốc Phúc Khang",
   "address": "B10/13 Võ Văn Vân ấp 2,  xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7642382,
   "Latitude": 106.5682164
 },
 {
   "STT": 948,
   "Name": "Nhà thuốc Nguyên Khang",
   "address": "B12A/1 Đường liên ấp 1-2-3 ấp 2, xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.782102,
   "Latitude": 106.5803956
 },
 {
   "STT": 949,
   "Name": "Nhà thuốc Ngọc Diễm",
   "address": "B11/12 Võ Văn Vân, ấp 2,  xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8098231,
   "Latitude": 106.5788559
 },
 {
   "STT": 950,
   "Name": "Nhà thuốc Hoàng Nghĩa",
   "address": "14, Tổ 1, KP7 Quốc lộ 1A, Phường   Tân Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8408959,
   "Latitude": 106.6237652
 },
 {
   "STT": 951,
   "Name": "Nhà thuốc An Khang",
   "address": "162/4, tổ 5, kp1  Lê Văn Khương, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8768074,
   "Latitude": 106.6490245
 },
 {
   "STT": 952,
   "Name": "Nhà thuốc An Lạc",
   "address": "194, tổ 68, kp 6 Phan Văn Hớn, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8280885,
   "Latitude": 106.6208057
 },
 {
   "STT": 953,
   "Name": "Nhà thuốc An Phước",
   "address": "11, Kp6 Trường Chinh, Phường   Tân Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8408959,
   "Latitude": 106.6237652
 },
 {
   "STT": 954,
   "Name": "Nhà thuốc Anh Đào",
   "address": "648  Nguyễn Ảnh Thủ, Tổ 46, KP4, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.847424,
   "Latitude": 106.6007091
 },
 {
   "STT": 955,
   "Name": "Nhà thuốc Anh Đoan",
   "address": "640, kp1 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8420845,
   "Latitude": 106.6305882
 },
 {
   "STT": 956,
   "Name": "Nhà thuốc Anh Huy",
   "address": "127 Đường TTN 08, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8316489,
   "Latitude": 106.6195628
 },
 {
   "STT": 957,
   "Name": "Nhà thuốc Anh Tú",
   "address": "243 Trường Chinh, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8341762,
   "Latitude": 106.620516
 },
 {
   "STT": 958,
   "Name": "Nhà thuốc Băng Châu",
   "address": "65A, tổ 20, kp 2 Đường ĐHT 11, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.844945,
   "Latitude": 106.6295368
 },
 {
   "STT": 959,
   "Name": "Nhà thuốc Băng Tâm",
   "address": "207 , tổ 5, kp 5 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8387328,
   "Latitude": 106.6301036
 },
 {
   "STT": 960,
   "Name": "Nhà thuốc Bảo Chi",
   "address": "6, tổ 3, kp 1 Đường TL40, Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8571802,
   "Latitude": 106.6448606
 },
 {
   "STT": 961,
   "Name": "Nhà thuốc Bảo Minh",
   "address": "45, tổ 5, kp 3 Đường TTH 01, Phường   Tân Thới Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8307416,
   "Latitude": 106.6056595
 },
 {
   "STT": 962,
   "Name": "Nhà thuốc Bảo Trân",
   "address": "68 Đường ĐHT 10, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8429633,
   "Latitude": 106.6279864
 },
 {
   "STT": 963,
   "Name": "Nhà thuốc Bình Minh",
   "address": "40/7A Tô Ký, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8668981,
   "Latitude": 106.6155982
 },
 {
   "STT": 964,
   "Name": "Nhà thuốc Bình Phước",
   "address": "239/2 Đường TA 19, Phường   Thới An,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8633623,
   "Latitude": 106.652855
 },
 {
   "STT": 965,
   "Name": "Nhà thuốc Cát Tường",
   "address": "171 QL1A, KP3, Phường   An Phú Đông,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8612433,
   "Latitude": 106.6855204
 },
 {
   "STT": 966,
   "Name": "Nhà thuốc Châu Thy",
   "address": "576, tổ 1, kp 3 Lê Văn Khương, Phường   Thới An,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9075653,
   "Latitude": 106.644091
 },
 {
   "STT": 967,
   "Name": "Nhà thuốc Chi Mai",
   "address": "24/1C Quốc Lộ 1A KP1, Phường   An Phú Đông,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8593246,
   "Latitude": 106.7064524
 },
 {
   "STT": 968,
   "Name": "Nhà thuốc Cộng Hòa",
   "address": "39 Phan Văn Hớn, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8275974,
   "Latitude": 106.6222918
 },
 {
   "STT": 969,
   "Name": "Nhà thuốc Đại Lộc ",
   "address": "47A/4, kp2, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8552617,
   "Latitude": 106.641516
 },
 {
   "STT": 970,
   "Name": "Nhà thuốc Đại Phúc 4",
   "address": "307 Đường TCH 13, Phường   Tân Chánh Hiêp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8655303,
   "Latitude": 106.6216493
 },
 {
   "STT": 971,
   "Name": "Nhà thuốc Đại Phương",
   "address": "71, tổ 38, kp3  Bùi Văn Ngữ, Phường   Tân Chánh Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.873886,
   "Latitude": 106.634963
 },
 {
   "STT": 972,
   "Name": "Nhà thuốc Đại Thành Thái",
   "address": "86,tổ 6, kp1 Đường TL 30, Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8801208,
   "Latitude": 106.6812697
 },
 {
   "STT": 973,
   "Name": "Nhà thuốc Đăng nguyên",
   "address": "285, khu phố 3 Đường TA 32, Phường   Thới an,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8307416,
   "Latitude": 106.6056595
 },
 {
   "STT": 974,
   "Name": "Nhà thuốc Diệp Linh",
   "address": "648 Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8792076,
   "Latitude": 106.6266103
 },
 {
   "STT": 975,
   "Name": "Nhà thuốc Diệu Anh",
   "address": "MP nhà số 37/6 Tổ 30, KP3A, Phường   Tân Thới Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8603672,
   "Latitude": 106.6438673
 },
 {
   "STT": 976,
   "Name": "Nhà thuốc Đức Hải",
   "address": "15/14A, tổ 61, kp 5 Phan Văn Hớn, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8279297,
   "Latitude": 106.6119452
 },
 {
   "STT": 977,
   "Name": "Nhà thuốc Đức Hạnh",
   "address": "66 Quốc lộ 22, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.84619,
   "Latitude": 106.6119
 },
 {
   "STT": 978,
   "Name": "Nhà thuốc Đức Huy",
   "address": "495 (số cũ 84H), kp 3 Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8765366,
   "Latitude": 106.6466116
 },
 {
   "STT": 979,
   "Name": "Nhà thuốc Đức Minh",
   "address": "52/5A Tổ 20, Khu phố 2, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8457457,
   "Latitude": 106.6322541
 },
 {
   "STT": 980,
   "Name": "Nhà thuốc Duy Anh",
   "address": "218 , tổ 9, kp 3 Hà Huy Giáp, Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8643634,
   "Latitude": 106.6799846
 },
 {
   "STT": 981,
   "Name": "Nhà thuốc Gia Bảo",
   "address": "165 Đường TA 19, Phường   Thới an,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8636213,
   "Latitude": 106.6520659
 },
 {
   "STT": 982,
   "Name": "Nhà thuốc Giáng Sinh",
   "address": "474 Trường Chinh KP6, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8408959,
   "Latitude": 106.6237652
 },
 {
   "STT": 983,
   "Name": "Nhà thuốc Hải An",
   "address": "245, kp 2A Lê Thị Riêng, Phường   Thới An,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8763878,
   "Latitude": 106.6549815
 },
 {
   "STT": 984,
   "Name": "Nhà thuốc Hân Châu",
   "address": "32 Đường TTN 13 C, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8318001,
   "Latitude": 106.6185075
 },
 {
   "STT": 985,
   "Name": "Nhà thuốc Hàng Sao",
   "address": "22A  Nguyễn Văn Quá , Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.837528,
   "Latitude": 106.629445
 },
 {
   "STT": 986,
   "Name": "Nhà thuốc Hiệp Thành",
   "address": "202, tổ 19, kp 2 Đường HT17, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8746315,
   "Latitude": 106.6399843
 },
 {
   "STT": 987,
   "Name": "Nhà thuốc Hiếu Minh II",
   "address": "84/5, kp 2 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8374723,
   "Latitude": 106.6293622
 },
 {
   "STT": 988,
   "Name": "Nhà thuốc Hoa Tranh",
   "address": "795/3E, tổ 52, kp 3 Tổ 52, KP3, Phường   An Phú Đông,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8658702,
   "Latitude": 106.7021973
 },
 {
   "STT": 989,
   "Name": "Nhà thuốc Hoài Sơn",
   "address": "288A Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8406102,
   "Latitude": 106.6300897
 },
 {
   "STT": 990,
   "Name": "Nhà thuốc Hoàng Anh",
   "address": "29/8 Phan Văn Hớn, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.827368,
   "Latitude": 106.6245138
 },
 {
   "STT": 991,
   "Name": "Nhà thuốc Hoàng Diễm",
   "address": "211A Lê Văn Khương, Phường   Hiệp Thành.,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.884925,
   "Latitude": 106.6480098
 },
 {
   "STT": 992,
   "Name": "Nhà thuốc Hoàng Hà",
   "address": "20 Tổ 49, Khu phố 3, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8307416,
   "Latitude": 106.6056595
 },
 {
   "STT": 993,
   "Name": "Nhà thuốc Hoàng Minh",
   "address": "67, tổ 73, kp 6 Đường TTN 05, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8292884,
   "Latitude": 106.6143944
 },
 {
   "STT": 994,
   "Name": "Nhà thuốc Hoàng Ngân",
   "address": "1058 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8519072,
   "Latitude": 106.6377385
 },
 {
   "STT": 995,
   "Name": "Nhà thuốc Hoàng Ngọc",
   "address": "6A, kp 3 Đường TTN 01, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8315949,
   "Latitude": 106.6199562
 },
 {
   "STT": 996,
   "Name": "Nhà thuốc Hoàng Oanh",
   "address": "B116 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8420845,
   "Latitude": 106.6305882
 },
 {
   "STT": 997,
   "Name": "Nhà thuốc Hoàng Thương",
   "address": "6 Tổ 30, Khu phố 4, Phường   Tân Chánh Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8673665,
   "Latitude": 106.6259368
 },
 {
   "STT": 998,
   "Name": "Nhà thuốc Hồng Đức",
   "address": "464, tổ 19, kp3 Đường TTH 02, Phường   Tân Thới Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8678205,
   "Latitude": 106.6372704
 },
 {
   "STT": 999,
   "Name": "Nhà thuốc Hồng Nga",
   "address": "5C11, tổ 6. kp 3 Khu Nam long, Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8584138,
   "Latitude": 106.6773093
 },
 {
   "STT": 1000,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "157A, kp 2 HT 17, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8764908,
   "Latitude": 106.6415784
 },
 {
   "STT": 1001,
   "Name": "Nhà thuốc Hồng Nhân",
   "address": "2M/L Đường TMT 13, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8585961,
   "Latitude": 106.6174425
 },
 {
   "STT": 1002,
   "Name": "Nhà thuốc Hồng Nhung",
   "address": "887 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8517546,
   "Latitude": 106.6374855
 },
 {
   "STT": 1003,
   "Name": "Nhà thuốc Hồng Thiện",
   "address": "28I/2, kp3 Đường HT 13, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.879573,
   "Latitude": 106.63825
 },
 {
   "STT": 1004,
   "Name": "Nhà thuốc Hùng Trang",
   "address": "42/7, tổ 8, kp 1 Đường TTN 01, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8315949,
   "Latitude": 106.6199562
 },
 {
   "STT": 1005,
   "Name": "Nhà thuốc Hương Toàn 1",
   "address": "387A, tổ 18, kp 1 Tô Ngọc Vân, Phường   Thạnh Xuân,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8571224,
   "Latitude": 106.6707232
 },
 {
   "STT": 1006,
   "Name": "Nhà thuốc Huyền Trinh",
   "address": "65D/97 Tổ 10, KP1, Phường   Tân Thới Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8560717,
   "Latitude": 106.6346978
 },
 {
   "STT": 1007,
   "Name": "Nhà thuốc Khánh Băng 2",
   "address": "493, kp 2 Lê Văn Khương, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9075934,
   "Latitude": 106.6440947
 },
 {
   "STT": 1008,
   "Name": "Nhà thuốc Kim Hồng",
   "address": "24/1C Quốc lộ 1A, Phường   An Phú Đông,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8607868,
   "Latitude": 106.7147761
 },
 {
   "STT": 1009,
   "Name": "Nhà thuốc Long Phụng ",
   "address": "28B2, tổ 6, kp 3 Khu Nam long, Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8584138,
   "Latitude": 106.6773093
 },
 {
   "STT": 1010,
   "Name": "Nhà thuốc Mai Lan",
   "address": "348c, tổ 18, kp 3B Hà Huy Giáp, Phường   Thạnh Lôc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8443112,
   "Latitude": 106.630882
 },
 {
   "STT": 1011,
   "Name": "Nhà thuốc Mai Phương",
   "address": "510 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8398122,
   "Latitude": 106.6301205
 },
 {
   "STT": 1012,
   "Name": "Nhà thuốc Mạnh Thắng",
   "address": "5/5 , kp 2 Hà Huy Giáp , Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.877329,
   "Latitude": 106.678437
 },
 {
   "STT": 1013,
   "Name": "Nhà thuốc Minh Diệu",
   "address": "5 Đường TMT 13, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8584066,
   "Latitude": 106.6175284
 },
 {
   "STT": 1014,
   "Name": "Nhà thuốc Minh Đức",
   "address": "43 Phan Văn Hớn, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8285108,
   "Latitude": 106.619276
 },
 {
   "STT": 1015,
   "Name": "Nhà thuốc Minh Hải",
   "address": "88A Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.881575,
   "Latitude": 106.6326087
 },
 {
   "STT": 1016,
   "Name": "Nhà thuốc Minh Hoàng",
   "address": "29 Vườn Lài, KP1, Phường   An Phú Đông,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8596467,
   "Latitude": 106.6920772
 },
 {
   "STT": 1017,
   "Name": "Nhà thuốc Minh Khang",
   "address": "131/4, kp3 Đường TA 10, Phường   Thới an,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8861206,
   "Latitude": 106.6516863
 },
 {
   "STT": 1018,
   "Name": "Nhà thuốc Minh Phát",
   "address": "98, tổ 8, kp6 Đường HT 16, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8807645,
   "Latitude": 106.6819287
 },
 {
   "STT": 1019,
   "Name": "Nhà thuốc Minh Phụng",
   "address": "602/1 Trường Chinh, Tổ 22, KP7, Phường   Tân Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7928192,
   "Latitude": 106.6528481
 },
 {
   "STT": 1020,
   "Name": "Nhà thuốc Minh Thiện",
   "address": "32B . Kp2 Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.877136,
   "Latitude": 106.641856
 },
 {
   "STT": 1021,
   "Name": "Nhà thuốc Minh Thư",
   "address": "8/4A Ta Đường TA 32, Phường   Thới an,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8806319,
   "Latitude": 106.6509151
 },
 {
   "STT": 1022,
   "Name": "Nhà thuốc Minh Trí",
   "address": "134/1 Tổ 17, KP2, Phường   Tân Chánh Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.85764,
   "Latitude": 106.626084
 },
 {
   "STT": 1023,
   "Name": "Nhà thuốc Minh Trọng",
   "address": "24,   tổ 50 Đường TTN 13, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8318089,
   "Latitude": 106.6184459
 },
 {
   "STT": 1024,
   "Name": "Nhà thuốc Minh Vy",
   "address": "A52, kp 2 Tô Ký, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8544331,
   "Latitude": 106.6250826
 },
 {
   "STT": 1025,
   "Name": "Nhà thuốc Mỹ Dung",
   "address": "190, kp 6 Tô Ngọc Vân , Phường   Thạnh Xuân,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8571224,
   "Latitude": 106.6707232
 },
 {
   "STT": 1026,
   "Name": "Nhà thuốc Mỹ Thuận",
   "address": "568 Tổ 18, KP1 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8443112,
   "Latitude": 106.630882
 },
 {
   "STT": 1027,
   "Name": "Nhà thuốc Ngọc Diệp",
   "address": "7/1 Tổ 35, Khu phố 3, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8307416,
   "Latitude": 106.6056595
 },
 {
   "STT": 1028,
   "Name": "Nhà thuốc Ngọc Hằng",
   "address": "787, tổ 5, kp 1 Quốc lộ 1A, Phường   Thạnh Xuân,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8614409,
   "Latitude": 106.6798573
 },
 {
   "STT": 1029,
   "Name": "Nhà thuốc Ngọc Huyền",
   "address": "Mp số nhà 5E/3 Đường HT 37, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8845605,
   "Latitude": 106.6359311
 },
 {
   "STT": 1030,
   "Name": "Nhà thuốc Ngọc Lan",
   "address": "124/4B Trường Chinh, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8344539,
   "Latitude": 106.62006
 },
 {
   "STT": 1031,
   "Name": "Nhà thuốc Ngọc Thành",
   "address": "257 Đường ĐHT 02, Phường   Tân Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8408959,
   "Latitude": 106.6237652
 },
 {
   "STT": 1032,
   "Name": "Nhà thuốc Ngọc Thủy",
   "address": "18/1, kp 3 Trường Chinh, Phường   Tân Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8408959,
   "Latitude": 106.6237652
 },
 {
   "STT": 1033,
   "Name": "Nhà thuốc Nguyễn Nam",
   "address": "15, tổ 21, kp5 Đường ĐHT 03, Phường   Tân Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8303268,
   "Latitude": 106.626935
 },
 {
   "STT": 1034,
   "Name": "Nhà thuốc Nguyên Triều",
   "address": "4D , tổ 40 Quốc lộ 22, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8474893,
   "Latitude": 106.6124117
 },
 {
   "STT": 1035,
   "Name": "Nhà thuốc Nguyệt Minh",
   "address": "1/B, tổ 31, kp 3 Nguyễn Ảnh Thủ- HL 80, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8765366,
   "Latitude": 106.6466116
 },
 {
   "STT": 1036,
   "Name": "Nhà thuốc Nhà thuốc 40",
   "address": "TTN 08, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8265622,
   "Latitude": 106.6090546
 },
 {
   "STT": 1037,
   "Name": "Nhà thuốc Nhà thuốc số 10",
   "address": "Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8420845,
   "Latitude": 106.6305882
 },
 {
   "STT": 1038,
   "Name": "Nhà thuốc Nhân Tâm",
   "address": "1/4, tổ 8, kp 2 Hà Huy Giáp, Phường   Thạnh Lôc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8643634,
   "Latitude": 106.6799846
 },
 {
   "STT": 1039,
   "Name": "Nhà thuốc Nhân Văn",
   "address": "257 , tổ 13, kp 2 Đường TTH 22, Phường   Tân Thới Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8571802,
   "Latitude": 106.6448606
 },
 {
   "STT": 1040,
   "Name": "Nhà thuốc Nhật Vy",
   "address": "73, TỔ 14, KP 6 Đường ĐHT 39, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8318498,
   "Latitude": 106.6277048
 },
 {
   "STT": 1041,
   "Name": "Nhà thuốc Như Phúc",
   "address": "C3, kp 2 Đường HT 13, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8724614,
   "Latitude": 106.6346542
 },
 {
   "STT": 1042,
   "Name": "Nhà thuốc Như Quân",
   "address": "57/4 Tổ 30, Khu phố 3, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8307416,
   "Latitude": 106.6056595
 },
 {
   "STT": 1043,
   "Name": "Nhà thuốc Pháp Việt",
   "address": "126, tổ 7, kp3 Hà Huy Giáp, Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8643634,
   "Latitude": 106.6799846
 },
 {
   "STT": 1044,
   "Name": "Nhà thuốc Phong Lan 2",
   "address": "682, tổ 4, kp 1 Hà Huy Giáp, Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8845315,
   "Latitude": 106.6815445
 },
 {
   "STT": 1045,
   "Name": "Nhà thuốc Phú Châu",
   "address": "473, tổ 5, kp 1 Trường Chinh, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8331216,
   "Latitude": 106.6209519
 },
 {
   "STT": 1046,
   "Name": "Nhà thuốc Phước An",
   "address": "30/3A , tổ 9, kp 1 Lê Văn Khương, Phường   Thới An,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8921951,
   "Latitude": 106.6475281
 },
 {
   "STT": 1047,
   "Name": "Nhà thuốc Phước Bình",
   "address": "27A, tổ 20, kp 2 Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8777859,
   "Latitude": 106.6514775
 },
 {
   "STT": 1048,
   "Name": "Nhà thuốc Phước Hiệp",
   "address": "2A, tổ 46, kp4 Đường TMT, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.856572,
   "Latitude": 106.614148
 },
 {
   "STT": 1049,
   "Name": "Nhà thuốc Phước Lộc",
   "address": "112/2, tổ 11, kp 2 Đường TA16, Phường   Thới An,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.900764,
   "Latitude": 106.691323
 },
 {
   "STT": 1050,
   "Name": "Nhà thuốc Phước Thanh II",
   "address": "704, kp 2 Tô Ký, Phường   Tân Chánh Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8707544,
   "Latitude": 106.614654
 },
 {
   "STT": 1051,
   "Name": "Nhà thuốc Phước Thiện",
   "address": "61/4A, tổ 7, kp 3 Lê Văn Khương, Phường   Thới An,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9075653,
   "Latitude": 106.644091
 },
 {
   "STT": 1052,
   "Name": "Nhà thuốc Phước Thuận",
   "address": "219M  Nguyễn Ảnh Thủ, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8603052,
   "Latitude": 106.6104846
 },
 {
   "STT": 1053,
   "Name": "Nhà thuốc Phương Anh",
   "address": "295B, tổ 11, kp 2 Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8775711,
   "Latitude": 106.6514973
 },
 {
   "STT": 1054,
   "Name": "Nhà thuốc Phương Duyên",
   "address": "958 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8498258,
   "Latitude": 106.636658
 },
 {
   "STT": 1055,
   "Name": "Nhà thuốc Phượng Hoàng",
   "address": "940/4A Đường HT 39, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8729341,
   "Latitude": 106.6347549
 },
 {
   "STT": 1056,
   "Name": "Nhà thuốc Phương Nam",
   "address": "90A, tổ 11, kp 6 Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9022802,
   "Latitude": 106.6896958
 },
 {
   "STT": 1057,
   "Name": "Nhà thuốc Phương Quỳnh",
   "address": "233 Tổ 14 Khu phố 3, Phường   Tân Thới Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8307416,
   "Latitude": 106.6056595
 },
 {
   "STT": 1058,
   "Name": "Nhà thuốc Phương Thảo",
   "address": "87 , tổ 8, kp 5 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8807645,
   "Latitude": 106.6819287
 },
 {
   "STT": 1059,
   "Name": "Nhà thuốc Phương Thúy",
   "address": "04 Đường TMT 13, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.854243,
   "Latitude": 106.6144257
 },
 {
   "STT": 1060,
   "Name": "Nhà thuốc Phương Yến",
   "address": "105/1A , tổ 1, kp 3 Lê Văn Khương, Phường   Thới An,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8628537,
   "Latitude": 106.6499287
 },
 {
   "STT": 1061,
   "Name": "Nhà thuốc Phương Yến 2",
   "address": "688 TA 28, tổ 9 Đường TA 28, Phường   Thới An,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8643689,
   "Latitude": 106.6589695
 },
 {
   "STT": 1062,
   "Name": "Nhà thuốc Minh Hương",
   "address": "39-Kp1 Đường HT 37, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.851483,
   "Latitude": 106.7248601
 },
 {
   "STT": 1063,
   "Name": "Nhà thuốc Phuương Linh",
   "address": "311, TCH 02 Kp11, Phường   Tân Chánh Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8767235,
   "Latitude": 106.6406636
 },
 {
   "STT": 1064,
   "Name": "Nhà thuốc Quang minh",
   "address": "30/2,tổ 24 Đường TMT 13, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8535846,
   "Latitude": 106.6138962
 },
 {
   "STT": 1065,
   "Name": "Nhà thuốc Quang Trung",
   "address": "19M/2 , kp 1 Tô Ký, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8668213,
   "Latitude": 106.6153384
 },
 {
   "STT": 1066,
   "Name": "Nhà thuốc Quang Tuấn",
   "address": "13/7, tổ 55, kp 9 Đường TCH 13, Phường   Tân Chánh Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8655303,
   "Latitude": 106.6216493
 },
 {
   "STT": 1067,
   "Name": "Nhà thuốc Quốc Trọng",
   "address": "454 tổ 15, kp 1 Hà Huy Giáp, Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8904513,
   "Latitude": 106.6859525
 },
 {
   "STT": 1068,
   "Name": "Nhà thuốc Quý Châu",
   "address": "53/7 Tổ 18, Khu phố 2, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8457457,
   "Latitude": 106.6322541
 },
 {
   "STT": 1069,
   "Name": "Nhà thuốc Quỳnh Anh I",
   "address": "146, tổ 10, kp 1 Lê Văn Khương, Phường   Thới An,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8921951,
   "Latitude": 106.6475281
 },
 {
   "STT": 1070,
   "Name": "Nhà thuốc Quỳnh Hương",
   "address": "73H, tổ 29, kp 3 Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8765366,
   "Latitude": 106.6466116
 },
 {
   "STT": 1071,
   "Name": "Nhà thuốc Quỳnh Như",
   "address": "321, kp 3 Đường HT 13, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.879431,
   "Latitude": 106.638155
 },
 {
   "STT": 1072,
   "Name": "Nhà thuốc Riết Thân",
   "address": "C95 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8420845,
   "Latitude": 106.6305882
 },
 {
   "STT": 1073,
   "Name": "Nhà thuốc Số 499",
   "address": "499, kp 2 Tô Ký, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8520202,
   "Latitude": 106.6264987
 },
 {
   "STT": 1074,
   "Name": "Nhà thuốc Số 8",
   "address": "352 Tổ 26, Khu phố 2, Phường   Tân Chánh Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8547775,
   "Latitude": 106.6178219
 },
 {
   "STT": 1075,
   "Name": "Nhà thuốc Sơn ca",
   "address": "583, tổ 11,kp1 Tô ký, Phường   Trung Mỹ Tây,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8561683,
   "Latitude": 106.6326731
 },
 {
   "STT": 1076,
   "Name": "Nhà thuốc Sông Trà 2",
   "address": "359, tổ 2, kp 2  Hà Huy Giáp , Phường   Thạnh Xuân,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8643634,
   "Latitude": 106.6799846
 },
 {
   "STT": 1077,
   "Name": "Nhà thuốc Tâm Phúc",
   "address": "66H Tổ 33, KP3, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8802193,
   "Latitude": 106.638769
 },
 {
   "STT": 1078,
   "Name": "Nhà thuốc Tân Hưng Thuận",
   "address": "55,  tổ 12, kp 5 Đường ĐHT 02, Phường   Tân Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.83903,
   "Latitude": 106.6207
 },
 {
   "STT": 1079,
   "Name": "Nhà thuốc Tân Tiến",
   "address": "849, tổ 48, kp 4 Nguyễn Ảnh Thủ, Phường   Tân Chánh Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8646497,
   "Latitude": 106.6135362
 },
 {
   "STT": 1080,
   "Name": "Nhà thuốc Thái Nhân",
   "address": "559 , tổ 20, kp 1 Đường TTH 02, Phường   Tân Thới Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8590424,
   "Latitude": 106.6333375
 },
 {
   "STT": 1081,
   "Name": "Nhà thuốc Thanh Bình",
   "address": "268A, kp 2 Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8779289,
   "Latitude": 106.6393341
 },
 {
   "STT": 1082,
   "Name": "Nhà thuốc Thanh Duy",
   "address": "293, kp 3 Đường HT 13, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.879431,
   "Latitude": 106.638155
 },
 {
   "STT": 1083,
   "Name": "Nhà thuốc Thanh Hồng",
   "address": "204, kp 3 Nguyễn Oanh, Phường   Thạnh Lộc,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8687014,
   "Latitude": 106.6888887
 },
 {
   "STT": 1084,
   "Name": "Nhà thuốc Thanh Loan",
   "address": "285/4 , kp 5 Phan Văn Hớn, Phường   Tân Thới Nhất,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8303578,
   "Latitude": 106.6130606
 },
 {
   "STT": 1085,
   "Name": "Nhà thuốc Thạnh Lộc",
   "address": "Chợ Thạnh Lộc, Phường   Thạnh Xuân,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8686587,
   "Latitude": 106.6837048
 },
 {
   "STT": 1086,
   "Name": "Nhà thuốc Thanh Nhân",
   "address": "15, Tổ 21, KP5 Đường ĐHT 31, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8374161,
   "Latitude": 106.6254377
 },
 {
   "STT": 1087,
   "Name": "Nhà thuốc Thành Phương",
   "address": "1061 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8519584,
   "Latitude": 106.6377126
 },
 {
   "STT": 1088,
   "Name": "Nhà thuốc Thanh Tâm",
   "address": "145 Nguyễn Ảnh Thủ, Phường   Hiệp Thành,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8797223,
   "Latitude": 106.6359374
 },
 {
   "STT": 1089,
   "Name": "Nhà thuốc Hồng Trang ",
   "address": "109  Ngô Quyền, KP  2,  Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8438775,
   "Latitude": 106.7771218
 },
 {
   "STT": 1090,
   "Name": "Nhà thuốc Thanh Tuyền ",
   "address": "35 Man Thiện, KP  5, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8457763,
   "Latitude": 106.7865099
 },
 {
   "STT": 1091,
   "Name": "Nhà thuốc An Thanh",
   "address": "265 Lê Văn Việt, KP  4, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8442857,
   "Latitude": 106.7847618
 },
 {
   "STT": 1092,
   "Name": "Nhà thuốc Như Y",
   "address": "209  Lê Lợi, khu phố 2, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.84553,
   "Latitude": 106.7756
 },
 {
   "STT": 1093,
   "Name": "Nhà thuốc Minh Châu",
   "address": "90 Trần Hưng Đạo, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8478923,
   "Latitude": 106.7750966
 },
 {
   "STT": 1094,
   "Name": "Nhà thuốc Thiên An",
   "address": "107 Lê Văn Việt, KP 3, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.846721,
   "Latitude": 106.777699
 },
 {
   "STT": 1095,
   "Name": "Nhà thuốc An Tâm",
   "address": "5 Quang Trung, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8480722,
   "Latitude": 106.7748092
 },
 {
   "STT": 1096,
   "Name": "Nhà thuốc Hiệp Phú",
   "address": "28 Lê Văn Việt, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8475331,
   "Latitude": 106.7761057
 },
 {
   "STT": 1097,
   "Name": "Nhà thuốc Phúc Đan",
   "address": "32 Trần Hưng Đạo, KP 2, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8460568,
   "Latitude": 106.7744118
 },
 {
   "STT": 1098,
   "Name": "Nhà thuốc Hùng Hạnh II",
   "address": "111 Man Thiên, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8526773,
   "Latitude": 106.7880379
 },
 {
   "STT": 1099,
   "Name": "Nhà thuốc Phương Linh",
   "address": "12 Lê Văn Việt, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.846871,
   "Latitude": 106.776364
 },
 {
   "STT": 1100,
   "Name": "Nhà thuốc Tuấn Kiệt",
   "address": "67 Ngô Quyền, Phường   Hiệp Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8444154,
   "Latitude": 106.7761574
 },
 {
   "STT": 1101,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "21B Làng Tăng Phú, Phường   Tăng Nhơn Phú A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8445301,
   "Latitude": 106.7872545
 },
 {
   "STT": 1102,
   "Name": "Nhà thuốc Đức Trọng",
   "address": "4 Lê Văn Việt, Phường   Tăng Nhơn Phú A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.84417,
   "Latitude": 106.789094
 },
 {
   "STT": 1103,
   "Name": "Nhà thuốc Phước An",
   "address": "B15  Lã Xuân Oai, Phường   Tăng Nhơn Phú  A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8398715,
   "Latitude": 106.7953202
 },
 {
   "STT": 1104,
   "Name": "Nhà thuốc Bình An ",
   "address": "A 1/7 Lê Văn Việt, KP 2, Phường   Tăng Nhơn Phú A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.844851,
   "Latitude": 106.791008
 },
 {
   "STT": 1105,
   "Name": "Nhà thuốc Hùng Hạnh II",
   "address": "C 2/2 Lê Văn Việt, KP 2, Phường   Tăng Nhơn Phú A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8448236,
   "Latitude": 106.7948636
 },
 {
   "STT": 1106,
   "Name": "Nhà thuốc Đức Hạnh",
   "address": "472 A1 Lê Văn Việt, Phường   Tăng Nhơn Phú A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8468178,
   "Latitude": 106.8001059
 },
 {
   "STT": 1107,
   "Name": "Nhà thuốc Số 16",
   "address": "Kios 3, Lê Văn Việt, Chợ TNP, Phường   Tăng Nhơn Phú A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.844152,
   "Latitude": 106.78914
 },
 {
   "STT": 1108,
   "Name": "Nhà thuốc Thùy Trang",
   "address": "595 A Lê Văn Việt, Phường   Tăng Nhơn Phú A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8467845,
   "Latitude": 106.7982779
 },
 {
   "STT": 1109,
   "Name": "Nhà thuốc Phúc Dân",
   "address": "211 Lã Xuân Oai, Phường   Tăng Nhơn Phú A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.841265,
   "Latitude": 106.790181
 },
 {
   "STT": 1110,
   "Name": "Nhà thuốc Anh Thư",
   "address": "420 Lê Văn Việt, Phường   Tăng Nhơn Phú A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8456612,
   "Latitude": 106.7954892
 },
 {
   "STT": 1111,
   "Name": "Nhà thuốc Ngọc Anh",
   "address": "224 Đình Phong Phú, KP 3, Phường   Tăng Nhơn Phú B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8354968,
   "Latitude": 106.7828576
 },
 {
   "STT": 1112,
   "Name": "Nhà thuốc Phước Khang",
   "address": "59 Đình Phong Phú, KP 1, Phường   Tăng Nhơn Phú B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8354968,
   "Latitude": 106.7828576
 },
 {
   "STT": 1113,
   "Name": "Nhà thuốc Quang Trung",
   "address": "52 Quang Trung, KP 5, Phường   Tăng Nhơn Phú B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8307175,
   "Latitude": 106.7853922
 },
 {
   "STT": 1114,
   "Name": "Nhà thuốc Phương Thảo",
   "address": "330 (B164) Lê Văn Việt, Phường   Tăng Nhơn Phú B ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.844237,
   "Latitude": 106.785445
 },
 {
   "STT": 1115,
   "Name": "Nhà thuốc Tâm Phúc II",
   "address": "185 Tăng Nhơn Phú, Phường   Tăng Nhơn Phú B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8330605,
   "Latitude": 106.7777875
 },
 {
   "STT": 1116,
   "Name": "Nhà thuốc Thanh Loan",
   "address": "80 Đình Phong Phú, Phường   Tăng Nhơn Phú B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8403653,
   "Latitude": 106.7811605
 },
 {
   "STT": 1117,
   "Name": "Nhà thuốc Nhị Châu",
   "address": "12 Đình Phong Phú, Phường   Tăng Nhơn Phú B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8432359,
   "Latitude": 106.781857
 },
 {
   "STT": 1118,
   "Name": "Nhà thuốc Ngọc Châu",
   "address": "220 Lê Văn Việt, Phường   Tăng Nhơn Phú B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8443486,
   "Latitude": 106.785053
 },
 {
   "STT": 1119,
   "Name": "Nhà thuốc Kim Dung",
   "address": "70 Tăng Nhơn Phú, Phường   Tăng Nhơn Phú B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8354968,
   "Latitude": 106.7828576
 },
 {
   "STT": 1120,
   "Name": "Nhà thuốc Cúc Phương",
   "address": "435 C  Hoàng Hữu Nam, Phường   Long Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8749109,
   "Latitude": 106.8151492
 },
 {
   "STT": 1121,
   "Name": "Nhà thuốc Tường Vi",
   "address": "1233 Nguyễn Xiển, KP  Thái Bình Ii, Phường   Long Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8884422,
   "Latitude": 106.8334791
 },
 {
   "STT": 1122,
   "Name": "Nhà thuốc Đông Phương ",
   "address": "479 A Hoàng Hữu Nam, KP  Giãn Dân, Phường   Long Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8763095,
   "Latitude": 106.8153646
 },
 {
   "STT": 1123,
   "Name": "Nhà thuốc Trung Hùng ",
   "address": "1025 Nguyễn Xiển, Phường   Long Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8593043,
   "Latitude": 106.8364017
 },
 {
   "STT": 1124,
   "Name": "Nhà thuốc Minh Nguyễn",
   "address": "96 Đường 16, KP  Vĩnh Thuận, Phường   Long Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8845856,
   "Latitude": 106.8231036
 },
 {
   "STT": 1125,
   "Name": "Nhà thuốc Quỳnh Chi ",
   "address": "85/2B Nguyễn Văn Tăng, Phường    Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.843175,
   "Latitude": 106.828788
 },
 {
   "STT": 1126,
   "Name": "Nhà thuốc Phúc Thịnh",
   "address": "458 Nguyễn Văn Tăng, KP  Gò Công, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8419869,
   "Latitude": 106.823628
 },
 {
   "STT": 1127,
   "Name": "Nhà thuốc Thanh Bình",
   "address": "243 Nguyễn Văn Tăng, KP  Gò Công, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8419869,
   "Latitude": 106.823628
 },
 {
   "STT": 1128,
   "Name": "Nhà thuốc Quỳnh Anh",
   "address": "102 Nguyễn Văn Tăng, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.847547,
   "Latitude": 106.814154
 },
 {
   "STT": 1129,
   "Name": "Nhà thuốc Đức Tâm",
   "address": "288 Hoàng Hữu Nam, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8628022,
   "Latitude": 106.8136435
 },
 {
   "STT": 1130,
   "Name": "Nhà thuốc Mỹ Liên",
   "address": "449 A Nguyễn Văn Tăng, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8421949,
   "Latitude": 106.8278095
 },
 {
   "STT": 1131,
   "Name": "Nhà thuốc An Gia",
   "address": "125 Nguyễn Văn Tăng, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8448866,
   "Latitude": 106.8176061
 },
 {
   "STT": 1132,
   "Name": "Nhà thuốc Thủy Quyền",
   "address": "774 Nguyễn Xiển, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.85227,
   "Latitude": 106.833695
 },
 {
   "STT": 1133,
   "Name": "Nhà thuốc Hồng Ngân",
   "address": "830A Nguyễn Xiển, KP  Long Hòa, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8458491,
   "Latitude": 106.8298809
 },
 {
   "STT": 1134,
   "Name": "Nhà thuốc Hiếu Tâm",
   "address": "322 Nguyễn Văn Tăng, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8411792,
   "Latitude": 106.8235782
 },
 {
   "STT": 1135,
   "Name": "Nhà thuốc Hồng Ngân ",
   "address": "830 A Nguyễn Xiển, KP  Long Hòa, Phường    Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8419869,
   "Latitude": 106.823628
 },
 {
   "STT": 1136,
   "Name": "Nhà thuốc Hoàng Nguyễn",
   "address": "362 A, Đỗ Xuân Hợp, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8223947,
   "Latitude": 106.7711898
 },
 {
   "STT": 1137,
   "Name": "Nhà thuốc Thuận Thiên",
   "address": "84A, Đỗ Xuân Hợp, KP  Ii, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8220541,
   "Latitude": 106.7704956
 },
 {
   "STT": 1138,
   "Name": "Nhà thuốc Ngọc Minh",
   "address": "46 Tây Hòa, KP  Ii, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8264289,
   "Latitude": 106.763447
 },
 {
   "STT": 1139,
   "Name": "Nhà thuốc Gia Lương",
   "address": "32A, Tây Hòa, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.826204,
   "Latitude": 106.763126
 },
 {
   "STT": 1140,
   "Name": "Nhà thuốc 360",
   "address": "484 (245 B) Tây Hòa, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8263269,
   "Latitude": 106.761197
 },
 {
   "STT": 1141,
   "Name": "Nhà thuốc Phúc Phượng ",
   "address": "121 Tây Hòa, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.825175,
   "Latitude": 106.762665
 },
 {
   "STT": 1142,
   "Name": "Nhà thuốc 168",
   "address": "2B Đường 168, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8297055,
   "Latitude": 106.7670761
 },
 {
   "STT": 1143,
   "Name": "Nhà thuốc Hùng Việt",
   "address": "246 Tây Hòa, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8261569,
   "Latitude": 106.7644021
 },
 {
   "STT": 1144,
   "Name": "Nhà thuốc Trâm Anh",
   "address": "108 Đỗ Xuân Hợp, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8224159,
   "Latitude": 106.7712075
 },
 {
   "STT": 1145,
   "Name": "Nhà thuốc Thanh Hà",
   "address": "257 Nam Hòa, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.820791,
   "Latitude": 106.7622389
 },
 {
   "STT": 1146,
   "Name": "Nhà thuốc Thủy Ngân",
   "address": "183 Tây Hòa, Phường   Phước Long A,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8265307,
   "Latitude": 106.7610276
 },
 {
   "STT": 1147,
   "Name": "Nhà thuốc Gia Lương 8",
   "address": "01 Dương Đình Hội, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8169109,
   "Latitude": 106.7747176
 },
 {
   "STT": 1148,
   "Name": "Nhà thuốc Nguyễn Đức",
   "address": "24 Tăng Nhơn Phú, Phường   Phước Long B, ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8272625,
   "Latitude": 106.7692053
 },
 {
   "STT": 1149,
   "Name": "Nhà thuốc An Lành",
   "address": "83 Tăng Nhơn Phú, KP 2, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8283743,
   "Latitude": 106.7723528
 },
 {
   "STT": 1150,
   "Name": "Nhà thuốc Trâm Anh II",
   "address": "31 Đ 339,  KP 4, P Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8147076,
   "Latitude": 106.7794935
 },
 {
   "STT": 1151,
   "Name": "Nhà thuốc Ai Nhi",
   "address": "101 Đường 61, KP 3, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.828464,
   "Latitude": 106.772422
 },
 {
   "STT": 1152,
   "Name": "Nhà thuốc Hoàng Trinh",
   "address": "106/1A, Đường 109, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8207564,
   "Latitude": 106.7786867
 },
 {
   "STT": 1153,
   "Name": "Nhà thuốc Phước  Long",
   "address": "305 Đỗ Xuân Hợp, P Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.820009,
   "Latitude": 106.7747459
 },
 {
   "STT": 1154,
   "Name": "Nhà thuốc Hoàng Lê ",
   "address": "543 Đỗ Xuân Hợp, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8144372,
   "Latitude": 106.7751633
 },
 {
   "STT": 1155,
   "Name": "Nhà thuốc Công Hiển",
   "address": "259 Đỗ Xuân Hợp, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.822096,
   "Latitude": 106.7715302
 },
 {
   "STT": 1156,
   "Name": "Nhà thuốc Khang Điền",
   "address": "111 Dương Đình Hội, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.820676,
   "Latitude": 106.7787241
 },
 {
   "STT": 1157,
   "Name": "Nhà thuốc Mỹ Linh II",
   "address": "182 Dương Đình Hội, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8257256,
   "Latitude": 106.7797278
 },
 {
   "STT": 1158,
   "Name": "Nhà thuốc Ngọc Chi",
   "address": "21 Dương Đình Hội, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.825334,
   "Latitude": 106.777443
 },
 {
   "STT": 1159,
   "Name": "Nhà thuốc Số 51",
   "address": "51 Đỗ Xuân Hợp, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.819472,
   "Latitude": 106.773107
 },
 {
   "STT": 1160,
   "Name": "Nhà thuốc Số 62",
   "address": "139D Đỗ Xuân Hợp, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8290547,
   "Latitude": 106.7685897
 },
 {
   "STT": 1161,
   "Name": "Nhà thuốc Mỹ Dung",
   "address": "173 Đỗ Xuân Hợp, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.827697,
   "Latitude": 106.768005
 },
 {
   "STT": 1162,
   "Name": "Nhà thuốc Kim Đô",
   "address": "56/4 D Đường 147, Phường   Phước Long B,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8320352,
   "Latitude": 106.7716861
 },
 {
   "STT": 1163,
   "Name": "Nhà thuốc Số 77",
   "address": "91 Đường 18, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8147961,
   "Latitude": 106.7712092
 },
 {
   "STT": 1164,
   "Name": "Nhà thuốc Vân Anh",
   "address": "84 Đại Lộ II, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8177313,
   "Latitude": 106.773007
 },
 {
   "STT": 1165,
   "Name": "Nhà thuốc Hoàng Thông ",
   "address": "Số 1, Đường 5, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.818288,
   "Latitude": 106.773085
 },
 {
   "STT": 1166,
   "Name": "Nhà thuốc Bảo Trâm",
   "address": "5 Đường 7, KP 3, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.816694,
   "Latitude": 106.7740029
 },
 {
   "STT": 1167,
   "Name": "Nhà thuốc Trí Tâm ",
   "address": "14 Đại Lộ II, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8177313,
   "Latitude": 106.773007
 },
 {
   "STT": 1168,
   "Name": "Nhà thuốc Thanh Vân",
   "address": "121 Đường 14, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8110692,
   "Latitude": 106.7701415
 },
 {
   "STT": 1169,
   "Name": "Nhà thuốc Minh Châu II",
   "address": "81A1, Đường số 4, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.818392,
   "Latitude": 106.771585
 },
 {
   "STT": 1170,
   "Name": "Nhà thuốc Xuân Hợp",
   "address": "2B Đại Lộ II, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8177313,
   "Latitude": 106.773007
 },
 {
   "STT": 1171,
   "Name": "Nhà thuốc Vũ Nguyệt",
   "address": "87A Đường 10, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8138909,
   "Latitude": 106.7725359
 },
 {
   "STT": 1172,
   "Name": "Nhà thuốc Đức Thiện",
   "address": "80A Đường 3, Phường   Phước Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.819189,
   "Latitude": 106.771305
 },
 {
   "STT": 1173,
   "Name": "Nhà thuốc Phước Hải",
   "address": "671 Lê Văn Việt, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.851197,
   "Latitude": 106.8118152
 },
 {
   "STT": 1174,
   "Name": "Nhà thuốc Mỹ Anh",
   "address": "Số 01E, Đường 154, Khu Phố Cây Dầu, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8678621,
   "Latitude": 106.8048441
 },
 {
   "STT": 1175,
   "Name": "Nhà thuốc Tuấn Minh",
   "address": "21 Đường 154, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8678315,
   "Latitude": 106.8054063
 },
 {
   "STT": 1176,
   "Name": "Nhà thuốc Lan Anh",
   "address": "36 Đường 154, KP 3, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8676021,
   "Latitude": 106.805991
 },
 {
   "STT": 1177,
   "Name": "Nhà thuốc Nguyễn Hà",
   "address": "89A, Đường 154, KP 3, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8674683,
   "Latitude": 106.8076958
 },
 {
   "STT": 1178,
   "Name": "Nhà thuốc Vạn Hạnh",
   "address": "20 Nam Cao, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.851331,
   "Latitude": 106.807226
 },
 {
   "STT": 1179,
   "Name": "Nhà thuốc Phúc Khang II",
   "address": "62 Đường 154, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8673062,
   "Latitude": 106.8079347
 },
 {
   "STT": 1180,
   "Name": "Nhà thuốc Anh Minh ",
   "address": "61E  Nam Cao, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8599393,
   "Latitude": 106.7997744
 },
 {
   "STT": 1181,
   "Name": "Nhà thuốc Hưng Thịnh I",
   "address": "7 Hoàng Hữu Nam, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8503278,
   "Latitude": 106.8136432
 },
 {
   "STT": 1182,
   "Name": "Nhà thuốc Lê Hoàng",
   "address": "102 Nam Cao, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8583692,
   "Latitude": 106.8008963
 },
 {
   "STT": 1183,
   "Name": "Nhà thuốc Gia Hiệp",
   "address": "323 Hoàng Hữu Nam, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8680992,
   "Latitude": 106.8137061
 },
 {
   "STT": 1184,
   "Name": "Nhà thuốc Lê Hùng",
   "address": "89 Nam Cao, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.86024,
   "Latitude": 106.7995402
 },
 {
   "STT": 1185,
   "Name": "Nhà thuốc Hoa Tiên",
   "address": "5 Nam Cao, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8538213,
   "Latitude": 106.804523
 },
 {
   "STT": 1186,
   "Name": "Nhà thuốc Phi Ngọc ",
   "address": "53 Cầu Xây, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8542448,
   "Latitude": 106.8084199
 },
 {
   "STT": 1187,
   "Name": "Nhà thuốc An Tâm I",
   "address": "693 Lê Văn Việt, Phường   Tân Phú,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8503978,
   "Latitude": 106.8131301
 },
 {
   "STT": 1188,
   "Name": "Nhà thuốc Minh Minh",
   "address": "31 Đường 882, Phường   Phú Hữu,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790124,
   "Latitude": 106.7976594
 },
 {
   "STT": 1189,
   "Name": "Nhà thuốc Long Trường ",
   "address": "1318 Ng Duy Trinh, Long Trường,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8049374,
   "Latitude": 106.816484
 },
 {
   "STT": 1190,
   "Name": "Nhà thuốc Mùi Lan 54",
   "address": "1372 Ng Duy Trinh, KP  Phước Lai, Phường   Long Trường,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8056313,
   "Latitude": 106.8182266
 },
 {
   "STT": 1191,
   "Name": "Nhà thuốc SPG Minh Phú",
   "address": "1329 Ng D Trinh, KP  Phước Lai, Phường   Long Trường,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.5990802,
   "Latitude": 106.6939803
 },
 {
   "STT": 1192,
   "Name": "Nhà thuốc Nhật Đăng",
   "address": "768 Lã Xuân Oai, Phường   Long Trường,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8059325,
   "Latitude": 106.8169227
 },
 {
   "STT": 1193,
   "Name": "Nhà thuốc Duy Trinh",
   "address": "1508 Nguyễn Duy Trinh, Phường   Long Trường,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8066422,
   "Latitude": 106.8239169
 },
 {
   "STT": 1194,
   "Name": "Nhà thuốc Hải Anh",
   "address": "1483 Ng Duy Trinh, Phường   Trường Thạnh,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8062599,
   "Latitude": 106.8220639
 },
 {
   "STT": 1195,
   "Name": "Nhà thuốc NT An ",
   "address": "3/2 A, Đường Lò Lu, Phường   Trường Thạnh,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.824676,
   "Latitude": 106.808714
 },
 {
   "STT": 1196,
   "Name": "Nhà thuốc Đăng Anh I",
   "address": "169 A, Đường Lò Lu, Phường   Trường Thạnh,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8251836,
   "Latitude": 106.8168374
 },
 {
   "STT": 1197,
   "Name": "Nhà thuốc Châu Hải ",
   "address": "154 Nguyễn Chế Nghĩa, Phường   12,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74226,
   "Latitude": 106.653826
 },
 {
   "STT": 1198,
   "Name": "Nhà thuốc Nhân Hòa",
   "address": "495 Bến Bình Đông, Phường   13,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7465531,
   "Latitude": 106.6564981
 },
 {
   "STT": 1199,
   "Name": "Nhà thuốc Đặng Hồ ",
   "address": "61 Nguyễn Chế Nghĩa, Phường   13,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.743563,
   "Latitude": 106.6534
 },
 {
   "STT": 1200,
   "Name": "Nhà thuốc Khoa Nguyên",
   "address": "2 Tuy Lý Vương, Phường   13,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7444455,
   "Latitude": 106.6558686
 },
 {
   "STT": 1201,
   "Name": "Nhà thuốc Đại Quang ",
   "address": "440 Tùng Thiện Vương, Phường   13,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.744459,
   "Latitude": 106.6561
 },
 {
   "STT": 1202,
   "Name": "Nhà thuốc Bảo Linh ",
   "address": "7 Cao Xuân Dục, Phường   13,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.746636,
   "Latitude": 106.659699
 },
 {
   "STT": 1203,
   "Name": "Nhà thuốc Hùng Cường",
   "address": "133/23e/11 Cây Sung, Phường   14,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.737593,
   "Latitude": 106.645054
 },
 {
   "STT": 1204,
   "Name": "Nhà thuốc Bình Minh",
   "address": "132/69 Cây Sung, Phường   14,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7373991,
   "Latitude": 106.6454668
 },
 {
   "STT": 1205,
   "Name": "Nhà thuốc Thanh Thuế",
   "address": "277/58K Bình Đông, Phường   14,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.741009,
   "Latitude": 106.648757
 },
 {
   "STT": 1206,
   "Name": "Nhà thuốc 289 Hồng Đức",
   "address": "161/1/2 Bình Đông, Phường   14,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.739733,
   "Latitude": 106.64613
 },
 {
   "STT": 1207,
   "Name": "Nhà thuốc Trung Tín ",
   "address": "97 Hoài Thanh, Phường   14,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7377749,
   "Latitude": 106.647943
 },
 {
   "STT": 1208,
   "Name": "Nhà thuốc 301 Trung Hưng",
   "address": "51 Hoài Thanh, Phường   14,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.738709,
   "Latitude": 106.649848
 },
 {
   "STT": 1209,
   "Name": "Nhà thuốc Kim Cúc ",
   "address": "148K Bình Đông, Phường   14,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.739532,
   "Latitude": 106.646833
 },
 {
   "STT": 1210,
   "Name": "Nhà thuốc Hiền Đức",
   "address": "13 c/x Bình Đông, Phường   15,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7360827,
   "Latitude": 106.6387948
 },
 {
   "STT": 1211,
   "Name": "Nhà thuốc Minh Hiếu",
   "address": "2F Lương Văn Can, Phường   15,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.735775,
   "Latitude": 106.639106
 },
 {
   "STT": 1212,
   "Name": "Nhà thuốc Thiện Tâm",
   "address": "34A Rạch Cát, Phường   15,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.717003,
   "Latitude": 106.627689
 },
 {
   "STT": 1213,
   "Name": "Nhà thuốc Ngọc Ngân",
   "address": "226A Lưu Hữu Phước, Phường   15,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.728524,
   "Latitude": 106.636297
 },
 {
   "STT": 1214,
   "Name": "Nhà thuốc Ngọc Phướng ",
   "address": "20 Lương Văn Can, Phường   15,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.735078,
   "Latitude": 106.63947
 },
 {
   "STT": 1215,
   "Name": "Nhà thuốc Việt Hùng ",
   "address": " 2/3 Mễ Cốc, Phường   15,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.729428,
   "Latitude": 106.6331359
 },
 {
   "STT": 1216,
   "Name": "Nhà thuốc Ngọc Long ",
   "address": "41 Mai Hắc Đế, Phường   15,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7189664,
   "Latitude": 106.6291814
 },
 {
   "STT": 1217,
   "Name": "Nhà thuốc Minh Thư ",
   "address": "29 Rạch Cát, Phường   15,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.717383,
   "Latitude": 106.627708
 },
 {
   "STT": 1218,
   "Name": "Nhà thuốc Mộc An ",
   "address": "24 Lương Văn Can, Phường   15,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.735032,
   "Latitude": 106.639561
 },
 {
   "STT": 1219,
   "Name": "Nhà thuốc Kim Loan",
   "address": "49/2E Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.720808,
   "Latitude": 106.628038
 },
 {
   "STT": 1220,
   "Name": "Nhà thuốc Thiên Trang",
   "address": "49 Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7304022,
   "Latitude": 106.6326562
 },
 {
   "STT": 1221,
   "Name": "Nhà thuốc Hồng Phúc",
   "address": "172/195/9B An Dương Vương, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7255855,
   "Latitude": 106.6197872
 },
 {
   "STT": 1222,
   "Name": "Nhà thuốc Hữu Lý",
   "address": "1A  An Dương Vương, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.711397,
   "Latitude": 106.618412
 },
 {
   "STT": 1223,
   "Name": "Nhà thuốc Anh Châu",
   "address": "64A An Dương Vương, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7162421,
   "Latitude": 106.6190543
 },
 {
   "STT": 1224,
   "Name": "Nhà thuốc Phú Định An",
   "address": "36D An Dương Vương, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.721772,
   "Latitude": 106.619804
 },
 {
   "STT": 1225,
   "Name": "Nhà thuốc Diệu Thư",
   "address": "364 Bến Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7223942,
   "Latitude": 106.628984
 },
 {
   "STT": 1226,
   "Name": "Nhà thuốc Kim Long",
   "address": "210D An Dương Vương, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7304991,
   "Latitude": 106.620748
 },
 {
   "STT": 1227,
   "Name": "Nhà thuốc Thanh Mai",
   "address": "2 Bến Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7107012,
   "Latitude": 106.619363
 },
 {
   "STT": 1228,
   "Name": "Nhà thuốc Minh Lý",
   "address": "14 Hồ Học lãm, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7113249,
   "Latitude": 106.6180694
 },
 {
   "STT": 1229,
   "Name": "Nhà thuốc Thảo Minh",
   "address": "87 Bến Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7284212,
   "Latitude": 106.6318711
 },
 {
   "STT": 1230,
   "Name": "Nhà thuốc Ngọc Huệ",
   "address": "79/31 Bến Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7201927,
   "Latitude": 106.6277853
 },
 {
   "STT": 1231,
   "Name": "Nhà thuốc Đăng Khoa",
   "address": "11 Đường số 26, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7192153,
   "Latitude": 106.6226853
 },
 {
   "STT": 1232,
   "Name": "Nhà thuốc Bảo Ngọc ",
   "address": "195 Đường 41 Bến Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.724954,
   "Latitude": 106.623077
 },
 {
   "STT": 1233,
   "Name": "Nhà thuốc Phương Linh",
   "address": "37/2A  Bến Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7107012,
   "Latitude": 106.619363
 },
 {
   "STT": 1234,
   "Name": "Nhà thuốc Thanh Thúy ",
   "address": "296 Đường 41 Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7254349,
   "Latitude": 106.622356
 },
 {
   "STT": 1235,
   "Name": "Nhà thuốc Hải Sơn ",
   "address": "79/90 Bến Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7284212,
   "Latitude": 106.6318711
 },
 {
   "STT": 1236,
   "Name": "Nhà thuốc Thanh Vy ",
   "address": "46A Bến Phú Định, Phường   16,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7110841,
   "Latitude": 106.6209117
 },
 {
   "STT": 1237,
   "Name": "Nhà thuốc Nguyên Lý ",
   "address": "254 Âu Dương Lân, Phường   3,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7439699,
   "Latitude": 106.683854
 },
 {
   "STT": 1238,
   "Name": "Nhà thuốc Phương Anh ",
   "address": "47 Âu Dương Lân, Phường   3,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747419,
   "Latitude": 106.6823469
 },
 {
   "STT": 1239,
   "Name": "Nhà thuốc NT số 6 ",
   "address": "310 Phạm Thế Hiển, Phường   3,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748802,
   "Latitude": 106.682406
 },
 {
   "STT": 1240,
   "Name": "Nhà thuốc Ngọc Vinh ",
   "address": "40 Đặng Chất, Phường   3,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7471472,
   "Latitude": 106.6845277
 },
 {
   "STT": 1241,
   "Name": "Nhà thuốc Minh Phương ",
   "address": "172 Âu Dương Lân, Phường   3,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745238,
   "Latitude": 106.683108
 },
 {
   "STT": 1242,
   "Name": "Nhà thuốc Lộc Linh",
   "address": "26/9 Bông Sao, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.739288,
   "Latitude": 106.662338
 },
 {
   "STT": 1243,
   "Name": "Nhà thuốc Tâm An",
   "address": "506 Phạm Thế Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74678,
   "Latitude": 106.678505
 },
 {
   "STT": 1244,
   "Name": "Nhà thuốc Vân Anh",
   "address": "14 Đông Hồ, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.739886,
   "Latitude": 106.670951
 },
 {
   "STT": 1245,
   "Name": "Nhà thuốc Phúc Nhi",
   "address": "170/26 Đường 204 Cao Lỗ, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7376547,
   "Latitude": 106.6783878
 },
 {
   "STT": 1246,
   "Name": "Nhà thuốc Nhân Nghĩa",
   "address": "151 Đường số 8, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.741975,
   "Latitude": 106.670619
 },
 {
   "STT": 1247,
   "Name": "Nhà thuốc Thiên Phúc",
   "address": "48 Đường 715 Tạ Quang Bửu, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7375332,
   "Latitude": 106.6752921
 },
 {
   "STT": 1248,
   "Name": "Nhà thuốc Anh Huy",
   "address": "33A Đường 16 nối dài, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7415978,
   "Latitude": 106.6732821
 },
 {
   "STT": 1249,
   "Name": "Nhà thuốc Phúc Thịnh",
   "address": "123A Đường 13, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7388113,
   "Latitude": 106.6716514
 },
 {
   "STT": 1250,
   "Name": "Nhà thuốc An Phú",
   "address": "28 Cao Lỗ, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74419,
   "Latitude": 106.679905
 },
 {
   "STT": 1251,
   "Name": "Nhà thuốc Hiền Khanh",
   "address": "165 Đường 204 Cao Lỗ, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.736199,
   "Latitude": 106.6757313
 },
 {
   "STT": 1252,
   "Name": "Nhà thuốc Hưng Tuấn ",
   "address": "905 Phạm Thế Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.743563,
   "Latitude": 106.671913
 },
 {
   "STT": 1253,
   "Name": "Nhà thuốc Bách Tùng",
   "address": "869 Phạm Thế Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7438914,
   "Latitude": 106.6725495
 },
 {
   "STT": 1254,
   "Name": "Nhà thuốc Quang Hiển",
   "address": "8/2A1 Phạm Hùng, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7479597,
   "Latitude": 106.6689511
 },
 {
   "STT": 1255,
   "Name": "Nhà thuốc Kim Ngân ",
   "address": "125-127 Đường số 8, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.742073,
   "Latitude": 106.671196
 },
 {
   "STT": 1256,
   "Name": "Nhà thuốc Phương Linh",
   "address": "184 Cao Lỗ, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7391059,
   "Latitude": 106.6784048
 },
 {
   "STT": 1257,
   "Name": "Nhà thuốc Minh Phúc",
   "address": "31 Đường 783 Tạ Quang Bửu, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7378145,
   "Latitude": 106.6736095
 },
 {
   "STT": 1258,
   "Name": "Nhà thuốc Huy Lý ",
   "address": "54 Đường 643 Tạ  Quang Bửu, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7379859,
   "Latitude": 106.6767982
 },
 {
   "STT": 1259,
   "Name": "Nhà thuốc Hồng Phát ",
   "address": "616 Phạm Thế Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745637,
   "Latitude": 106.676463
 },
 {
   "STT": 1260,
   "Name": "Nhà thuốc Xuân Nghiêm",
   "address": "27 Bùi Minh Trực, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.736478,
   "Latitude": 106.658663
 },
 {
   "STT": 1261,
   "Name": "Nhà thuốc Hoà Bình",
   "address": "02 lô 8 Phạm Thế Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7435489,
   "Latitude": 106.6703109
 },
 {
   "STT": 1262,
   "Name": "Nhà thuốc Trung Nghĩa ",
   "address": "18 Đông Hồ, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.739832,
   "Latitude": 106.6709599
 },
 {
   "STT": 1263,
   "Name": "Nhà thuốc Trang Phương",
   "address": "989 Phạm Thế Hiển, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.742949,
   "Latitude": 106.6683819
 },
 {
   "STT": 1264,
   "Name": "Nhà thuốc Minh Quân ",
   "address": "008 lô A C/c Phạm Thế  Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7450756,
   "Latitude": 106.675835
 },
 {
   "STT": 1265,
   "Name": "Nhà thuốc Phước Thịnh ",
   "address": "1/16A Chánh Hưng, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.737717,
   "Latitude": 106.670899
 },
 {
   "STT": 1266,
   "Name": "Nhà thuốc Mai Khôi",
   "address": "221 Bông sao, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.741713,
   "Latitude": 106.661403
 },
 {
   "STT": 1267,
   "Name": "Nhà thuốc NT số 16 ",
   "address": "873 Phạm Thế Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74367,
   "Latitude": 106.672471
 },
 {
   "STT": 1268,
   "Name": "Nhà thuốc Thanh Mai",
   "address": "119B Đường số 13, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7391691,
   "Latitude": 106.6714686
 },
 {
   "STT": 1269,
   "Name": "Nhà thuốc Tô Châu ",
   "address": "925 Phạm Thế Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.743207,
   "Latitude": 106.669882
 },
 {
   "STT": 1270,
   "Name": "Nhà thuốc Trâm Anh ",
   "address": "14 Đường số 152 Cao  Lỗ, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7417661,
   "Latitude": 106.6709509
 },
 {
   "STT": 1271,
   "Name": "Nhà thuốc Xuân Vinh ",
   "address": "37 Đường số 17, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.741094,
   "Latitude": 106.672034
 },
 {
   "STT": 1272,
   "Name": "Nhà thuốc Ngọc Mai ",
   "address": "625 Phạm Thế Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74559,
   "Latitude": 106.677277
 },
 {
   "STT": 1273,
   "Name": "Nhà thuốc Khang Nguyên II ",
   "address": "76 Cao Lỗ, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7426186,
   "Latitude": 106.676651
 },
 {
   "STT": 1274,
   "Name": "Nhà thuốc Hoằng Trí",
   "address": "831A Tạ Quang Bửu, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.735796,
   "Latitude": 106.6684806
 },
 {
   "STT": 1275,
   "Name": "Nhà thuốc Bông Sao",
   "address": "79 Bông sao, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74128,
   "Latitude": 106.6611549
 },
 {
   "STT": 1276,
   "Name": "Nhà thuốc Phú Thịnh ",
   "address": "320B Phạm Hùng, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7390863,
   "Latitude": 106.6694463
 },
 {
   "STT": 1277,
   "Name": "Nhà thuốc Tuệ Trí",
   "address": "172 Phạm Hùng, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7415918,
   "Latitude": 106.6687147
 },
 {
   "STT": 1278,
   "Name": "Nhà thuốc Nguyễn Hiền",
   "address": "125 Bùi Minh Trực, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.738618,
   "Latitude": 106.65962
 },
 {
   "STT": 1279,
   "Name": "Nhà thuốc Ngân Hà 5",
   "address": "364/13/12 Phạm Hùng, Phường   5,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.729919,
   "Latitude": 106.6772326
 },
 {
   "STT": 1280,
   "Name": "Nhà thuốc Triệu Vương",
   "address": "2978C Phạm Thế Hiển, Phường   7,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7119618,
   "Latitude": 106.6275662
 },
 {
   "STT": 1281,
   "Name": "Nhà thuốc Thanh Duyên",
   "address": "2977 Phạm Thế Hiển, Phường   7,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7172953,
   "Latitude": 106.632735
 },
 {
   "STT": 1282,
   "Name": "Nhà thuốc Thiên Châu",
   "address": "158A Ba Tơ, Phường   7,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7091265,
   "Latitude": 106.6198337
 },
 {
   "STT": 1283,
   "Name": "Nhà thuốc Phương Trang",
   "address": "3039 Phạm Thế Hiển, Phường   7,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.715824,
   "Latitude": 106.632146
 },
 {
   "STT": 1284,
   "Name": "Nhà thuốc Phạm Thế Hiển",
   "address": "2572 Phạm Thế Hiển, Phường   7,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.727644,
   "Latitude": 106.637282
 },
 {
   "STT": 1285,
   "Name": "Nhà thuốc Ngọc Sơn ",
   "address": "3325A Ba Tơ, Phường   7,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.707896,
   "Latitude": 106.622626
 },
 {
   "STT": 1286,
   "Name": "Nhà thuốc Thiện Thanh",
   "address": "2455 Phạm Thế Hiển, Phường   7,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7295909,
   "Latitude": 106.639088
 },
 {
   "STT": 1287,
   "Name": "Nhà thuốc Gia Khiêm",
   "address": "81 Hưng Phú, Phường   8,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.750468,
   "Latitude": 106.6808344
 },
 {
   "STT": 1288,
   "Name": "Nhà thuốc Hồng Đức ",
   "address": "102D Hưng Phú, Phường   8,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.750652,
   "Latitude": 106.6803
 },
 {
   "STT": 1289,
   "Name": "Nhà thuốc Ngọc Dung ",
   "address": "95C Hưng Phú, Phường   8,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7504214,
   "Latitude": 106.6804325
 },
 {
   "STT": 1290,
   "Name": "Nhà thuốc Quang Minh",
   "address": "21 Hưng Phú, Phường   8,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7506237,
   "Latitude": 106.6819436
 },
 {
   "STT": 1291,
   "Name": "Nhà thuốc Hồng Nga",
   "address": "206A Hưng Phú, Phường   8,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.750125,
   "Latitude": 106.678424
 },
 {
   "STT": 1292,
   "Name": "Nhà thuốc Mỹ Huệ",
   "address": "471 Hưng Phú, Phường   9,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.746994,
   "Latitude": 106.6733859
 },
 {
   "STT": 1293,
   "Name": "Nhà thuốc Nguyễn Duy",
   "address": "47 Nguyễn Duy, Phường   9,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7462953,
   "Latitude": 106.6746882
 },
 {
   "STT": 1294,
   "Name": "Nhà thuốc Cẩm Duy",
   "address": "341A  Nguyễn Duy, Phường   9,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.744292,
   "Latitude": 106.666951
 },
 {
   "STT": 1295,
   "Name": "Nhà thuốc Vĩnh An ",
   "address": "715 Hưng Phú, Phường   9,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7459217,
   "Latitude": 106.6676268
 },
 {
   "STT": 1296,
   "Name": "Nhà thuốc Như Ngọc ",
   "address": "767 Hưng Phú, Phường   9,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74564,
   "Latitude": 106.666562
 },
 {
   "STT": 1297,
   "Name": "Nhà thuốc Minh Triết ",
   "address": "349 Hưng Phú, Phường   9,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748,
   "Latitude": 106.675602
 },
 {
   "STT": 1298,
   "Name": "Nhà thuốc Thanh Châu ",
   "address": "472 Hưng Phú, Phường   9,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747087,
   "Latitude": 106.672381
 },
 {
   "STT": 1299,
   "Name": "Nhà thuốc Hưng Phú ",
   "address": "491 Hưng Phú, Phường   9,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7469924,
   "Latitude": 106.6730847
 },
 {
   "STT": 1300,
   "Name": "Nhà thuốc Phương Nam",
   "address": "738 Hưng Phú, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74584,
   "Latitude": 106.666243
 },
 {
   "STT": 1301,
   "Name": "Nhà thuốc Hoàng Vũ",
   "address": "632 Hưng Phú, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7463632,
   "Latitude": 106.6686178
 },
 {
   "STT": 1302,
   "Name": "Nhà thuốc Kim Nguyên",
   "address": "889 Hưng Phú, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745169,
   "Latitude": 106.66358
 },
 {
   "STT": 1303,
   "Name": "Nhà thuốc Thủy Giang",
   "address": "20 Dã Tượng, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.746134,
   "Latitude": 106.664543
 },
 {
   "STT": 1304,
   "Name": "Nhà thuốc Tuyết Nhi",
   "address": "919 Hưng Phú, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745045,
   "Latitude": 106.662903
 },
 {
   "STT": 1305,
   "Name": "Nhà thuốc Quỳnh Anh 2",
   "address": "693 Ba Đình, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747858,
   "Latitude": 106.6683879
 },
 {
   "STT": 1306,
   "Name": "Nhà thuốc Gia Khang ",
   "address": "59 Dã Tượng, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745545,
   "Latitude": 106.6648249
 },
 {
   "STT": 1307,
   "Name": "Nhà thuốc Ngọc Anh",
   "address": "867 Hưng Phú, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745249,
   "Latitude": 106.664028
 },
 {
   "STT": 1308,
   "Name": "Nhà thuốc Phúc Xuân ",
   "address": "88 Ba Đình, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747959,
   "Latitude": 106.6679769
 },
 {
   "STT": 1309,
   "Name": "Nhà thuốc Ngọc Hoa ",
   "address": "668 Hưng Phú, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.746223,
   "Latitude": 106.668017
 },
 {
   "STT": 1310,
   "Name": "Nhà thuốc Ngọc Luật ",
   "address": "B1 Ba Đình, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748139,
   "Latitude": 106.668325
 },
 {
   "STT": 1311,
   "Name": "Nhà thuốc Ngọc Mỹ",
   "address": "414 Ba Đình, Phường   10,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745616,
   "Latitude": 106.662503
 },
 {
   "STT": 1312,
   "Name": "Nhà thuốc Hoàng Việt",
   "address": "83 Xóm Củi, Phường   11,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7463527,
   "Latitude": 106.6605627
 },
 {
   "STT": 1313,
   "Name": "Nhà thuốc Ngọc Trân",
   "address": "61 Tùng Thiện Vương, Phường   11,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748552,
   "Latitude": 106.66626
 },
 {
   "STT": 1314,
   "Name": "Nhà thuốc NT Số 39A",
   "address": "91 Nguyễn Chế Nghĩa, Phường   12,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74292,
   "Latitude": 106.653764
 },
 {
   "STT": 1315,
   "Name": "Nhà thuốc NT Số 39",
   "address": "158 Nguyễn Chế Nghĩa, Phường   12,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.742214,
   "Latitude": 106.653853
 },
 {
   "STT": 1316,
   "Name": "Nhà thuốc Phong Phú",
   "address": "2 Phong Phú, Phường   12,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7461559,
   "Latitude": 106.659908
 },
 {
   "STT": 1317,
   "Name": "Nhà thuốc Ý Mi 2 ",
   "address": "110 Nguyễn Chế Nghĩa, Phường   12,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7427759,
   "Latitude": 106.653526
 },
 {
   "STT": 1318,
   "Name": "Nhà thuốc Nguyên Vũ",
   "address": "67 Cao Xuân Dục, Phường   12,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7450573,
   "Latitude": 106.6583906
 },
 {
   "STT": 1319,
   "Name": "Nhà thuốc Minh Hường",
   "address": "1283 Tỉnh lộ 43, Phường   Bình Chiểu, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8923319,
   "Latitude": 106.7237073
 },
 {
   "STT": 1320,
   "Name": "Nhà thuốc Xuân An",
   "address": "220 Lê Thị Hoa, KPhường   5, Phường   Bình Chiểu, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.876804,
   "Latitude": 106.745425
 },
 {
   "STT": 1321,
   "Name": "Nhà thuốc Ngọc Hồng",
   "address": "952/1 Tỉnh lộ 43, KPhường  1, Phường   Bình Chiểu, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8693906,
   "Latitude": 106.7342932
 },
 {
   "STT": 1322,
   "Name": "Nhà thuốc Kim Ánh",
   "address": "60/2 Võ văn Ngân, KPhường  1, Phường   Bình Thọ, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8503,
   "Latitude": 106.758982
 },
 {
   "STT": 1323,
   "Name": "Nhà thuốc Bích Liên",
   "address": "374 Võ Văn Ngân, Phường   Bình Thọ, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.849453,
   "Latitude": 106.7718919
 },
 {
   "STT": 1324,
   "Name": "Nhà thuốc CHAC 2",
   "address": "42 Đặng Văn Bi, Phường   Bình Thọ, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8412888,
   "Latitude": 106.7650469
 },
 {
   "STT": 1325,
   "Name": "Nhà thuốc Hoài Tâm",
   "address": "41 Đường 11, Kp1, Phường   Bình Thọ, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8503118,
   "Latitude": 106.7624231
 },
 {
   "STT": 1326,
   "Name": "Nhà thuốc Nhà thuốc Nhật Trang",
   "address": "209 Hiệp Bình, KPhường   7, Phường   Hiệp Bình Chánh, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8453542,
   "Latitude": 106.7265236
 },
 {
   "STT": 1327,
   "Name": "Nhà thuốc Như Hằng II",
   "address": "110B Hiệp Bình, Kp8, Phường   Hiệp Bình Chánh, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8273624,
   "Latitude": 106.7141
 },
 {
   "STT": 1328,
   "Name": "Nhà thuốc Thiên Lộc",
   "address": "419/16A Kha Vạn Cân, Kp6, Phường   Hiệp Bình Chánh, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.829658,
   "Latitude": 106.720533
 },
 {
   "STT": 1329,
   "Name": "Nhà thuốc Bích Trâm",
   "address": "263 Kha Vạn Cân, Phường   Hiệp Bình Chánh, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8265969,
   "Latitude": 106.7128205
 },
 {
   "STT": 1330,
   "Name": "Nhà thuốc Vũ Mai",
   "address": "542 Quốc Lộ 13, KPhường   6, Phường   Hiệp Bình Phước, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8469677,
   "Latitude": 106.7182249
 },
 {
   "STT": 1331,
   "Name": "Nhà thuốc Bình An",
   "address": "03 Đường 9, Kp5, Phường   Hiệp Bình Phước, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8431326,
   "Latitude": 106.7112834
 },
 {
   "STT": 1332,
   "Name": "Nhà thuốc Minh Hoàng",
   "address": "42 Q.Lộ 13 (cũ), KPhường  3, Phường   Hiệp Bình Phước, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8604228,
   "Latitude": 106.7219953
 },
 {
   "STT": 1333,
   "Name": "Nhà thuốc Tuyết Tâm",
   "address": "646 Kha Vạn Cân, Kp5, Phường   Linh Đông, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8429113,
   "Latitude": 106.7479634
 },
 {
   "STT": 1334,
   "Name": "Nhà thuốc An Khang",
   "address": "15 Đường 35, Kp2, Phường   Linh Đông, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8562297,
   "Latitude": 106.7474663
 },
 {
   "STT": 1335,
   "Name": "Nhà thuốc Phúc Khang",
   "address": "05 Đường 8, KPhường  1, Phường   Linh Xuân, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8814474,
   "Latitude": 106.7717395
 },
 {
   "STT": 1336,
   "Name": "Nhà thuốc Minh Phát",
   "address": "65/34 Đường 5, Kp2, Phường   Linh Xuân, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8890583,
   "Latitude": 106.767039
 },
 {
   "STT": 1337,
   "Name": "Nhà thuốc Đức Thiên II",
   "address": "249 Đường 11, Kp4, Phường   Linh Xuân, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8902606,
   "Latitude": 106.7682753
 },
 {
   "STT": 1338,
   "Name": "Nhà thuốc An Khang 2",
   "address": "A49 Đường B, chợ đầu mối, Phường   Tam Bình, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.869786,
   "Latitude": 106.7281558
 },
 {
   "STT": 1339,
   "Name": "Nhà thuốc Vinh Duyên",
   "address": "227 Gò Dưa, Kp2, Phường   Tam Bình, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.871064,
   "Latitude": 106.742626
 },
 {
   "STT": 1340,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "A27 Đường B, Khu nhà ở Chợ đầu mối nông sản thực phẩm, KPhường  5, Phường   Tam Bình, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8412333,
   "Latitude": 106.725857
 },
 {
   "STT": 1341,
   "Name": "Nhà thuốc Hoài Nam",
   "address": "79 Đường Gò Dưa, KPhường   3, Phường   Tam Bình, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8684996,
   "Latitude": 106.7346346
 },
 {
   "STT": 1342,
   "Name": "Nhà thuốc Nam Hà",
   "address": "614 Tỉnh lộ 43, Phường   Tam Bình, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8709868,
   "Latitude": 106.7328992
 },
 {
   "STT": 1343,
   "Name": "Nhà thuốc Lê Anh",
   "address": "130 Phú Châu, Kp1, Phường   Tam Bình, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8694444,
   "Latitude": 106.7474138
 },
 {
   "STT": 1344,
   "Name": "Nhà thuốc Hạnh Dung",
   "address": "288 Gò Dưa, Kp2, Phường   Tam Bình, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8725508,
   "Latitude": 106.7441203
 },
 {
   "STT": 1345,
   "Name": "Nhà thuốc Bình Nguyên",
   "address": "500 Tỉnh lộ 43, Kp5, Phường   Tam Phú, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8704215,
   "Latitude": 106.7329878
 },
 {
   "STT": 1346,
   "Name": "Nhà thuốc Thu Vân",
   "address": "139B Đặng Văn Bi, KPhường  5, Phường   Trường Thọ, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8437198,
   "Latitude": 106.7624103
 },
 {
   "STT": 1347,
   "Name": "Nhà thuốc Minh Tâm 8",
   "address": "53A Đường 2, Kp8, Phường   Trường Thọ, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8323626,
   "Latitude": 106.7547515
 },
 {
   "STT": 1348,
   "Name": "Nhà thuốc Vi Thành",
   "address": "740 Kha vạn Cân, Phường   Trường Thọ, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8507962,
   "Latitude": 106.754811
 },
 {
   "STT": 1349,
   "Name": "Nhà thuốc Chơn Nghĩa",
   "address": "126 Trần Quý, Phường  6,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759103,
   "Latitude": 106.655358
 },
 {
   "STT": 1350,
   "Name": "Nhà thuốc Dạ Nam",
   "address": "204 Tuệ Tĩnh, Phường  12,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7606666,
   "Latitude": 106.6530204
 },
 {
   "STT": 1351,
   "Name": "Nhà thuốc Toàn Phúc",
   "address": "1172G Đường 3/2, Phường  12,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759203,
   "Latitude": 106.651111
 },
 {
   "STT": 1352,
   "Name": "Nhà thuốc Phương Vy",
   "address": "534 Nguyễn Chí Thanh, Phường  7,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7586124,
   "Latitude": 106.661221
 },
 {
   "STT": 1353,
   "Name": "Nhà thuốc Vĩnh An",
   "address": "606 Nguyễn Chí Thanh, Phường  7,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7582462,
   "Latitude": 106.6597126
 },
 {
   "STT": 1354,
   "Name": "Nhà thuốc Quang Thuận",
   "address": "610 Nguyễn Chí Thanh, Phường  7,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7581613,
   "Latitude": 106.6596537
 },
 {
   "STT": 1355,
   "Name": "Nhà thuốc Vân Nga",
   "address": "387 Minh Phụng, Phường  10,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759523,
   "Latitude": 106.643959
 },
 {
   "STT": 1356,
   "Name": "Nhà thuốc Thiên Vân ",
   "address": "276 Lạc Long Quân, Phường  10,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.766224,
   "Latitude": 106.642491
 },
 {
   "STT": 1357,
   "Name": "Nhà thuốc Tiến Thành",
   "address": "437 Minh Phụng, Phường  10,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.760896,
   "Latitude": 106.644192
 },
 {
   "STT": 1358,
   "Name": "Nhà thuốc Kim Trinh",
   "address": "341/3C Lạc Long Quân, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.768971,
   "Latitude": 106.640681
 },
 {
   "STT": 1359,
   "Name": "Nhà thuốc Thanh Trung",
   "address": "94/58 Hòa Bình, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.770089,
   "Latitude": 106.6389839
 },
 {
   "STT": 1360,
   "Name": "Nhà thuốc Minh Quân",
   "address": "349D Lạc Long Quân, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.770644,
   "Latitude": 106.644828
 },
 {
   "STT": 1361,
   "Name": "Nhà thuốc Hạnh Ngôn",
   "address": "92A Hòa Bình, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.769043,
   "Latitude": 106.638071
 },
 {
   "STT": 1362,
   "Name": "Nhà thuốc Minh Hùng",
   "address": "023B cc Lạc Long Quân, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7695207,
   "Latitude": 106.6447887
 },
 {
   "STT": 1363,
   "Name": "Nhà thuốc Nhân Đức",
   "address": "012D Cc Lạc Long Quân, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7693968,
   "Latitude": 106.6448926
 },
 {
   "STT": 1364,
   "Name": "Nhà thuốc Huy Thịnh",
   "address": "158 Ông Ích Khiêm, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.76664,
   "Latitude": 106.643316
 },
 {
   "STT": 1365,
   "Name": "Nhà thuốc Đa Khoa",
   "address": "300 Lạc Long Quân, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.767027,
   "Latitude": 106.642852
 },
 {
   "STT": 1366,
   "Name": "Nhà thuốc Phú Bình",
   "address": "3V Lạc Long Quân, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771677,
   "Latitude": 106.64529
 },
 {
   "STT": 1367,
   "Name": "Nhà thuốc Kiều Trang",
   "address": "341/96D Lạc Long Quân, Phường  5,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762189,
   "Latitude": 106.63888
 },
 {
   "STT": 1368,
   "Name": "Nhà thuốc Phúc Lâm",
   "address": "106I/27 Lạc Long Quân, Phường  3,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.76137,
   "Latitude": 106.639748
 },
 {
   "STT": 1369,
   "Name": "Nhà thuốc Hải Nguyên",
   "address": "387 Hàn Hải Nguyên, Phường  2,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758354,
   "Latitude": 106.642342
 },
 {
   "STT": 1370,
   "Name": "Nhà thuốc 340C",
   "address": "340C Minh Phụng, Phường  2,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758272,
   "Latitude": 106.644112
 },
 {
   "STT": 1371,
   "Name": "Nhà thuốc Hồng Tuyến",
   "address": "12 Tân Hóa, Phường  1,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.755818,
   "Latitude": 106.637219
 },
 {
   "STT": 1372,
   "Name": "Nhà thuốc Trúc Loan",
   "address": "704 Hồng Bàng, Phường  1,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.754732,
   "Latitude": 106.640679
 },
 {
   "STT": 1373,
   "Name": "Nhà thuốc Anh Thảo",
   "address": "50 Trần Quý, Phường  6,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759859,
   "Latitude": 106.657442
 },
 {
   "STT": 1374,
   "Name": "Nhà thuốc Thái Hà",
   "address": "88 Tuệ Tĩnh, Phường  13,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762905,
   "Latitude": 106.6545141
 },
 {
   "STT": 1375,
   "Name": "Nhà thuốc Cần Tiến",
   "address": "88 Lãnh Binh Thăng, Phường  13,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7624769,
   "Latitude": 106.6547437
 },
 {
   "STT": 1376,
   "Name": "Nhà thuốc Diễm Ngọc",
   "address": "9 Đường số 5 Cx Bình Thới, Phường  8,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7607722,
   "Latitude": 106.6478906
 },
 {
   "STT": 1377,
   "Name": "Nhà thuốc Anh Vũ",
   "address": "163 Lãnh Binh Thăng, Phường  12,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762377,
   "Latitude": 106.653587
 },
 {
   "STT": 1378,
   "Name": "Nhà thuốc Hữu Tín",
   "address": "252 Tôn Thất Hiệp, Phường  12,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7617248,
   "Latitude": 106.6521935
 },
 {
   "STT": 1379,
   "Name": "Nhà thuốc Phương Châu",
   "address": "174/65/32 Thái Phiên, Phường  8,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.760093,
   "Latitude": 106.648448
 },
 {
   "STT": 1380,
   "Name": "Nhà thuốc Bảo Xinh",
   "address": "136B Bình Thới, Phường  14,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.767224,
   "Latitude": 106.649248
 },
 {
   "STT": 1381,
   "Name": "Nhà thuốc Phương Trình",
   "address": "32/91 Ông Ích Khiêm, Phường  14,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.767685,
   "Latitude": 106.646312
 },
 {
   "STT": 1382,
   "Name": "Nhà thuốc Khang Phát",
   "address": "45B Đường số 2 CX Lữ Gia, Phường  15,  Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.770281,
   "Latitude": 106.657737
 },
 {
   "STT": 1383,
   "Name": "Nhà thuốc Thiên Long 4 - Công ty TNHH một thành viên dược Thiên Long",
   "address": "D19/2 Ấp 4, xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8124813,
   "Latitude": 106.5806832
 },
 {
   "STT": 1384,
   "Name": "Địa điểm kinh doanh Công ty TNHH dược phẩm Ngân Lộc - Nhà thuốc Ngân Hà 16",
   "address": "265  Đường 9A KDC Trung Sơn, xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7320539,
   "Latitude": 106.6895842
 },
 {
   "STT": 1385,
   "Name": "Địa điểm kinh doanh công ty cổ phần dược phẩm Gia Định - Nhà thuốc Tân Kiên",
   "address": "C5/26 Đường Hưng Nhơn, ấp 3, xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7123237,
   "Latitude": 106.596618
 },
 {
   "STT": 1386,
   "Name": "Hiệu thuốc số 30 ",
   "address": "896 Nguyễn Văn Quá, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.848379,
   "Latitude": 106.6357226
 },
 {
   "STT": 1387,
   "Name": "Hiệu thuốc số 39 - Chi nhánh Công ty CP DP Gia Định",
   "address": "138 Đường ĐHT 02, Phường   Đông Hưng Thuận,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8372681,
   "Latitude": 106.6274804
 },
 {
   "STT": 1388,
   "Name": "HT Số 23",
   "address": "44 A Nguyễn Văn Tăng, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8468769,
   "Latitude": 106.817087
 },
 {
   "STT": 1389,
   "Name": "SPG Minh Thư ",
   "address": "1209 Nguyễn Xiển, Phường   Long Bình,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8634309,
   "Latitude": 106.837621
 },
 {
   "STT": 1390,
   "Name": "SPG Kim Hồng",
   "address": "104 Nguyễn V  Tăng, KP  Chân Phúc Cẩm, Phường   Long Thạnh Mỹ,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.84434,
   "Latitude": 106.8159529
 },
 {
   "STT": 1391,
   "Name": "SPG Hồng Gấm ",
   "address": "530 Lã Xuân Oai, KP  Phước Hiệp, Phường   Long Trường,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8178698,
   "Latitude": 106.8093178
 },
 {
   "STT": 1392,
   "Name": "Hiệu Thuốc 56",
   "address": "30 Đường 1, Phường   Long Trường,  Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7985802,
   "Latitude": 106.8099391
 },
 {
   "STT": 1393,
   "Name": "Nhị Trưng 2 (Công ty TNHH An Thiên)",
   "address": "821 Phạm Thế Hiển, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7439281,
   "Latitude": 106.673592
 },
 {
   "STT": 1394,
   "Name": "Địa điểm kinh doanh công ty cổ phần dược phẩm Gia Định- Hiệu thuốc số 68",
   "address": "57 Đường  2, Phường   Hiệp Bình Phước, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.83765,
   "Latitude": 106.716216
 },
 {
   "STT": 1395,
   "Name": "TTYKHK",
   "address": "285 Bà Hom, P13,  Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.75579,
   "Latitude": 106.6247379
 },
 {
   "STT": 1396,
   "Name": "Phano 28",
   "address": "30 Bờ Bao Tân Thắng, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8018901,
   "Latitude": 106.6180286
 },
 {
   "STT": 1397,
   "Name": "Cửa hàng Medicare 29 (S29 Tầng 2 AEON)",
   "address": "30 Bờ Bao Tân Thắng, Phường   Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8018901,
   "Latitude": 106.6180286
 },
 {
   "STT": 1398,
   "Name": "Pha No 17",
   "address": "208 Tân Hương, Phường   Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7900993,
   "Latitude": 106.621654
 },
 {
   "STT": 1399,
   "Name": "HT Số 15 - Chợ Lớn",
   "address": "130A Kênh Tân Hóa, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7780728,
   "Latitude": 106.6397571
 },
 {
   "STT": 1400,
   "Name": "HT 48",
   "address": "124 Trịnh Đình Trọng, Phường   Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.775095,
   "Latitude": 106.64425
 },
 {
   "STT": 1401,
   "Name": "TRUNG TÂM Y TẾ TP NINH BÌNH2",
   "address": "111 Đường TCH 21, Phường   Tân Chánh Hiệp,  Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8607196,
   "Latitude": 106.6305146
 },
 {
   "STT": 1402,
   "Name": "Hoàng Kim (BV Quận 8)",
   "address": "82 Cao Lỗ, Phường   4,  Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7416193,
   "Latitude": 106.6764763
 },
 {
   "STT": 1403,
   "Name": "Bệnh viện Quận Thủ Đức",
   "address": "29 Phú Châu, KPhường  5, Phường   Tam Phú, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8643398,
   "Latitude": 106.7456713
 },
 {
   "STT": 1404,
   "Name": "Nhà thuốc Quỳnh Dao",
   "address": "4C Phạm Vấn, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785221,
   "Latitude": 106.624878
 },
 {
   "STT": 1405,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "262 Tô Hiệu, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7760171,
   "Latitude": 106.6284565
 },
 {
   "STT": 1406,
   "Name": "Nhà thuốc Hiền Ngân",
   "address": "151Bis Lũy Bán Bích, Phường  Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.76703,
   "Latitude": 106.6319
 },
 {
   "STT": 1407,
   "Name": "Nhà thuốc Bảo Tín",
   "address": "37 Thống Nhất, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790353,
   "Latitude": 106.635764
 },
 {
   "STT": 1408,
   "Name": "Nhà thuốc Tâm Việt",
   "address": "491 Âu Cơ, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.780149,
   "Latitude": 106.644018
 },
 {
   "STT": 1409,
   "Name": "Nhà thuốc Ý Thức",
   "address": "173/38 Khuông Việt, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.779748,
   "Latitude": 106.641996
 },
 {
   "STT": 1410,
   "Name": "Nhà thuốc Hoài Nam",
   "address": "1771 Khuông Việt, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7809355,
   "Latitude": 106.642641
 },
 {
   "STT": 1411,
   "Name": "Nhà thuốc Phước Ngọc",
   "address": "23 Trịnh Đình Thảo, Phường  Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.775585,
   "Latitude": 106.634148
 },
 {
   "STT": 1412,
   "Name": "Nhà thuốc Ngọc Hoa",
   "address": "197 Tân Hương, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7896784,
   "Latitude": 106.6180801
 },
 {
   "STT": 1413,
   "Name": "Nhà thuốc Thanh Loan",
   "address": "175 Tân Hương, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7896773,
   "Latitude": 106.6180753
 },
 {
   "STT": 1414,
   "Name": "Nhà thuốc Nguyên Thảo",
   "address": "152 Tân Hương, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7897134,
   "Latitude": 106.6252191
 },
 {
   "STT": 1415,
   "Name": "Nhà thuốc Á Châu",
   "address": "123 Văn Cao, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.78617,
   "Latitude": 106.6219
 },
 {
   "STT": 1416,
   "Name": "Nhà thuốc Minh Đức",
   "address": "1A Phạm Quý Thích, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7898306,
   "Latitude": 106.6213244
 },
 {
   "STT": 1417,
   "Name": "Nhà thuốc Lan Anh 8",
   "address": "281 Tân Hương, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7900531,
   "Latitude": 106.6210639
 },
 {
   "STT": 1418,
   "Name": "Nhà thuốc Ngọc Mai",
   "address": "27 Gò Dầu, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.79551,
   "Latitude": 106.628062
 },
 {
   "STT": 1419,
   "Name": "Nhà thuốc Minh Hồng",
   "address": "96A Trương Vĩnh Ký, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939019,
   "Latitude": 106.633873
 },
 {
   "STT": 1420,
   "Name": "Nhà thuốc Kim Dung",
   "address": "59 Gò Dầu, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.795648,
   "Latitude": 106.627203
 },
 {
   "STT": 1421,
   "Name": "Nhà thuốc Thanh Phương",
   "address": "27 Tân Sơn Nhì, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.796388,
   "Latitude": 106.630744
 },
 {
   "STT": 1422,
   "Name": "Nhà thuốc Thành Tâm",
   "address": "178 Nguyễn Súy, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.794279,
   "Latitude": 106.6226876
 },
 {
   "STT": 1423,
   "Name": "Nhà thuốc An Nhiên",
   "address": "427B Tân Kỳ Tân Quý, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7998155,
   "Latitude": 106.6204583
 },
 {
   "STT": 1424,
   "Name": "Nhà thuốc Vy Lan 3",
   "address": "132 Thoại Ngọc Hầu, Phường  Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.780415,
   "Latitude": 106.633477
 },
 {
   "STT": 1425,
   "Name": "Nhà thuốc Tâm Anh",
   "address": "137 Thoại Ngọc Hầu, Phường  Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.780998,
   "Latitude": 106.635143
 },
 {
   "STT": 1426,
   "Name": "Nhà thuốc Phương Lâm",
   "address": "266 Gò Dầu, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7954055,
   "Latitude": 106.6150696
 },
 {
   "STT": 1427,
   "Name": "Nhà thuốc Anh Thư",
   "address": "152 Nguyễn Cửu Đàm, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8002124,
   "Latitude": 106.6283092
 },
 {
   "STT": 1428,
   "Name": "Nhà thuốc Thiên Phúc",
   "address": "137 Nguyễn Cửu Đàm, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8000529,
   "Latitude": 106.6281372
 },
 {
   "STT": 1429,
   "Name": "Nhà thuốc Thanh Trà",
   "address": "144 Lũy Bán Bích, Phường  Tân Thới Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.769534,
   "Latitude": 106.631927
 },
 {
   "STT": 1430,
   "Name": "Nhà thuốc Tuấn Phong",
   "address": "120A Khuông Việt, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7779009,
   "Latitude": 106.640765
 },
 {
   "STT": 1431,
   "Name": "Nhà thuốc HT số 42",
   "address": "237A Tân Kỳ Tân Quý, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.80077,
   "Latitude": 106.6227048
 },
 {
   "STT": 1432,
   "Name": "Nhà thuốc Phương Thủy",
   "address": "228 Tân Hương, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7900269,
   "Latitude": 106.6197311
 },
 {
   "STT": 1433,
   "Name": "Nhà thuốc Linh",
   "address": "18/45 Đỗ Nhuận, Phường  Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.804547,
   "Latitude": 106.6258019
 },
 {
   "STT": 1434,
   "Name": "Nhà thuốc Quốc Anh",
   "address": "224 Tân Hương, Phường  Tân Quý, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7901429,
   "Latitude": 106.6204643
 },
 {
   "STT": 1435,
   "Name": "Nhà thuốc An Hưng",
   "address": "30 Huỳnh Văn Chính, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.781586,
   "Latitude": 106.641124
 },
 {
   "STT": 1436,
   "Name": "Nhà thuốc Hồng lợi 1",
   "address": "105 Hòa Bình, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.770277,
   "Latitude": 106.631308
 },
 {
   "STT": 1437,
   "Name": "Nhà thuốc PKĐK Hy Vọng",
   "address": "1031 Thoại Ngọc Hầu, Phường  Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7852659,
   "Latitude": 106.6407245
 },
 {
   "STT": 1438,
   "Name": "Nhà thuốc Phương Thảo",
   "address": "20 Nguyễn Hữu Tiến, Phường  Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8084318,
   "Latitude": 106.6258826
 },
 {
   "STT": 1439,
   "Name": "Nhà thuốc Trọng Nhân",
   "address": "333 Tây Thạnh, Phường  Tây Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8101075,
   "Latitude": 106.6198934
 },
 {
   "STT": 1440,
   "Name": "Nhà thuốc An Phúc",
   "address": "35 Nguyễn Lý, Phường  Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777747,
   "Latitude": 106.627498
 },
 {
   "STT": 1441,
   "Name": "Nhà thuốc Hoàng Phúc",
   "address": "64 Lê Thiệt, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785999,
   "Latitude": 106.621762
 },
 {
   "STT": 1442,
   "Name": "Nhà thuốc Phương Lan",
   "address": "366 Nguyễn Sơn, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7844339,
   "Latitude": 106.622114
 },
 {
   "STT": 1443,
   "Name": "Nhà thuốc Quang Huy",
   "address": "231/79/5 Khuông Việt, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.780452,
   "Latitude": 106.642492
 },
 {
   "STT": 1444,
   "Name": "Nhà thuốc Bảo Minh",
   "address": "14 Bờ Bao Tân Thắng, Phường  Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8068539,
   "Latitude": 106.6216343
 },
 {
   "STT": 1445,
   "Name": "Nhà thuốc Minh Khuê",
   "address": "33/1 Lê Trọng Tấn, Phường  Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8040895,
   "Latitude": 106.6319281
 },
 {
   "STT": 1446,
   "Name": "Nhà thuốc Thanh Hồng",
   "address": "137A/249 Tân Kỳ Tân Quý, Phường  Tân Sơn Nhì, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8026783,
   "Latitude": 106.6283955
 },
 {
   "STT": 1447,
   "Name": "Lâm Thủy SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "139 Trương Phước Phan, Phường  Bình Trị Đông, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758615,
   "Latitude": 106.6114004
 },
 {
   "STT": 1448,
   "Name": "An Thiên SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "28/5 Huỳnh Tấn Phát, KP6, Thị trấn Nhà Bè, Quận Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.696558,
   "Latitude": 106.739919
 },
 {
   "STT": 1449,
   "Name": "Bình An SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "219 A1 Nơ Trang Long, Phường  12, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8156079,
   "Latitude": 106.697682
 },
 {
   "STT": 1450,
   "Name": "Red Apple SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "30/1A Ngô Thời Nhiệm, Phường  7, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777619,
   "Latitude": 106.686545
 },
 {
   "STT": 1451,
   "Name": "Đăng Thư SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "36A Đường Số 5, KP 3, Phường  Tân Tạo, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7503152,
   "Latitude": 106.5914917
 },
 {
   "STT": 1452,
   "Name": "Nguyễn Gia SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "306 Nguyễn Sơn, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.783841,
   "Latitude": 106.623987
 },
 {
   "STT": 1453,
   "Name": "Lữ Gia SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "89 Nguyễn Thị Nhỏ, Phường  9, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771464,
   "Latitude": 106.652444
 },
 {
   "STT": 1454,
   "Name": "Minh Châu SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "176A Gò Xoài, Phường  Bình Hưng Hòa A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785035,
   "Latitude": 106.604423
 },
 {
   "STT": 1455,
   "Name": "Đức Tín SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "127 Chu Văn An, Phường  26, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8107114,
   "Latitude": 106.7108719
 },
 {
   "STT": 1456,
   "Name": "Hòa Bình SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "173 Lê Văn Chí, Phường  Linh Trung, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8612829,
   "Latitude": 106.7792499
 },
 {
   "STT": 1457,
   "Name": "Hà Châu SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "136 Đông Hồ, Phường  8, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7843746,
   "Latitude": 106.6515445
 },
 {
   "STT": 1458,
   "Name": "Tuyết Nhung SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "257 Lạc Long Quân, Phường  3, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7647392,
   "Latitude": 106.6421181
 },
 {
   "STT": 1459,
   "Name": "SKV - Công ty TNHH MTV Sức Khỏe Vàng",
   "address": "905 Phạm Thế Hiển, Phường  4, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.743563,
   "Latitude": 106.671913
 },
 {
   "STT": 1460,
   "Name": "Đại lý số 02 - Công ty Sapharco",
   "address": "B10/10 Ấp 2, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6630417,
   "Latitude": 106.5672462
 },
 {
   "STT": 1461,
   "Name": "Đại lý số 06 - Công ty Sapharco",
   "address": "F2/17 Ấp 6, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.818282,
   "Latitude": 106.5681569
 },
 {
   "STT": 1462,
   "Name": "Đại lý số 09 - Công ty Sapharco",
   "address": "A5/29 Ấp 1, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7559001,
   "Latitude": 106.5881869
 },
 {
   "STT": 1463,
   "Name": "Đại lý số 11 - Công ty Sapharco",
   "address": "C3/26 Ấp 3, Xã Hưng Long, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6629492,
   "Latitude": 106.6206998
 },
 {
   "STT": 1464,
   "Name": "Đại lý số 13 - Công ty Sapharco",
   "address": "D2/3A Ấp 4, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8077511,
   "Latitude": 106.5724026
 },
 {
   "STT": 1465,
   "Name": "Đại lý số 17 - Công ty Sapharco",
   "address": "A8/26A Ấp 1, Xã Qui Đức, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6425712,
   "Latitude": 106.6438673
 },
 {
   "STT": 1466,
   "Name": "Đại lý số 18 - Công ty Sapharco",
   "address": "3/30A Ấp 3, Xã Tân Quí Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6761289,
   "Latitude": 106.5917107
 },
 {
   "STT": 1467,
   "Name": "Đại lý số 20 - Công ty Sapharco",
   "address": "7A225 Ấp 7, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.847681,
   "Latitude": 106.5865704
 },
 {
   "STT": 1468,
   "Name": "Đại lý số 21 - Công ty Sapharco",
   "address": "56A/3 Ấp 1, Xã An Phú Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6822525,
   "Latitude": 106.6248823
 },
 {
   "STT": 1469,
   "Name": "Đại lý số 22 - Công ty Sapharco",
   "address": "E9/18, KP 5 Thị trấn Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6844901,
   "Latitude": 106.5731392
 },
 {
   "STT": 1470,
   "Name": "Đại lý số 23 - Công ty Sapharco",
   "address": "Kiosque số 2, KP 1 Thị trấn Tân Túc, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6988903,
   "Latitude": 106.5930255
 },
 {
   "STT": 1471,
   "Name": "Đại lý số 24 - Công ty Sapharco",
   "address": "C5/22 Ấp 3, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7114241,
   "Latitude": 106.5986786
 },
 {
   "STT": 1472,
   "Name": "Đại lý số 26 - Công ty Sapharco",
   "address": "Kiosque số 8, Khu B, Chợ Hưng Long, Xã Hưng Long, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6579417,
   "Latitude": 106.6081706
 },
 {
   "STT": 1473,
   "Name": "Đại lý số 29 - Công ty Sapharco",
   "address": "C4/137 Ấp 3, Xã Bình Lợi, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7529732,
   "Latitude": 106.4806442
 },
 {
   "STT": 1474,
   "Name": "Đại lý số 31 - Công ty Sapharco",
   "address": "E3 Số nhà 34, Ấp 3 Chợ Cầu Xáng, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790994,
   "Latitude": 106.5154525
 },
 {
   "STT": 1475,
   "Name": "Đại lý số 32 - Công ty Sapharco",
   "address": "3A/65/1 Ấp 3, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.860347,
   "Latitude": 106.592133
 },
 {
   "STT": 1476,
   "Name": "Đại lý số 33 - Công ty Sapharco",
   "address": "242 Ấp 1, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7732635,
   "Latitude": 106.5519705
 },
 {
   "STT": 1477,
   "Name": "Đại lý số 35 - Công ty Sapharco",
   "address": "A4/29 Chợ Cầu Xáng, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790994,
   "Latitude": 106.5154525
 },
 {
   "STT": 1478,
   "Name": "Đại lý số 37 - Công ty Sapharco",
   "address": "D2/38A Ấp 4, Xã Đa Phước, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.659338,
   "Latitude": 106.687495
 },
 {
   "STT": 1479,
   "Name": "Đại lý số 39 - Công ty Sapharco",
   "address": "6/3 Ấp 4, Xã Tân Quí Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6591187,
   "Latitude": 106.6051463
 },
 {
   "STT": 1480,
   "Name": "Đại lý số 41 - Công ty Sapharco",
   "address": "Lô 5F Số nhà 19, Khu Phố Chợ Cầu Xáng, Ấp 3, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790994,
   "Latitude": 106.5154525
 },
 {
   "STT": 1481,
   "Name": "Đại lý số 42 - Công ty Sapharco",
   "address": "A7/22/Ấp 1, Xã Qui Đức, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.776099,
   "Latitude": 106.5780894
 },
 {
   "STT": 1482,
   "Name": "Đại lý số 43 - Công ty Sapharco",
   "address": "D19/28C Hương Lộ 80 Ấp 4, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.817264,
   "Latitude": 106.586934
 },
 {
   "STT": 1483,
   "Name": "Đại lý số 47 - Công ty Sapharco",
   "address": "300A/12 Ấp 1, Xã An Phú Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.683901,
   "Latitude": 106.6057518
 },
 {
   "STT": 1484,
   "Name": "Đại lý số 48 - Công ty Sapharco",
   "address": "A5/7YẤp 1, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7911649,
   "Latitude": 106.5642998
 },
 {
   "STT": 1485,
   "Name": "Đại lý số 49 - Công ty Sapharco",
   "address": "C9/24 Ấp 3, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939733,
   "Latitude": 106.5792914
 },
 {
   "STT": 1486,
   "Name": "Đại lý số 50 - Công ty Sapharco",
   "address": "E7/177 Ấp 5, Xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.722304,
   "Latitude": 106.6989253
 },
 {
   "STT": 1487,
   "Name": "Đại lý số 52 - Công ty Sapharco",
   "address": "F1C/68 Ấp 6, Xã Hưng Long, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.688829,
   "Latitude": 106.6240138
 },
 {
   "STT": 1488,
   "Name": "Đại lý số 54 - Công ty Sapharco",
   "address": "B8/28D Ấp 2, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7117404,
   "Latitude": 106.5972267
 },
 {
   "STT": 1489,
   "Name": "Đại lý số 56 - Công ty Sapharco",
   "address": "G16/71A Ấp 7, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8479835,
   "Latitude": 106.5866452
 },
 {
   "STT": 1490,
   "Name": "Đại lý số 57 - Công ty Sapharco",
   "address": "C6/8 Ấp 3, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939733,
   "Latitude": 106.5792914
 },
 {
   "STT": 1491,
   "Name": "Đại lý số 60 - Công ty Sapharco",
   "address": "E18/12 Hương lộ 80, Ấp 5, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8113963,
   "Latitude": 106.5757703
 },
 {
   "STT": 1492,
   "Name": "Đại lý số 61 - Công ty Sapharco",
   "address": "16/20 Ấp 3, Xã Tân Quí Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6761289,
   "Latitude": 106.5917107
 },
 {
   "STT": 1493,
   "Name": "Đại lý số 62 - Công ty Sapharco",
   "address": "A12/09, Quốc Lộ 50 Ấp 1, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7405549,
   "Latitude": 106.656396
 },
 {
   "STT": 1494,
   "Name": "Đại lý số 64 - Công ty Sapharco",
   "address": "A4/117 Ấp 1, Xã Tân Nhựt, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7155493,
   "Latitude": 106.5525145
 },
 {
   "STT": 1495,
   "Name": "Đại lý số 65 - Công ty Sapharco",
   "address": "F7/56A Ấp 6, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.818547,
   "Latitude": 106.5494046
 },
 {
   "STT": 1496,
   "Name": "Đại lý số 69 - Công ty Sapharco",
   "address": "4/27 Ấp 3, Xã Tân Quí Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6761289,
   "Latitude": 106.5917107
 },
 {
   "STT": 1497,
   "Name": "Đại lý số 71 - Công ty Sapharco",
   "address": "A5/149A Ấp 1, Quốc Lộ 50, Xã Đa Phước, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6463769,
   "Latitude": 106.6564798
 },
 {
   "STT": 1498,
   "Name": "Đại lý số 73 - Công ty Sapharco",
   "address": "A5/159D Ấp 1, Xã Tân Nhựt, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7540281,
   "Latitude": 106.5876561
 },
 {
   "STT": 1499,
   "Name": "Đại lý số 83 - Công ty Sapharco",
   "address": "B20/10 Ấp 2, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7941362,
   "Latitude": 106.578819
 },
 {
   "STT": 1500,
   "Name": "Đại lý số 89 - Công ty Sapharco",
   "address": "13/12 Ấp 3, Xã Tân Quí Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6761289,
   "Latitude": 106.5917107
 },
 {
   "STT": 1501,
   "Name": "Đại lý số 90 - Công ty Sapharco",
   "address": "D8/49 Ấp 4, Xã Bình Lợi, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.701159,
   "Latitude": 106.68563
 },
 {
   "STT": 1502,
   "Name": "Đại lý số 92 - Công ty Sapharco",
   "address": "E6/12 Ấp 5, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.864872,
   "Latitude": 106.570554
 },
 {
   "STT": 1503,
   "Name": "Đại lý số 93 - Công ty Sapharco",
   "address": "E12/29B Ấp 5, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.866288,
   "Latitude": 106.564845
 },
 {
   "STT": 1504,
   "Name": "Đại lý số 94 - Công ty Sapharco",
   "address": "B15/307A Ấp 2, Xã Tân Nhựt, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7376457,
   "Latitude": 106.5479036
 },
 {
   "STT": 1505,
   "Name": "Đại lý số 95 - Công ty Sapharco",
   "address": "B5/137, Quốc Lộ 50 Ấp 2, Xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7378519,
   "Latitude": 106.6563273
 },
 {
   "STT": 1506,
   "Name": "Đại lý số 96 - Công ty Sapharco",
   "address": "B9/12B Ấp 2 Hương Lộ 11, Xã Hưng Long, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6565213,
   "Latitude": 106.5863879
 },
 {
   "STT": 1507,
   "Name": "Đại lý số 97 - Công ty Sapharco",
   "address": "D14/34 Ấp 4, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6982056,
   "Latitude": 106.684944
 },
 {
   "STT": 1508,
   "Name": "Đại lý số 100 - Công ty Sapharco",
   "address": "A1/6C Ấp 1, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7127569,
   "Latitude": 106.5985183
 },
 {
   "STT": 1509,
   "Name": "Đại lý số 101 - Công ty Sapharco",
   "address": "A10/1D Nguyễn Cửu Phú, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6970437,
   "Latitude": 106.579826
 },
 {
   "STT": 1510,
   "Name": "Đại lý số 102 - Công ty Sapharco",
   "address": "B8/16 Ấp 3, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7067721,
   "Latitude": 106.6928492
 },
 {
   "STT": 1511,
   "Name": "Đại lý số 103 - Công ty Sapharco",
   "address": "C3/21, Đường Chánh Hưng, Xã Bình Hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.732575,
   "Latitude": 106.673897
 },
 {
   "STT": 1512,
   "Name": "Đại lý số 104 - Công ty Sapharco",
   "address": "D5/6A1/2A Ấp 4, Xã Bình Lợi, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939388,
   "Latitude": 106.5787705
 },
 {
   "STT": 1513,
   "Name": "Đại lý số 105 - Công ty Sapharco",
   "address": "D5/133, Quốc Lộ 50 Ấp 4, Xã Phong Phú, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7378841,
   "Latitude": 106.656341
 },
 {
   "STT": 1514,
   "Name": "Đại lý số 106 - Công ty Sapharco",
   "address": "A1/8 Ấp 1, Hương Lộ 11, Xã Tân Quí Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6678817,
   "Latitude": 106.5890894
 },
 {
   "STT": 1515,
   "Name": "Đại lý số 107 - Công ty Sapharco",
   "address": "A1/17 Ấp 1, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7784476,
   "Latitude": 106.5741809
 },
 {
   "STT": 1516,
   "Name": "Đại lý số 108 - Công ty Sapharco",
   "address": "1A 115 Ấp 1, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7761435,
   "Latitude": 106.542813
 },
 {
   "STT": 1517,
   "Name": "Đại lý số 110 - Công ty Sapharco",
   "address": "D8/16 Ấp 4 Xã Bình Lợi, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6591958,
   "Latitude": 106.6879783
 },
 {
   "STT": 1518,
   "Name": "Đại lý số 112 - Công ty Sapharco",
   "address": "B3 316 Ấp 2, Xã Bình Lợi, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7961716,
   "Latitude": 106.5793333
 },
 {
   "STT": 1519,
   "Name": "Đại lý số 114 - Công ty Sapharco",
   "address": "5B6 Ấp 5, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.866182,
   "Latitude": 106.565433
 },
 {
   "STT": 1520,
   "Name": "Đại lý số 115 - Công ty Sapharco",
   "address": "369B/6, Xã An Phú Tây, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6880126,
   "Latitude": 106.6085003
 },
 {
   "STT": 1521,
   "Name": "Đại lý số 117 - Công ty Sapharco",
   "address": "C9/10 Ấp 3, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6621018,
   "Latitude": 106.5689036
 },
 {
   "STT": 1522,
   "Name": "Đại lý số 118 - Công ty Sapharco",
   "address": "G15/29B Ấp 7, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.847681,
   "Latitude": 106.5865704
 },
 {
   "STT": 1523,
   "Name": "Đại lý số 119 - Công ty Sapharco",
   "address": "A1/15 Ấp 1, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7775554,
   "Latitude": 106.5375714
 },
 {
   "STT": 1524,
   "Name": "Đại lý số 120 - Công ty Sapharco",
   "address": "B19/397 Ấp 2, Xã Tân Nhựt, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7376457,
   "Latitude": 106.5479036
 },
 {
   "STT": 1525,
   "Name": "Đại lý số 121 - Công ty Sapharco",
   "address": "E10/11 Hương Lộ 80 Ấp 5, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8113963,
   "Latitude": 106.5757703
 },
 {
   "STT": 1526,
   "Name": "Đại lý số 124 - Công ty Sapharco",
   "address": "C6/191 Ấp 3, Xã Tân Nhựt, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7938973,
   "Latitude": 106.5798371
 },
 {
   "STT": 1527,
   "Name": "Đại lý số 126 - Công ty Sapharco",
   "address": "A9/16 Ấp 1, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7563244,
   "Latitude": 106.5882396
 },
 {
   "STT": 1528,
   "Name": "Đại lý số 127 - Công ty Sapharco",
   "address": "D7/25A Ấp 4, Xã Tân Kiên, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7322911,
   "Latitude": 106.5813356
 },
 {
   "STT": 1529,
   "Name": "Đại lý số 130 - Công ty Sapharco",
   "address": "F1/1 Ấp 6, Xã Lê Minh Xuân, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7698181,
   "Latitude": 106.5449272
 },
 {
   "STT": 1530,
   "Name": "Đại lý số 131 - Công ty Sapharco",
   "address": "B11/12 Ấp 2, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6577383,
   "Latitude": 106.5894985
 },
 {
   "STT": 1531,
   "Name": "Đại lý số 133 - Công ty Sapharco",
   "address": "6A39/1 Ấp 6, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.818282,
   "Latitude": 106.5681569
 },
 {
   "STT": 1532,
   "Name": "Đại lý số 135 - Công ty Sapharco",
   "address": "A1/66B Ấp 1, Xã Bình Chánh, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6453258,
   "Latitude": 106.5418846
 },
 {
   "STT": 1533,
   "Name": "Đại lý số 138 - Công ty Sapharco",
   "address": "3C70/1 Ấp 3, Xã Phạm Văn Hai, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7939733,
   "Latitude": 106.5792914
 },
 {
   "STT": 1535,
   "Name": "Đại lý số 26 - Công ty Sapharco",
   "address": "328 Rừng Sát, Bình Phước, Bình Khánh, Huyện Cần Giờ, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6446012,
   "Latitude": 106.7870662
 },
 {
   "STT": 1536,
   "Name": "Đại lý số 32 - Công ty Sapharco",
   "address": "185 Ấp Thanh Bình, Xã Thạnh An, Huyện Cần Giờ, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.4678755,
   "Latitude": 106.9734929
 },
 {
   "STT": 1537,
   "Name": "Đại lý số 36 - Công ty Sapharco",
   "address": "450/2 Đường Đào Cữ, Xã Cần Thạnh, Huyện Cần Giờ, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.41057,
   "Latitude": 106.9647824
 },
 {
   "STT": 1538,
   "Name": "Đại lý số 01 - Công ty Sapharco",
   "address": "Ấp Đình, Xã Tân Phú Trung, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.875766,
   "Latitude": 106.5955707
 },
 {
   "STT": 1539,
   "Name": "Đại lý số 02 - Công ty Sapharco",
   "address": "169 Quốc lộ 22, Ấp Đình, Xã Tân Phú Trung, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9356158,
   "Latitude": 106.5477324
 },
 {
   "STT": 1540,
   "Name": "Đại lý số 03 - Công ty Sapharco",
   "address": "318/ 7 Ấp Đình, Xã Tân Phú Trung, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.87628,
   "Latitude": 106.594692
 },
 {
   "STT": 1541,
   "Name": "Đại lý số 04 - Công ty Sapharco",
   "address": "370 Ấp Đình, Xã Tân Phú Trung, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8777196,
   "Latitude": 106.5962195
 },
 {
   "STT": 1542,
   "Name": "Đại lý số 05 - Công ty Sapharco",
   "address": "822/12B Ấp Tân Tiến, Xã Tân Thông Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9555622,
   "Latitude": 106.5133215
 },
 {
   "STT": 1543,
   "Name": "Đại lý số 06 - Công ty Sapharco",
   "address": "Ấp Tân Tiến, Xã Tân Thông Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9620271,
   "Latitude": 106.5046477
 },
 {
   "STT": 1544,
   "Name": "Đại lý số 07 - Công ty Sapharco",
   "address": "180/ 7E KP 5 Thị trấn Củ Chi, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9747373,
   "Latitude": 106.4963302
 },
 {
   "STT": 1545,
   "Name": "Đại lý số 08 - Công ty Sapharco",
   "address": "Tổ 8, Ấp 12, Xã Tân Thạnh Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9800748,
   "Latitude": 106.5915495
 },
 {
   "STT": 1546,
   "Name": "Đại lý số 09 - Công ty Sapharco",
   "address": "219 Tỉnh lộ 8, Khu phố 3, Thị trấn Củ Chi, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9747373,
   "Latitude": 106.4963302
 },
 {
   "STT": 1547,
   "Name": "Đại lý số 10 - Công ty Sapharco",
   "address": "384 Khu phố 4, Thị trấn Củ Chi, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9766912,
   "Latitude": 106.502066
 },
 {
   "STT": 1548,
   "Name": "Đại lý số 12 - Công ty Sapharco",
   "address": "839 Tổ 2, Khu phố 5, Thị trấn Củ Chi, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9765115,
   "Latitude": 106.49563
 },
 {
   "STT": 1549,
   "Name": "Đại lý số 13 - Công ty Sapharco",
   "address": "Tổ 7 KP 8 Thị trấn Củ Chi, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9725812,
   "Latitude": 106.496089
 },
 {
   "STT": 1550,
   "Name": "Đại lý số 16 - Công ty Sapharco",
   "address": "Ấp Bàu Tre 2, Xã Tân An Hôi, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9840731,
   "Latitude": 106.4716425
 },
 {
   "STT": 1551,
   "Name": "Đại lý số 17 - Công ty Sapharco",
   "address": "Tổ 4, Ấp Mũi Côn Đại, Xã Phước Hiệp, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9778597,
   "Latitude": 106.4603908
 },
 {
   "STT": 1552,
   "Name": "Đại lý số 18 - Công ty Sapharco",
   "address": "Ấp Chợ, Xã Phước Thạnh, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0039855,
   "Latitude": 106.4308985
 },
 {
   "STT": 1553,
   "Name": "Đại lý số 19 - Công ty Sapharco",
   "address": "Tổ 2, Ấp Phước Lộc, Xã Phước Thạnh, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0117038,
   "Latitude": 106.4290019
 },
 {
   "STT": 1554,
   "Name": "Đại lý số 20 - Công ty Sapharco",
   "address": "Ấp Chợ, Xã Phước Thạnh, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0039855,
   "Latitude": 106.4308985
 },
 {
   "STT": 1555,
   "Name": "Đại lý số 21 - Công ty Sapharco",
   "address": "Ấp Phước Hưng, Xã Phước Thạnh, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0106066,
   "Latitude": 106.4326267
 },
 {
   "STT": 1556,
   "Name": "Đại lý số 22 - Công ty Sapharco",
   "address": "Ấp Phước Hưng, Xã Phước Thạnh, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0106066,
   "Latitude": 106.4326267
 },
 {
   "STT": 1557,
   "Name": "Đại lý số 23 - Công ty Sapharco",
   "address": "Ấp Trung Bình, Xã Trung Lập Thượng, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0505031,
   "Latitude": 106.4496124
 },
 {
   "STT": 1559,
   "Name": "Đại lý số 25 - Công ty Sapharco",
   "address": "Số 9, Tổ 7, Ấp Trung Bình, Xã Trung Lập Thượng, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0505031,
   "Latitude": 106.4496124
 },
 {
   "STT": 1560,
   "Name": "Đại lý số 26 - Công ty Sapharco",
   "address": "Ấp Trung Hòa, Xã Trung Lập Hạ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0094048,
   "Latitude": 106.4708514
 },
 {
   "STT": 1561,
   "Name": "Đại lý số 27 - Công ty Sapharco",
   "address": "Ấp Xóm Mới, Xã Trung Lập Hạ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.00477,
   "Latitude": 106.475536
 },
 {
   "STT": 1562,
   "Name": "Đại lý số 28 - Công ty Sapharco",
   "address": "Ấp Lô 6, Xã An Nhơn Tây, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0616635,
   "Latitude": 106.474793
 },
 {
   "STT": 1563,
   "Name": "Đại lý số 29 - Công ty Sapharco",
   "address": "Ấp Chợ Cũ, Xã An Nhơn Tây, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.074436,
   "Latitude": 106.4759262
 },
 {
   "STT": 1564,
   "Name": "Đại lý số 30 - Công ty Sapharco",
   "address": "Tổ 1, Ấp Chợ Cũ, Xã An Nhơn Tây, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0876344,
   "Latitude": 106.5125541
 },
 {
   "STT": 1565,
   "Name": "Đại lý số 31 - Công ty Sapharco",
   "address": "Số 315, Tỉnh lộ 7, Ấp Chợ Cũ, Xã An Nhơn Tây, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0876344,
   "Latitude": 106.5125541
 },
 {
   "STT": 1566,
   "Name": "Đại lý số 32 - Công ty Sapharco",
   "address": "Ấp Chợ Cũ, Xã An Nhơn Tây, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.074436,
   "Latitude": 106.4759262
 },
 {
   "STT": 1567,
   "Name": "Đại lý số 33 - Công ty Sapharco",
   "address": "Ấp Chợ, Xã Phú Hòa Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0258001,
   "Latitude": 106.5646094
 },
 {
   "STT": 1568,
   "Name": "Đại lý số 34 - Công ty Sapharco",
   "address": "Ấp Chợ, Xã Phú Hòa Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0258001,
   "Latitude": 106.5646094
 },
 {
   "STT": 1569,
   "Name": "Đại lý số 35 - Công ty Sapharco",
   "address": "Tổ 67, Ấp Chợ, Xã Phú Hòa Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0258182,
   "Latitude": 106.5651626
 },
 {
   "STT": 1570,
   "Name": "Đại lý số 37 - Công ty Sapharco",
   "address": "Tổ 17, Ấp Phú Mỹ, Xã Phú Hòa Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0214225,
   "Latitude": 106.5571011
 },
 {
   "STT": 1571,
   "Name": "Đại lý số 38 - Công ty Sapharco",
   "address": "85 Tỉnh lộ 15, Ấp 1A, Xã Tân Thạnh Tây, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9783196,
   "Latitude": 106.5803276
 },
 {
   "STT": 1572,
   "Name": "Đại lý số 39 - Công ty Sapharco",
   "address": "Ấp 1, Xã Tân Thạnh Tây, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9838096,
   "Latitude": 106.574157
 },
 {
   "STT": 1573,
   "Name": "Đại lý số 40 - Công ty Sapharco",
   "address": "Ấp Phú Lợi, Xã Phú Mỹ Hưng, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.1295039,
   "Latitude": 106.4781177
 },
 {
   "STT": 1574,
   "Name": "Đại lý số 41 - Công ty Sapharco",
   "address": "Ấp An Hòa, Xã An Phú, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.1079382,
   "Latitude": 106.488353
 },
 {
   "STT": 1575,
   "Name": "Đại lý số 42 - Công ty Sapharco",
   "address": "Ấp 1, Xã Hòa phú, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9798659,
   "Latitude": 106.616409
 },
 {
   "STT": 1576,
   "Name": "Đại lý số 43 - Công ty Sapharco",
   "address": "Ấp 3, Xã Phạm Văn Cội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0389802,
   "Latitude": 106.5360146
 },
 {
   "STT": 1577,
   "Name": "Đại lý số 44 - Công ty Sapharco",
   "address": "Tỉnh lộ 8, Ấp 12, Xã Tân Thạnh Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9827699,
   "Latitude": 106.5814887
 },
 {
   "STT": 1578,
   "Name": "Đại lý số 45 - Công ty Sapharco",
   "address": "Tổ 3, Ấp 1, Xã Bình Mỹ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9626211,
   "Latitude": 106.6475567
 },
 {
   "STT": 1579,
   "Name": "Đại lý số 46 - Công ty Sapharco",
   "address": "Ấp 1, Xã Bình Mỹ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.937821,
   "Latitude": 106.644845
 },
 {
   "STT": 1580,
   "Name": "Đại lý số 47 - Công ty Sapharco",
   "address": "Ấp 2A, Xã Hòa phú, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9712734,
   "Latitude": 106.6132227
 },
 {
   "STT": 1581,
   "Name": "Đại lý số 48 - Công ty Sapharco",
   "address": "Tổ 2, Ấp Trạm Bơm, Xã tân Phú Trung, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.929157,
   "Latitude": 106.5548234
 },
 {
   "STT": 1582,
   "Name": "Đại lý số 50 - Công ty Sapharco",
   "address": "Tổ 3, Ấp Đình, Xã Tân Phú Trung, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.877563,
   "Latitude": 106.592411
 },
 {
   "STT": 1583,
   "Name": "Đại lý số 51 - Công ty Sapharco",
   "address": "Ấp Tân Tiến, Xã Tân Thông Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9620271,
   "Latitude": 106.5046477
 },
 {
   "STT": 1584,
   "Name": "Đại lý số 52 - Công ty Sapharco",
   "address": "Tổ 2, Ấp Hậu, Xã Tân Thông Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9555622,
   "Latitude": 106.5133215
 },
 {
   "STT": 1585,
   "Name": "Đại lý số 53 - Công ty Sapharco",
   "address": "Ấp Đình, Xã Tân Phú Trung, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.875766,
   "Latitude": 106.5955707
 },
 {
   "STT": 1586,
   "Name": "Đại lý số 54 - Công ty Sapharco",
   "address": "Ấp Chợ, Xã Phước Thạnh, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0039855,
   "Latitude": 106.4308985
 },
 {
   "STT": 1587,
   "Name": "Đại lý số 56 - Công ty Sapharco",
   "address": "Ấp Tân Tiến, Xã Tân Thông Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9620271,
   "Latitude": 106.5046477
 },
 {
   "STT": 1588,
   "Name": "Đại lý số 57 - Công ty Sapharco",
   "address": "Tổ 5, Ấp 8, Xã Tân Thạnh Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9542297,
   "Latitude": 106.605953
 },
 {
   "STT": 1589,
   "Name": "Đại lý số 58 - Công ty Sapharco",
   "address": "Ấp 1, Xã Tân Thạnh Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9582139,
   "Latitude": 106.5933037
 },
 {
   "STT": 1590,
   "Name": "Đại lý số 59 - Công ty Sapharco",
   "address": "315 Ấp Chợ, Xã Trung An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9329535,
   "Latitude": 106.5642998
 },
 {
   "STT": 1591,
   "Name": "Đại lý số 60 - Công ty Sapharco",
   "address": "512 Tỉnh lộ 7, Ấp Mỹ KhánhA, Xã Thi Mỹ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9906645,
   "Latitude": 106.4084838
 },
 {
   "STT": 1592,
   "Name": "Đại lý số 61 - Công ty Sapharco",
   "address": "Ấp 5, Xã Bình Mỹ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.934113,
   "Latitude": 106.6434288
 },
 {
   "STT": 1593,
   "Name": "Đại lý số 63 - Công ty Sapharco",
   "address": "Tổ 9, Ấp Trảng Lắm, Xã Trung Lập Hạ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.999888,
   "Latitude": 106.4817152
 },
 {
   "STT": 1594,
   "Name": "Đại lý số 64 - Công ty Sapharco",
   "address": "Tổ 1, Ấp 4, Xã Phước Vĩnh An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9618511,
   "Latitude": 106.5289241
 },
 {
   "STT": 1595,
   "Name": "Đại lý số 67 - Công ty Sapharco",
   "address": "Ấp Bàu Tre 1, Xã Tân An Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9783225,
   "Latitude": 106.4691964
 },
 {
   "STT": 1596,
   "Name": "Đại lý số 68 - Công ty Sapharco",
   "address": "Hương lộ 2, Tổ 2, Xã Phước Vĩnh An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0142299,
   "Latitude": 106.4664346
 },
 {
   "STT": 1597,
   "Name": "Đại lý số 70 - Công ty Sapharco",
   "address": "Tổ 3, Ấp An Hòa, Xã An Phú, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.1079382,
   "Latitude": 106.488353
 },
 {
   "STT": 1598,
   "Name": "Đại lý số 72 - Công ty Sapharco",
   "address": "Lộ 4, Ấp Tân Tiến, Xã Tân Thông Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9649557,
   "Latitude": 106.507257
 },
 {
   "STT": 1599,
   "Name": "Đại lý số 73 - Công ty Sapharco",
   "address": "Tổ 3, Ấp Cây Trắc, Xã Phú Hòa Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0181953,
   "Latitude": 106.5342191
 },
 {
   "STT": 1600,
   "Name": "Đại lý số 74 - Công ty Sapharco",
   "address": "Tổ 3B, Ấp Phú Hiệp, Xã Phú Mỹ Hưng, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.1413926,
   "Latitude": 106.4621329
 },
 {
   "STT": 1601,
   "Name": "Đại lý số 75 - Công ty Sapharco",
   "address": "11 Nguyễn Văn Ni, KP 6 Thị trấn Củ Chi, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9665068,
   "Latitude": 106.4965114
 },
 {
   "STT": 1602,
   "Name": "Đại lý số 77 - Công ty Sapharco",
   "address": "Quốc lộ 22, Ấp Chánh, Xã Tân Thông Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9783225,
   "Latitude": 106.4691964
 },
 {
   "STT": 1603,
   "Name": "Đại lý số 78 - Công ty Sapharco",
   "address": "Tổ 12, Ấp 3, Xã Phước Vĩnh An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0440609,
   "Latitude": 106.5462348
 },
 {
   "STT": 1604,
   "Name": "Đại lý số 79 - Công ty Sapharco",
   "address": "Tỉnh lộ 8, tổ 13, Ấp Thạnh An, Xã Trung An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9852234,
   "Latitude": 106.5826151
 },
 {
   "STT": 1605,
   "Name": "Đại lý số 80 - Công ty Sapharco",
   "address": "Tổ 3, Ấp Bàu Tròn, Xã Nhuận Đức, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.024223,
   "Latitude": 106.484996
 },
 {
   "STT": 1606,
   "Name": "Đại lý số 81 - Công ty Sapharco",
   "address": "52 Nguyễn Thị Ranh, Ấp Xóm Mới, Xã Trung Lập Hạ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9842185,
   "Latitude": 106.4704215
 },
 {
   "STT": 1607,
   "Name": "Đại lý số 82 - Công ty Sapharco",
   "address": "Tổ 5, Ấp Bình Thượng 2, Xã Thái Mỹ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.99126,
   "Latitude": 106.3984412
 },
 {
   "STT": 1609,
   "Name": "Đại lý số 85 - Công ty Sapharco",
   "address": "48, Tỉnh lộ 8, Tổ 2, Ấp Hội Thạnh, Xã Trung An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9838981,
   "Latitude": 106.5745979
 },
 {
   "STT": 1610,
   "Name": "Đại lý số 86 - Công ty Sapharco",
   "address": "Tổ 6, Ấp 4, Xã Phước Vĩnh An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9852327,
   "Latitude": 106.5214134
 },
 {
   "STT": 1611,
   "Name": "Đại lý số 87 - Công ty Sapharco",
   "address": "Tỉnh lộ 15, Ấp 1, Xã Tân Thạnh Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9682634,
   "Latitude": 106.5891043
 },
 {
   "STT": 1612,
   "Name": "Đại lý số 88 - Công ty Sapharco",
   "address": "Tổ 4, Ấp Ng Tư, Xã Nhuận Đức, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0386606,
   "Latitude": 106.488483
 },
 {
   "STT": 1613,
   "Name": "Đại lý số 89 - Công ty Sapharco",
   "address": "QL 22, Tổ 10, Ấp Cy Trơm, Xã Phước Hiệp, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9891271,
   "Latitude": 106.4517236
 },
 {
   "STT": 1614,
   "Name": "Đại lý số 90 - Công ty Sapharco",
   "address": "Tổ 6, Ấp Xóm Chùa, Xã Tân An Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9593528,
   "Latitude": 106.4818167
 },
 {
   "STT": 1615,
   "Name": "Đại lý số 91 - Công ty Sapharco",
   "address": "1393 Tổ 3 Ấp Hội Thạnh, Xã Trung An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8230989,
   "Latitude": 106.6296638
 },
 {
   "STT": 1616,
   "Name": "Đại lý số 92 - Công ty Sapharco",
   "address": "Tổ 1, Ấp Chợ, Xã Tân Phú Trung, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9330856,
   "Latitude": 106.5672165
 },
 {
   "STT": 1617,
   "Name": "Đại lý số 93 - Công ty Sapharco",
   "address": "Tổ 21, Ấp 3, Xã Phạm Văn Cội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0437502,
   "Latitude": 106.5463119
 },
 {
   "STT": 1618,
   "Name": "Đại lý số 95 - Công ty Sapharco",
   "address": "Tổ 4, Ấp Chợ, Xã Phước Thạnh, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0039855,
   "Latitude": 106.4308985
 },
 {
   "STT": 1619,
   "Name": "Đại lý số 97 - Công ty Sapharco",
   "address": "05 Nguyễn Thị Lắng, Ấp Chợ, Xã Tân Phú Trung, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9329693,
   "Latitude": 106.5520648
 },
 {
   "STT": 1620,
   "Name": "Đại lý số 100 - Công ty Sapharco",
   "address": "198 Tỉnh lộ 7, Tổ 5, Ấp Lào Táo Trung, Xã Trung Lập Hạ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9931415,
   "Latitude": 106.3808494
 },
 {
   "STT": 1621,
   "Name": "Đại lý số 101 - Công ty Sapharco",
   "address": "364 Tổ 7, Ấp Thạnh An, Xã Trung An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0048859,
   "Latitude": 106.5893129
 },
 {
   "STT": 1622,
   "Name": "Đại lý số 102 - Công ty Sapharco",
   "address": "Số 199B, Tỉnh lộ 2, Tổ 3, Ấp 3, Xã Phước Vĩnh An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0441883,
   "Latitude": 106.5470395
 },
 {
   "STT": 1623,
   "Name": "Đại lý số 103 - Công ty Sapharco",
   "address": "Tổ 3, Ấp Bình Hạ Tây, Xã Thái Mỹ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.990917,
   "Latitude": 106.409935
 },
 {
   "STT": 1624,
   "Name": "Đại lý số 104 - Công ty Sapharco",
   "address": "TL 7, Ấp Lào Táo Thượng, Xã Trung Lập Thượng, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0383463,
   "Latitude": 106.4513299
 },
 {
   "STT": 1625,
   "Name": "Đại lý số 105 - Công ty Sapharco",
   "address": "Tổ 10, Ấp Cây Sộp, Xã Tân An Hội, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.002898,
   "Latitude": 106.4942846
 },
 {
   "STT": 1626,
   "Name": "Đại lý số 106 - Công ty Sapharco",
   "address": "Tổ 8, Ấp Hội Thạnh, Xã Trung An, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0048859,
   "Latitude": 106.5893129
 },
 {
   "STT": 1627,
   "Name": "Đại lý số 109 - Công ty Sapharco",
   "address": "Tổ 8, Tỉnh lộ 9, Ấp 1, Xã Bình Mỹ, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.937821,
   "Latitude": 106.644845
 },
 {
   "STT": 1628,
   "Name": "Đại lý số 01 - Công ty Sapharco",
   "address": "3/22A Đặng Thúc Vịnh, Ấp 1, Xã Đông Thạnh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.905923,
   "Latitude": 106.639502
 },
 {
   "STT": 1629,
   "Name": "Đại lý số 02 - Công ty Sapharco",
   "address": "125/4 Ấp Trung Chánh 1, Xã Trung Chánh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8614322,
   "Latitude": 106.6104551
 },
 {
   "STT": 1630,
   "Name": "Đại lý số 03 - Công ty Sapharco",
   "address": "2/2 Ấp Chánh 2, Xã Xã Tân Xuân, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8672932,
   "Latitude": 106.5990654
 },
 {
   "STT": 1631,
   "Name": "Đại lý số 08 - Công ty Sapharco",
   "address": "1/1 ĐẤp Mỹ Huề, Xã Trung Chánh, Huyện Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8632585,
   "Latitude": 106.6023301
 },
 {
   "STT": 1632,
   "Name": "Đại lý số 09 - Công ty Sapharco",
   "address": "40/1 Quang Trung, KP 2, Thị trấn Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8883126,
   "Latitude": 106.5978609
 },
 {
   "STT": 1633,
   "Name": "Đại lý số 11 - Công ty Sapharco",
   "address": "9/4 Ấp 4, Xã Đông Thạnh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9045894,
   "Latitude": 106.652365
 },
 {
   "STT": 1634,
   "Name": "Đại lý số 15 - Công ty Sapharco",
   "address": "101/57C Ấp 4, Xã Đông Thạnh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.906045,
   "Latitude": 106.651098
 },
 {
   "STT": 1635,
   "Name": "Đại lý số 16 - Công ty Sapharco",
   "address": "60/15B Tiền Lân, Xã Bà Điểm, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.844897,
   "Latitude": 106.5922329
 },
 {
   "STT": 1636,
   "Name": "Đại lý số 17 - Công ty Sapharco",
   "address": "5/2 Ấp Đông, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.876922,
   "Latitude": 106.6160741
 },
 {
   "STT": 1637,
   "Name": "Đại lý số 18 - Công ty Sapharco",
   "address": "43/5 Lê Thị Hà, Ấp Chánh 2, Xã Tân Xuân, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.873352,
   "Latitude": 106.598315
 },
 {
   "STT": 1638,
   "Name": "Đại lý số 22 - Công ty Sapharco",
   "address": "23/3 Ấp Tân Tiến, Xã Xuân Thới Đông, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8687479,
   "Latitude": 106.5890718
 },
 {
   "STT": 1639,
   "Name": "Đại lý số 24 - Công ty Sapharco",
   "address": "1/ 9 Bùi Công Trừng, Ấp 2, Nhị Bình, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9114178,
   "Latitude": 106.6744814
 },
 {
   "STT": 1640,
   "Name": "Đại lý số 25 - Công ty Sapharco",
   "address": "14/ 20 Bùi Văn Ngữ, Ấp Đông, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8706241,
   "Latitude": 106.6161149
 },
 {
   "STT": 1641,
   "Name": "Đại lý số 26 - Công ty Sapharco",
   "address": "24 Ấp 7, Xã Đông Thạnh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8479938,
   "Latitude": 106.5864728
 },
 {
   "STT": 1642,
   "Name": "Đại lý số 28 - Công ty Sapharco",
   "address": "60/ 1B, Ấp Xuân Thới Đông 1, Xã Xuân Thới Đông, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8606646,
   "Latitude": 106.6032062
 },
 {
   "STT": 1643,
   "Name": "Đại lý số 31 - Công ty Sapharco",
   "address": "49 Lê Thị Hà, Xã Tân Xuân, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.880553,
   "Latitude": 106.5993494
 },
 {
   "STT": 1644,
   "Name": "Đại lý số 32 - Công ty Sapharco",
   "address": "34/3 TL 14, Ấp 3, Xuân Thới Thượng, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.856781,
   "Latitude": 106.595048
 },
 {
   "STT": 1645,
   "Name": "Đại lý số 34 - Công ty Sapharco",
   "address": "98/3A, Dương Công Khi, Ấp 1, Xuân Thới Thượng, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8708796,
   "Latitude": 106.5630851
 },
 {
   "STT": 1646,
   "Name": "Đại lý số 37 - Công ty Sapharco",
   "address": "5/2B Trung Đông, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.896183,
   "Latitude": 106.615246
 },
 {
   "STT": 1647,
   "Name": "Đại lý số 38 - Công ty Sapharco",
   "address": "4/41C Quang Trung, Ấp Nam Thới, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8883745,
   "Latitude": 106.5967358
 },
 {
   "STT": 1648,
   "Name": "Đại lý số 39 - Công ty Sapharco",
   "address": "KIOS 6, Ấp 5, Xã Xuân Thới Sơn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8687479,
   "Latitude": 106.5890718
 },
 {
   "STT": 1649,
   "Name": "Đại lý số 43 - Công ty Sapharco",
   "address": "10/82 Ấp 2, Xã Nhị Bình, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9132585,
   "Latitude": 106.6733441
 },
 {
   "STT": 1650,
   "Name": "Đại lý số 44 - Công ty Sapharco",
   "address": "KiosẤp 5, Chợ Xuân Thới Thượng, Xã Xuân Thới Thượng, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8627503,
   "Latitude": 106.5711513
 },
 {
   "STT": 1651,
   "Name": "Đại lý số 45 - Công ty Sapharco",
   "address": "52/7 Ấp 1, Xã Xuân Thới Sơn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8631506,
   "Latitude": 106.5685711
 },
 {
   "STT": 1652,
   "Name": "Đại lý số 46 - Công ty Sapharco",
   "address": "Kios số 1, Chợ Nhị Xuân, Ấp 5, Xã Xuân Thới Sơn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8715653,
   "Latitude": 106.5371947
 },
 {
   "STT": 1653,
   "Name": "Đại lý số 51 - Công ty Sapharco",
   "address": "Số 1, Đặng Thúc Vịnh, Ấp Trung Đông, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.89517,
   "Latitude": 106.6212
 },
 {
   "STT": 1654,
   "Name": "Đại lý số 52 - Công ty Sapharco",
   "address": "59/4 E Xuân Thới Đông 1, Xã Xuân Thới Đông, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8606646,
   "Latitude": 106.6032062
 },
 {
   "STT": 1655,
   "Name": "Đại lý số 54 - Công ty Sapharco",
   "address": "88/1 Ấp Tân Thới 1, Xã Tân Hiệp, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.893388,
   "Latitude": 106.595603
 },
 {
   "STT": 1656,
   "Name": "Đại lý số 55 - Công ty Sapharco",
   "address": "88/5A Nguyễn Ảnh Thủ, Tây Lân, Xã Bà Điểm, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8460179,
   "Latitude": 106.5999565
 },
 {
   "STT": 1657,
   "Name": "Đại lý số 56 - Công ty Sapharco",
   "address": "24/4D Đông Lân, Xã Bà Điểm, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.840946,
   "Latitude": 106.607785
 },
 {
   "STT": 1658,
   "Name": "Đại lý số 57 - Công ty Sapharco",
   "address": "96/1 Nguyễn Ảnh Thủ, Tây Lân, Xã Bà Điểm, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.842953,
   "Latitude": 106.5995607
 },
 {
   "STT": 1659,
   "Name": "Đại lý số 59 - Công ty Sapharco",
   "address": "19/1B Bắc Lân, Xã Bà Điểm, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.854166,
   "Latitude": 106.6004932
 },
 {
   "STT": 1660,
   "Name": "Đại lý số 60 - Công ty Sapharco",
   "address": "1/92 Ấp Nhị Tân, Xã Tân Thới Nhì, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8930687,
   "Latitude": 106.5720007
 },
 {
   "STT": 1661,
   "Name": "Đại lý số 61 - Công ty Sapharco",
   "address": "5/ 4A Bắc Lân, Xã Bà Điểm, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.850882,
   "Latitude": 106.600166
 },
 {
   "STT": 1662,
   "Name": "Đại lý số 62 - Công ty Sapharco",
   "address": "2/6C Bắc Lân, Xã Bà Điểm, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.852058,
   "Latitude": 106.599607
 },
 {
   "STT": 1663,
   "Name": "Đại lý số 63 - Công ty Sapharco",
   "address": "7/31C Ấp 3, Đông Thạnh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9161497,
   "Latitude": 106.651683
 },
 {
   "STT": 1664,
   "Name": "Đại lý số 64 - Công ty Sapharco",
   "address": "2/201A Dương Công khi, Xã Tân Thới Nhì, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8917736,
   "Latitude": 106.5703493
 },
 {
   "STT": 1665,
   "Name": "Đại lý số 65 - Công ty Sapharco",
   "address": "43/3 QL 22, Xuân Thới Đông 2, Xã Xuân Thới Đông, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8606646,
   "Latitude": 106.6032062
 },
 {
   "STT": 1666,
   "Name": "Đại lý số 66 - Công ty Sapharco",
   "address": "50 Nguyễn Ảnh Thủ, Ấp Đông, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.875144,
   "Latitude": 106.6227417
 },
 {
   "STT": 1667,
   "Name": "Đại lý số 67 - Công ty Sapharco",
   "address": "41/1E QL 1A, Đông Lân, Xã Bà Điểm, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8429794,
   "Latitude": 106.6160917
 },
 {
   "STT": 1668,
   "Name": "Đại lý số 68 - Công ty Sapharco",
   "address": "45/5 Tam Đông, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8856525,
   "Latitude": 106.6160809
 },
 {
   "STT": 1669,
   "Name": "Đại lý số 69 - Công ty Sapharco",
   "address": "11/5B Nguyễn Ảnh Thủ, Hậu Lân, Xã Bà Điểm, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8515055,
   "Latitude": 106.6037911
 },
 {
   "STT": 1670,
   "Name": "Đại lý số 70 - Công ty Sapharco",
   "address": "225/1107 Phan Văn Hớn, TL 14, Ấp 5, Xuân Thới Thượng, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.847725,
   "Latitude": 106.590808
 },
 {
   "STT": 1671,
   "Name": "Đại lý số 71 - Công ty Sapharco",
   "address": "3/128 Nhị Tân 1, Xã Tân Thới Nhì, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8917103,
   "Latitude": 106.5748662
 },
 {
   "STT": 1672,
   "Name": "Đại lý số 73 - Công ty Sapharco",
   "address": "4/3 Phan Văn Hớn, Ấp 1, Xã Xuân Thới Thượng, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8631506,
   "Latitude": 106.5685711
 },
 {
   "STT": 1673,
   "Name": "Đại lý số 74 - Công ty Sapharco",
   "address": "36/5 Trung Chánh 2, Xã Trung Chánh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8579932,
   "Latitude": 106.6066585
 },
 {
   "STT": 1674,
   "Name": "Đại lý số 75 - Công ty Sapharco",
   "address": "97D Trần Văn mười, Ấp 3, Xã Xuân Thới Thượng, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8539186,
   "Latitude": 106.5897322
 },
 {
   "STT": 1675,
   "Name": "Đại lý số 76 - Công ty Sapharco",
   "address": "3/3 Nguyễn Văn Bứa, Ấp 4, Xã Xuân Thới Sơn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8657688,
   "Latitude": 106.5497392
 },
 {
   "STT": 1676,
   "Name": "Đại lý số 78 - Công ty Sapharco",
   "address": "190 Ấp Đông, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8768525,
   "Latitude": 106.6164622
 },
 {
   "STT": 1677,
   "Name": "Đại lý số 79 - Công ty Sapharco",
   "address": "28/2 Lê Văn Khương, Ấp 5, Xã Đông Thạnh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9043868,
   "Latitude": 106.6440848
 },
 {
   "STT": 1678,
   "Name": "Đại lý số 82 - Công ty Sapharco",
   "address": "18/2C Trung Đông, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.896183,
   "Latitude": 106.615246
 },
 {
   "STT": 1679,
   "Name": "Đại lý số 83 - Công ty Sapharco",
   "address": "7/4C Nguyễn Thị Sóc, Mỹ Hòa 4, Xuân Thới Đông, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8606646,
   "Latitude": 106.6032062
 },
 {
   "STT": 1680,
   "Name": "Đại lý số 84 - Công ty Sapharco",
   "address": "100/29 Ấp 4, Xã Đông Thạnh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.904813,
   "Latitude": 106.650217
 },
 {
   "STT": 1681,
   "Name": "Đại lý số 85 - Công ty Sapharco",
   "address": "34/2 Ấp 2, Xã Xuân Thới Sơn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.854868,
   "Latitude": 106.58026
 },
 {
   "STT": 1682,
   "Name": "Đại lý số 86 - Công ty Sapharco",
   "address": "149 Bùi Công Trừng, Ấp 4, Xã Nhị Bình, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9070597,
   "Latitude": 106.6584772
 },
 {
   "STT": 1683,
   "Name": "Đại lý số 87 - Công ty Sapharco",
   "address": "38A Ấp 4, Xã Xuân Thới Sơn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8576537,
   "Latitude": 106.5722058
 },
 {
   "STT": 1684,
   "Name": "Đại lý số 88 - Công ty Sapharco",
   "address": "10/7B, Thới Tứ, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.890381,
   "Latitude": 106.6116651
 },
 {
   "STT": 1685,
   "Name": "Đại lý số 90 - Công ty Sapharco",
   "address": "69/4 Ấp Tân Thới 1, Xã Tân Hiệp, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.896898,
   "Latitude": 106.598177
 },
 {
   "STT": 1686,
   "Name": "Đại lý số 91 - Công ty Sapharco",
   "address": "23 Bùi Công trừng, Ấp 4, Xã Đông Thạnh, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9077773,
   "Latitude": 106.6537851
 },
 {
   "STT": 1687,
   "Name": "Đại lý số 92 - Công ty Sapharco",
   "address": "4/1 LẤp Thới tây 1, Xã Tân Hiệp , Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.904838,
   "Latitude": 106.594841
 },
 {
   "STT": 1688,
   "Name": "Đại lý số 94 - Công ty Sapharco",
   "address": "170/5C Tam Đông, Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8852814,
   "Latitude": 106.6167362
 },
 {
   "STT": 1689,
   "Name": "Đại lý số 99 - Công ty Sapharco",
   "address": "39/1B Ấp 1, Xã Xuân Thới Thượng, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8630416,
   "Latitude": 106.5614322
 },
 {
   "STT": 1690,
   "Name": "Đại lý số 01 - Công ty Sapharco",
   "address": "2/13C, Khu phố 5, Thị trấn Nhà Bè, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7009559,
   "Latitude": 106.736063
 },
 {
   "STT": 1691,
   "Name": "Đại lý số 02 - Công ty Sapharco",
   "address": "07 Ấp 1, Xã Phước Lộc, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7104811,
   "Latitude": 106.6877045
 },
 {
   "STT": 1692,
   "Name": "Đại lý số 03 - Công ty Sapharco",
   "address": "159 Lê Văn Lương, Ấp 2, Xã Phước Kiển, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7132377,
   "Latitude": 106.7011933
 },
 {
   "STT": 1693,
   "Name": "Đại lý số 09 - Công ty Sapharco",
   "address": "110/5 Huỳnh Tấn Phát, Xã Phú Xuân, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.689628,
   "Latitude": 106.7418337
 },
 {
   "STT": 1694,
   "Name": "Đại lý số 10 - Công ty Sapharco",
   "address": "56/8B Liên Tỉnh 15, KP VI, Thị trấn Nhà Bè, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6943704,
   "Latitude": 106.7411559
 },
 {
   "STT": 1695,
   "Name": "Đại lý số 11 - Công ty Sapharco",
   "address": "23/7 Liên Tỉnh 15, KP 4, Thị trấn Nhà Bè, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7943776,
   "Latitude": 106.7348898
 },
 {
   "STT": 1696,
   "Name": "Đại lý số 14 - Công ty Sapharco",
   "address": "5/31 Ấp 4, Lê Văn Lương, Nhơn Đức, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6580549,
   "Latitude": 106.689222
 },
 {
   "STT": 1697,
   "Name": "Đại lý số 19 - Công ty Sapharco",
   "address": "27/9C Huỳnh Tấn Phát, KP 6, Thị trấn Nhà Bè, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.682674,
   "Latitude": 106.746646
 },
 {
   "STT": 1698,
   "Name": "Đại lý số 20 - Công ty Sapharco",
   "address": "768 Huỳnh Tấn Phát, Xã Phú Xuân, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6785148,
   "Latitude": 106.7531012
 },
 {
   "STT": 1699,
   "Name": "Đại lý số 21 - Công ty Sapharco",
   "address": "88 Nguyễn Văn Tạo, Xã Hiệp Phước, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.638227,
   "Latitude": 106.733723
 },
 {
   "STT": 1700,
   "Name": "Đại lý số 22 - Công ty Sapharco",
   "address": "166 Ấp 5, Lê Văn Lương, Phước Kiểng, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.720145,
   "Latitude": 106.698622
 },
 {
   "STT": 1701,
   "Name": "Đại lý số 23 - Công ty Sapharco",
   "address": "240 Huỳnh Tấn Phát, KP 6, Thị trấn Nhà Bè, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6857957,
   "Latitude": 106.7442579
 },
 {
   "STT": 1702,
   "Name": "Đại lý số 31 - Công ty Sapharco",
   "address": "69B Lê Văn Lương, Xã Phước Kiểng, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.710743,
   "Latitude": 106.702339
 },
 {
   "STT": 1703,
   "Name": "Đại lý số 34 - Công ty Sapharco",
   "address": "521 Ấp 2, Nguyễn Văn Tạo, Xã Long Thới, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6029582,
   "Latitude": 106.7411772
 },
 {
   "STT": 1704,
   "Name": "Đại lý số 35 - Công ty Sapharco",
   "address": "81 Ấp 5, Xã Phước Kiểng, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.722065,
   "Latitude": 106.698206
 },
 {
   "STT": 1705,
   "Name": "Đại lý số 37 - Công ty Sapharco",
   "address": "84/2 Ấp 6, Huỳnh Tấn Phát, Phú Xuân, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7142063,
   "Latitude": 106.7370096
 },
 {
   "STT": 1706,
   "Name": "Đại lý số 40 - Công ty Sapharco",
   "address": "01 Phan Văn Bảy, Xã Hiệp Phước, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6352379,
   "Latitude": 106.7440626
 },
 {
   "STT": 1707,
   "Name": "Đại lý số 42 - Công ty Sapharco",
   "address": "1010A Lê Văn Lương, Xã Nhơn Đức, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7006229,
   "Latitude": 106.7040979
 },
 {
   "STT": 1708,
   "Name": "Đại lý số 43 - Công ty Sapharco",
   "address": "6/5 Ấp 2, Nguyễn Bình, Xã Nhơn Đức, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.67655,
   "Latitude": 106.7053118
 },
 {
   "STT": 1709,
   "Name": "Đại lý số 44 - Công ty Sapharco",
   "address": "85A Đào Sư Tích, Xã Phước Lộc, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7121328,
   "Latitude": 106.6970268
 },
 {
   "STT": 1710,
   "Name": "Bệnh viện Quận Tân Phú",
   "address": "611 Âu Cơ, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7835549,
   "Latitude": 106.6420248
 },
 {
   "STT": 1711,
   "Name": "Nhà thuốc Hồng Thu",
   "address": "38/1 Trần Khắc Trân, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.79357,
   "Latitude": 106.691249
 },
 {
   "STT": 1712,
   "Name": "Nhà thuốc số 42",
   "address": "32 Trần Khắc Chân, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.793299,
   "Latitude": 106.691275
 },
 {
   "STT": 1713,
   "Name": "Hiệu thuốc 16 - Chị Phúc",
   "address": "66-68 Bà Lê Chân, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7902251,
   "Latitude": 106.6893482
 },
 {
   "STT": 1714,
   "Name": "Hiệu thuốc Trung Tâm - Cô Trinh",
   "address": "298 Hai BàTrưng, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.789305,
   "Latitude": 106.690504
 },
 {
   "STT": 1715,
   "Name": "Hiệu thuốc Trung Tâm Chị Đào",
   "address": "298 Hai BàTrưng, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.789305,
   "Latitude": 106.690504
 },
 {
   "STT": 1716,
   "Name": "Hiệu Thuốc Trung Tâm(Cô Mười)",
   "address": "Số 298 Hai Bà Trưng,Phường  Tân Định- Quận 1, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.789305,
   "Latitude": 106.690504
 },
 {
   "STT": 1717,
   "Name": "Quầy thuốc Chú Thành",
   "address": "44 Đặng Dung, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924752,
   "Latitude": 106.6893375
 },
 {
   "STT": 1718,
   "Name": "Quầy thuốc Chị Phương",
   "address": "44 Đặng Dung, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924752,
   "Latitude": 106.6893375
 },
 {
   "STT": 1719,
   "Name": "KEY Quầy thuốc Cô Vân",
   "address": "44 Đặng Dung, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924752,
   "Latitude": 106.6893375
 },
 {
   "STT": 1720,
   "Name": "KEY Quầy thuốc Loan Thuận",
   "address": "44 Đặng Dung, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924752,
   "Latitude": 106.6893375
 },
 {
   "STT": 1721,
   "Name": "Quầy thuốc Chị Thoại",
   "address": "44 Đặng Dung, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924752,
   "Latitude": 106.6893375
 },
 {
   "STT": 1722,
   "Name": "Quầy thuốc Cô Hương",
   "address": "44 Đặng Dung, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924752,
   "Latitude": 106.6893375
 },
 {
   "STT": 1723,
   "Name": "Quầy thuốc Cô Tuyết",
   "address": "44 Đặng Dung, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924752,
   "Latitude": 106.6893375
 },
 {
   "STT": 1724,
   "Name": "Quầy thuốc Anh Hạnh",
   "address": "44 Đặng Dung, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924752,
   "Latitude": 106.6893375
 },
 {
   "STT": 1725,
   "Name": "Quầy thuốc Cô Vân",
   "address": "44 Đặng Dung, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7924752,
   "Latitude": 106.6893375
 },
 {
   "STT": 1726,
   "Name": "Hiệu thuốc số 20 - Cô Vân",
   "address": "190E Trần Quang Khải, Phường  Tân Định, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.791597,
   "Latitude": 106.688801
 },
 {
   "STT": 1727,
   "Name": "Nhà thuốc Âu Minh",
   "address": "38 Nguyễn Huy Tự, Phường  Đa Kao, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7928213,
   "Latitude": 106.6965427
 },
 {
   "STT": 1728,
   "Name": "Nhà thuốc Đa Kao",
   "address": "17 Nguyễn Huy Tự, Phường  Đa Kao, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7925191,
   "Latitude": 106.6969873
 },
 {
   "STT": 1729,
   "Name": "Nhà thuốc Ngọc Duyên",
   "address": "52 Nguyễn Bỉnh Khiêm, Phường  Đa Kao, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7919119,
   "Latitude": 106.7002651
 },
 {
   "STT": 1730,
   "Name": "Nhà thuốc số 4",
   "address": "113 Điện Biên Phủ, Phường  Đa Kao, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.789088,
   "Latitude": 106.696474
 },
 {
   "STT": 1731,
   "Name": "Hiệu thuốc số 7 ",
   "address": "50 Phan Bội Châu, Phường  Bến thành, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7733576,
   "Latitude": 106.6984692
 },
 {
   "STT": 1732,
   "Name": "Nhà thuốc Đức Dung",
   "address": "156 Cô Giang, Phường  Cô Giang, Quận 1,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7621749,
   "Latitude": 106.693656
 },
 {
   "STT": 1733,
   "Name": "Nhà thuốc Song Diễm 77",
   "address": "45B Quốc Hương, Phường  Thảo Điền, Quận 2,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8024632,
   "Latitude": 106.731733
 },
 {
   "STT": 1734,
   "Name": "Nhà thuốc Mỹ Đức",
   "address": "16 Quốc Hương, Phường  Thảo Điền, Quận 2,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8010903,
   "Latitude": 106.7327011
 },
 {
   "STT": 1735,
   "Name": "Nhà thuốc Đức Thiện",
   "address": "383 Đường số 16, Khu đô thị An Phú- An Khánh, Phường  An Phú, Quận 2,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8062231,
   "Latitude": 106.7506863
 },
 {
   "STT": 1736,
   "Name": "Nhà thuốc Mỹ Lệ KEY",
   "address": "343 Nguyễn Thị Định, Phường  Bình Trưng Tây, Quận 2,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7787196,
   "Latitude": 106.7636395
 },
 {
   "STT": 1737,
   "Name": "Nhà thuốc Ánh Hoan",
   "address": "375 Nguyễn Duy Trinh, Phường  Bình Trưng Tây, Quận 2,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7883739,
   "Latitude": 106.767653
 },
 {
   "STT": 1738,
   "Name": "Nhà thuốc An Khánh",
   "address": "311_A13 Chợ Bình Khánh Lương Định Của, Phường  Bình Khánh, Quận 2,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7884729,
   "Latitude": 106.7367348
 },
 {
   "STT": 1739,
   "Name": "Nhà thuốc Minh Thiên",
   "address": "25/1A Lương Định Của, Phường  Bình Khánh, Quận 2, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785259,
   "Latitude": 106.729945
 },
 {
   "STT": 1740,
   "Name": "Công ty CP DP FPT Long Châu (NT Long Châu 3 cũ)",
   "address": "379 Hai Bà Trưng, Phường  8, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7900971,
   "Latitude": 106.688752
 },
 {
   "STT": 1741,
   "Name": "Nhà thuốc Liên Châu",
   "address": "361 Hai Bà Trưng, Phường  8, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7898339,
   "Latitude": 106.6891838
 },
 {
   "STT": 1742,
   "Name": "Nhà thuốc Da Liễu Táo Đỏ",
   "address": "30/1A Ngô Thời Nhiệm, Phường  7, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.777619,
   "Latitude": 106.686545
 },
 {
   "STT": 1743,
   "Name": " Nhà thuốc Hồng Ngọc 1",
   "address": "87 Trần Quang Diệu, Phường  14, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.78853,
   "Latitude": 106.677874
 },
 {
   "STT": 1744,
   "Name": "Nhà thuốc Minh Khôi",
   "address": "518 Bis Lê Văn Sỹ, Phường  14, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7898891,
   "Latitude": 106.674697
 },
 {
   "STT": 1745,
   "Name": "Công ty Dược Mỹ Châu",
   "address": "338 Lê Văn Sỹ, Phường  14, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.787441,
   "Latitude": 106.678986
 },
 {
   "STT": 1746,
   "Name": "Nhà thuốc Nam Quang",
   "address": "400A Lê Văn Sỹ, Phường  14, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.788759,
   "Latitude": 106.676595
 },
 {
   "STT": 1747,
   "Name": "Nhà thuốc Thái Hòa",
   "address": "398 Cách Mạng Tháng 8, Phường  11, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7833679,
   "Latitude": 106.6717621
 },
 {
   "STT": 1748,
   "Name": "Nhà thuốc Ba bảy",
   "address": "394C Cách Mạng Tháng 8, Phường  11, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.783286,
   "Latitude": 106.671906
 },
 {
   "STT": 1749,
   "Name": "Nhà thuốc Thúy Phượng",
   "address": "354 Trần Văn Đang, Phường  11, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785049,
   "Latitude": 106.671811
 },
 {
   "STT": 1750,
   "Name": "Nhà thuốc Kim Chi",
   "address": "207B Trần Văn Đang, Phường  11, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785021,
   "Latitude": 106.672158
 },
 {
   "STT": 1751,
   "Name": "Công ty Dược Mỹ Châu",
   "address": "338 Lê Văn Sỹ, Phường  11, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790126,
   "Latitude": 106.673216
 },
 {
   "STT": 1752,
   "Name": "Hiệu thuốc số 4",
   "address": "68 Cách Mạng Tháng 8, Phường  6, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7748549,
   "Latitude": 106.6872985
 },
 {
   "STT": 1753,
   "Name": "Công ty XNK y tế thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "address": "68 Cách Mạng Tháng 8, Phường  6, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7748549,
   "Latitude": 106.6872985
 },
 {
   "STT": 1754,
   "Name": "Nhà thuốc Làn da đẹp",
   "address": "65 Hồ Xuân Hương, Phường  6, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7758792,
   "Latitude": 106.6870813
 },
 {
   "STT": 1755,
   "Name": "Công ty dược Quận 3",
   "address": "243 Hai Bà Trưng, Phường  6, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7873482,
   "Latitude": 106.6924968
 },
 {
   "STT": 1756,
   "Name": "Nhà thuốc Tuấn Ngọc",
   "address": "27 Bà Huyện Thanh Quan, Phường  9, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.781866,
   "Latitude": 106.681264
 },
 {
   "STT": 1757,
   "Name": "Nhà thuốc Vườn Chuối",
   "address": "39 Vườn Chuối, Phường  4, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7736368,
   "Latitude": 106.682588
 },
 {
   "STT": 1758,
   "Name": "Nhà thuốc Hữu Nam",
   "address": "275 Võ Văn Tần, Phường  5, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7722337,
   "Latitude": 106.6865804
 },
 {
   "STT": 1759,
   "Name": "Nhà thuốc Bồi Ngươn Đường",
   "address": "174/32A Nguyễn Thiện Thuật, Phường  03, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7690517,
   "Latitude": 106.6799291
 },
 {
   "STT": 1760,
   "Name": "Nhà thuốc Đại Phúc",
   "address": "152 Trần Quốc Thảo, Phường  02, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785297,
   "Latitude": 106.68271
 },
 {
   "STT": 1761,
   "Name": "Nhà thuốc Âu Châu 2",
   "address": "551 Nguyễn Đình Chiểu, Phường  02, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7681372,
   "Latitude": 106.6804795
 },
 {
   "STT": 1762,
   "Name": "Nhà thuốc Ngọc Châu 2",
   "address": "177 Nguyễn Thiện Thuật, Phường  02, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7684458,
   "Latitude": 106.6792244
 },
 {
   "STT": 1763,
   "Name": "Nhà thuốc Văn Minh",
   "address": "153 Nguyễn Thiện Thuật, Phường  02, Quận 3, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7680945,
   "Latitude": 106.6796321
 },
 {
   "STT": 1764,
   "Name": "Nhà thuốc số 36",
   "address": "30 Hoàng Diệu, Phường  12, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.765065,
   "Latitude": 106.706419
 },
 {
   "STT": 1765,
   "Name": "Nhà thuốc số 28",
   "address": "198-200 Nguyễn Tất Thành,Phường  13, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7624288,
   "Latitude": 106.708345
 },
 {
   "STT": 1766,
   "Name": "Nhà thuốc Hà My",
   "address": "369 Đoàn Văn Bơ,Phường  13, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7587288,
   "Latitude": 106.7115728
 },
 {
   "STT": 1767,
   "Name": "Nhà thuốc Sao Mai",
   "address": "269 Lô K, Khu tái thiết Hoàng Diệu, Phường  09, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7913296,
   "Latitude": 106.6515306
 },
 {
   "STT": 1768,
   "Name": "Nhà thuốc Hữu Nghị 1",
   "address": "68 Đoàn Văn Bơ, Phường  09, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7639077,
   "Latitude": 106.70277
 },
 {
   "STT": 1769,
   "Name": "Nhà thuốc Hữu Nghị 2",
   "address": "309 Hoàng Diệu, Phường  6, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759979,
   "Latitude": 106.699309
 },
 {
   "STT": 1770,
   "Name": "Nhà thuốc Anh Thư",
   "address": "Q20 Nguyễn Hữu Hào, Phường  6, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7615335,
   "Latitude": 106.7010473
 },
 {
   "STT": 1771,
   "Name": "Nhà thuốc Thiên Kim",
   "address": "38R Nguyễn Hữu Hào, Phường  8, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7632407,
   "Latitude": 106.700177
 },
 {
   "STT": 1772,
   "Name": "Nhà thuốc Tôn Đản",
   "address": "58 Tôn Đản, Phường  10, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7610119,
   "Latitude": 106.7073939
 },
 {
   "STT": 1773,
   "Name": "Nhà thuốc Mai Trâm",
   "address": "817B Đoàn Văn Bơ, Phường  18, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566934,
   "Latitude": 106.7148887
 },
 {
   "STT": 1774,
   "Name": "Nhà thuốc Bảo Long",
   "address": "22 Vĩnh Hội, Phường  4, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.755982,
   "Latitude": 106.702661
 },
 {
   "STT": 1775,
   "Name": "Nhà thuốc Vân Anh",
   "address": "209/13E Tôn Thất Thuyết, Phường  3, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.754021,
   "Latitude": 106.6993449
 },
 {
   "STT": 1776,
   "Name": "Nhà thuốc Kim Phát Dược Phòng",
   "address": "269 Tôn Thất Thuyết, Phường  3, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.75318,
   "Latitude": 106.696453
 },
 {
   "STT": 1777,
   "Name": "Nhà thuốc Diệp Khả",
   "address": "682 Đoàn Văn Bơ, Phường  16, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.763946,
   "Latitude": 106.702704
 },
 {
   "STT": 1778,
   "Name": "Nhà thuốc Thanh Tuyền",
   "address": "253 Tôn Đản, Phường  15, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.756251,
   "Latitude": 106.706254
 },
 {
   "STT": 1779,
   "Name": "Nhà thuốc Minh Quân 3",
   "address": "332 Bến Vân Đồn, Phường  1, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.75628,
   "Latitude": 106.6907669
 },
 {
   "STT": 1780,
   "Name": "Nhà thuốc Bảo Châu 1",
   "address": "13 Nguyễn Khoái, Phường  1, Quận 4, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.753329,
   "Latitude": 106.694982
 },
 {
   "STT": 1781,
   "Name": "Nhà thuốc Mỹ Anh KEY",
   "address": "37 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7564856,
   "Latitude": 106.6581188
 },
 {
   "STT": 1782,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "37D Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566629,
   "Latitude": 106.6581058
 },
 {
   "STT": 1783,
   "Name": "Nhà thuốc Bảo Ngân",
   "address": "49 Thuận Kiều, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7568561,
   "Latitude": 106.6581478
 },
 {
   "STT": 1784,
   "Name": "Nhà thuốc Phú Cường",
   "address": "267A Nguyễn Chí Thanh, Phường  12, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7572778,
   "Latitude": 106.6566566
 },
 {
   "STT": 1785,
   "Name": "Nhà thuốc Minh Loan",
   "address": "003 Chung cư Phan Văn Trị, Phường  2, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7557258,
   "Latitude": 106.6783424
 },
 {
   "STT": 1786,
   "Name": "Nhà thuốc Việt Hải",
   "address": "452 Nguyễn Trãi, Phường  8, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7551685,
   "Latitude": 106.6715948
 },
 {
   "STT": 1787,
   "Name": "Nhà thuốc Gia Hưng",
   "address": "103 Phù Đổng Thiên Vương, Phường  11, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7549164,
   "Latitude": 106.6623193
 },
 {
   "STT": 1788,
   "Name": "Nhà thuốc Tân Trường Hưng KEY",
   "address": "816 Nguyễn Trãi, Phường  14, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7530369,
   "Latitude": 106.6576541
 },
 {
   "STT": 1789,
   "Name": "Nhà thuốc Nhung Thành KEY",
   "address": "87 Nguyễn Văn Đừng, Phường  6, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7524077,
   "Latitude": 106.670926
 },
 {
   "STT": 1790,
   "Name": "Nhà thuốc Vạn Hưng KEY",
   "address": "25 Hải Thượng Lãn Ông, Phường  10, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7504615,
   "Latitude": 106.6628042
 },
 {
   "STT": 1791,
   "Name": "Nhà thuốc Minh Phát",
   "address": "73B Hải Thượng Lãn Ông, Phường  10, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7507436,
   "Latitude": 106.6612654
 },
 {
   "STT": 1792,
   "Name": "Nhà thuốc Đại Khánh",
   "address": "75 Lương Nhữ Học, Phường  10, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7513709,
   "Latitude": 106.6599611
 },
 {
   "STT": 1793,
   "Name": "Nhà thuốc Hằng Thái",
   "address": "65A Triệu Quang Phục, Phường  10, Quận 5, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7513289,
   "Latitude": 106.6613347
 },
 {
   "STT": 1794,
   "Name": "Nhà thuốc Lan Phương",
   "address": "944 An Dương Vương, Phường  13, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7573479,
   "Latitude": 106.6253374
 },
 {
   "STT": 1795,
   "Name": "Nhà thuốc Thành Nguyên",
   "address": "31G Tân Hòa Đông, Phường  13, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.755747,
   "Latitude": 106.632799
 },
 {
   "STT": 1796,
   "Name": "Nhà thuốc Minh Vy",
   "address": "326 Nguyễn Văn Luông, Phường  12, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749977,
   "Latitude": 106.635658
 },
 {
   "STT": 1797,
   "Name": "Nhà thuốc Phú Lâm",
   "address": "1019 Hồng Bàng, Phường  12, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7544305,
   "Latitude": 106.6359421
 },
 {
   "STT": 1798,
   "Name": "Nhà thuốc Á Châu 4",
   "address": "67A Hậu Giang, Phường  5, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748331,
   "Latitude": 106.643942
 },
 {
   "STT": 1799,
   "Name": "Nhà thuốc Y Dược",
   "address": "14 Minh Phụng, Phường  5, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747894,
   "Latitude": 106.642855
 },
 {
   "STT": 1800,
   "Name": "Nhà thuốc Huyền Ngọc Dược",
   "address": "68 Đường 26, Phường  11, Quận 6, Phường  11, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7421059,
   "Latitude": 106.6278045
 },
 {
   "STT": 1801,
   "Name": "Nhà thuốc Tháp Mười ( HT Sapharco)",
   "address": "80 Tháp Mười, Phường  2, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7504963,
   "Latitude": 106.6536489
 },
 {
   "STT": 1802,
   "Name": "Nhà thuốc Tân Quang Huy",
   "address": "22 Phạm Đình Hổ, Phường  2, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.747371,
   "Latitude": 106.6496617
 },
 {
   "STT": 1803,
   "Name": "Nhà thuốc Kim Ngọc",
   "address": "44 Hậu Giang, Phường  2, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749784,
   "Latitude": 106.647728
 },
 {
   "STT": 1804,
   "Name": "Nhà thuốc Nhơn Hòa",
   "address": "71 Nguyễn Thị Nhỏ, Phường  2, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7527756,
   "Latitude": 106.6505691
 },
 {
   "STT": 1805,
   "Name": "Nhà thuốc Tâm Giao",
   "address": "125C Văn Thân, Phường  08, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.741968,
   "Latitude": 106.6390739
 },
 {
   "STT": 1806,
   "Name": "Nhà thuốc Thu Hiền",
   "address": "295 Phạm Văn Chí, Phường  3, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.743922,
   "Latitude": 106.6455129
 },
 {
   "STT": 1807,
   "Name": "Nhà thuốc Khải Lộc",
   "address": "344 An Dương Vương, Phường  10, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7382457,
   "Latitude": 106.622064
 },
 {
   "STT": 1808,
   "Name": "Nhà thuốc Thủy KEY",
   "address": "195 Trần Văn Kiểu, Phường  10, Quận 6, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7409722,
   "Latitude": 106.6278268
 },
 {
   "STT": 1809,
   "Name": "Nhà thuốc Hoàn Thành",
   "address": "135 Bùi Văn Ba, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74809,
   "Latitude": 106.734396
 },
 {
   "STT": 1810,
   "Name": "Nhà thuốc Nhân Tâm",
   "address": "487/5 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7536185,
   "Latitude": 106.7283857
 },
 {
   "STT": 1811,
   "Name": "Nhà thuốc Bảo Duy",
   "address": "649 Huỳnh Tấn Phát, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7387844,
   "Latitude": 106.7302251
 },
 {
   "STT": 1812,
   "Name": "Nhà thuốc Nhân Hưng",
   "address": "487/35 Huỳnh Tấn Phát, Khu phố 1, Phường  Tân Thuận Đông, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749952,
   "Latitude": 106.728852
 },
 {
   "STT": 1813,
   "Name": "Nhà thuốc Ngọc Minh 2",
   "address": "286 Huỳnh Tấn Phát, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7382125,
   "Latitude": 106.7302687
 },
 {
   "STT": 1814,
   "Name": "Nhà thuốc Nam Hưng",
   "address": "6A Lâm Văn Bền, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748102,
   "Latitude": 106.716029
 },
 {
   "STT": 1815,
   "Name": "Nhà thuốc Nguyên Minh 1",
   "address": "59 Tân Mỹ, Phường  Tân Thuận Tây, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7516202,
   "Latitude": 106.7197728
 },
 {
   "STT": 1816,
   "Name": "Nhà thuốc Nhật Thủy",
   "address": "75 Lê Văn Lương, Phường  Tân Kiểng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749787,
   "Latitude": 106.7050243
 },
 {
   "STT": 1817,
   "Name": "Nhà thuốc Phật Linh 2",
   "address": "861/73A Trần Xuân Soạn, Phường  Tân hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.750444,
   "Latitude": 106.698429
 },
 {
   "STT": 1818,
   "Name": "Nhà thuốc Ngân Hà",
   "address": "793/60C Trần Xuân Soạn, Phường  Tân hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.750515,
   "Latitude": 106.698776
 },
 {
   "STT": 1819,
   "Name": "Nhà thuốc FV Phar",
   "address": "26 Nguyễn Thị Thập, Khu dân cư Him Lam, Phường  Tân hưng, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.743934,
   "Latitude": 106.695036
 },
 {
   "STT": 1820,
   "Name": "Nhà thuốc Phú Thịnh 1",
   "address": "538 Huỳnh Tấn Phát, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7381207,
   "Latitude": 106.7302813
 },
 {
   "STT": 1821,
   "Name": "Nhà thuốc Hữu Tâm",
   "address": "178 Nguyễn Thị Thập, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7382919,
   "Latitude": 106.7163803
 },
 {
   "STT": 1822,
   "Name": "Nhà thuốc Trâm Nguyên",
   "address": "27B Lâm Văn Bền, Phường  Bình Thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7396499,
   "Latitude": 106.716112
 },
 {
   "STT": 1823,
   "Name": "Nhà thuốc Phú Châu",
   "address": "30/28 Lâm Văn Bền, Phường  tân quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7396118,
   "Latitude": 106.7159061
 },
 {
   "STT": 1824,
   "Name": "Nhà thuốc Hữu Nghị",
   "address": "21/6 Lâm Văn Bền, Phường  tân quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.73871,
   "Latitude": 106.715771
 },
 {
   "STT": 1825,
   "Name": "Nhà thuốc Vân Trí",
   "address": "94 Lâm Văn Bền, Phường  tân quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.739261,
   "Latitude": 106.7159109
 },
 {
   "STT": 1826,
   "Name": "Nhà thuốc Khánh Linh 1",
   "address": "76A Lâm Văn Bền, Phường  tân quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74173,
   "Latitude": 106.715744
 },
 {
   "STT": 1827,
   "Name": "Nhà thuốc Hoàng Gia",
   "address": "81 Mai Văn Vĩnh, Phường  tân quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7388758,
   "Latitude": 106.7137001
 },
 {
   "STT": 1828,
   "Name": "Nhà thuốc Phương Anh",
   "address": "87 Mai Văn Vĩnh, Phường  tân quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745196,
   "Latitude": 106.714566
 },
 {
   "STT": 1829,
   "Name": "Nhà thuốc Phong Châu",
   "address": "51/54 Mai Văn Vĩnh, Phường  tân quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7388603,
   "Latitude": 106.7137
 },
 {
   "STT": 1830,
   "Name": "Nhà thuốc Hy Vọng KEY",
   "address": "58 Đường số 10, Khu phố 3, Phường  tân quy, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7440548,
   "Latitude": 106.710087
 },
 {
   "STT": 1831,
   "Name": "Nhà thuốc Đan Huy",
   "address": "502/25 Huỳnh Tấn Phát, Phường  Phú thuận, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.730222,
   "Latitude": 106.733104
 },
 {
   "STT": 1832,
   "Name": "Nhà thuốc Từ Phương 2",
   "address": "74 Tân Mỹ, Phường  tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7362365,
   "Latitude": 106.7181191
 },
 {
   "STT": 1833,
   "Name": "Nhà thuốc Tân Mỹ 2",
   "address": "46 Tân Mỹ, Phường  tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7368027,
   "Latitude": 106.718198
 },
 {
   "STT": 1834,
   "Name": "Nhà thuốc Tân Phú",
   "address": "51 Tân Mỹ, Phường  tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7517785,
   "Latitude": 106.7196969
 },
 {
   "STT": 1835,
   "Name": "Nhà thuốc Phương Hiếu",
   "address": "642 Huỳnh Tân Phát, Phường  tân Phú, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7297481,
   "Latitude": 106.7323729
 },
 {
   "STT": 1836,
   "Name": "Nhà thuốc Hoàng Trang",
   "address": "498 Lê Văn Lương, Phường  tân Phong, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7347729,
   "Latitude": 106.7021949
 },
 {
   "STT": 1837,
   "Name": "Nhà thuốc SC Pharrma",
   "address": "B009 khu Hùng Vương R16, Phường  tân Phong, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7293821,
   "Latitude": 106.7099181
 },
 {
   "STT": 1838,
   "Name": "Nhà thuốc Phước Long KEY",
   "address": "166A Phạm Hữu Lầu, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.705101,
   "Latitude": 106.7382329
 },
 {
   "STT": 1839,
   "Name": "Nhà thuốc Xuân Tùng",
   "address": "Block B Tầng trệt Khu dân cư Kỷ Nguyên-Chung cư Era Town, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7001614,
   "Latitude": 106.7311262
 },
 {
   "STT": 1840,
   "Name": "Nhà thuốc Minh Thi 2",
   "address": "169 Phạm Hữu Lầu, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7043483,
   "Latitude": 106.7307709
 },
 {
   "STT": 1841,
   "Name": "Nhà thuốc Hoàn Châu",
   "address": "149E Phạm Hữu Lầu, Phường  Phú Mỹ, Quận 7, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.705799,
   "Latitude": 106.7440602
 },
 {
   "STT": 1842,
   "Name": "Nhà thuốc Dương Bá Trạc",
   "address": "77 Phạm Thế Hiển, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.750694,
   "Latitude": 106.687641
 },
 {
   "STT": 1843,
   "Name": "Nhà thuốc Trung Sơn",
   "address": "254 Dương Bá Trạc, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745681,
   "Latitude": 106.689217
 },
 {
   "STT": 1844,
   "Name": "Nhà thuốc Phan Hiền",
   "address": "274 Dương Bá Trạc, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745256,
   "Latitude": 106.689106
 },
 {
   "STT": 1845,
   "Name": "Nhà thuốc Hữu Nghị",
   "address": "152A Nguyễn Thị Tần, Phường  2, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745618,
   "Latitude": 106.686081
 },
 {
   "STT": 1846,
   "Name": "Nhà thuốc Yến Nhi",
   "address": "170 Âu Dương Lân, Phường  3, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745274,
   "Latitude": 106.68309
 },
 {
   "STT": 1847,
   "Name": "Nhà thuốc Minh Phương",
   "address": "172 Âu Dương Lân, Phường  3, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745238,
   "Latitude": 106.683108
 },
 {
   "STT": 1848,
   "Name": "Nhà thuốc Thúy Liễu",
   "address": "34 Nguyễn Thị Tần, Phường  3, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.74798,
   "Latitude": 106.685137
 },
 {
   "STT": 1849,
   "Name": "Nhà thuốc Mỹ Huệ",
   "address": "471 Hưng Phú, Phường  9, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.746994,
   "Latitude": 106.6733859
 },
 {
   "STT": 1850,
   "Name": "Nhà thuốc Hưng Phú",
   "address": "491 Hưng Phú, Phường  9, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7469924,
   "Latitude": 106.6730847
 },
 {
   "STT": 1851,
   "Name": "Nhà thuốc Gia Khang",
   "address": "59 Dã Tượng, Phường  10, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.745545,
   "Latitude": 106.6648249
 },
 {
   "STT": 1852,
   "Name": "Nhà thuốc Trung Nghĩa",
   "address": "18 Đông Hồ, Phường  4, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.739832,
   "Latitude": 106.6709599
 },
 {
   "STT": 1853,
   "Name": "Nhà thuốc Nhân Nghĩa",
   "address": "151 Đường số 8,P4,Q8, Phường  4, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.741975,
   "Latitude": 106.670619
 },
 {
   "STT": 1854,
   "Name": "Nhà thuốc Nhân Hoàng",
   "address": "97 Nguyễn Thị Mười, Phường  4, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7505173,
   "Latitude": 106.6590159
 },
 {
   "STT": 1855,
   "Name": "Nhà thuốc Tấn Huy",
   "address": "176 Đặng Phúc Liêng, Phường  4, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7417149,
   "Latitude": 106.670079
 },
 {
   "STT": 1856,
   "Name": "Nhà thuốc Châu Hải",
   "address": "162 Nguyễn Chế Nghĩa, Phường  12, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.742169,
   "Latitude": 106.653881
 },
 {
   "STT": 1857,
   "Name": "Nhà thuốc Mai Khôi",
   "address": "229 Bông Sao, Phường  5, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.740871,
   "Latitude": 106.661821
 },
 {
   "STT": 1858,
   "Name": "Nhà thuốc số 16",
   "address": "1283A Phạm Thế Hiển, Phường  5, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.741126,
   "Latitude": 106.658101
 },
 {
   "STT": 1859,
   "Name": "Hiệu thuốc số 23",
   "address": "114 Liên Tỉnh 5, Phường  5, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7361554,
   "Latitude": 106.6567027
 },
 {
   "STT": 1860,
   "Name": "Nhà thuốc số 397",
   "address": "397 Liên Tỉnh 5, Phường  5, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7361484,
   "Latitude": 106.6572003
 },
 {
   "STT": 1861,
   "Name": "Nhà thuốc Trung Hưng",
   "address": "51 Hoài Thanh, Phường  14, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.738709,
   "Latitude": 106.649848
 },
 {
   "STT": 1862,
   "Name": "Nhà thuốc Ngọc Kiều",
   "address": "296 Bùi Minh Trực, Phường  6, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7358712,
   "Latitude": 106.6519485
 },
 {
   "STT": 1863,
   "Name": "Nhà thuốc Ngân Hà 3",
   "address": "258D Bùi Minh Trực, Phường  6, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.73749,
   "Latitude": 106.65554
 },
 {
   "STT": 1864,
   "Name": "Nhà thuốc số 4",
   "address": "1515 Phạm Thế Hiển, Phường  6, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.73961,
   "Latitude": 106.654211
 },
 {
   "STT": 1865,
   "Name": "Nhà Thuốc Phúc Hậu 2",
   "address": "44 Đường Số 5 KDC Bình Đăng, Phường  6, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7298247,
   "Latitude": 106.6524562
 },
 {
   "STT": 1866,
   "Name": "Nhà thuốc Tâm Khoa 1",
   "address": "Kios số 5 - 53 Bến Phú Định, Phường  16, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7181435,
   "Latitude": 106.6268229
 },
 {
   "STT": 1867,
   "Name": "Nhà Thuốc Tâm Khoa",
   "address": "40/3/2 Bến Phú Định, Phường  16, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7136307,
   "Latitude": 106.6227892
 },
 {
   "STT": 1868,
   "Name": "Nhà thuốc Kim Long",
   "address": "210D An Dương Vương, Phường  16, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7304991,
   "Latitude": 106.620748
 },
 {
   "STT": 1869,
   "Name": "Nhà thuốc Thiên Châu",
   "address": "201 Ba Tơ, Phường  7, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.70718,
   "Latitude": 106.6231
 },
 {
   "STT": 1870,
   "Name": "Nhà thuốc Ngọc Sơn",
   "address": "3325a Ba Tơ, Phường  7, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.707896,
   "Latitude": 106.622626
 },
 {
   "STT": 1871,
   "Name": "Nhà thuốc Mộc An",
   "address": "24 Lương Văn Can, Phường  7, Quận 8, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.735032,
   "Latitude": 106.639561
 },
 {
   "STT": 1872,
   "Name": "Nhà thuốc Anh Quân",
   "address": "740 Nguyễn Xiển, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8524886,
   "Latitude": 106.8333507
 },
 {
   "STT": 1873,
   "Name": "Nhà thuốc Kim Hồng",
   "address": "104 Nguyễn Văn Tăng, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.84434,
   "Latitude": 106.8159529
 },
 {
   "STT": 1874,
   "Name": "Nhà thuốc Phương Trang",
   "address": "205 Nguyễn Văn Tăng, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.842687,
   "Latitude": 106.8285606
 },
 {
   "STT": 1875,
   "Name": "Nhà thuốc Hà Phương",
   "address": "461 Nguyễn Văn Tăng, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8423639,
   "Latitude": 106.8280024
 },
 {
   "STT": 1876,
   "Name": "Nhà thuốc Anh Trang",
   "address": "548C Lê Văn Việt, Phường  Long Thạnh Mỹ, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8494488,
   "Latitude": 106.8105401
 },
 {
   "STT": 1877,
   "Name": "Nhà thuốc Thanh Trang",
   "address": "30 Nam Cao, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.853495,
   "Latitude": 106.806228
 },
 {
   "STT": 1878,
   "Name": "Nhà thuốc Tâm Mai",
   "address": "72 Nam Cao, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8597013,
   "Latitude": 106.799942
 },
 {
   "STT": 1879,
   "Name": "Nhà thuốc Gia Hiệp",
   "address": "323 Hoàng Hữu Nam, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8680992,
   "Latitude": 106.8137061
 },
 {
   "STT": 1880,
   "Name": "Nhà Thuốc Phúc Khang",
   "address": "Số 62 Đường 154 - Phường  Tân Phú - Quận, Phường  Tân Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8673062,
   "Latitude": 106.8079347
 },
 {
   "STT": 1881,
   "Name": "Nhà thuốc Phúc Đan",
   "address": "32 Trần Hưng Đạo, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8460568,
   "Latitude": 106.7744118
 },
 {
   "STT": 1882,
   "Name": "Nhà Thuốc Ngọc Hiền",
   "address": "53 Trần Hưng Đạo, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8479066,
   "Latitude": 106.7751008
 },
 {
   "STT": 1883,
   "Name": "Nhà thuốc Hiệp Phú",
   "address": "28 Lê Văn Việt, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8475331,
   "Latitude": 106.7761057
 },
 {
   "STT": 1884,
   "Name": "Nhà thuốc Hùng Hạnh",
   "address": "457 Lê Văn Việt, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8444076,
   "Latitude": 106.7853236
 },
 {
   "STT": 1885,
   "Name": "Nhà thuốc Phương Linh",
   "address": "12 Lê Văn Việt, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.846871,
   "Latitude": 106.776364
 },
 {
   "STT": 1886,
   "Name": "Nhà thuốc Đức Quí",
   "address": "279 Lê Văn Việt, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8443635,
   "Latitude": 106.7851203
 },
 {
   "STT": 1887,
   "Name": "Nhà thuốc Hữu Phúc",
   "address": "109 Lê Văn Việt, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.848339,
   "Latitude": 106.775318
 },
 {
   "STT": 1888,
   "Name": "Nhà thuốc An Thanh",
   "address": "265 Lê Văn Việt, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8442857,
   "Latitude": 106.7847618
 },
 {
   "STT": 1889,
   "Name": "Nhà thuốc Mỹ Vân",
   "address": "Thửa 20,Tờ 19,Trương Văn Thành, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8520976,
   "Latitude": 106.7806408
 },
 {
   "STT": 1890,
   "Name": "Nhà thuốc Anh Thư 2",
   "address": "113 Man Thiện, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8527496,
   "Latitude": 106.7880594
 },
 {
   "STT": 1891,
   "Name": "Nhà thuốc Anh Thảo",
   "address": "449C Lê Văn Việt, Phường  Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8453499,
   "Latitude": 106.7935152
 },
 {
   "STT": 1892,
   "Name": "Nhà thuốc Thiên Ân",
   "address": "99 Lê Văn Việt, Phường  Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8473052,
   "Latitude": 106.801992
 },
 {
   "STT": 1893,
   "Name": "Nhà thuốc Trung Khoa",
   "address": "224 Đường Đình Phong Phú, Phường  Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8369521,
   "Latitude": 106.7814858
 },
 {
   "STT": 1894,
   "Name": "Nhà thuốc Hồng Thắm",
   "address": "259 Đình Phong Phú, Phường  Nhơn Phú A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.83149,
   "Latitude": 106.7841
 },
 {
   "STT": 1895,
   "Name": "Nhà thuốc Phương Thảo",
   "address": "330 Lê Văn Việt, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8443764,
   "Latitude": 106.7851785
 },
 {
   "STT": 1896,
   "Name": "Nhà thuốc Thiên Ân",
   "address": "99 Lê Văn Việt, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.846551,
   "Latitude": 106.777424
 },
 {
   "STT": 1897,
   "Name": "Nhà thuốc Diệu Linh 2",
   "address": "46 Đường số 12, Phường  Hiệp Phú, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8379537,
   "Latitude": 106.7756071
 },
 {
   "STT": 1898,
   "Name": "Nhà thuốc Ái Nhi",
   "address": "101 Đường 61, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.828464,
   "Latitude": 106.772422
 },
 {
   "STT": 1899,
   "Name": "Nhà thuốc Phước Thiện",
   "address": "355B Đỗ Xuân Hợp, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8214512,
   "Latitude": 106.7721349
 },
 {
   "STT": 1900,
   "Name": "Nhà Thuốc Phúc Gia Hòa",
   "address": "525C Đỗ Xuân Hợp KP3, Phường  Phước Long B, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8155337,
   "Latitude": 106.7747175
 },
 {
   "STT": 1901,
   "Name": "Nhà thuốc Hùng Việt",
   "address": "246 Đỗ Xuân Hợp, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8242139,
   "Latitude": 106.7692788
 },
 {
   "STT": 1902,
   "Name": "Nhà thuốc Quang Minh",
   "address": "132 Đỗ Xuân Hợp, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8299596,
   "Latitude": 106.7671628
 },
 {
   "STT": 1903,
   "Name": "Nhà thuốc Thanh Tín",
   "address": "100A Đường 61, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.828464,
   "Latitude": 106.772422
 },
 {
   "STT": 1904,
   "Name": "Nhà thuốc Thiên Hậu",
   "address": "130 Tây Hòa, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.825975,
   "Latitude": 106.763921
 },
 {
   "STT": 1905,
   "Name": "Nhà thuốc Tây Hòa",
   "address": "32A Tây Hòa, Phường  Phước Long A, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.826204,
   "Latitude": 106.763126
 },
 {
   "STT": 1906,
   "Name": "Nhà thuốc Hoàng Châu",
   "address": "01 Đường Lò Lu, Phường  Trường Thạnh, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8263013,
   "Latitude": 106.8179875
 },
 {
   "STT": 1907,
   "Name": "Nhà thuốc An",
   "address": "39 Lò Lu, Phường  Trường Thạnh, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.824614,
   "Latitude": 106.808348
 },
 {
   "STT": 1908,
   "Name": "Nhà thuốc Tây Việt Thắng",
   "address": "1496 Nguyễn Duy Trinh, Phường  Long Trường, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8061972,
   "Latitude": 106.8218532
 },
 {
   "STT": 1909,
   "Name": "Nhà thuốc Bích Thu",
   "address": "1329 Nguyễn Duy Trinh, Phường  Long Trường, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.805048,
   "Latitude": 106.8164152
 },
 {
   "STT": 1910,
   "Name": "Nhà thuốc Long Trường 2",
   "address": "1170 Nguyễn Duy Trinh, Phường  Long Trường, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8022921,
   "Latitude": 106.8125474
 },
 {
   "STT": 1911,
   "Name": "Nhà thuốc Bảo Trâm",
   "address": "05 Đường số 07, Phường  Phước Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8137558,
   "Latitude": 106.7721204
 },
 {
   "STT": 1912,
   "Name": "Nhà thuốc Đức Thiện",
   "address": "80A Đường Số 3, Phường  Phước Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.819189,
   "Latitude": 106.771305
 },
 {
   "STT": 1913,
   "Name": "Nhà thuốc Số 77",
   "address": "91 Đường số 18, Phường  Phước Bình, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8147961,
   "Latitude": 106.7712092
 },
 {
   "STT": 1914,
   "Name": "Nhà Thuốc Lê Nguyên",
   "address": "843 Nguyễn Duy Trinh, Phường  Phú Hữu, Quận 9, Quận 9, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7912697,
   "Latitude": 106.7961772
 },
 {
   "STT": 1915,
   "Name": "Nhà thuốc Kim Châu",
   "address": "16 Hồ Bá Kiện, Phường  15, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7810806,
   "Latitude": 106.6691679
 },
 {
   "STT": 1916,
   "Name": "Nhà thuốc Khang Phúc",
   "address": "77 Thành Thái, Phường  14, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7703651,
   "Latitude": 106.6656744
 },
 {
   "STT": 1917,
   "Name": "Nhà thuốc Gia Hân",
   "address": "284/43 Lý Thường Kiệt, Phường  Phước Bình,  Quận 9, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7593227,
   "Latitude": 106.6612279
 },
 {
   "STT": 1918,
   "Name": "Nhà thuốc Thiên Phát",
   "address": "690 Đường 3/2, Phường  Phước Bình,  Quận 9, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7687587,
   "Latitude": 106.6689151
 },
 {
   "STT": 1919,
   "Name": "Nhà thuốc Phú Lâm",
   "address": "1019 Hồng Bàng, Phường  12, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7544305,
   "Latitude": 106.6359421
 },
 {
   "STT": 1920,
   "Name": "Nhà thuốc Đức Nguyên 5",
   "address": "523 Sư Vạn Hạnh, Phường  12, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7688082,
   "Latitude": 106.6708548
 },
 {
   "STT": 1921,
   "Name": "Nhà thuốc Thanh Hải",
   "address": "29 Hoàng Dư Khương, Phường  12, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7753646,
   "Latitude": 106.6719667
 },
 {
   "STT": 1922,
   "Name": "Nhà thuốc Đăng Châu",
   "address": "285/93 Cách Mạng Tháng 8, Phường  12, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.780208,
   "Latitude": 106.67267
 },
 {
   "STT": 1923,
   "Name": "Nhà thuốc Đức Tuấn KEY",
   "address": "92 Thành Thái, Phường  12, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7737361,
   "Latitude": 106.6646826
 },
 {
   "STT": 1924,
   "Name": "Nhà thuốc Thiên Lộc",
   "address": "006 Lô C, Chung cư Ấn Quang, Phường  9, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.764797,
   "Latitude": 106.6709986
 },
 {
   "STT": 1925,
   "Name": "Nhà thuốc Nhân Văn",
   "address": "283A Lý Thái Tổ, Phường  9, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.767818,
   "Latitude": 106.6714686
 },
 {
   "STT": 1926,
   "Name": "Nhà thuốc Hồng Sương",
   "address": "308 Bà Hạt, Phường  9, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7660807,
   "Latitude": 106.6687545
 },
 {
   "STT": 1927,
   "Name": "Nhà thuốc Ngôi Sao 1 KEY",
   "address": "541 Bà Hạt, Phường  8, Quận 10, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7651909,
   "Latitude": 106.6657368
 },
 {
   "STT": 1928,
   "Name": "Nhà thuốc Kim Ngọc",
   "address": "44 Hậu Giang, Phường  2, Quận 10, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.749784,
   "Latitude": 106.647728
 },
 {
   "STT": 1929,
   "Name": "Nhà thuốc Kim Ngọc",
   "address": "358 Ngô Gia Tự, Phường  4, Quận 10, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7624169,
   "Latitude": 106.670466
 },
 {
   "STT": 1930,
   "Name": "Nhà thuốc số 62",
   "address": "228 Nguyễn Duy Dương, Phường  4, Quận 10, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7629629,
   "Latitude": 106.6705999
 },
 {
   "STT": 1931,
   "Name": "Nhà thuốc Tâm An",
   "address": "38 Ngô Quyền, Phường  5, quận 10, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7602214,
   "Latitude": 106.6654793
 },
 {
   "STT": 1932,
   "Name": "Nhà thuốc Trường Sinh",
   "address": "233 Ngô Quyền, Phường  6, Quận 10, Quận 10, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7650417,
   "Latitude": 106.6641386
 },
 {
   "STT": 1933,
   "Name": "Nhà thuốc Việt Đức",
   "address": "92 Nguyễn Thị Nhỏ,Phường  15, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.77247,
   "Latitude": 106.653059
 },
 {
   "STT": 1934,
   "Name": "Nhà thuốc 140",
   "address": "140 Nguyễn Thị Nhỏ,Phường  15, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.773445,
   "Latitude": 106.653182
 },
 {
   "STT": 1935,
   "Name": "Nhà thuốc Phương Nguyên",
   "address": "341/K19 Lạc Long Quân, Phường  5, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7674912,
   "Latitude": 106.6428065
 },
 {
   "STT": 1936,
   "Name": " Nhà thuốc số 1",
   "address": "14 Ông Ích Khiêm, Phường  14, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.76568,
   "Latitude": 106.6463179
 },
 {
   "STT": 1937,
   "Name": "Nhà thuốc Kim Loan",
   "address": "10 Ông Ích Khiêm, Phường  14, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.765556,
   "Latitude": 106.646334
 },
 {
   "STT": 1938,
   "Name": "Nhà thuốc Thảo Trinh",
   "address": "129 Đường 100 Bình Thới, Phường  14, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7691185,
   "Latitude": 106.6500315
 },
 {
   "STT": 1939,
   "Name": "Nhà thuốc Hiền Hòa",
   "address": "106i /19 Lạc Long Quân, Phường  3, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.761456,
   "Latitude": 106.639841
 },
 {
   "STT": 1940,
   "Name": "Nhà thuốc Vi An Đường",
   "address": "142 Lạc Long Quận, Phường  10, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759164,
   "Latitude": 106.6417699
 },
 {
   "STT": 1941,
   "Name": "Nhà thuốc Ngọc Thủy",
   "address": "114 Tôn Thất Hiệp, Phường  13, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.764273,
   "Latitude": 106.652986
 },
 {
   "STT": 1942,
   "Name": "Nhà thuốc Ngọc Ánh",
   "address": "47 Dương Đình Nghệ, Phường  8, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.759706,
   "Latitude": 106.649617
 },
 {
   "STT": 1943,
   "Name": "Nhà thuốc Hoàng Châu",
   "address": "46 Đường 02, Phường  8, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7514025,
   "Latitude": 106.5999531
 },
 {
   "STT": 1944,
   "Name": "Nhà thuốc Phương Thành",
   "address": "51 Đường số 2, Phường  8, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7631989,
   "Latitude": 106.6486295
 },
 {
   "STT": 1945,
   "Name": "Nhà thuốc Lợi Thanh Bình",
   "address": "148 Đội Cung, Phường  9, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.764382,
   "Latitude": 106.6469586
 },
 {
   "STT": 1946,
   "Name": "Nhà thuốc Anh Vũ",
   "address": "163 Lãnh Binh Thăng, Phường  12, Quận11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.762377,
   "Latitude": 106.653587
 },
 {
   "STT": 1947,
   "Name": "Nhà thuốc Mỹ Linh",
   "address": "618 Nguyễn Chí Thanh, Phường  7, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758153,
   "Latitude": 106.659055
 },
 {
   "STT": 1948,
   "Name": "Nhà thuốc Phương Vy",
   "address": "534 Nguyễn Chí Thanh, Phường  7, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7586124,
   "Latitude": 106.661221
 },
 {
   "STT": 1949,
   "Name": "Nhà thuốc Hải Châu",
   "address": "016 lô C2, Phường  6, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.755817,
   "Latitude": 106.649151
 },
 {
   "STT": 1950,
   "Name": "Nhà thuốc Hoàng Phúc",
   "address": "27 Lạc Long Quân, Phường  1, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7589715,
   "Latitude": 106.6409013
 },
 {
   "STT": 1951,
   "Name": "Nhà thuốc 340C",
   "address": "340C Minh Phụng, Phường  2, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.758272,
   "Latitude": 106.644112
 },
 {
   "STT": 1952,
   "Name": "Nhà thuốc Tuyết Anh",
   "address": "89 Lò Siêu, Phường  16, Quận 11, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.755716,
   "Latitude": 106.6484119
 },
 {
   "STT": 1953,
   "Name": "Nhà thuốc Hương Toàn",
   "address": "551/11 Tô Ngọc Vân, Phường  Thạnh Xuân, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8571224,
   "Latitude": 106.6707232
 },
 {
   "STT": 1954,
   "Name": "Nhà thuốc Ân Huy",
   "address": "46/3 Tô Ngọc Vân, Phường  Thạnh Xuân, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8571224,
   "Latitude": 106.6707232
 },
 {
   "STT": 1955,
   "Name": "Nhà thuốc Hương Toàn",
   "address": "532-534 Tô Ngọc Vân, Phường  Thạnh Xuân, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8570952,
   "Latitude": 106.6707115
 },
 {
   "STT": 1956,
   "Name": "Nhà thuốc Gia Bình",
   "address": "459 Hà Huy Giáp, Phường  Thạnh Xuân, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8850503,
   "Latitude": 106.6814229
 },
 {
   "STT": 1957,
   "Name": "Nhà thuốc Kim Anh",
   "address": "10 Bùi Công Trừng, Phường  Thạnh Lộc, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8974133,
   "Latitude": 106.6891644
 },
 {
   "STT": 1958,
   "Name": "Nhà thuốc Phúc Khang 1",
   "address": "27 Thạnh Lộc 19, Phường  Thạnh Lộc, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8666019,
   "Latitude": 106.6811957
 },
 {
   "STT": 1959,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "157A HT17, Phường  Hiệp Thành, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8780125,
   "Latitude": 106.6364462
 },
 {
   "STT": 1960,
   "Name": "Nhà thuốc Minh Châu 189",
   "address": "74 Ấp 1 Nguyễn Ảnh Thủ, Phường  Hiệp Thành, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8761436,
   "Latitude": 106.6484874
 },
 {
   "STT": 1961,
   "Name": "Nhà thuốc Minh Khuê",
   "address": "434 Lê Văn Khương, Phường  Thới An, Quận 12, Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8806267,
   "Latitude": 106.6488307
 },
 {
   "STT": 1962,
   "Name": "Nhà thuốc Minh Châu 7",
   "address": "991 Nguyễn Ảnh Thủ, Phường  Tân Chánh Hiệp, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8713171,
   "Latitude": 106.619627
 },
 {
   "STT": 1963,
   "Name": "Nhà thuốc My Châu 5",
   "address": "46 Nguyễn Ảnh Thủ, Phường  Tân Chánh Hiệp, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8794912,
   "Latitude": 106.6360379
 },
 {
   "STT": 1964,
   "Name": "Nhà thuốc Long Anh",
   "address": "43/19A Vườn Lài, Phường  An Phú Đông, Quận 12, Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8514643,
   "Latitude": 106.6931895
 },
 {
   "STT": 1965,
   "Name": "Nhà thuốc Huyền Trinh",
   "address": "65D/97 TTH 20, Phường  Tân Thới Hiệp, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8603672,
   "Latitude": 106.6438673
 },
 {
   "STT": 1966,
   "Name": "Nhà thuốc Ngôi Sao",
   "address": "1180 Nguyễn Văn Qúa, Phường  Tân Thới Hiệp, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8552997,
   "Latitude": 106.6390739
 },
 {
   "STT": 1967,
   "Name": "Nhà thuốc 499",
   "address": "499 Tô Ký, Phường  Trung Mỹ tây, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.852667,
   "Latitude": 106.6263196
 },
 {
   "STT": 1968,
   "Name": "Nhà thuốc Anh Tuấn",
   "address": "04 Trung Mỹ Tây 2A, Phường  Trung Mỹ tây, Quận 12,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.848481,
   "Latitude": 106.6143751
 },
 {
   "STT": 1969,
   "Name": "Nhà thuốc Thanh Tuyền 2",
   "address": "705 Nguyễn Văn Qúa,Phường  Đông Hưng Thuận, Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.838893,
   "Latitude": 106.630165
 },
 {
   "STT": 1970,
   "Name": "Nhà thuốc Đại Trường Sinh",
   "address": "106 Phan Văn Hớn,Phường  Tân Thới Nhất, Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8287537,
   "Latitude": 106.618849
 },
 {
   "STT": 1971,
   "Name": "Nhà thuốc Minh Phụng",
   "address": "602/1 Trường Chinh,Phường  Tân Thới Nhất, Quận 12, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8331216,
   "Latitude": 106.6209519
 },
 {
   "STT": 1972,
   "Name": "Nhà thuốc Chợ Đệm",
   "address": "A8/8 Nguyễn Hữu Trí, KPhường  1,Thị trấn Tân Túc, Huyện Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6963374,
   "Latitude": 106.5933314
 },
 {
   "STT": 1973,
   "Name": "Nha thuốc Phương Thảo",
   "address": "1A 251 ấp 1 Trần Văn Giàu,Xã Phạm Văn Hai, Huyện Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7761435,
   "Latitude": 106.542813
 },
 {
   "STT": 1974,
   "Name": "Nhà thuốc Hoàng Huy",
   "address": "6A42/1 An Hạ, Ấp 6,Xã Phạm Văn Hai, Huyện Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8143103,
   "Latitude": 106.5199845
 },
 {
   "STT": 1975,
   "Name": "Nhà thuốc Phước Lộc",
   "address": "F1/60T Quách Điêu, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.832747,
   "Latitude": 106.570025
 },
 {
   "STT": 1976,
   "Name": "Nhà thuốc Bảo Nguyên",
   "address": "F1/17B Hương Lộ 80, Xã Vĩnh Lộc A, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8113963,
   "Latitude": 106.5757703
 },
 {
   "STT": 1977,
   "Name": "Nhà thuốc Mỹ Linh",
   "address": "C9/30 Võ Văn Vân, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7940649,
   "Latitude": 106.5765067
 },
 {
   "STT": 1978,
   "Name": "Nhà thuốc Phát Tài",
   "address": "C1/3B, Võ Văn Vân, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8069958,
   "Latitude": 106.5773735
 },
 {
   "STT": 1979,
   "Name": "Nhà thuốc Hương Trầm",
   "address": "D7/9 Nguyễn Thị Tú, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8163397,
   "Latitude": 106.6007505
 },
 {
   "STT": 1980,
   "Name": "Nhà thuốc Hồng Đức 9",
   "address": "E8/19C Ấp 5, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7883633,
   "Latitude": 106.5532511
 },
 {
   "STT": 1981,
   "Name": "Nhà thuốc Vạn Đức",
   "address": "F4/10B, Đường 6B, Xã Vĩnh Lộc B, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7925196,
   "Latitude": 106.5735165
 },
 {
   "STT": 1982,
   "Name": "Nhà thuốc Châu Ân",
   "address": "A5B/159B ấp 1,Xã Tân Nhựt, Huyện Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7517046,
   "Latitude": 106.5873024
 },
 {
   "STT": 1983,
   "Name": "Nhà thuốc Hồng Phúc",
   "address": "B5/1 Trần Đại Nghĩa,Xã Tân Kiên, Huyện Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7226927,
   "Latitude": 106.599385
 },
 {
   "STT": 1984,
   "Name": "Nhà thuốc Lang Trinh",
   "address": "66 Đường số 10,khu dân cư Bình Hưng, Xã Bình hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.734213,
   "Latitude": 106.6888201
 },
 {
   "STT": 1985,
   "Name": "Nhà thuốc Phúc Hậu",
   "address": "60 Đường số 10,khu dân cư Bình Hưng, Xã Bình hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7340004,
   "Latitude": 106.6901056
 },
 {
   "STT": 1986,
   "Name": "Nhà thuốc Minh Hiếu 2",
   "address": "C4/1 Phạm Hùng, Xã Bình hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7322833,
   "Latitude": 106.6747605
 },
 {
   "STT": 1987,
   "Name": "Nhà thuốc Quỳnh Như",
   "address": "103 Đường số 8, khu dân cư Bình Hưng, Xã Bình hưng, Quận Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7356906,
   "Latitude": 106.690641
 },
 {
   "STT": 1988,
   "Name": "Nhà thuốc Sinh Đôi 2",
   "address": "D1/8-D1/8A Quốc Lộ 50 ấp 4,Xã Phong Phú, Huyện Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.740445,
   "Latitude": 106.6563363
 },
 {
   "STT": 1989,
   "Name": "Đại lý số 97",
   "address": "D14/34 Đinh Đức Thiện, Xã Bình Chánh, Huyện Bình Chánh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6578834,
   "Latitude": 106.5815436
 },
 {
   "STT": 1990,
   "Name": "Nhà thuốc Hồng",
   "address": "385 Hương Lộ 3, Phường  Bình Hưng Hòa, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8007172,
   "Latitude": 106.6112959
 },
 {
   "STT": 1991,
   "Name": "Nhà thuốc Phương Linh",
   "address": "762 Lê Trọng Tấn, Phường  Bình Hưng Hòa, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8158014,
   "Latitude": 106.6033548
 },
 {
   "STT": 1992,
   "Name": "Nhà thuốc Xuân Anh",
   "address": "149 Phạm Đăng Giản, Phường  Bình Hưng Hòa, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8164372,
   "Latitude": 106.6035554
 },
 {
   "STT": 1993,
   "Name": "Nhà thuốc Linh Đan",
   "address": "38 Phạm Đăng Giản, Phường  Bình Hưng Hòa, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8198629,
   "Latitude": 106.6053904
 },
 {
   "STT": 1994,
   "Name": "Nhà thuốc Thanh Bình 2",
   "address": "314 Mã Lò, Phường  Bình Hưng Hòa, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7864456,
   "Latitude": 106.5980262
 },
 {
   "STT": 1995,
   "Name": "Nhà thuốc Bảo Châu",
   "address": "210 Đường 26/3, Phường  Bình Hưng Hòa, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7981732,
   "Latitude": 106.6055061
 },
 {
   "STT": 1996,
   "Name": "Nhà thuốc Y Bình",
   "address": "175 Đường số 12, Phường  Bình Hưng Hòa, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7956104,
   "Latitude": 106.6049675
 },
 {
   "STT": 1997,
   "Name": "Nhà thuốc Phong Bình",
   "address": "129 Ấp Chiến Lược, Phường  Bình Hưng Hòa A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.791005,
   "Latitude": 106.606461
 },
 {
   "STT": 1998,
   "Name": "Nhà thuốc Quốc Thịnh",
   "address": "50 Lê Văn Quới, Phường  Bình Hưng Hòa A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.77774,
   "Latitude": 106.604101
 },
 {
   "STT": 1999,
   "Name": "Nhà thuốc Ngọc Khải",
   "address": "401 Bình Thành, Phường  Bình Hưng Hòa B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8189756,
   "Latitude": 106.5873599
 },
 {
   "STT": 2000,
   "Name": "Nhà thuốc Khánh Trang",
   "address": "124 Nguyễn Thị Tú, Phường  Bình Hưng Hòa B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8158685,
   "Latitude": 106.5966005
 },
 {
   "STT": 2001,
   "Name": "Nhà thuốc Ngọc Trang",
   "address": "82 Đường số 29, Phường  Bình Trị Đông, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7565724,
   "Latitude": 106.6125468
 },
 {
   "STT": 2002,
   "Name": "Nhà thuốc Quỳnh Giao",
   "address": "738 Tỉnh Lộ 10, Phường  Bình Trị Đông, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7581953,
   "Latitude": 106.6114685
 },
 {
   "STT": 2003,
   "Name": "Nhà thuốc Minh Châu",
   "address": "531 Hương Lộ 2, Phường  Bình Trị Đông, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771399,
   "Latitude": 106.616651
 },
 {
   "STT": 2004,
   "Name": "Nhà thuốc Vân Nga",
   "address": "466 Hương Lộ 2,Phường  Bình Trị Đông A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7724,
   "Latitude": 106.6185492
 },
 {
   "STT": 2005,
   "Name": "Nhà thuốc Lê Quang",
   "address": "83 Đường số 1, Phường  Bình Trị Đông B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.748473,
   "Latitude": 106.617401
 },
 {
   "STT": 2006,
   "Name": "Nhà thuốc Phú Lâm 2",
   "address": "163 Đường số 1, Phường  Bình Trị Đông B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7483521,
   "Latitude": 106.6169809
 },
 {
   "STT": 2007,
   "Name": "Nhà thuốc Thiên Lộc 2",
   "address": "98 Đường số 1, Phường  Bình Trị Đông B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7485482,
   "Latitude": 106.6173666
 },
 {
   "STT": 2008,
   "Name": "Nhà thuốc Hoàng Huy",
   "address": "356 Tên Lửa, Phường  Bình Trị Đông B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7530053,
   "Latitude": 106.6099748
 },
 {
   "STT": 2009,
   "Name": "Nhà thuốc Châu Pharmacy",
   "address": "74 Đường 17A, Phường  Bình Trị Đông B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7432696,
   "Latitude": 106.6110532
 },
 {
   "STT": 2010,
   "Name": "Nhà thuốc Mỹ Châu",
   "address": "269 Tân Hòa Đông, Phường  Bình Trị Đông B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.760036,
   "Latitude": 106.624547
 },
 {
   "STT": 2011,
   "Name": "Nhà thuốc Nhân Nghĩa",
   "address": "187 Đường số 19, Phường  Bình Trị Đông B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7532053,
   "Latitude": 106.6125128
 },
 {
   "STT": 2012,
   "Name": "Nhà thuốc Anh Mỹ",
   "address": "608A Hương Lộ 2, Phường  Bình Trị Đông B, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7690284,
   "Latitude": 106.611652
 },
 {
   "STT": 2013,
   "Name": "Nhà thuốc Bạch Yến",
   "address": "1318 Tỉnh lộ 10, Phường  Tân Tạo, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7563031,
   "Latitude": 106.5924087
 },
 {
   "STT": 2014,
   "Name": "Nhà thuốc Tường Vy",
   "address": "136 Lê Đình Cẩn, Phường  Tân Tạo, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7594446,
   "Latitude": 106.6013936
 },
 {
   "STT": 2015,
   "Name": "Nhà thuốc Phương Nam",
   "address": "232 Lê Đình Cẩn, Phường  Tân Tạo, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7599156,
   "Latitude": 106.5961814
 },
 {
   "STT": 2016,
   "Name": "Nhà thuốc Vạn Phước",
   "address": "1153 Quốc Lộ 1A, Phường  Tân Tạo, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7566134,
   "Latitude": 106.5920306
 },
 {
   "STT": 2017,
   "Name": "Nhà thuốc Hồng Phúc",
   "address": "20B Trần Đại Nghĩa, Phường  Tân Tạo A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7228712,
   "Latitude": 106.5996552
 },
 {
   "STT": 2018,
   "Name": "Nhà thuốc Hồng Phúc skv2",
   "address": "52 Trần Đại Nghĩa, Phường  Tân Tạo A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7235242,
   "Latitude": 106.5983867
 },
 {
   "STT": 2019,
   "Name": " Nhà thuốc Minh Châu",
   "address": "190 Tân Hòa Đông, Phường  Tân Tạo A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7563523,
   "Latitude": 106.6323574
 },
 {
   "STT": 2020,
   "Name": "Nhà thuốc Hiền Mai",
   "address": "458 Kinh Dương Vương, Phường  An Lạc, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7399805,
   "Latitude": 106.616669
 },
 {
   "STT": 2021,
   "Name": "Nhà thuốc Ngọc Bích",
   "address": "514 Kinh Dương Vương, Phường  An Lạc, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7371716,
   "Latitude": 106.6142624
 },
 {
   "STT": 2022,
   "Name": "Nhà thuốc 116",
   "address": "402 Kinh Dương Vương, Phường  An Lạc, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7414618,
   "Latitude": 106.618452
 },
 {
   "STT": 2023,
   "Name": "Nhà thuốc Nhật An",
   "address": "447 Hồ Học Lãm, Phường  An Lạc, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7248726,
   "Latitude": 106.6092338
 },
 {
   "STT": 2024,
   "Name": "Nhà thuốc Phúc An",
   "address": "14 Đường 17B, Phường  An Lạc, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7445203,
   "Latitude": 106.6161433
 },
 {
   "STT": 2025,
   "Name": "Nhà thuốc Y Dược",
   "address": "317 Tỉnh Lộ 10,Phường  An Lạc A, Quận Bình Tân, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7561788,
   "Latitude": 106.6236598
 },
 {
   "STT": 2026,
   "Name": "Nhà thuốc Minh Hường",
   "address": "428B Nơ Trang Long, Phường  13, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8193603,
   "Latitude": 106.7027587
 },
 {
   "STT": 2027,
   "Name": "Nhà thuốc An Sinh",
   "address": "415 Nơ Trang Long, Phường  13, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8204323,
   "Latitude": 106.7044635
 },
 {
   "STT": 2028,
   "Name": "Nhà thuốc Phương Nghi",
   "address": "2D1 Bình Lợi, Phường  13, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8253851,
   "Latitude": 106.7068674
 },
 {
   "STT": 2029,
   "Name": "Nhà thuốc Phương Tâm",
   "address": "334C Phan Văn Trị, Phường  11, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8183794,
   "Latitude": 106.6949499
 },
 {
   "STT": 2030,
   "Name": "Nhà thuốc Bảo An Đường",
   "address": "302 Lê Quang Định, Phường  11, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8107318,
   "Latitude": 106.6916237
 },
 {
   "STT": 2031,
   "Name": "Nhà thuốc Thanh Đa",
   "address": "50 Bình Quới, Phường  27, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.81769,
   "Latitude": 106.723091
 },
 {
   "STT": 2032,
   "Name": "Nhà thuốc Thanh Long",
   "address": "55 Bình Quới, Phường  27, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.817581,
   "Latitude": 106.723137
 },
 {
   "STT": 2033,
   "Name": "Nhà thuốc Miền Đông",
   "address": "199 Nguyễn Xí, Phường  26, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8150328,
   "Latitude": 106.7077622
 },
 {
   "STT": 2034,
   "Name": "Nhà thuốc MINH CHÂU PHARMACITY",
   "address": "173 Nguyễn Xí, Phường  26, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8133938,
   "Latitude": 106.7094469
 },
 {
   "STT": 2035,
   "Name": "Nhà thuốc Việt Đức KEY",
   "address": "168 Nguyễn Xí, Phường  26, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8144735,
   "Latitude": 106.7085272
 },
 {
   "STT": 2036,
   "Name": "Nhà thuốc Phạm Lê",
   "address": "317 Đinh Bộ Lĩnh, Phường  26, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.815413,
   "Latitude": 106.7101319
 },
 {
   "STT": 2037,
   "Name": "Nhà thuốc Hồng Oanh",
   "address": "87 Chu Văn An, Phường  26, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8107035,
   "Latitude": 106.7108735
 },
 {
   "STT": 2038,
   "Name": "Công ty Cổ Phần Dược Phẩm Pharmacity",
   "address": "248A Nơ Trang Long, Phường  12, quận Bình Thạnh, Thành phốhường  HCM, VN, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8137165,
   "Latitude": 106.697567
 },
 {
   "STT": 2039,
   "Name": "Nhà thuốc Tú Lệ",
   "address": "174A Đường D2, Phường  25, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8065174,
   "Latitude": 106.7161129
 },
 {
   "STT": 2040,
   "Name": "Nhà thuốc Pharmacy Plus 001",
   "address": "147 Đường D2, Phường  25, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8050796,
   "Latitude": 106.7159986
 },
 {
   "STT": 2041,
   "Name": "Hiệu thuốc số 24",
   "address": "668 Xô Viết Nghệ Tĩnh, Phường  25, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.812284,
   "Latitude": 106.715505
 },
 {
   "STT": 2042,
   "Name": "Nhà thuốc Nam Sanh Đường",
   "address": "320 Xô Viết Nghệ Tĩnh, Phường  25, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.803117,
   "Latitude": 106.71155
 },
 {
   "STT": 2043,
   "Name": "Nhà thuốc Gia An",
   "address": "356/14 Xô Viết Nghệ Tĩnh, Phường  25, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8037759,
   "Latitude": 106.711882
 },
 {
   "STT": 2044,
   "Name": "Nhà thuốc Thu Thu Trang",
   "address": "147/8B Ung Văn Khiêm, Phường  25, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.806638,
   "Latitude": 106.718877
 },
 {
   "STT": 2045,
   "Name": "Nhà thuốc số 28",
   "address": "222 Hoàng Hoa Thám, Phường  05, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8089904,
   "Latitude": 106.6863817
 },
 {
   "STT": 2046,
   "Name": "Nhà thuốc My Phương",
   "address": "25B Nguyễn Thượng Hiền,Phường  05, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8065144,
   "Latitude": 106.6843752
 },
 {
   "STT": 2047,
   "Name": "Công ty Dược Gia Định - Hiệu thuốc số 2",
   "address": "72 Bis Bạch Đằng,Phường  24, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.803252,
   "Latitude": 106.7049094
 },
 {
   "STT": 2048,
   "Name": "Nhà thuốc My Phương 5",
   "address": "46 Nguyễn Văn Đậu,Phường  06, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8066359,
   "Latitude": 106.6872379
 },
 {
   "STT": 2049,
   "Name": "Công ty Dược Gia Định - Hiệu thuốc số 1",
   "address": "06 Nơ Trang Long,Phường  14, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.809377,
   "Latitude": 106.695541
 },
 {
   "STT": 2050,
   "Name": "Nhà thuốc Âu Dược. KEY",
   "address": "20 Lê Quang Định,Phường  14, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8032021,
   "Latitude": 106.6985109
 },
 {
   "STT": 2051,
   "Name": "Nhà thuốc Ông Vịnh",
   "address": "128 Vạn Kiếp,Phường  03, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.799864,
   "Latitude": 106.693417
 },
 {
   "STT": 2052,
   "Name": "Nhà thuốc Diệu Trung",
   "address": "67/4/113 Vũ Huy Tấn,Phường  03, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7956635,
   "Latitude": 106.6949427
 },
 {
   "STT": 2053,
   "Name": "Nhà thuốc số 19",
   "address": "217A Xô Viết Nghệ Tĩnh, Phường  17, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8000029,
   "Latitude": 106.7111086
 },
 {
   "STT": 2054,
   "Name": "Nhà thuốc Minh Khoa",
   "address": "90 Xô Viết Nghệ Tĩnh, Phường  17, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7957266,
   "Latitude": 106.7101166
 },
 {
   "STT": 2055,
   "Name": "Nhà thuốc Hồng Lan",
   "address": "52 Phan Văn Hân, Phường  17, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.794211,
   "Latitude": 106.7062739
 },
 {
   "STT": 2056,
   "Name": "Nhà thuốc Lan Anh",
   "address": "73 Phan Văn Hân, Phường  17, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.793878,
   "Latitude": 106.705761
 },
 {
   "STT": 2057,
   "Name": "Nhà thuốc Thanh Thanh",
   "address": "113 Phan Văn Hân, Phường  17, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792826,
   "Latitude": 106.706918
 },
 {
   "STT": 2058,
   "Name": "Hiệu thuốc số 79",
   "address": "220/22 Xô Viết Nghệ Tĩnh, Phường  21, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7983619,
   "Latitude": 106.711618
 },
 {
   "STT": 2059,
   "Name": "Nhà thuốc số 38",
   "address": "163 Ngô Tất Tố, Phường  22, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.791736,
   "Latitude": 106.714431
 },
 {
   "STT": 2060,
   "Name": "Nhà thuốc An",
   "address": "121 Ngô Tất Tố, Phường  22, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792017,
   "Latitude": 106.7139289
 },
 {
   "STT": 2061,
   "Name": "Nhà thuốc Bảo Tâm",
   "address": "137 Võ Duy Ninh, Phường  22, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7917178,
   "Latitude": 106.716892
 },
 {
   "STT": 2062,
   "Name": "Nhà thuốc Kim Phú KEY",
   "address": "166 Xô Viết Nghệ Tĩnh, Phường  22, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7974336,
   "Latitude": 106.7109419
 },
 {
   "STT": 2063,
   "Name": "Nhà thuốc Dr. JO Pharmacy",
   "address": "L2-SH. 09 Tòa Landmark 2 Vinhomes central park, 720A Điện Biên Phủ, Phường  22, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7940351,
   "Latitude": 106.7212952
 },
 {
   "STT": 2064,
   "Name": "Nhà thuốc Cẩm Trúc",
   "address": "50 Phan Văn Hân, Phường  19, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.791804,
   "Latitude": 106.707061
 },
 {
   "STT": 2065,
   "Name": "Nhà thuốc Tuệ Minh",
   "address": "21 Pham Viết Chánh, Phường  19, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.790378,
   "Latitude": 106.709178
 },
 {
   "STT": 2066,
   "Name": "Nhà thuốc số 19",
   "address": "94 Nguyễn Văn Lạc, Phường  19, Quận Bình Thạnh,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792277,
   "Latitude": 106.7088
 },
 {
   "STT": 2067,
   "Name": "Nhà thuốc Kim Ánh",
   "address": "377 Bình Quới, Phường  28, Quận Bình Thạnh, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.827452,
   "Latitude": 106.7368871
 },
 {
   "STT": 2068,
   "Name": "Nhà thuốc Mỹ Trân(Tấn Lộc cũ)",
   "address": "72 Tỉnh lộ 8,Thị trấn Củ Chi, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9699793,
   "Latitude": 106.4892715
 },
 {
   "STT": 2069,
   "Name": "NT Vinh Khánh",
   "address": "81 Tỉnh Lộ 15 Ấp Phú Mỹ Xã Phú Hòa Đông H.Củ Chi, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 11.0208536,
   "Latitude": 106.563735
 },
 {
   "STT": 2070,
   "Name": "Nhà thuốc Vân Đầy",
   "address": "745A Tỉnh lộ 15,Xã Tân Thạnh Đông, Huyện Củ Chi, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9731836,
   "Latitude": 106.585097
 },
 {
   "STT": 2071,
   "Name": "Nhà thuốc Phú Cường",
   "address": "89A Hương lộ 2, Xã Tân Phú trung, Quận Củ Chi,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9421506,
   "Latitude": 106.5433703
 },
 {
   "STT": 2072,
   "Name": "Đại lý số 4",
   "address": "352B Quốc lộ 22, Xã Tân Phú trung, Quận Củ Chi,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9410232,
   "Latitude": 106.5370577
 },
 {
   "STT": 2073,
   "Name": "Đại lý số 3",
   "address": "07 Đường 78, Xã Tân Phú trung, Quận Củ Chi,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9419227,
   "Latitude": 106.5396371
 },
 {
   "STT": 2074,
   "Name": "Nhà thuốc Nguyên Phương",
   "address": "191A Hồ Văn Tắng, Xã Tân Phú trung, Quận Củ Chi,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9579027,
   "Latitude": 106.5460973
 },
 {
   "STT": 2075,
   "Name": "Nhà Thuốc Thanh Thảo",
   "address": "Số 56 Tổ 1,ấp chợ,X.Tân Phú Trung- H. Củ Chi, Xã Tân Phú trung, Quận Củ Chi,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9329535,
   "Latitude": 106.5642998
 },
 {
   "STT": 2076,
   "Name": "Nhà thuốc Minh Châu",
   "address": "29 Trần Tử Bình, Xã Tân Thông Hội, Quận Củ Chi,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9655134,
   "Latitude": 106.5080845
 },
 {
   "STT": 2077,
   "Name": "Nhà thuốc Tân Lập",
   "address": "10 Liêu Bình Hương, Xã Tân Thông Hội, Quận Củ Chi,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9640084,
   "Latitude": 106.5038547
 },
 {
   "STT": 2078,
   "Name": "Nhà thuốc Minh Châu A",
   "address": "1215 Lê Đức Thọ, Phường  13, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8544011,
   "Latitude": 106.6571235
 },
 {
   "STT": 2079,
   "Name": "Nhà thuốc Cẩm Anh",
   "address": "224 Nguyễn Oanh, Phường  17, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.837838,
   "Latitude": 106.6755631
 },
 {
   "STT": 2080,
   "Name": "Nhà thuốc Nhật Thanh 1",
   "address": "21 Phạm Huy Thông, Phường  17, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.833396,
   "Latitude": 106.681231
 },
 {
   "STT": 2081,
   "Name": "Nhà thuốc Tuấn Tú",
   "address": "Đường số 5 p17 q.gò vấp, Phường  17, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8462557,
   "Latitude": 106.6759117
 },
 {
   "STT": 2082,
   "Name": "Nhà thuốc Lan Anh",
   "address": "382 Nguyễn Oanh, Phường  6, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8447367,
   "Latitude": 106.677576
 },
 {
   "STT": 2083,
   "Name": "Nhà thuốc Mai Uyên",
   "address": "122 Lê Đức Thọ, Phường  6, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8346038,
   "Latitude": 106.6818087
 },
 {
   "STT": 2084,
   "Name": "Nhà thuốc Duy Châu",
   "address": "139B Lê Đức Thọ,Phường  16, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8453579,
   "Latitude": 106.671519
 },
 {
   "STT": 2085,
   "Name": "Nhà thuốc Trí Đức",
   "address": "384 Phan Huy ích, p12, Quận Gò Vấp, Thành phốHCM, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8412242,
   "Latitude": 106.6382126
 },
 {
   "STT": 2086,
   "Name": "Nhà thuốc Tất Thành",
   "address": "448 Phan Huy Ích, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8428165,
   "Latitude": 106.6392222
 },
 {
   "STT": 2087,
   "Name": "Nhà thuốc Vân Anh",
   "address": "50 Bùi Quang Là, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8352605,
   "Latitude": 106.6391571
 },
 {
   "STT": 2088,
   "Name": "Nhà thuốc Lê Anh 1",
   "address": "163/5 Bùi Quang Là, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8358159,
   "Latitude": 106.6376901
 },
 {
   "STT": 2089,
   "Name": "Nhà thuốc Trường Sơn 6",
   "address": "1238 Lê Đức Thọ, Phường  12, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8530084,
   "Latitude": 106.659773
 },
 {
   "STT": 2090,
   "Name": "Nhà thuốc Thảo Ly 09",
   "address": "221 Phạm Văn Chiêu,Phường  14, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.850128,
   "Latitude": 106.6524249
 },
 {
   "STT": 2091,
   "Name": "Nhà thuốc Đỗ Lý",
   "address": "05 Quang Trung, Phường  10, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8349892,
   "Latitude": 106.6636234
 },
 {
   "STT": 2092,
   "Name": "Nhà thuốc Đại Chúng",
   "address": "256 Quang Trung, Phường  10, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8293701,
   "Latitude": 106.6717063
 },
 {
   "STT": 2093,
   "Name": "Nhà thuốc Thanh Bình",
   "address": "484 Nguyễn Thái Sơn, Phường  5, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8273882,
   "Latitude": 106.6904652
 },
 {
   "STT": 2094,
   "Name": "Nhà thuốc Nguyễn Văn Nghi",
   "address": "108 Nguyễn Văn Nghi, Phường  5, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.823314,
   "Latitude": 106.687912
 },
 {
   "STT": 2095,
   "Name": "Nhà thuốc Phú Thịnh",
   "address": "173/24 Dương Quảng Hàm, Phường  5, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.828758,
   "Latitude": 106.690014
 },
 {
   "STT": 2096,
   "Name": "Nhà thuốc Minh Thảo",
   "address": "110 Lê Đức Thọ, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.834361,
   "Latitude": 106.681929
 },
 {
   "STT": 2097,
   "Name": "Nhà thuốc Nhật Thanh",
   "address": "74 Lê Đức Thọ, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8332355,
   "Latitude": 106.6823785
 },
 {
   "STT": 2098,
   "Name": "Nhà thuốc Anh Quốc",
   "address": "360 Nguyễn Văn Nghi, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.826299,
   "Latitude": 106.6815196
 },
 {
   "STT": 2099,
   "Name": "Nhà thuốc Minh Quân",
   "address": "04 Nguyễn Oanh, P7, Quận Gò Vấp, Phường  7, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8265729,
   "Latitude": 106.6800422
 },
 {
   "STT": 2100,
   "Name": "Nhà thuốc Như Hà",
   "address": "179 Nguyễn Thái Sơn, Phường  4, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.823355,
   "Latitude": 106.6857432
 },
 {
   "STT": 2101,
   "Name": " Nhà thuốc Đức Thịnh",
   "address": "185 Nguyễn Thái Sơn, Phường  4, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.823456,
   "Latitude": 106.685745
 },
 {
   "STT": 2102,
   "Name": "Nhà thuốc Hoàng Mỹ",
   "address": "15 Lê Lợi, Phường  4, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.820381,
   "Latitude": 106.686001
 },
 {
   "STT": 2103,
   "Name": "Nhà thuốc Đại Đức Mạnh",
   "address": "427 Phan Văn Trị, Phường  1, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8213893,
   "Latitude": 106.6935889
 },
 {
   "STT": 2104,
   "Name": "Nhà thuốc Nhân ái",
   "address": "656 Lê Quang Định, Phường  1, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8203982,
   "Latitude": 106.689713
 },
 {
   "STT": 2105,
   "Name": "Nhà thuốc An Bình",
   "address": "290 Hoàng Hoa Thám, Phường  1, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8105458,
   "Latitude": 106.6827518
 },
 {
   "STT": 2106,
   "Name": "Nhà thuốc Minh Phước",
   "address": "57/4F Phạm Văn Chiêu, Phường  9, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.848065,
   "Latitude": 106.648618
 },
 {
   "STT": 2107,
   "Name": "Nhà thuốc Trường Sơn",
   "address": "34B Phạm Văn Chiêu, Phường  8, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.843677,
   "Latitude": 106.646785
 },
 {
   "STT": 2108,
   "Name": "Nhà thuốc Thanh Tùng",
   "address": "290 Hoàng Hoa Thám, Phường  1, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8105458,
   "Latitude": 106.6827518
 },
 {
   "STT": 2109,
   "Name": "Nhà thuốc Huy Phong",
   "address": "385 Thống Nhất, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8438413,
   "Latitude": 106.6646813
 },
 {
   "STT": 2110,
   "Name": "Nhà thuốc Minh Châu",
   "address": "221 Thống Nhất, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8370119,
   "Latitude": 106.6648328
 },
 {
   "STT": 2111,
   "Name": "Nhà thuốc Kim Nga",
   "address": "269 Thống Nhất, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8386344,
   "Latitude": 106.6650918
 },
 {
   "STT": 2112,
   "Name": "Nhà thuốc Bảo Thu",
   "address": "36 Lê Văn Thọ, Phường  11, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8377912,
   "Latitude": 106.6580046
 },
 {
   "STT": 2113,
   "Name": "Nhà thuốc Mỹ Anh",
   "address": "687- 689 Nguyễn Kiệm, Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8130485,
   "Latitude": 106.6786747
 },
 {
   "STT": 2114,
   "Name": "Nhà thuốc Kim Anh",
   "address": "131 Nguyễn Kiệm, Phường  3, Quận Gò Vấp, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8131151,
   "Latitude": 106.6786777
 },
 {
   "STT": 2115,
   "Name": "Nhà thuốc Huỳnh Nga",
   "address": "8/73 Khu phố 5, TT Hóc Môn, Thị trấn Hóc Môn, Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8747316,
   "Latitude": 106.6503657
 },
 {
   "STT": 2116,
   "Name": "Nhà thuốc Hồng Hải",
   "address": "19 Lê Lợi, Thị trấn Hóc Môn, Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8890126,
   "Latitude": 106.5943756
 },
 {
   "STT": 2117,
   "Name": "Nhà thuốc Thuấn Phát",
   "address": "97 Bùi Công Trừng, Xã Đông Thạnh, Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9070243,
   "Latitude": 106.6582075
 },
 {
   "STT": 2118,
   "Name": "Nhà thuốc Bách Hợp",
   "address": "214A Đặng Thúc Vịnh, Xã Đông Thạnh, Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.9066557,
   "Latitude": 106.6405452
 },
 {
   "STT": 2119,
   "Name": "Nhà thuốc Mỹ Châu 7",
   "address": "334 Trịnh Thị Miếng,Xã Thới Tam Thôn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8888998,
   "Latitude": 106.6144916
 },
 {
   "STT": 2120,
   "Name": " Nhà thuốc Sen",
   "address": "Kios số 1, Chợ Nhị Xuân,Xã Xuân Thới Sơn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8715653,
   "Latitude": 106.5371947
 },
 {
   "STT": 2121,
   "Name": "Nhà thuốc Ngọc Trinh",
   "address": "40/2C Lê thị Hà,Xã Tân Xuân, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.879353,
   "Latitude": 106.59803
 },
 {
   "STT": 2122,
   "Name": "Nhà thuốc Mi Châu",
   "address": "2/132 Lê Thị Hà,Xã Tân Xuân, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8798949,
   "Latitude": 106.5993299
 },
 {
   "STT": 2123,
   "Name": "Nhà thuốc Phương My",
   "address": "13/6 Trần Văn Mười,Xã Xuân Thới Đông, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8655599,
   "Latitude": 106.5899268
 },
 {
   "STT": 2124,
   "Name": "Nhà thuốc Quang Hy",
   "address": "59/3D Trần Văn Mười,Xã Xuân Thới Đông, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8687479,
   "Latitude": 106.5890718
 },
 {
   "STT": 2125,
   "Name": "Nhà thuốc Hoa Châu",
   "address": "68/3 Ấp Đồng Tâm, Xã Trung Chánh, Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8648173,
   "Latitude": 106.6070638
 },
 {
   "STT": 2126,
   "Name": "Nhà thuốc Tân Châu",
   "address": "45/2B Đường Song Hành, Xã Trung Chánh, Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.85896,
   "Latitude": 106.6072
 },
 {
   "STT": 2127,
   "Name": "Nhà thuốc Hiền Châu",
   "address": "42/3A Trung Mỹ, Xã Trung Chánh, Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8703649,
   "Latitude": 106.6080437
 },
 {
   "STT": 2128,
   "Name": "Nhà thuốc Mỹ Châu 9",
   "address": "104/86 Xuân Thới Thượng, Xã Xuân Thới Thượng, Hóc Môn, Huyện Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.859167,
   "Latitude": 106.571524
 },
 {
   "STT": 2129,
   "Name": "Nhà thuốc Kim Dung",
   "address": "63/2A Ấp 3, Xã Xuân Thới Thượng, Hóc Môn,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8586288,
   "Latitude": 106.5955986
 },
 {
   "STT": 2130,
   "Name": "Nhà thuốc Quang Huy",
   "address": "1/4 Phan Văn Hớn, Xã Bà Điểm, Hóc Môn, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8457684,
   "Latitude": 106.5929262
 },
 {
   "STT": 2131,
   "Name": "Nhà thuốc Minh Nghĩa",
   "address": "46 Lê Văn Lương, Xã Phước Kiển, Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7275978,
   "Latitude": 106.6991023
 },
 {
   "STT": 2132,
   "Name": "Nhà thuốc Đăng Khoa",
   "address": "15B Lê Văn Lương, Xã Phước Kiển, Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.712627,
   "Latitude": 106.701918
 },
 {
   "STT": 2133,
   "Name": "Nhà thuốc Quang Nghị key",
   "address": "47B Lê Văn Lương, Ấp 2, Xã Phước Kiển, Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.712843,
   "Latitude": 106.6978
 },
 {
   "STT": 2134,
   "Name": "Nhà thuốc Việt Thái",
   "address": "2/2 Nguyễn Bình, Ấp 2, Xã Nhơn Đức, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6769481,
   "Latitude": 106.7052577
 },
 {
   "STT": 2135,
   "Name": "Nhà thuốc Phương Mai",
   "address": "47/5A Huỳnh Tấn Phát, Ấp 5, Xã Phú Xuân, Nhà Bè,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6755015,
   "Latitude": 106.7608393
 },
 {
   "STT": 2136,
   "Name": "Nhà thuốc số 20",
   "address": "768A Huỳnh Tấn Phát, Xã Phú Xuân, Nhà Bè,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6785148,
   "Latitude": 106.7531012
 },
 {
   "STT": 2137,
   "Name": "Nhà thuốc Hoài Sơn",
   "address": "18/2A Nguyễn Bình, Xã Phú Xuân, Nhà Bè,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6780837,
   "Latitude": 106.7534865
 },
 {
   "STT": 2138,
   "Name": "Nhà thuốc Phương Minh",
   "address": "25 Nguyễn Văn Tạo, Ấp 1, Xã Long Thới, Huyện Nhà Bè, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.643358,
   "Latitude": 106.7327464
 },
 {
   "STT": 2139,
   "Name": "Nhà thuốc Vân Châu",
   "address": "1164 Nguyễn Văn Tạo, Ấp 3, Xã Hiệp Phước, Nhà Bè,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6111579,
   "Latitude": 106.7395636
 },
 {
   "STT": 2140,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "725 Nguyễn Văn Tạo, Ấp 1, Xã Hiệp Phước, Nhà Bè,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.6315518,
   "Latitude": 106.7359635
 },
 {
   "STT": 2141,
   "Name": "Nhà thuốc Mỹ Thành",
   "address": "100D Thích Quảng Đức,Phường  05, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8062935,
   "Latitude": 106.68114
 },
 {
   "STT": 2142,
   "Name": "Nhà thuốc Bảo An",
   "address": "19 Đỗ Tấn Phong,Phường  09, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.801195,
   "Latitude": 106.677931
 },
 {
   "STT": 2143,
   "Name": "Nhà thuốc Thắng Lợi",
   "address": "A2-004 Chung Cư Phan Xích Long, Phường  7, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7994135,
   "Latitude": 106.6866129
 },
 {
   "STT": 2144,
   "Name": "Nhà thuốc Phương Anh",
   "address": "28 Nguyễn Công Hoan, Phường  7, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8021619,
   "Latitude": 106.6906991
 },
 {
   "STT": 2145,
   "Name": "Nhà thuốc La Nhị An",
   "address": "182 Phan Đăng Lưu, Phường  3, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.800515,
   "Latitude": 106.681505
 },
 {
   "STT": 2146,
   "Name": "Nhà thuốc Trung Nguyên",
   "address": "372 Phan Xích Long, Phường  2, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8005901,
   "Latitude": 106.68436
 },
 {
   "STT": 2147,
   "Name": "Nhà thuốc Phương Nhi",
   "address": "43A Phan Xích Long, Phường  2, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.800203,
   "Latitude": 106.6845132
 },
 {
   "STT": 2148,
   "Name": "Nhà thuốc Kim Châu KEY",
   "address": "23 Hoàng Văn Thụ, Phường  15, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.799029,
   "Latitude": 106.679616
 },
 {
   "STT": 2149,
   "Name": "Nhà thuốc Toàn Thắng",
   "address": "19 Nguyễn Trọng Tuyển, Phường  15, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.797101,
   "Latitude": 106.68058
 },
 {
   "STT": 2150,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "277 Phan Đình Phùng, Phường  15, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.797045,
   "Latitude": 106.681055
 },
 {
   "STT": 2151,
   "Name": "Nhà thuốc Phú Quý",
   "address": "84C Trần Hữu Trang, Phường  10, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.793744,
   "Latitude": 106.672596
 },
 {
   "STT": 2152,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "125A Trần Hữu Trang, Phường  10, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792913,
   "Latitude": 106.672237
 },
 {
   "STT": 2153,
   "Name": "Nhà Thuốc Kim Phúc",
   "address": "19 Đặng Văn Ngữ, Phường  10, Phường  10, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.795451,
   "Latitude": 106.669876
 },
 {
   "STT": 2154,
   "Name": "Nhà thuốc 63",
   "address": "38 Huỳnh Văn Bánh,Phường  17, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7958298,
   "Latitude": 106.6818091
 },
 {
   "STT": 2155,
   "Name": "Nhà thuốc Phú Thành",
   "address": "466 Huỳnh Văn Bánh,Phường  14, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.792254,
   "Latitude": 106.668779
 },
 {
   "STT": 2156,
   "Name": "Công ty Dược Phano",
   "address": "31 Hồ Biểu Chánh,Phường  12, Quận Phú Nhuận, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.79196,
   "Latitude": 106.676367
 },
 {
   "STT": 2157,
   "Name": "Nhà thuốc số 42",
   "address": "355 Hoàng Văn Thụ, Phường  2, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.799993,
   "Latitude": 106.661126
 },
 {
   "STT": 2158,
   "Name": "Nhà thuốc Tâm Trí",
   "address": "Số 48 Phổ Quang, Phường  2, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.80437,
   "Latitude": 106.6665979
 },
 {
   "STT": 2159,
   "Name": "Nhà thuốc Trung Việt",
   "address": "1216 Cách Mạng Tháng Tám, Phường  4, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7922514,
   "Latitude": 106.6545922
 },
 {
   "STT": 2160,
   "Name": "Nhà thuốc Đức Minh",
   "address": "1224 Cách Mạng Tháng Tám, Phường  4, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7922514,
   "Latitude": 106.6545922
 },
 {
   "STT": 2161,
   "Name": "Nhà thuốc An Thái",
   "address": "30 Hoàng Hoa Thám, Phường  12, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.797855,
   "Latitude": 106.647284
 },
 {
   "STT": 2162,
   "Name": "Nhà thuốc số 28",
   "address": "30/7 Hoàng Hoa Thám, Phường  12, Quận Tân Bình, HCM, Phường  12, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7979489,
   "Latitude": 106.6478039
 },
 {
   "STT": 2163,
   "Name": "Nhà thuốc Minh Châu",
   "address": "126 Trường Chinh, Phường  12, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.794571,
   "Latitude": 106.650764
 },
 {
   "STT": 2164,
   "Name": "Nhà thuốc Diệu Châu",
   "address": "51 Lê Trung Nghĩa, Phường  12, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.800518,
   "Latitude": 106.6509219
 },
 {
   "STT": 2165,
   "Name": "Nhà thuốc Toàn Tâm",
   "address": "26 Nguyễn Quang Bích, Phường  13, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.804766,
   "Latitude": 106.6460823
 },
 {
   "STT": 2166,
   "Name": "Nhà thuốc Đức Tài",
   "address": "106 Bình Giã, Phường  13, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8015651,
   "Latitude": 106.6450787
 },
 {
   "STT": 2167,
   "Name": "Nhà thuốc Trung Nghĩa",
   "address": "28 Trần Văn Danh, Phường  13, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.807269,
   "Latitude": 106.6474815
 },
 {
   "STT": 2168,
   "Name": "Nhà thuốc An Khang",
   "address": "47 Nguyễn Bặc,Phường  03, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7947064,
   "Latitude": 106.6624732
 },
 {
   "STT": 2169,
   "Name": "Nhà thuốc Minh Khôi",
   "address": "128 Hồng Lạc,Phường  11, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.786607,
   "Latitude": 106.648578
 },
 {
   "STT": 2170,
   "Name": "Nhà thuốc Phú Đức",
   "address": "170 Nghĩa Phát, Phường  07, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785732,
   "Latitude": 106.657787
 },
 {
   "STT": 2171,
   "Name": "Nhà thuốc Song Ngọc",
   "address": "680 Cách Mạng Tháng Tám, Phường  5, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7872372,
   "Latitude": 106.6641676
 },
 {
   "STT": 2172,
   "Name": "Nhà thuốc số 41",
   "address": "01 Ni Sư Huỳnh Liên, Phường  10, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7851522,
   "Latitude": 106.6456697
 },
 {
   "STT": 2173,
   "Name": "Nhà thuốc Huỳnh Liên",
   "address": "03 Ni Sư Huỳnh Liên, Phường  10, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7852211,
   "Latitude": 106.64565
 },
 {
   "STT": 2174,
   "Name": "Nhà Thuốc Thùy Trang.Key",
   "address": "19 Trần Văn Quang,p10,Quận Tân Bình, Phường  10, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7793771,
   "Latitude": 106.6492788
 },
 {
   "STT": 2175,
   "Name": "Công Ty TNHH Dược Phẩm Và Y Tế Tiện Lợi",
   "address": "619 Lạc Long Quân, Phường  10, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.779202,
   "Latitude": 106.6496839
 },
 {
   "STT": 2176,
   "Name": "Nhà thuốc Nhật Tân",
   "address": "226 Nghĩa Phát, Phường  6, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7847394,
   "Latitude": 106.656931
 },
 {
   "STT": 2177,
   "Name": "Nhà thuốc Hồng Hà 2",
   "address": "0903 626 129,Phường  08, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8014659,
   "Latitude": 106.6525974
 },
 {
   "STT": 2178,
   "Name": "Nhà thuốc Phương Thảo 1",
   "address": "254/104 Âu Cơ, Phường  09, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7781369,
   "Latitude": 106.645777
 },
 {
   "STT": 2179,
   "Name": "Nhà thuốc Vũ",
   "address": "15 chung cư 1 bàu cát, Phường  14, Quận Tân Bình, Phường  14, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7903,
   "Latitude": 106.643
 },
 {
   "STT": 2180,
   "Name": "Nhà thuốc Kiều Linh",
   "address": "Sạp K1/13 Chợ Bàu Cát, Phường  14, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7915937,
   "Latitude": 106.6420292
 },
 {
   "STT": 2181,
   "Name": "Nhà thuốc Hòa Mai",
   "address": "011 Chung cư Tân Sơn Nhì, Phường  14, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7954995,
   "Latitude": 106.6392663
 },
 {
   "STT": 2182,
   "Name": "Nhà thuốc Hồng Hạnh",
   "address": "41 Nguyễn Sỹ Sách, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8193697,
   "Latitude": 106.6345681
 },
 {
   "STT": 2183,
   "Name": "Nhà thuốc Tiến Trang",
   "address": "B1 Nguyễn Sỹ Sách, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8207998,
   "Latitude": 106.6357247
 },
 {
   "STT": 2184,
   "Name": "Nhà thuốc Phước An",
   "address": "28 Phan huy ích, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8274162,
   "Latitude": 106.6316557
 },
 {
   "STT": 2185,
   "Name": "Nhà thuốc Gia Huy",
   "address": "133 Nguyễn Phúc Chu, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.823444,
   "Latitude": 106.6359839
 },
 {
   "STT": 2186,
   "Name": "Nhà thuốc Minh Tuệ",
   "address": "14/17B Nguyễn Phúc Chu, Phường  15, Quận Tân Bình, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.822163,
   "Latitude": 106.63118
 },
 {
   "STT": 2187,
   "Name": "Nhà thuốc 252",
   "address": "354 Tân Sơn Nhì, Phường  Tân Sơn Nhì, Tân Phú, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7958285,
   "Latitude": 106.6297364
 },
 {
   "STT": 2188,
   "Name": "Nhà thuốc Bích Vân",
   "address": "151 Tân Kỳ Tân Quý, Phường  Tân Sơn Nhì, Tân Phú, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8026751,
   "Latitude": 106.6282191
 },
 {
   "STT": 2189,
   "Name": "Nhà thuốc Thanh An",
   "address": "57 Đường S11, Phường  Tây Thạnh, Tân Phú, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8138819,
   "Latitude": 106.6206939
 },
 {
   "STT": 2190,
   "Name": "Nhà thuốc Phước Tiên",
   "address": "299/57 Tây Thạnh, Phường  Tây Thạnh, Tân Phú, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.810085,
   "Latitude": 106.620999
 },
 {
   "STT": 2191,
   "Name": "Nhà thuốc Hạnh Phuớc",
   "address": "16 đuờng D16, Phường  Tây Thạnh, Tân Phú, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8142931,
   "Latitude": 106.6269584
 },
 {
   "STT": 2192,
   "Name": "Nhà thuốc Ngọc Phúc",
   "address": "99 Lê Trọng Tấn,Phường  Sơn Kỳ, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8075114,
   "Latitude": 106.6215479
 },
 {
   "STT": 2193,
   "Name": "Nhà thuốc Hoa Sơn",
   "address": "77 Gò Dầu, Phường  Tân Quý, Tân Phú, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7957205,
   "Latitude": 106.6261758
 },
 {
   "STT": 2194,
   "Name": "Nhà Thuốc Hà Tiên",
   "address": "63/4 Gò Dầu, Phường  Tân Quý, Tân Phú, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.795812,
   "Latitude": 106.616798
 },
 {
   "STT": 2195,
   "Name": "Nhà thuốc Tân Phú 2",
   "address": "115 Tân Quý, Phường  Tân Quý, Tân Phú, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7910497,
   "Latitude": 106.6207919
 },
 {
   "STT": 2196,
   "Name": "Nhà thuốc Như Lan",
   "address": "56 Độc Lập, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7919968,
   "Latitude": 106.6355203
 },
 {
   "STT": 2197,
   "Name": "Nhà thuốc Khang Châu",
   "address": "137A, Nguyễn Xuân Khoát, Phường  Tân Thành, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7881334,
   "Latitude": 106.6335576
 },
 {
   "STT": 2198,
   "Name": "Nhà thuốc Quỳnh Dao",
   "address": "4C Phạm Vấn, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.785221,
   "Latitude": 106.624878
 },
 {
   "STT": 2199,
   "Name": "Nhà thuốc Bảo Tâm",
   "address": "348 Nguyễn Sơn, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.784242,
   "Latitude": 106.622717
 },
 {
   "STT": 2200,
   "Name": "Nhà thuốc Minh Nghĩa",
   "address": "342A Phú Thọ Hoà, Phường  Phú Thọ Hòa, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7850019,
   "Latitude": 106.6261071
 },
 {
   "STT": 2201,
   "Name": "Nhà thuốc Home Medicare",
   "address": "51 Nguyễn Sơn,Phường  Phú Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.78107,
   "Latitude": 106.631934
 },
 {
   "STT": 2202,
   "Name": "Nhà thuốc Bảo Nghi",
   "address": "121 Trịnh Đình Thảo, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7738979,
   "Latitude": 106.6375458
 },
 {
   "STT": 2203,
   "Name": "Nhà thuốc SK 82",
   "address": "82 Trịnh Đình Trọng, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.775292,
   "Latitude": 106.645028
 },
 {
   "STT": 2204,
   "Name": "Nhà thuốc Hồng Loan",
   "address": "18C/001 chung cư Huỳnh Văn Chính, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7813165,
   "Latitude": 106.6414725
 },
 {
   "STT": 2205,
   "Name": "Nhà Thuốc Thùy Duơng.KEY",
   "address": "529 A Âu Cơ, Phường  Phú Trung, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.781418,
   "Latitude": 106.643126
 },
 {
   "STT": 2206,
   "Name": " Nhà thuốc Ngọc Phúc",
   "address": "99 Lê Trọng Tấn,Phường  Hòa Thạnh, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8056726,
   "Latitude": 106.6294075
 },
 {
   "STT": 2207,
   "Name": "Nhà thuốc Diệu Minh",
   "address": "68/2 Cây Keo, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771127,
   "Latitude": 106.631511
 },
 {
   "STT": 2208,
   "Name": "Nhà thuốc Tuấn Vũ",
   "address": "52 Cây Keo, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771556,
   "Latitude": 106.630086
 },
 {
   "STT": 2209,
   "Name": "Nhà thuốc số 99",
   "address": "99 Cây Keo, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771686,
   "Latitude": 106.628724
 },
 {
   "STT": 2210,
   "Name": "Nhà thuốc Hồng Minh",
   "address": "219 Lý Thánh Tông, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7672989,
   "Latitude": 106.6241443
 },
 {
   "STT": 2211,
   "Name": "Nhà thuốc Tuấn Thông",
   "address": "278 Hòa Bình, Phường  Hiệp Tân, Quận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7731083,
   "Latitude": 106.6265976
 },
 {
   "STT": 2212,
   "Name": "Nhà thuốc Thành Đạt",
   "address": "68/2 Cây Keo, Phường  Hiệp Tân,  Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771127,
   "Latitude": 106.631511
 },
 {
   "STT": 2213,
   "Name": "Nhà thuốc Quang Minh",
   "address": "52 Cây Keo, Phường  Hiệp Tân,  Tân Phú,  Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771556,
   "Latitude": 106.630086
 },
 {
   "STT": 2214,
   "Name": "Nhà thuốc Phúc Khang",
   "address": "99 Cây Keo, Phường  Hiệp TânQuận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.771686,
   "Latitude": 106.628724
 },
 {
   "STT": 2215,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "219 Lý Thánh Tông, Phường  Hiệp TânQuận Tân Phú, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7672989,
   "Latitude": 106.6241443
 },
 {
   "STT": 2216,
   "Name": "Nhà thuốc Tâm Phúc",
   "address": "278 Hòa Bình, Phường  Hiệp TânQuận Tân Phú ,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.7731083,
   "Latitude": 106.6265976
 },
 {
   "STT": 2217,
   "Name": "Nhà thuốc Mỹ Linh",
   "address": "1231/11 Tỉnh lộ 43, Phường  Bình Chiểu, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8929675,
   "Latitude": 106.7227583
 },
 {
   "STT": 2218,
   "Name": "Nhà thuốc An Bình KEY",
   "address": "262 Hoàng Diệu 2, Phường  Bình Chiểu, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8546308,
   "Latitude": 106.7683963
 },
 {
   "STT": 2219,
   "Name": "Nhà thuốc Hồng Ân",
   "address": "1005 Tỉnh Lộ 43, Phường  Bình Chiểu, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.880414,
   "Latitude": 106.7295517
 },
 {
   "STT": 2220,
   "Name": "Nhà thuốc Thu Hiền",
   "address": "05 Đường số 6, Phường  Linh Trung, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8501961,
   "Latitude": 106.7680364
 },
 {
   "STT": 2221,
   "Name": "Nhà thuốc Phượng",
   "address": "29 Linh Trung, Phường  Linh Trung, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8618499,
   "Latitude": 106.7654834
 },
 {
   "STT": 2222,
   "Name": "Nhà thuốc Hoa Lợi",
   "address": "25 Đường số 17, Phường  Linh Trung, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8552914,
   "Latitude": 106.7607251
 },
 {
   "STT": 2223,
   "Name": "Nhà thuốc Việt Thắng",
   "address": "125 Lê Văn Chí, Phường  Linh Trung, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.85865,
   "Latitude": 106.7775793
 },
 {
   "STT": 2224,
   "Name": "Nhà thuốc Tuyền Đức",
   "address": "44 Lê Văn Chí, Phường  Linh Trung, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8675302,
   "Latitude": 106.7819411
 },
 {
   "STT": 2225,
   "Name": "Nhà thuốc Đức Sanh Đuờng KEY",
   "address": "47 Lô A Chợ Tam Bình, Phường  Tam Bình, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8704119,
   "Latitude": 106.7355707
 },
 {
   "STT": 2226,
   "Name": "Nhà thuốc Thanh Trúc",
   "address": "Kiot 08 chợ Tam Bình, Phường  Tam Bình, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.87059,
   "Latitude": 106.7355
 },
 {
   "STT": 2227,
   "Name": "Nhà thuốc Quỳnh Như",
   "address": "13 Tam Hà, Phường  Tam Phú, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.862753,
   "Latitude": 106.746476
 },
 {
   "STT": 2228,
   "Name": "Nhà thuốc Tấn Tài",
   "address": "16 Phú Châu, Phường  Tam Phú, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8632336,
   "Latitude": 106.7454683
 },
 {
   "STT": 2229,
   "Name": "Nhà thuốc Thiên Kim 8",
   "address": "16A Phú Châu, Phường  Tam Phú, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.86337,
   "Latitude": 106.7456
 },
 {
   "STT": 2230,
   "Name": "Nhà thuốc Ngọc Hồng",
   "address": "54 Quốc lộ 13, Phường  Hiệp Bình Phước, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.856978,
   "Latitude": 106.723355
 },
 {
   "STT": 2231,
   "Name": "Nhà thuốc 199",
   "address": "A2-10 Dự Án Nhà Ở, Khu Phố 4, Phường  Hiệp Bình Phước, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.850903,
   "Latitude": 106.7255912
 },
 {
   "STT": 2232,
   "Name": "Nhà thuốc Minh Hưng",
   "address": "205A Quốc lộ 13 cũ, Phường  Hiệp Bình Phước, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8649557,
   "Latitude": 106.7201552
 },
 {
   "STT": 2233,
   "Name": "Nhà thuốc Như Hằng 2",
   "address": "110B Hiệp Bình, Phường  Hiệp Bình Chánh, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8420692,
   "Latitude": 106.7312708
 },
 {
   "STT": 2234,
   "Name": "Nhà thuốc Khanh",
   "address": "138 Quốc lộ 13, Phường  Hiệp Bình Chánh, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8308195,
   "Latitude": 106.714155
 },
 {
   "STT": 2235,
   "Name": "Nhà thuốc Tân Trang",
   "address": "41 Đường số 18, Phường  Hiệp Bình Chánh, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8253868,
   "Latitude": 106.7191851
 },
 {
   "STT": 2236,
   "Name": "Nhà thuốc Mỹ Châu",
   "address": "42 Đường số 23, Phường  Hiệp Bình Chánh, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8271718,
   "Latitude": 106.7275309
 },
 {
   "STT": 2237,
   "Name": "Nhà thuốc Như Phượng",
   "address": "8A Đường Tam Bình,Phường  Hiệp Bình Chánh,Q.Thủ Đức, Phường  Hiệp Bình Chánh, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8459408,
   "Latitude": 106.7287242
 },
 {
   "STT": 2238,
   "Name": "Nhà thuốc Phước Bình",
   "address": "475 Kha Vạn Cân, Phường  Hiệp Bình Chánh, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8408326,
   "Latitude": 106.7443277
 },
 {
   "STT": 2239,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "44 Hoàng Diệu 2, Phường  Linh Chiểu, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8543483,
   "Latitude": 106.7726248
 },
 {
   "STT": 2240,
   "Name": "Nhà thuốc Bạch Dương",
   "address": "1200 Kha Vạn Cân, Phường  Linh Chiểu, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8578353,
   "Latitude": 106.7581179
 },
 {
   "STT": 2241,
   "Name": "Nhà thuốc Bảo Ngân 2",
   "address": "1150 Kha Vạn Cân, Phường  Linh Chiểu, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.859721,
   "Latitude": 106.7602182
 },
 {
   "STT": 2242,
   "Name": "Nhà thuốc Thanh Nhân",
   "address": "61A Chương Dương, Phường  Linh Chiểu, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.854657,
   "Latitude": 106.759207
 },
 {
   "STT": 2243,
   "Name": "Nhà thuốc Anh Thy",
   "address": "05 Đường số 7, Phường  Linh Chiểu, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8567065,
   "Latitude": 106.7661531
 },
 {
   "STT": 2244,
   "Name": "Nhà thuốc Giang Sơn 2",
   "address": "99-101 Tô Ngọc Vân, Phường  Linh Tây, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8526669,
   "Latitude": 106.7523402
 },
 {
   "STT": 2245,
   "Name": "Nhà thuốc Thanh Long",
   "address": "701/2 Kha Vạn Cân, Phường  Linh Tây, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.85836,
   "Latitude": 106.757253
 },
 {
   "STT": 2246,
   "Name": "Nhà thuốc Bích Liên",
   "address": "374 Võ Văn Ngân,Phường  Bình Thọ, Quận Thủ Đức, Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.849453,
   "Latitude": 106.7718919
 },
 {
   "STT": 2247,
   "Name": "Nhà thuốc Phúc An",
   "address": "910 Kha Vạn Cân, Phường  Trường Thọ, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8517193,
   "Latitude": 106.7550694
 },
 {
   "STT": 2248,
   "Name": "Nhà thuốc Lan Ngọc",
   "address": "11 Đoàn Công Hớn, Phường  Trường Thọ, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.849892,
   "Latitude": 106.756722
 },
 {
   "STT": 2249,
   "Name": "Nhà thuốc Thanh Thảo",
   "address": "60 Hồ Văn Tư, Phường  Trường Thọ, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.8490801,
   "Latitude": 106.7555039
 },
 {
   "STT": 2250,
   "Name": "Nhà thuốc số 62",
   "address": "27 Đặng Văn Bi, Phường  Trường Thọ, Quận Thủ Đức,Thành phố #u78_2 {
    left: 400px !important;
NINH BÌNH",
   "Longtitude": 10.848632,
   "Latitude": 106.760266
 }
];