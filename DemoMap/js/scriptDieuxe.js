﻿var lat = 10.773448;
var lon = 106.705328;
var map = new L.Map('map', {

        zoom: 15,
        minZoom: 6,
});



    // create a new tile layer
    var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 14
    });

    // add the layer to the map
    map.addLayer(layer);

    var cuuthuong = 1;
    var cuuhoa = 1;
    var chihuy = 1;

    var lstPolice = [{
        name: 'Cảnh sát PCCC Quận 3 ',
        address: 'Pccc Quận 3, 103 Lý Chính Thắng, Phường 8, Quận 3, Hồ Chí Minh'
    },{
        name: 'Cảnh Sát Pccc Quận 1',
        address: '258 Đường Trần Hưng Đạo, Phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh'
    },{
        name: 'Cảnh Sát Pccc Quận 4',
        address: '196 Tôn Thất Thuyết, Phường 3, Quận 4, Hồ Chí Minh'
    }];

    var lstHospital = [{
        name: 'Bệnh viện Bình Dân',
        address: '408 Điện Biên Phủ, Phường 11, Quận 10, Hồ Chí Minh'
    },{
        name: 'Bệnh viện Nguyễn Trãi',
        address: '314 Nguyễn Trãi, Phường 8, Quận 5, Hồ Chí Minh'
    }]

    // var lstCSGT = [{
    //     name: 'Đội Cảnh Sát Giao Thông Thị Trấn Đông Anh',
    //     lat: '21.174844',
    //     lon: '105.846972',
    //     address: 'QL3, Đông Anh, ĐỒNG NAI'
    // },{
    //     name: 'Đội CSGT Số 2',
    //     lat: '21.061890',
    //     lon: '105.808706',
    //     address: '8A Xuân La, Tây Hồ, ĐỒNG NAI'
    // }]


    var xethang1 = [[10.7896, 106.6854],[10.77507,106.70600],[10.77423,106.70504],[lat, lon]];
    var xethang2 = [[10.7611, 106.6893], [10.7681,106.6954],[10.7715,106.6934],[10.7779,106.7011],[lat, lon]];
    // var xebot1 = [[lat + 0.01, lon - 0.02], [lat + 0.041, lon - 0.047], [lat + 0.041, lon - 0.047], [lat + 0.01, lon - 0.02]];
    // var xebot2 = [[lat - 0.032, lon + 0.068], [lat + 0.02, lon - 0.02], [lat + 0.02, lon - 0.02], [lat - 0.032, lon + 0.068]];
    var xethang3 = [[10.7568, 106.7178], [10.7626,106.7083], [10.7708,106.7062], [lat, lon]];
    // var xethang6 = [[lat + 0.004, lon + 0.007], [lat + 0.01, lon + 0.005], [lat + 0.000, lon + 0.000], [lat, lon]];

    var xecuuthuong1 = [[10.7735, 106.6796], [10.7767,106.6836], [10.7715,106.6934],[10.7779,106.7011], [lat, lon]];
    var xecuuthuong2 = [[10.7566, 106.6750], [10.7597,106.6850],[10.7680,106.6888],[10.7780,106.7011],[lat, lon]];
    // var xecuuthuong3 = [[lat - 0.07218, lon - 0.076218], [lat - 0.065009, lon - 0.053655], [lat - 0.031934, lon - 0.047476], [lat - 0.004908, lon - 0.027906], [lat - 0.002727, lon - 0.004292]];
    // var xecuuthuong4 = [[lat + 0.038468, lon - 0.156898], [lat + 0.042758, lon - 0.11879], [lat + 0.055709, lon - 0.068836], [lat + 0.032826, lon - 0.030384], [lat - 0.003216, lon - 0.00368]];

    //var mymap = [[lat, lon], [lat + 0.12, lon + 0.12],[lat - 0.12, lon - 0.12]];

    // var londonBrusselFrankfurtAmsterdamLondon = [[51.507222, -0.1275], [50.85, 4.35],
    // [50.116667, 8.683333], [52.366667, 4.9], [51.507222, -0.1275]];

    // var barcelonePerpignanPauBordeauxMarseilleMonaco = [
    //     [41.385064, 2.173403],
    //     [42.698611, 2.895556],
    //     [43.3017, -0.3686],
    //     [44.837912, -0.579541],
    //     [43.296346, 5.369889],
    //     [43.738418, 7.424616]
    // ];


//map.fitBounds(mymap);

    map.setView([lat, lon], 16);

    var FireIcon = L.icon({
        iconUrl: 'images/fire-2-32_2.gif',
        iconSize: [36, 36], // size of the icon
    });

    var CCIcon = L.icon({
        iconUrl: 'images/Ol_icon_blue_example.png',
        iconSize: [32, 32], // size of the icon
    });

    var CTIcon = L.icon({
        iconUrl: 'images/Ol_icon_red_example.png',
        iconSize: [32, 32], // size of the icon
    });

    var CHIcon = L.icon({
        iconUrl: 'images/jeep.png',
        iconSize: [32, 32], // size of the icon
    });

    var marker = L.marker([lat, lon], { icon: FireIcon }).addTo(map);

    if (cuuhoa == 1) {
        //========================================================================
        var marker1 = L.Marker.movingMarker(xethang1,
            [40000, 12000, 40000], { autostart: true, loop: false, icon: CCIcon }).addTo(map);

        marker1.loops = 0;
        marker1.bindPopup();
        marker1.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe thang 1 thuộc: ' + lstPolice[0].name + '<br> Còn <strong>8</strong> phút nữa hiện trường' + '<br>' + '<video width="200" height="150" controls autoplay muted><source src="videos/videoplayback 2.mp4" type="video/mp4"></video>')
                .openOn(map);
        });

        marker1.once('click', function () {
            marker1.start();
            marker1.closePopup();
            marker1.unbindPopup();
            marker1.on('click', function () {
                if (marker1.isRunning()) {
                    marker1.pause();
                } else {
                    marker1.start();
                }
            });
        });

        //========================================================================

        var marker2 = L.Marker.movingMarker(xethang2,
            [50000, 40000, 40000, 50000], { autostart: true, loop: false, icon: CCIcon }).addTo(map);

        marker2.loops = 0;
        marker2.bindPopup();
        marker2.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe thang 2 thuộc: ' + lstPolice[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .openOn(map);
        });
        marker2.once('click', function () {
            marker2.start();
            marker2.closePopup();
            marker2.unbindPopup();
            marker2.on('click', function () {
                if (marker2.isRunning()) {
                    marker2.pause();
                } else {
                    marker2.start();
                }
            });
        });

        //========================================================================
        var marker3 = L.Marker.movingMarker(xethang3,
            [50000, 40000, 40000, 50000], { autostart: true, loop: false, icon: CCIcon }).addTo(map);

        marker3.loops = 0;
        marker3.bindPopup();
        marker3.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe thang 2 thuộc: ' + lstPolice[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .openOn(map);
        });
        marker3.once('click', function () {
            marker3.start();
            marker3.closePopup();
            marker3.unbindPopup();
            marker3.on('click', function () {
                if (marker3.isRunning()) {
                    marker3.pause();
                } else {
                    marker3.start();
                }
            });
        });
                
    }
    if (cuuthuong == 1) {
        var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
            [32000, 10000, 32000], { autostart: true, loop: false, icon: CTIcon }).addTo(map);

        markerCT1.loops = 0;
        markerCT1.bindPopup();
        markerCT1.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe cứu thương 1 thuộc: ' + lstHospital[2].name + '<br> Còn <strong>10</strong> phút nữa hiện trường')
                .openOn(map);
        });
        markerCT1.once('click', function () {
            markerCT1.start();
            markerCT1.closePopup();
            markerCT1.unbindPopup();
            markerCT1.on('click', function () {
                if (markerCT1.isRunning()) {
                    markerCT1.pause();
                } else {
                    markerCT1.start();
                }
            });
        });

        var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
            [35000, 30000, 35000], { autostart: true, loop: false, icon: CTIcon }).addTo(map);

        markerCT2.loops = 0;
        markerCT2.bindPopup();
        markerCT2.on('mouseover', function(e) {
            //open popup;
             var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe cứu thương 2 thuộc: ' + lstHospital[1].name + '<br> Còn <strong>10</strong> phút nữa hiện trường')
                .openOn(map);
        });

        markerCT2.once('click', function () {
            markerCT2.start();
            markerCT2.closePopup();
            markerCT2.unbindPopup();
            markerCT2.on('click', function () {
                if (markerCT2.isRunning()) {
                    markerCT2.pause();
                } else {
                    markerCT2.start();
                }
            });
        });
    }

    // if (chihuy == 1)
    // {
    //     var markerCH = L.marker([lat - 0.02, lon - 0.02], { icon: CHIcon }).addTo(map);
    // }
 
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
