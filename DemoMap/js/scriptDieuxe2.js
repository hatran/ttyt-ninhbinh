﻿var lat = 20.9537;
var lon = 107.0824;
var map = new L.Map('map', {

        zoom: 15,
        minZoom: 6,
});

    // create a new tile layer
    var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 20
    });

    // add the layer to the map
    map.addLayer(layer);

    var cuuthuong = getParameterByName('ct');
    var cuuhoa = 1
    var chihuy = getParameterByName('chh');

    var lstGarbagePoint1 = [{
        name: 'Điểm tập kết rác thải Lán Bè',
        lat: '20.9519',
        lon: '107.0719',
        address: 'Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Đồng Hồ Lán Bè',
        lat: '20.95290',
        lon: '107.09945',
        address: 'Đồng Hồ, Lán Bè, Bạch Đằng, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường 25 - 4',
        lat: '20.9521',
        lon: '107.0807',
        address: 'Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải Hàng Than - Ba Đèo',
        lat: '20.95279',
        lon: '107.07323',
        address: 'Ba Đèo, Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Lê Lợi',
        lat: '20.9590',
        lon: '107.0749',
        address: 'Lê Lợi, Lán Bè, Yết Kiêu, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Lê Lai',
        lat: '20.9577',
        lon: '107.0838',
        address: 'Lê Lai, Lán Bè, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    }];

    var lstGarbagePoint2 = [{
        name: 'Điểm tập kết rác thải đường Lán Bè Bạch Đằng',
        lat: '20.9526',
        lon: '107.0907',
        address: 'Lán Bè, Bạch Đằng, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Võ Nguyên Giáp',
        lat: '20.95115',
        lon: '107.10317',
        address: 'Võ Nguyên Giáp, Lán Bè, Hồng Hải, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Văn Lang',
        lat: '20.95100',
        lon: '107.07809',
        address: 'Văn Lang, Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Bến Đoan Mỹ Gia',
        lat: '20.94859',
        lon: '107.07308',
        address: 'Khu Mỹ Gia, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Trần Thái Tông - Nguyễn Thái Học',
        lat: '20.9626',
        lon: '107.0737',
        address: 'Trần Thái Tông, Lán Bè, Yết Kiêu, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    }];

    var lstGarbagePoint3 = [{
        name: 'Điểm tập kết rác đường Giếng Đồn',
        lat: '20.95551',
        lon: '107.08674',
        address:'Giếng Đồn, Lán Bè, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác đường Dốc Học',
        lat: '20.9501',
        lon: '107.0758',
        address:'Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Nhà Thờ Hòn Gai',
        lat: '20.95319',
        lon: '107.08350',
        address: 'Nhà thờ Hòn Gai, Nhà Thờ, Lán Bè, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Lam Sơn',
        lat: '20.9635',
        lon: '107.0789',
        address: 'Lam Sơn, Lán Bè, Yết Kiêu, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Namm'
    }];

    var lstBaiRac = [{
        name: 'Bãi rác Đèo Sen',
        lat: '20.99667',
        lon: '107.10846',
        address: 'Trần Phú, Lán Bè, Hà Khánh, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    }]

    // var lstCSGT = [{
    //     name: 'Đội Cảnh Sát Giao Thông Thị Trấn Đông Anh',
    //     lat: '21.174844',
    //     lon: '105.846972',
    //     address: 'QL3, Đông Anh, ĐỒNG NAI'
    // },{
    //     name: 'Đội CSGT Số 2',
    //     lat: '21.061890',
    //     lon: '105.808706',
    //     address: '8A Xuân La, Tây Hồ, ĐỒNG NAI'
    // }]

//     var pccc01 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.943939, 107.110889),        
//         L.latLng(lat,lon)
//     ],
//     createMarker: function(waypointIndex, waypoint, numberOfWaypoints) {
//             return L.marker(L.latLng(20.943939, 107.110889));
//         },
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'red', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);


//     var pccc02 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.963892, 107.100439),        
//         L.latLng(20.943939, 107.110889),  
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'red', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

//     var pccc03 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.973117, 107.097797),        
//         L.latLng(20.963892, 107.100439),  
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'red', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);
    

//     var cuuThuogn01 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.982240, 107.084298),       
//         L.latLng(20.973117, 107.097797), 
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'red', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

//     var cuuThuogn02 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.964904, 107.075740),        
//         L.latLng(20.982240, 107.084298),
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'red', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);
//     var xerac11 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.959628, 107.118004),        
//         L.latLng(lat,lon)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'blue', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

//      var xerac12 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.966993, 107.107907),        
//         L.latLng(20.959628, 107.118004), 
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'blue', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);


//      var xerac13 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.959242, 107.090987),        
//         L.latLng(20.966993, 107.107907),
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'blue', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);


//      var xerac13 = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.949991, 107.080169),        
//         L.latLng(20.959242, 107.090987),
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'blue', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

var red_road = new L.Routing.control({
    waypoints: [
        L.latLng(20.95290, 107.09945), 
        L.latLng(20.9521, 107.0807),        
        L.latLng(20.95279, 107.07323),   
        L.latLng(20.9519,107.0719),     
        L.latLng(20.9590, 107.0749),        
        L.latLng(20.9577, 107.0838),
        L.latLng(20.99667, 107.10846),        


        
    ],
    // createMarker: function(waypointIndex, waypoint, numberOfWaypoints) {
    //         return L.marker(L.latLng(10.7896, 106.6854))
    //             .bindPopup('Tuyến đường từ: ' + lstPolice[0].name + 'di chuyển thông thoáng.'+' <br>'+' Sau 8 phút có thể tới nơi');
    //     },
    addWaypoints: false,
    lineOptions: {
      styles: [{color: 'red', opacity: 1, weight: 5}]
   },
   createMarker: function() { return null; },
   draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

// var yellow_road = new L.Routing.control({
//     waypoints: [
//         L.latLng(20.9519, 107.0719),        
//         L.latLng(20.99667,107.10846)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'yellow', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

//     var green_road = new L.Routing.control({
//     waypoints: [
//         L.latLng(10.7568, 106.7178),        
//         L.latLng(20.99667,107.10846)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'green', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);
    var xethang1 = [[lat, lon]];
    var xethang2 = [[20.963892, 107.100439]];
    // var xebot1 = [[lat + 0.01, lon - 0.02], [lat + 0.041, lon - 0.047], [lat + 0.041, lon - 0.047], [lat + 0.01, lon - 0.02]];
    // var xebot2 = [[lat - 0.032, lon + 0.068], [lat + 0.02, lon - 0.02], [lat + 0.02, lon - 0.02], [lat - 0.032, lon + 0.068]];
    var xethang3 = [[10.7568, 106.7178], [10.7626,106.7083], [10.7708,106.7062], [lat, lon]];
    // var xethang6 = [[lat + 0.004, lon + 0.007], [lat + 0.01, lon + 0.005], [lat + 0.000, lon + 0.000], [lat, lon]];

    var xecuuthuong1 = [[10.7735, 106.6796], [10.7767,106.6836], [10.7715,106.6934],[10.7779,106.7011], [lat, lon]];
    var xecuuthuong2 = [[10.7566, 106.6750], [10.7597,106.6850],[10.7680,106.6888],[10.7780,106.7011],[lat, lon]];
    // var xecuuthuong3 = [[lat - 0.07218, lon - 0.076218], [lat - 0.065009, lon - 0.053655], [lat - 0.031934, lon - 0.047476], [lat - 0.004908, lon - 0.027906], [lat - 0.002727, lon - 0.004292]];
    // var xecuuthuong4 = [[lat + 0.038468, lon - 0.156898], [lat + 0.042758, lon - 0.11879], [lat + 0.055709, lon - 0.068836], [lat + 0.032826, lon - 0.030384], [lat - 0.003216, lon - 0.00368]];

    //var mymap = [[lat, lon], [lat + 0.12, lon + 0.12],[lat - 0.12, lon - 0.12]];

    // var londonBrusselFrankfurtAmsterdamLondon = [[51.507222, -0.1275], [50.85, 4.35],
    // [50.116667, 8.683333], [52.366667, 4.9], [51.507222, -0.1275]];

    // var barcelonePerpignanPauBordeauxMarseilleMonaco = [
    //     [41.385064, 2.173403],
    //     [42.698611, 2.895556],
    //     [43.3017, -0.3686],
    //     [44.837912, -0.579541],
    //     [43.296346, 5.369889],
    //     [43.738418, 7.424616]
    // ];


//map.fitBounds(mymap);

     map.setView([lat, lon], 16);

    var thungracredIcon = L.icon({
        iconUrl: 'images/iconMaker2.gif',
        iconSize: [30, 30], // size of the icon
    });

    var thungracgreenIcon = L.icon({
        iconUrl: 'images/iconMaker1.png',
        iconSize: [30, 30], // size of the icon
    });

    var thungracgyellowIcon = L.icon({
        iconUrl: 'images/iconMaker3.png',
        iconSize: [30, 30], // size of the icon
    });

    var xechoracICon = L.icon({
        iconUrl: 'images/xechorac.png',
        iconSize: [30, 30], // size of the icon
    });

    var bairacIcon = L.icon({
        iconUrl: 'images/bairac.png',
        iconSize: [30, 30], // size of the icon
    });

    //Add multi maker
    for (var ik = 0; ik < lstGarbagePoint1.length; ik++) {
        var maker = L.marker([lstGarbagePoint1[ik].lat, lstGarbagePoint1[ik].lon], { icon: thungracredIcon }).addTo(map);
    }

    for (var ic = 0; ic < lstGarbagePoint2.length; ic++) {
        var maker = L.marker([lstGarbagePoint2[ic].lat, lstGarbagePoint2[ic].lon], { icon: thungracgreenIcon }).addTo(map);
    }

    for (var ia = 0; ia < lstGarbagePoint3.length; ia++) {
        var maker = L.marker([lstGarbagePoint3[ia].lat, lstGarbagePoint3[ia].lon], { icon: thungracgyellowIcon }).addTo(map);
    }

    for (var ix = 0; ix < lstBaiRac.length; ix++) {
        var maker = L.marker([lstBaiRac[ix].lat, lstBaiRac[ix].lon], { icon: bairacIcon }).addTo(map);
    }

    // var marker = L.marker([lat, lon], { icon: FireIcon }).addTo(map);

    if (cuuhoa == 1) {
        //========================================================================
        var marker1 = L.Marker.movingMarker(xethang1,
            [40000, 12000, 40000], { autostart: true, loop: false, icon: CCIcon }).addTo(map);

        marker1.loops = 0;
        marker1.bindPopup();
        marker1.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe thang 1 thuộc: ' + lstPolice[0].name + '<br> Còn <strong>8</strong> phút nữa hiện trường' + '<br>' + '<video width="200" height="150" controls autoplay muted><source src="videos/videoplayback 2.mp4" type="video/mp4"></video>')
                .openOn(map);
        });

        marker1.once('click', function () {
            marker1.start();
            marker1.closePopup();
            marker1.unbindPopup();
            marker1.on('click', function () {
                if (marker1.isRunning()) {
                    marker1.pause();
                } else {
                    marker1.start();
                }
            });
        });

        //========================================================================

        var marker2 = L.Marker.movingMarker(xethang2,
            [50000, 40000, 40000, 50000], { autostart: true, loop: false, icon: CCIcon }).addTo(map);

        marker2.loops = 0;
        marker2.bindPopup();
        marker2.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe thang 2 thuộc: ' + lstPolice[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .openOn(map);
        });
        marker2.once('click', function () {
            marker2.start();
            marker2.closePopup();
            marker2.unbindPopup();
            marker2.on('click', function () {
                if (marker2.isRunning()) {
                    marker2.pause();
                } else {
                    marker2.start();
                }
            });
        });

        //========================================================================
        var marker3 = L.Marker.movingMarker(xethang3,
            [50000, 40000, 40000, 50000], { autostart: true, loop: false, icon: CCIcon }).addTo(map);

        marker3.loops = 0;
        marker3.bindPopup();
        marker3.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe thang 2 thuộc: ' + lstPolice[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .openOn(map);
        });
        marker3.once('click', function () {
            marker3.start();
            marker3.closePopup();
            marker3.unbindPopup();
            marker3.on('click', function () {
                if (marker3.isRunning()) {
                    marker3.pause();
                } else {
                    marker3.start();
                }
            });
        });
                
    }
    if (cuuthuong == 1) {
        var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
            [32000, 10000, 32000], { autostart: true, loop: false, icon: CTIcon }).addTo(map);

        markerCT1.loops = 0;
        markerCT1.bindPopup();
        markerCT1.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe cứu thương 1 thuộc: ' + lstHospital[2].name + '<br> Còn <strong>10</strong> phút nữa hiện trường')
                .openOn(map);
        });
        markerCT1.once('click', function () {
            markerCT1.start();
            markerCT1.closePopup();
            markerCT1.unbindPopup();
            markerCT1.on('click', function () {
                if (markerCT1.isRunning()) {
                    markerCT1.pause();
                } else {
                    markerCT1.start();
                }
            });
        });

        var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
            [35000, 30000, 35000], { autostart: true, loop: false, icon: CTIcon }).addTo(map);

        markerCT2.loops = 0;
        markerCT2.bindPopup();
        markerCT2.on('mouseover', function(e) {
            //open popup;
             var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe cứu thương 2 thuộc: ' + lstHospital[1].name + '<br> Còn <strong>10</strong> phút nữa hiện trường')
                .openOn(map);
        });

        markerCT2.once('click', function () {
            markerCT2.start();
            markerCT2.closePopup();
            markerCT2.unbindPopup();
            markerCT2.on('click', function () {
                if (markerCT2.isRunning()) {
                    markerCT2.pause();
                } else {
                    markerCT2.start();
                }
            });
        });
    }

    // if (chihuy == 1)
    // {
    //     var markerCH = L.marker([lat - 0.02, lon - 0.02], { icon: CHIcon }).addTo(map);
    // }
 
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
