﻿
var lat = 20.96684;
var lon = 107.10819;
var map = new L.Map('map', {

        zoom: 14,
        minZoom: 14,
        maxZoom: 14,
});

    // create a new tile layer
    var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 18
    });

    map.addLayer(layer);

    map.setView([lat, lon], 8);

    var hospitalIcon = L.icon({
        iconUrl: 'images/pin-hopital.png',
        iconSize: [30, 30], // size of the icon
    });

    // var hospitalMainIcon = L.icon({
    //     iconUrl: 'images/iconMaker2.gif',
    //     iconSize: [40, 40], // size of the icon
    // });

    var FireIcon = L.icon({
        iconUrl: 'images/fire-2-32_2.gif',
        iconSize: [36, 36], // size of the icon
    });

    var carIcon = L.icon({
        iconUrl: 'images/ambulance.png',
        iconSize: [36, 36], // size of the icon
    });

    var lstHospital = data;
    var color= null;
    var lstColor = [
  {
    "color": "#003366",
  },
  {
    "color": "#0000FF"
  },
  {
    "color": "#33CC33"
  },
  {
    "color": "#990033"
  },
  {
    "color": "#FF66FF"
  },
  {
    "color": "#CD853F"
  },
  {
    "color": "#CDCD00"
  },
  {
    "color": "#8B658B"
  },
  {
    "color": "#5D478B"
  }
];

    var xecuuthuong1 = [[20.9547,107.0884], [20.9549,107.0881], [20.9568,107.0877],[20.9586,107.0891],[20.9609,107.0945],[20.9638,107.0978],[20.9635,107.1025],[20.9660,107.1042],[lat, lon]];
    var xecuuthuong2 = [[20.9512,107.1314], [20.9557,107.1302],[20.9588,107.1217],[20.9629,107.1193],[20.9666,107.1170],[lat, lon]];

    var marker = L.marker([20.96684, 107.10819], { icon: FireIcon }).addTo(map);
    var route = null;
    var maker = null;
    
    for (var ik = 0; ik < lstHospital.length; ik++) {
        // if(ik != 0) {
            maker = L.marker([lstHospital[ik].lat, lstHospital[ik].lon], { icon: hospitalIcon,carIcon }).addTo(map);
            color = lstColor[ik].color;
            route = new L.Routing.control({
                waypoints: [
                    L.latLng(lstHospital[ik].lat, lstHospital[ik].lon),        
                    L.latLng(lat,lon)
                ],
                addWaypoints: false,
                lineOptions: {
                  styles: [{color: color, opacity: 1, weight: 5}]
               },
               createMarker: function() { return null; },
               draggableWaypoints: false,
                routeWhileDragging: true
            }).addTo(map);
        // } 
        // else {
        //     var maker = L.marker([lstHospital[ik].lat, lstHospital[ik].lon], { icon: hospitalMainIcon }).addTo(map);
        // }
    }

var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
            [10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

        markerCT1.loops = 0;
        markerCT1.bindPopup();
        markerCT1.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe cứu thương biển số 14N - 90378 thuộc: Bệnh viện đa khoa tỉnh Quảng Ninh<br>Lái xe: Bùi Đức Toàn<br>Bác sỹ: Nông Văn Dũng<br>Điều dưỡng: Phạm Thái Dương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Đạt <br>Còn <strong>15</strong> phút nữa hiện trường')
                .openOn(map);
        });
        markerCT1.start();
        // markerCT1.once('click', function () {
        //     markerCT1.start();
        //     markerCT1.closePopup();
        //     markerCT1.unbindPopup();
        //     markerCT1.on('click', function () {
        //         if (markerCT1.isRunning()) {
        //             markerCT1.pause();
        //         } else {
        //             markerCT1.start();
        //         }
        //     });
        // });

        var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
            [10000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

        markerCT2.loops = 0;
        markerCT2.bindPopup();
        markerCT2.on('mouseover', function(e) {
            //open popup;
             var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('Xe cứu thương biển số 14N - 76904 thuộc: Trung tâm y tế Bãi Cháy<br>Lái xe: Ngô Xuân Hải<br>Bác sỹ: Lưu T. Quỳnh Nga<br>Điều dưỡng: Phạm Thị Thương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Long <br>Còn <strong>10</strong> phút nữa hiện trường')
                .openOn(map);
        });
        markerCT2.start();
        // markerCT2.once('click', function () {
        //     markerCT2.start();
        //     markerCT2.closePopup();
        //     markerCT2.unbindPopup();
        //     markerCT2.on('click', function () {
        //         if (markerCT2.isRunning()) {
        //             markerCT2.pause();
        //         } else {
        //             markerCT2.start();
        //         }
        //     });
        // });



    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function openModal(idModal) {
        document.getElementById(idModal).style.display = "block";
    }
